# Application Cripéo

## Fonctionnement de Cripéo

Cripéo utilise les outils suivants pour que tout fonctionne correctement : 
- Keycloak
- Graylog
- Un serveur SMTP

### Keycloak

#### Liste des rôles necessaires :
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Creation'
 - 'Organisations_Globales'
 - 'Organisation_Gestion'

#### Liste des groupes necessaires :
 - 'Administrateur Fonctionnel'
 - 'Administrateur Organisationnel'
 - 'Utilisateur'

##### Les rôles de l'Administrateur Fonctionnel
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Creation'
 - 'Organisations_Globales'
 - 'Organisation_Gestion'

##### Les rôles de l'Administrateur Organisationnel
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'
 - 'Organisation_Gestion'

##### Les rôles de l'Utilisateur
 - 'user',
 - 'Dossier_Lecture'
 - 'Dossier_Ecriture'

### Keycloak Admin
Pour l'ajout d'utilisateurs, vous devrez créer un utilisateur admin 
Ce compte permet d'avoir une liaison vers Keycloak avec une intéraction forte de l'api.

Vous devrez spécifier les droits de ce compte.

### GED Alfresco et stockage des documents

Anciennement l'outil utilisait une GED Alfesco pour stocker les documents, le stockage se fait maintenant directement sur le serveur, vous pouvez lier un volume docker sur le répertoire : 
`documentsInterne`

La ged Alfresco est toujours utilisée dans l'application pour stocker encore les derniers documents. Si vous débutez une nouvelle instance de cripéo, vous pouvez modifier et supprimer l'utilisation de cette ged.

### Equipe

Dans Cripéo vous pouvez créer des organisations.
Une fonctionnalité demandée était d'ajouter des équipes dans ces organisations qui devaient correspondre à des organisations simplifiées.

Les équipes permettent de faire des silos d'échanges. Le reste de l'organisation ne peut pas voir les informations et documents.

Cependant la fonctionnalité a été dépréciée. Mais il est toujours possible de l'utiliser. Mais l'utilisation peut générer des anomalies.

`Eviter l'utilisation`

### Secteur géographique

Il est possible d'ajouter des secteurs géographiques via un flux geojson.

`/!\` Cette fonctionnalité a été demandée pour une organisation, un seul flux a été mis en place,si vous devez l'utiliser, il sera preferable de tester avant toute utilisation en production.


## Configuration du fichier docker-compose

### Variables du Front-end
- WDS_SOCKET_PORT: 0
- REACT_APP_API_URL: Adresse du back-end (par exemple http://localhost:4583/api/v1)
- REACT_APP_KEYCLOAK_REALM: Le nom du royaume Keycloak pour le client
- REACT_APP_KEYCLOAK_URL: L'adresse du serveur Keycloak pour le client
- REACT_APP_KEYCLOAK_SSL: "external"
- REACT_APP_KEYCLOAK_RESOURCE: "authentification"
- REACT_APP_KEYCLOAK_PUBLIC_CLIENT: "true"

Vous devez créer un fichier .env
Ici, une variable ENV_FILE est intégré dans gitlab
