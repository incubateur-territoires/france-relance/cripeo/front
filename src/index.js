import React from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import App from './App';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';
import reduxThunk from 'redux-thunk';
import reducers from './reducers';
import reportWebVitals from './reportWebVitals';
import Keycloak from "keycloak-js";
import { KEYCLOAK_CONFIG } from './config/config';
import httpClient from './actions/httpClient';
import axios from "axios";

import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';


const store = configureStore({
  reducer: reducers,
  middleware: [reduxThunk],
});

const kc = Keycloak({
  url: KEYCLOAK_CONFIG['auth-server-url'],
  realm: KEYCLOAK_CONFIG.realm,
  clientId: KEYCLOAK_CONFIG.resource
});

const container = document.getElementById('root');
const root = createRoot(container);

kc.init({ onLoad: 'check-sso', "checkLoginIframe": false }).then(async (authenticated) => {
  if (authenticated) {
    store.getState().keycloak = kc;
    httpClient.setBearer(kc.token);
    setInterval(() => {
      kc.updateToken(60).then(async (check) => {
        if (!check) {
          console.log('Token non rafraichi');
        } else {
          httpClient.setBearer(kc.token);
          try {
            const userName = await kc.loadUserInfo();
            httpClient.setUserInfo(userName.name);
          } catch (e) {
            console.log(e);
          }
        }
      });
    }, 60000);

    try {
      const userName = await kc.loadUserInfo();
      httpClient.setUserInfo(userName.name);
    } catch (e) {
      console.log(e);
    }

    if (kc.realmAccess && kc.realmAccess.roles.includes('user')) {
      root.render(<Provider store={store}><App /></Provider>);
    } else {
      root.render(<Provider store={store}><div>Vous ne disposez pas des autorisations nécessaires pour accéder à cette application.</div></Provider>);
    }
  } else {
    kc.login();
  }
});
axios.interceptors.request.use(config => {
  config.headers = {
    ...config.headers, ...{
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache',
      'Expires': '0',
      Accept: 'application/json',
      Authorization: 'Bearer ' + httpClient.getBearer(),
      User: httpClient.getUserInfo()
    }
  };
  return config;
});

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
