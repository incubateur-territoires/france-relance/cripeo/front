import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";
import { NOTIFICATION_ADD } from "./types";

export const getTicket = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + `/ged/getTicket/`);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

