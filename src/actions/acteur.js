import { ACTEURS, ACTEUR, NOTIFICATION_ADD, EMAIL_UPDATE, EMAIL_DEL, ACTEURS_ORGANISATION, ACTEUR_CLEAN } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

export const getActeurs = () => async dispatch => {
    dispatch({ 'type': ACTEUR_CLEAN });
    try {
        const response = await axios.get(SERVEUR + '/acteurs');
        dispatch({ 'type': ACTEURS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getActeur = (id) => async dispatch => {
    try {
        if (!id) return false;
        const response = await axios.get(SERVEUR + '/acteurs/' + id);
        dispatch({ 'type': ACTEUR, payload: response.data });
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getActeurNoDispatch = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/acteurs/' + id);
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const setActeur = (data) => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/acteurs', data);
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch(getActeurs());
        return { success: true, acteur: response.data }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const addEquipe = (data) => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/acteurs/equipe', data);
        dispatch({ 'type': ACTEUR, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const deleteEmail = (data) => async dispatch => {
    try {
        const response = await axios.delete(SERVEUR + '/acteurs/email/' + data);
        dispatch({ 'type': EMAIL_DEL, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const updateEmail = (data) => async dispatch => {
    try {
        const response = await axios.put(SERVEUR + '/acteurs/email/', { ...data });
        dispatch({ 'type': EMAIL_UPDATE, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};


export const addEmail = (idActeur, data) => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/acteurs/email/', { ...data, idActeur });
        dispatch({ 'type': EMAIL_UPDATE, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const changeFrequenceMail = (idActeur, frequence) => async dispatch => {
    try {
        const response = await axios.put(SERVEUR + '/acteurs/frequence/', { frequence, id: idActeur });
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch(getActeurs());
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getEquipes = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/acteurs/byParentId/' + id);
        dispatch({ 'type': ACTEURS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getEquipeOrganisation = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/acteurs/byParentId/' + id);
        dispatch({ 'type': ACTEURS_ORGANISATION, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const addSecteurAdministratif = (id, secteur) => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/acteurs/secteur', { id, secteur });
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Secteur ajouté" } });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const deleteSecteurAdministratif = (id) => async dispatch => {
    try {
        const response = await axios.delete(SERVEUR + '/acteurs/secteur/' + id);
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Secteur supprimé" } });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const updateSecteurAdministratif = (id, secteur) => async dispatch => {
    try {
        const response = await axios.put(SERVEUR + '/acteurs/secteur/', { id, secteur });
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Secteur mis à jour" } });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const updateSecteurGeographique = (id, actif, url) => async dispatch => {
    try {
        const response = await axios.put(SERVEUR + '/acteurs/secteur-geographique', { id, actif, url });
        dispatch({ 'type': ACTEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Secteur géographique mis à jour" } });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const testSecteurGeo = async () => {
    try {
        const response = await axios.get(SERVEUR + '/acteurs/test-secteur-geographique');
        return response.status
    }
    catch (error) {
        console.log(error);
        return 503
    }
}