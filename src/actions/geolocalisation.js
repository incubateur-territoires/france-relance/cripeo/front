import axios from 'axios';
import { SERVEUR } from "../config/config";
import { CIRCONSCRIPTIONS } from "./types";

export const getCirconscription = (id, x, y) => async dispatch => {
  try {
    const circonscriptions = await axios.get(SERVEUR + '/circonscriptions/' + JSON.stringify({ id, x, y }));
    return circonscriptions.data;
  } catch (err) {
    console.log('Erreur: ', err);
  }
};

export const getCirconscriptions = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/circonscriptions/');
    dispatch({ 'type': CIRCONSCRIPTIONS, payload: response.data });
  } catch (err) {
    console.log('Erreur: ', err);
  }
};