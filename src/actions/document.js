import { DOCUMENTS, NOTIFICATION_ADD } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

export const getDocuments = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents');
    dispatch({ 'type': DOCUMENTS, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getAllDocumentsByDossier = async (dossier_id) => {
  try {
    const response = await axios.get(SERVEUR + '/documents/getAllFile/' + dossier_id);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    console.log(err);
    return msg;
  }
};


export const getDocumentsByEchange = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents/byEchangeId/' + id);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getDocumentsByDossier = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents/byDossierId/' + id);
    dispatch({ 'type': DOCUMENTS, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getDocumentsByDossierByActeur = (id, acteur) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents/byDossierIdByActeur/' + id + '/' + acteur);
    dispatch({ 'type': DOCUMENTS, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getDocumentById = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents/byId/' + id);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getFile = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/documents/getFile/' + id);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const droitFile = (data) => async dispatch => {
  try {
    await axios.post(SERVEUR + '/documents/droitFile/', data);
    dispatch(getDocumentsByDossierByActeur(data.dossierId, data.mainActeur));
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const gestiondroitsDocuments = (data) => async dispatch => {
  try {
    dispatch(droitFile(data));
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Les droits sur les documents a bien été modifiés." } });
  } catch (err) {
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur lors du changement des droits sur les documents." } });
  }
};