import { LOGIN } from "./types";

export const unsetLoginStatus = async () => {
    localStorage.setItem('loginStatus', "false");
};

export const getLoginStatus = async () => {
    return localStorage.getItem('loginStatus');
};

export const setRouteAfterLogin = (id) => async (dispatch) => {
    dispatch({ 'type': LOGIN, payload: { route: id } });
};

export const unsetRouteAfterLogin = (id) => async (dispatch) => {
    dispatch({ 'type': LOGIN, payload: null });
};