import axios from 'axios';
import { SERVEUR } from "../config/config";
import { NOTIFICATION_ADD } from "./types";

import { errorMessage } from "../utils/utils";

export const getVideoNames = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/tutoriel/names');
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getVideosByName = (name) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/tutoriel/byName/' + name);
    return response;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};