import {
  FILTRE_INIT,
  FILTRE_CLEAN,
  FILTRE_RECHERCHE,
  FILTRE_CATEGORIE,
  FILTRE_REFERENCE_EXISTE,
  FILTRE_SECTEUR_ADM,
  FILTRE_SECTEUR_GEO,
  FILTRE_URGENCE,
  FILTRE_URGENCE_ROOT,
  FILTRE_DEFAUT,
  NOTIFICATION_ADD,
  FILTRE_REFRESH
} from './types';
import axios from 'axios';
import { SERVEUR } from "../config/config";

export const getFiltres = (id) => async (dispatch) => {
  try {
    const response = await axios.get(SERVEUR + '/utilisateurs/filtres/' + id);
    dispatch({ 'type': FILTRE_DEFAUT, payload: response.data });
  } catch (err) {
    console.error("Impossible de récupérer les filtres parametrés");
  }
}

export const setParametrageFiltres = (id, filtres) => async (dispatch, getState) => {
  try {
    const auth = getState().utilisateur;
    const response = await axios.put(SERVEUR + '/utilisateurs/filtres', { id, filtres });
    if (id.toString() === auth._id.toString()) dispatch({ 'type': FILTRE_DEFAUT, payload: response.data });
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Paramétrage des filtres a bien été enregistré." } });
  } catch (err) {
    console.error("Impossible de récupérer les filtres parametrés");
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur lors de l'Enregistrement des Paramétrage des filtres." } });
  }
}

export const changeFiltre = (filtre, value) => async (dispatch, getState) => {
  try {
    const dossiersATraiter = getState().dossiersATraiter;
    const dossiersEnCreation = getState().dossiersEnCreation;
    const dossiersEnCours = getState().dossiersEnCours;
    const dossiersClos = getState().dossiersClos;

    const organisation = getState().utilisateur.organisation;

    const payload = { organisation, value, dossiers: { creations: dossiersEnCreation, aTraiter: dossiersATraiter, enCours: dossiersEnCours, clos: dossiersClos } };
    switch (filtre) {
      case 'recherche':
        dispatch({ type: FILTRE_RECHERCHE, payload });
        break;
      case 'categoryFilter':
        dispatch({ type: FILTRE_CATEGORIE, payload });
        break;
      case 'filterUrgence':
        dispatch({ type: FILTRE_URGENCE, payload });
        break;
      case 'filterUrgenceRoot':
        dispatch({ type: FILTRE_URGENCE_ROOT, payload });
        break;
      case 'filterReferenceExist':
        dispatch({ type: FILTRE_REFERENCE_EXISTE, payload });
        break;
      case 'secteurGeoSelectValue':
        dispatch({ type: FILTRE_SECTEUR_GEO, payload });
        break;
      case 'secteurAdmSelectValue':
        dispatch({ type: FILTRE_SECTEUR_ADM, payload });
        break;
      case 'init':
        dispatch({
          type: FILTRE_INIT,
          payload: { dossiers: { creations: dossiersEnCreation, aTraiter: dossiersATraiter, enCours: dossiersEnCours, clos: dossiersClos }, defauts: value, organisation }
        });
        break;
      case 'clean':
        dispatch({ type: FILTRE_CLEAN, payload });
        break;
      case "refresh":
        dispatch({ type: FILTRE_REFRESH, payload });
        break;
      default:
    }
  } catch (err) {
    console.log('Erreur: ', err);
    return null;
  }
};

