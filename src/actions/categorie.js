import { CATEGORIES, CATEGORIE, REMOVE_ACTION, UPDATE_ACTION, NOTIFICATION_ADD, REMOVE_CATEGORIE } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

export const getCategories = (idOrganisation) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/categories/byActeur/' + idOrganisation);
    dispatch({ 'type': CATEGORIES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getCategorie = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/categories/' + id);
    dispatch({ 'type': CATEGORIE, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const setCategorie = (data) => async dispatch => {
  try {
    const response = await axios.post(SERVEUR + '/categories', data);
    dispatch({ 'type': CATEGORIES, payload: response.data });
    dispatch(getCategories(data.emetteur.id))
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: "success", message: "Catégorie a bien été enregistrée." } });
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const removeCategorie = (idCategorie, idActeur) => async dispatch => {
  try {
    const response = await axios.delete(SERVEUR + '/categories/' + idCategorie);
    dispatch({ 'type': REMOVE_CATEGORIE, payload: response.data });
    dispatch(getCategories(idActeur))
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: "success", message: "Catégorie a bien été supprimée" } });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
  }
};

export const addAction = (idCategorie, data) => async dispatch => {
  try {
    await axios.post(SERVEUR + '/categories/action', { ...data, idCategorie });
    dispatch(getCategories(data.acteur_id));
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const removeAction = (data) => async dispatch => {
  try {
    const response = await axios.delete(SERVEUR + '/categories/action/' + data);
    dispatch({ 'type': REMOVE_ACTION, payload: response.data });
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const updateAction = (data) => async dispatch => {
  try {
    const response = await axios.put(SERVEUR + '/categories/action/', { ...data });
    dispatch({ 'type': UPDATE_ACTION, payload: response.data });
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getCategorieByActeur = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/categories/byActeur/' + id);
    dispatch({ 'type': CATEGORIES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getCategorieByActeurEmeDes = () => async (dispatch, getState) => {
  try {
    const auth = await getState().auth;
    if (auth) {
      const organisation = await getState().auth.organisation;
      const response = await axios.get(SERVEUR + '/categories/byActeurEmeDes/' + organisation.acteur_id);
      dispatch({ 'type': CATEGORIES, payload: response.data });
    }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

