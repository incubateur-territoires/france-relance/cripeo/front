import { DOSSIERS_HISTORIQUES, HISTORIQUE, HISTORIQUES, NOTIFICATION_ADD } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

export const getHistoriques = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/historiques');
    dispatch({ 'type': DOSSIERS_HISTORIQUES, payload: response.data });
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getHistorique = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/historiques' + id);
    dispatch({ 'type': HISTORIQUE, payload: response.data });
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getHistoriquesByDossier = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/historiques/dossier/' + id);
    dispatch({ 'type': HISTORIQUES, payload: response.data });
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};



