import axios from "axios";
import { SERVEUR } from "../config/config";
import {
  GROUPES,
  FETCH_USER,
  REGISTER_USER, NOTIFICATION_ADD,
  ROLES,
  MASKS,
  ACTEURS_ORGANISATION,
  UTILISATEUR
} from "./types";
import { errorMessage } from "../utils/utils";


export const getGroupes = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/keycloak/groupes');
    dispatch({ 'type': GROUPES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getRoles = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/keycloak/roles');
    dispatch({ 'type': ROLES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getMasks = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/keycloak/masks');
    dispatch({ 'type': MASKS, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const fetchUser = () => async (dispatch, getState) => {
  const kcStack = await getState().keycloak.tokenParsed;
  if (kcStack) {
    try {
      const userBase = await axios.get(SERVEUR + '/utilisateurs/byIdKc/' + kcStack.sub);
      const acteur = await axios.get(SERVEUR + '/acteurs/' + userBase.data.organisation.acteur_id);
      const acteurs = await axios.get(SERVEUR + '/acteurs/byParentId/' + userBase.data.organisation.acteur_id);

      dispatch({ type: ACTEURS_ORGANISATION, payload: acteurs.data });

      const equipes_user = userBase.data.equipes.map(equipe => {
        const indexActeur = acteurs.data.findIndex(act => act._id === equipe.acteur_id);
        return {
          acteur_id: equipe.acteur_id,
          entite: acteurs.data[indexActeur].entite
        }
      });

      const user = {
        name: {
          family_name: getState().keycloak.tokenParsed.family_name,
          given_name: getState().keycloak.tokenParsed.given_name,
        },
        username: getState().keycloak.tokenParsed.preferred_username,
        email: getState().keycloak.tokenParsed.email,
        organisation: {
          acteur_id: userBase.data.organisation.acteur_id,
          entite: acteur.data.entite
        },
        equipes: equipes_user,
        groupe: userBase.data.groupe,
        organisation_name: acteur.data.entite
      };

      dispatch({ type: FETCH_USER, payload: user });
    } catch (e) {
      const msg = errorMessage(e);
      dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
      return msg;
    }
  }
};

export const registerUser = () => async (dispatch) => {
  try {
    const response = await axios.post(SERVEUR + '/users/auth');
    dispatch({ 'type': REGISTER_USER, payload: response.data });
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const setLoginStatus = () => async () => {
  await localStorage.setItem('loginStatus', "true");
};

export const logoutUser = () => async (dispatch, getState) => {
  getState().keycloak.logout();
  dispatch({ type: FETCH_USER, payload: null });
  dispatch({ type: UTILISATEUR, payload: null });
  dispatch({ type: ACTEURS_ORGANISATION, payload: [] });
};