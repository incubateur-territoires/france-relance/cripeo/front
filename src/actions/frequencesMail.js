import { FREQUENCESMAIL, NOTIFICATION_ADD } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";



export const getFrequencesMail = () => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/frequences');
        dispatch({ 'type': FREQUENCESMAIL, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};