export const DOSSIER = 'dossier';
export const DOSSIER_ERR = 'dossier_error';
export const DOSSIER_CLEAR = 'dossier_clear';
export const DOSSIERS = 'dossiers';
export const DOSSIERS_CREATION = 'dossiersEnCreation';
export const DOSSIERS_CREATION_PUSH = 'dossiersEnCreation_push';
export const DOSSIERS_COURS = 'dossiersEnCours';
export const DOSSIERS_COURS_PUSH = 'dossiersEnCours_push';
export const DOSSIERS_TRAITER = 'dossiersATraiter';
export const DOSSIERS_TRAITER_PUSH = 'dossiersATraiter_push';
export const DOSSIERS_CLOS = 'dossiersClos';
export const DOSSIERS_CLOS_PUSH = 'dossiersClos_push';
export const CIVILITES = "civilites";

export const DOSSIERS_HISTORIQUES = 'dossiersHistoriques';
export const HISTORIQUES = 'historiques';
export const HISTORIQUE = 'historique';

export const ECHANGE = 'echange';
export const ECHANGE_CREATED = 'echange_created';
export const ENDED_CREATION_ECHANGE = 'ended_echange_creation';

export const ECHANGES = 'echanges';
export const ECHANGES_PUSH = 'echanges_push';

export const ACTEURS = 'acteurs';
export const ACTEUR_CLEAN = 'ACTEUR_CLEAN';
export const ACTEURS_ORGANISATION = 'acteurs_organisation';
export const ACTEUR = 'acteur';
export const NOTIFICATION_DEL = 'notification_del';
export const NOTIFICATION_ADD = 'notification_add';
export const REMOVE_INDIVIDU = 'remove_individu';
export const ETAT_WEBSERVICES = "etatWebServices";

export const NOTIFICATIONACTION_ADD = 'notificationaction_add';
export const NOTIFICATIONACTION_DEL = 'notificationaction_del';
export const NOTIFICATIONACTION_FETCH = 'notificationaction_fetch';

export const UTILISATEUR = "utilisateur";
export const UTILISATEURS = "utilisateurs";
export const UTILISATEURSEQUIPE = "utilisateursequipe";
export const GROUPES = "groupes";
export const ROLES = "roles";
export const MASKS = "masks";
export const UPDATE_INDIVIDU = 'update_individu';
export const UPDATE_ADRESSE = 'update_adresse';
export const UPDATE_REFERENCE = 'update_reference';
export const UPDATE_SECTEUR_ADMINISTRATIF = 'update_secteur_administratif';

export const FETCH_USER = 'fetch_user';
export const REGISTER_USER = 'register_user';
export const LOGIN = 'login';

export const CATEGORIES = 'categories';
export const CATEGORIE = 'categorie';
export const REMOVE_CATEGORIE = 'remove_categorie';
export const ACTIONS = 'actions';
export const ACTION = 'action';
export const REMOVE_ACTION = 'remove_action';
export const UPDATE_ACTION = 'update_action';

export const CIRCONSCRIPTIONS = 'circonscriptions';
export const EMAIL_DEL = 'email_del';
export const EMAIL_UPDATE = 'email_update';
export const DOCUMENTS = 'documents';
export const DOCUMENTSACTIONS = 'documentsactions';
export const DOCUMENTSLISTE = 'documentsliste';

export const FREQUENCESMAIL = 'frequences_mail';

export const FILTRE_INIT = "filtre_init";
export const FILTRE_CLEAN = "filtre_clean";
export const FILTRE_RECHERCHE = "filtre_recherche";
export const FILTRE_URGENCE = "filtre_urgence";
export const FILTRE_URGENCE_ROOT = "filtre_urgence_root";
export const FILTRE_REFERENCE_EXISTE = "filtre_reference_existe";
export const FILTRE_CATEGORIE = "filtre_categorie";
export const FILTRE_SECTEUR_GEO = "filtre_secteur_geo";
export const FILTRE_SECTEUR_ADM = "filtre_secteur_adm";
export const FILTRE_DEFAUT = "filtre_defauts";
export const FILTRE_REFRESH = "filtre_refresh";