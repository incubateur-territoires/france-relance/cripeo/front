import {
  NOTIFICATION_ADD,
  NOTIFICATION_DEL
} from "./types";
import { errorMessage } from "../utils/utils";

export const addNotification = (notification) => async dispatch => {
  try {
    dispatch({ 'type': NOTIFICATION_ADD, payload: notification });
  } catch (err) {
    return errorMessage(err);
  }
};

export const removeNotification = () => async dispatch => {
  try {
    dispatch({ 'type': NOTIFICATION_DEL });
  } catch (err) {
    return errorMessage(err);
  }
};