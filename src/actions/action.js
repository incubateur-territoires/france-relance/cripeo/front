import { ACTIONS, ACTION, NOTIFICATION_ADD } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

export const getActions = () => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/actions');
        dispatch({ 'type': ACTIONS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getAction = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/actions/' + id);
        dispatch({ 'type': ACTION, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const setAction = (data) => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/actions', data);
        dispatch({ 'type': ACTION, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getActionsByCategorie = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/actions/byCategorie/' + id);
        dispatch({ 'type': ACTIONS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const estDansAction = (protagoniste, orga, equipes) => {
    if (orga.toString() === protagoniste.toString()) return true;
    return equipes.reduce((acc, equipe) => {
        if (acc) return true;
        if (equipe.acteur_id.toString() === protagoniste.toString()) {
            return true;
        }
        return acc;
    }, false);
}

export const estDestinataireAction = (destinataire, orga, equipes) => {
    return estDansAction(destinataire, orga, equipes);
}

export const estEmetteurAction = (emetteur, orga, equipes) => {
    return estDansAction(emetteur, orga, equipes);
}

export const estActeurAction = (action, orga, equipes) => {
    const emetteur = estEmetteurAction(action.emetteur_id, orga, equipes);
    if (emetteur) return true;
    return estDestinataireAction(action.destinataire_id, orga, equipes);
}