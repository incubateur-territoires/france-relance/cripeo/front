import { UTILISATEURS, UTILISATEUR, NOTIFICATION_ADD, UTILISATEURSEQUIPE, NOTIFICATIONACTION_FETCH } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";

const BASE_URL = SERVEUR + '/utilisateurs';

export const getUtilisateurs = () => async dispatch => {
    try {
        const response = await axios.get(BASE_URL);
        dispatch({ 'type': UTILISATEURS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getUtilisateursByActeur = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/byActeur/' + id);
        dispatch({ 'type': UTILISATEURS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getUtilisateursByActeurNoDispatch = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/byActeur/' + id);
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getUtilisateur = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/' + id);
        dispatch({ 'type': UTILISATEUR, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};


export const getUtilisateurKc = (id_kc) => async dispatch => {
    try {
        const response = await axios.get(`${BASE_URL}/byIdKc/${id_kc}`);
        dispatch({ 'type': UTILISATEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATIONACTION_FETCH, payload: response.data.notifs });
        const interval = setInterval(async () => {
            const resUser = await axios.get(`${BASE_URL}/byIdKc/${id_kc}`);
            dispatch({ 'type': NOTIFICATIONACTION_FETCH, payload: resUser.data.notifs });
        }, 30000);
        return () => clearInterval(interval);

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getUtilisateurByLogin = (login) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/byLogin' + login);
        dispatch({ 'type': UTILISATEUR, payload: response.data });
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const setUtilisateur = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL, data);
        if (data._id) {
            dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'utilisateur est bien été modifié" } });
        } else {
            dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'utilisateur est ajouté" } });
        }
        dispatch({ 'type': UTILISATEUR, payload: response.data });
        dispatch(getUtilisateursByActeur(data.organisation.acteur_id));
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const deleteUtilisateur = (idUtilisateur, idActeur) => async dispatch => {
    try {
        await axios.delete(SERVEUR + '/utilisateurs/' + idUtilisateur);
        dispatch(getUtilisateursByActeur(idActeur));
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: "success", message: "L'utilisateur a bien été supprimé" } });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const deleteNotif = (id) => async dispatch => {
    try {
        const response = await axios.delete(BASE_URL + '/notification/' + id);
        dispatch({ 'type': UTILISATEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATIONACTION_FETCH, payload: response.data.notifs });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const luNotif = (id) => async dispatch => {
    try {
        const response = await axios.put(BASE_URL + '/notification/', { _id: id });
        dispatch({ 'type': UTILISATEUR, payload: response.data });
        dispatch({ 'type': NOTIFICATIONACTION_FETCH, payload: response.data.notifs });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const getUtilisateursByEquipe = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/byEquipe/' + id);
        dispatch({ 'type': UTILISATEURSEQUIPE, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const refreshDataUtilisateur = (utilisateur) => async dispatch => {
    try {
        const response = await axios.put(BASE_URL + '/refresh/', { utilisateur });
        dispatch({ 'type': UTILISATEURS, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Le raffraichissement de l'utilisateur est effectué" } });
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getUtilisateursByEquipeNoDispatch = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/byEquipe/' + id);
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};