import axios from 'axios';
import {
    DOSSIERS,
    DOSSIER_ERR,
    DOSSIERS_TRAITER,
    DOSSIERS_TRAITER_PUSH,
    DOSSIERS_COURS,
    DOSSIERS_COURS_PUSH,
    DOSSIERS_CLOS,
    DOSSIERS_CLOS_PUSH,
    DOSSIERS_CREATION,
    DOSSIERS_CREATION_PUSH,
    DOSSIER,
    DOSSIER_CLEAR,
    CIVILITES,
    ETAT_WEBSERVICES,
    REMOVE_INDIVIDU,
    UPDATE_INDIVIDU,
    UPDATE_ADRESSE,
    UPDATE_REFERENCE, NOTIFICATION_ADD,
    UPDATE_SECTEUR_ADMINISTRATIF
} from "./types";
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";
import { estActeurEchange } from './echange';
import { changeFiltre } from './filtres';

const BASE_URL = SERVEUR + "/dossiers"

export const getCivilites = () => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/civilites');
        dispatch({ 'type': CIVILITES, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiers = () => async dispatch => {
    try {
        const response = await axios.get(BASE_URL);
        dispatch({ 'type': DOSSIERS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};
export const getDossiersByActeur = () => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/acteur');

        dispatch({ 'type': DOSSIERS, payload: response.data });

        let dossiersEnCreation = [];
        let dossiersEnCours = [];
        let dossiersEntraitement = [];
        let dossiersClos = [];

        response.data.forEach(dossier => {

            if (dossier.etat.find(etat => etat === 'creation')) {
                dossiersEnCreation.push(dossier);
            }

            if (dossier.etat.find(etat => etat === 'enCours')) {
                dossiersEnCours.push(dossier);
            }

            if (dossier.etat.find(etat => etat === 'aTraiter')) {
                dossiersEntraitement.push(dossier);
            }

            if (dossier.etat.find(etat => etat === 'clos')) {
                dossiersClos.push(dossier);
            }
        });

        dispatch({ 'type': DOSSIERS_CREATION, payload: dossiersEnCreation });
        dispatch({ 'type': DOSSIERS_COURS, payload: dossiersEnCours });
        dispatch({ 'type': DOSSIERS_TRAITER, payload: dossiersEntraitement });
        dispatch({ 'type': DOSSIERS_CLOS, payload: dossiersClos });

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersEnCreationByActeur = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/acteur/encreation/' + id);
        dispatch({ 'type': DOSSIERS_CREATION, payload: response.data });
    } catch (err) {
        console.log(err);
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersEnCreationByUtilisateur = () => async (dispatch, getState) => {
    try {
        if (getState().auth) {
            callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/encreation/', DOSSIERS_CREATION, DOSSIERS_CREATION_PUSH);
            const interval = setInterval(async () => {
                callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/encreation/', DOSSIERS_CREATION, DOSSIERS_CREATION_PUSH);
            }, 100000);
            return () => clearInterval(interval);
        }

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const refreshDossiersEnCreationByUtilisateur = () => async (dispatch, getState) => {
    try {
        if (getState().auth) {
            callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/encreation/', DOSSIERS_CREATION, DOSSIERS_CREATION_PUSH);
        }

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};


export const getDossiersEnCoursByActeur = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/acteur/encours/' + id);
        dispatch({ 'type': DOSSIERS_COURS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersEnCoursByUtilisateur = () => async (dispatch, getState) => {
    try {
        if (getState().auth) {
            callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/encours/', DOSSIERS_COURS, DOSSIERS_COURS_PUSH);
            const interval = setInterval(async () => {
                callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/encours/', DOSSIERS_COURS, DOSSIERS_COURS_PUSH);
            }, 100000);
            return () => clearInterval(interval);
        }

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersATraiterByActeur = (id) => async dispatch => {
    try {
        const interval = setInterval(async () => {
            const response = await axios.get(BASE_URL + '/acteur/entraitement/' + id);
            dispatch({ 'type': DOSSIERS_TRAITER, payload: response.data });
        }, 100000);
        return () => clearInterval(interval);

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

const callFunctionDossiersByUtilisateur = async (dispatch, getState, url, type, typePush) => {
    try {
        const organisation = await getState().auth.organisation;
        const response = await axios.get(BASE_URL + `${url}${organisation.acteur_id}`,
            {
                // query URL without using browser cache
                headers: {
                    'Cache-Control': 'no-cache',
                    'Pragma': 'no-cache',
                    'Expires': '0',
                },
            }
        );
        dispatch({ 'type': type, payload: response.data });
        dispatch(changeFiltre("refresh"));
        // Listes des quipes
        const equipes = await getState().auth.equipes;
        equipes.forEach(async equipe => {
            const responseEqu = await axios.get(BASE_URL + `${url}${organisation.acteur_id}`,
                {
                    // query URL without using browser cache
                    headers: {
                        'Cache-Control': 'no-cache',
                        'Pragma': 'no-cache',
                        'Expires': '0',
                    },
                }
            );
            dispatch({ 'type': typePush, payload: responseEqu.data });
        });
    } catch (err) {

    }
};

export const getDossiersATraiterByUtilisateur = () => async (dispatch, getState) => {
    try {
        if (getState().auth) {
            callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/entraitement/', DOSSIERS_TRAITER, DOSSIERS_TRAITER_PUSH);
            const interval = setInterval(async () => {
                callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/entraitement/', DOSSIERS_TRAITER, DOSSIERS_TRAITER_PUSH);
            }, 100000);
            return () => clearInterval(interval);
        }

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersClosByActeur = (id) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/acteur/clos/' + id);
        dispatch({ 'type': DOSSIERS_CLOS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersClosByUtilisateur = () => async (dispatch, getState) => {
    try {
        if (getState().auth) {
            callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/clos/', DOSSIERS_CLOS, DOSSIERS_CLOS_PUSH);
            const interval = setInterval(async () => {
                callFunctionDossiersByUtilisateur(dispatch, getState, '/acteur/clos/', DOSSIERS_CLOS, DOSSIERS_CLOS_PUSH);
            }, 100000);
            return () => clearInterval(interval);
        }

    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossiersSimple = () => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/simple');
        dispatch({ 'type': DOSSIERS, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const getDossier = (dossierId) => async dispatch => {
    try {
        const response = await axios.get(BASE_URL + '/' + dossierId);
        if (response && response.status === 200) {
            dispatch({ 'type': DOSSIER, payload: response.data });
            return response.data;
        } else {
            dispatch({ 'type': DOSSIER_ERR, payload: response.status });
            return false;
        }
    } catch (err) {
        dispatch({ 'type': DOSSIER_ERR, payload: 404 });
        const msg = errorMessage(err);
        return msg;
    }
};

export const getEtatWebservices = () => async dispatch => {
    try {
        const response = await axios.post(SERVEUR + '/webService/test', {
            ws: [
                {
                    nom: "Gestion des documents",
                    url: "http://apl345:8080/share",
                    description: "GED Alfresco",
                    proxy: true
                },
                {
                    nom: "WebService adresse gouvernement",
                    url: "https://api-adresse.data.gouv.fr/search/?q=8+bd+du+port",
                    description: "Recherche d'adresse géolocalisée",
                    proxy: true
                }
            ]
        });
        dispatch({ type: ETAT_WEBSERVICES, payload: response.data });
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const addIndividu = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL + '/individu', data);
        dispatch({ 'type': DOSSIER, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'individu a bien été ajouté." } });
        return response.data;
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const clearDossier = () => async dispatch => {
    dispatch({ 'type': DOSSIER_CLEAR, payload: {} });
}

export const removeIndividu = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL + '/delete/individu', data);
        dispatch({ 'type': REMOVE_INDIVIDU, payload: response.data });
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const addDossier = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL, data);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "La création de l'individu est terminée" } });
        return response.data;
    } catch (err) {
        errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur lors de la création du dossier" } });
        return false;
    }
};

export const updateIndividu = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL + '/update/individu', data);
        dispatch({ 'type': UPDATE_INDIVIDU, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Mise à jour des informations de l'individu" } });
        dispatch(getDossier(data.id));
        dispatch(getDossiers());
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur lors de la modification du dossier" } });
        return msg;
    }
};

export const updateAdresse = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL + '/update/adresse', data);
        dispatch({ 'type': UPDATE_ADRESSE, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'adresse a été enregistrée" } });
        dispatch(getDossier(data.id));
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const updateReference = (data) => async dispatch => {
    try {
        const response = await axios.post(BASE_URL + '/update/reference', data);
        dispatch({ 'type': UPDATE_REFERENCE, payload: response.data });
        dispatch(getDossier(data.id));
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const estActeurDossier = (dossier, echanges, orga, equipes) => {
    if (dossier.acteur_id === orga) return true;
    if (echanges)
        echanges.forEach(echange => {
            if (estActeurEchange(echange, orga, equipes)) return true;
        })
    return false;
}

export const updateSecteurDossier = (dossier_id, acteur_id, secteur) => async dispatch => {
    try {
        const response = await axios.put(BASE_URL + '/secteurAdministratif', { id: dossier_id, acteur_id, secteur });
        dispatch({ 'type': UPDATE_SECTEUR_ADMINISTRATIF, payload: response.data });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Le secteur administratif a été enregistré" } });
        dispatch(getDossier(dossier_id));
        return { success: true }
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}

export const deleteOneDossier = (arg) => async dispatch => {
    try {
        await axios.put(BASE_URL + '/delete', { _id: arg.id, acteur_id: arg.acteur });
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: "success", message: "Dossier supprimé" } });
        dispatch(refreshDossiersEnCreationByUtilisateur());
    }
    catch (err) {
        console.log(err)
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: err } });
    }
}