import axios from "axios";
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";
import { NOTIFICATION_ADD } from "./types";

export const searchUserByMail = (email) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/keycloak/utilisateur/byEmail/' + email);
        return { success: true, data: response.data };
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
};

export const searchByKeycloakId = (id) => async dispatch => {
    try {
        const response = await axios.get(SERVEUR + '/keycloak/utilisateur/byId/' + id);
        return { success: true, data: response.data };
    } catch (err) {
        const msg = errorMessage(err);
        dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
        return msg;
    }
}


export const directSearchByKeycloakId = async (id) => {
    try {
        const response = await axios.get(SERVEUR + '/keycloak/utilisateur/byId/' + id);
        return { success: true, data: response.data }
    } catch (err) {
        const msg = errorMessage(err);
        return msg;
    }
}