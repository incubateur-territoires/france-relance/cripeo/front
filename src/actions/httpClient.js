import axios from 'axios'

// instantiate axios
const httpClient = axios.create()

httpClient.getToken = function () {
    return localStorage.getItem('token')
};

httpClient.setToken = function (token) {
    localStorage.setItem('token', token)
    return token
};

httpClient.setBearer = function (bearer) {
    localStorage.setItem('bearer', bearer)
    return bearer
};

httpClient.getBearer = function () {
    return localStorage.getItem('bearer')
};

httpClient.setUserInfo = function (userInfo) {
    localStorage.setItem('userInfo', userInfo)
}

httpClient.getUserInfo = function () {
    return localStorage.getItem('userInfo')
}
// During initial app load attempt to set a localStorage stored token
// as a default header for all api requests.
//httpClient.defaults.headers.common.token = httpClient.getToken()
httpClient.defaults.headers.common.bearer = httpClient.getBearer()
export default httpClient