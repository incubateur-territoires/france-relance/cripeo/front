import { ECHANGES, ECHANGES_PUSH, ECHANGE, NOTIFICATION_ADD, DOCUMENTS, DOCUMENTSACTIONS, ECHANGE_CREATED, ENDED_CREATION_ECHANGE } from "./types";
import axios from 'axios';
import { SERVEUR } from "../config/config";
import { errorMessage } from "../utils/utils";
import { getDocumentsByDossierByActeur, droitFile } from "./document";
import { getDossier } from "./dossier";


export const getEchanges = () => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/echanges');
    dispatch({ 'type': ECHANGES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getEchange = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/echanges/' + id);
    dispatch({ 'type': ECHANGE, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const setEchange = (data) => async dispatch => {
  try {
    const response = await axios.post(SERVEUR + '/echanges', data);
    const { echanges, echange } = response.data;
    dispatch({ 'type': ECHANGES, payload: echanges });
    dispatch(getEchange(echange._id));
    return {
      success: true,
      msg: "L'échange a bien été créé",
      echange
    };
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const endedCreationEchange = () => async dispatch => {
  dispatch({ 'type': ENDED_CREATION_ECHANGE, payload: null });
}

export const switchCloture = (id) => async dispatch => {
  try {
    const response = await axios.put(SERVEUR + '/echanges/cloture/' + id);
    if (response.data && response.data.length > 0) {
      dispatch(getEchangesByDossierByUtilisateur(response.data[0].dossier_id));
    }
    dispatch(getDossier(response.data[0].dossier_id));
    return {
      success: true,
      msg: "Modification de la clotûre de l'échange effective",
    };
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return {
      success: false,
      msg: "Erreur : lors de la modification de la clotûre",
    };
  }
}

export const echangeEstLu = (data) => async dispatch => {
  const response = await axios.put(SERVEUR + '/echanges/estLu', { ...data });
  dispatch(getEchangesByDossierByUtilisateur(response.data.dossier_id));
  dispatch(getDossier(response.data.dossier_id));
  dispatch(getEchange(response.data._id));
}

export const getEchangesByDossier = (id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + '/echanges/byDossierId/' + id);
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getEchangesByDossierByActeur = (dossier_id, acteur_id) => async dispatch => {
  try {
    const response = await axios.get(SERVEUR + `/echanges/byDossierIdByActeur/${dossier_id}/${acteur_id}`);
    dispatch({ 'type': ECHANGES, payload: response.data });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const getEchangesByDossierByUtilisateur = (dossier_id) => async (dispatch, getState) => {
  try {
    if (getState().auth) {
      // get Organisation
      const organisation = await getState().auth.organisation;
      const response = await axios.get(SERVEUR + `/echanges/byDossierIdByActeur/${dossier_id}/${organisation.acteur_id}`);
      dispatch({ 'type': ECHANGES, payload: response.data });
      // Listes des quipes
      const equipes = await getState().auth.equipes;
      equipes.forEach(async equipe => {
        const responseEquipe = await axios.get(SERVEUR + `/echanges/byDossierIdByActeur/${dossier_id}/${equipe.acteur_id}`);
        dispatch({ 'type': ECHANGES_PUSH, payload: responseEquipe.data });
      });
    }

  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const estUrgent = (echange_id) => async (dispatch) => {
  try {
    const response = await axios.put(SERVEUR + `/echanges/estUrgent/${echange_id}`);
    dispatch(getEchangesByDossierByUtilisateur(response.data.dossier_id));
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const addActionEchange = (data) => async (dispatch, getState) => {
  try {
    const auth = getState().auth
    const response = await axios.post(SERVEUR + '/echanges/addAction', data);
    dispatch(getEchangesByDossierByActeur(response.data.dossier_id, auth.organisation.acteur_id));
    dispatch({ 'type': ECHANGE, payload: response.data });
    dispatch(getDossier(response.data.dossier_id));
    if (response.data.dossier_id && auth && auth.organisation.acteur_id) {
      dispatch(getDocumentsByDossierByActeur(response.data.dossier_id, auth.organisation.acteur_id));
    }
    dispatch({ 'type': DOCUMENTSACTIONS, payload: response.data });
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'action a bien été ajouté." } });
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
};

export const uploadDocument = (id, data) => async dispatch => {
  try {
    const response = await axios.post(SERVEUR + `/echanges/upload/${id}`, data, {});
    dispatch({ 'type': DOCUMENTS, payload: response.data });
    return true;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const deleteFile = (id) => async (dispatch, getState) => {
  try {
    await axios.delete(SERVEUR + `/echanges/deleteFichier/` + id);
    dispatch(getDocumentsByDossierByActeur(getState().dossier._id, getState().auth.organisation.acteur_id));
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Le document a bien été supprimé." } });
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur lors de la suppression du document." } });
    return msg;
  }
}

export const deleteEchange = (id) => async (dispatch, getState) => {
  try {
    const auth = getState().auth;
    await axios.delete(SERVEUR + `/echanges/deleteEchange/` + id);
    dispatch(getEchangesByDossierByUtilisateur(getState().dossier._id));
    if (getState().dossier._id && auth && auth.organisation.acteur_id) {
      dispatch(getDocumentsByDossierByActeur(getState().dossier._id, auth.organisation.acteur_id));
    }
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "L'échange a bien été supprimé" } });
    return { success: true }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const uploadDocumentAction = (id, data) => async dispatch => {
  try {
    const reader = new FileReader();
    reader.readAsDataURL(data);
    let newData = new FormData();
    newData.append('filedata', data);
    const response = await axios.post(SERVEUR + `/echanges/uploadaction/${id}`, newData, {});
    dispatch({ 'type': DOCUMENTSACTIONS, payload: response.data });
    return response.data;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const integreDocumentsAction = (echangeId, documents) => async (dispatch, getState) => {
  try {
    const response = await axios.put(SERVEUR + `/echanges/integreDocumentAction/${echangeId}`, { documents: documents }, {});
    dispatch(getDocumentsByDossierByActeur(response.data.echange.dossier_id, getState().auth.organisation.acteur_id));
    return true;
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const gestionInformationEchange = (data, docDroit) => async dispatch => {
  try {
    dispatch(informationEchange(data, docDroit));
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'success', message: "Les organismes en informations sont enregistrés" } });
  } catch (err) {
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: "Erreur : organismes non enregistrés" } });
  }
};

export const informationEchange = (data, docDroit) => async dispatch => {
  try {
    const response = await axios.put(SERVEUR + `/echanges/droits`, { ...data });
    dispatch({ 'type': ECHANGES, payload: response.data });
    const echange = response.data.find(e => e._id === data.id);
    dispatch({ 'type': ECHANGE_CREATED, payload: echange });
    if (docDroit && docDroit.length > 0) {
      for (const doc of docDroit) {
        dispatch(droitFile(doc));
      }
    }
  } catch (err) {
    const msg = errorMessage(err);
    dispatch({ 'type': NOTIFICATION_ADD, payload: { variant: 'error', message: msg } });
    return msg;
  }
}

export const estDansEchange = (protagoniste, orga, equipes) => {
  if (orga.toString() === protagoniste.toString()) return true;
  return equipes.reduce((acc, equipe) => {
    if (acc) return true;
    if (equipe.acteur_id.toString() === protagoniste.toString()) {
      return true;
    }
    return acc;
  }, false);
}

export const estDestinataire = (destinataire, orga, equipes) => {
  return estDansEchange(destinataire, orga, equipes);
}

export const estEmetteur = (emetteur, orga, equipes) => {
  return estDansEchange(emetteur, orga, equipes);

}

export const estActeurEchange = (echange, orga, equipes) => {
  if (!echange || !echange.categorie) return false;
  const emetteur = estEmetteur(echange.categorie.emetteur_id, orga, equipes);
  if (emetteur) return true;
  return estDestinataire(echange.categorie.destinataire_id, orga, equipes);
}



export const estActeurInfoEchange = (information, orga, equipes) => {
  return estDansEchange(information.acteur_id, orga, equipes);
}

export const estActeurInfosEchange = (informations, orga, equipes) => {
  return informations.reduce((acc, info) => {
    if (acc) return true;
    if (estActeurInfoEchange(info, orga, equipes)) {
      return true;
    }
    return acc;
  }, false);
}
