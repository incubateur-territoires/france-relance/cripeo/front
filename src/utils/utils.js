const errorMessage = (err) => {
    let msg = err;
    if (err && err.message) msg = err.message;
    if (err.response && err.response.data && err.response.data.errors) {
        console.error('Erreur: ', err.response.data.errors);
        if (Array.isArray(err.response.data.errors) && err.response.data.errors[0] && err.response.data.errors[0].msg) {
            msg = err.response.data.errors[0].msg;
        }
        else {
            console.log(err.response.data.errors);
        }
    } else if (err.response && err.response.data && err.response.data.error) {
        msg = err.response.data.error.message;
    }

    console.error('Message: ', msg);
    return msg;
}

module.exports = {
    errorMessage
}