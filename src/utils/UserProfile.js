var UserProfile = (function () {
  var family_name = "";
  var given_name = "";
  var email = "";
  var username = "";
  var groupe = "";
  var mongoId = "";
  var organisation = "";
  var organisation_groupe = "";
  var acteur = "";

  var getName = function () {
    return family_name;
  };

  var setName = function (value) {
    family_name = value;
  };

  var getGivenName = function () {
    return given_name;
  };

  var setGivenName = function (value) {
    given_name = value;
  };

  var getEmail = function () {
    return email;
  };

  var setEmail = function (value) {
    email = value;
  };

  var getUsername = function () {
    return username;
  };

  var setUsername = function (value) {
    username = value;
  };

  var getmongoId = function () {
    return mongoId;
  };

  var setmongoId = function (value) {
    mongoId = value;
  };

  var getorganisation = function () {
    return organisation;
  };

  var setorganisation = function (value) {
    organisation = value;
  };

  var getorganisationGroupe = function () {
    return organisation_groupe;
  };

  var setorganisationGroupe = function (value) {
    organisation_groupe = value;
  };

  var getActeur = function () {
    return acteur;
  };

  var setActeur = function (value) {
    acteur = value;
  };

  var getGroupe = function () {
    return groupe;
  };

  var setGroupe = function (value) {
    groupe = value;
  };

  return {
    getName: getName,
    getGivenName: getGivenName,
    getEmail: getEmail,
    getUsername: getUsername,
    getmongoId: getmongoId,
    getorganisation: getorganisation,
    getorganisationGroupe: getorganisationGroupe,
    getActeur: getActeur,
    getGroupe: getGroupe,
    setName: setName,
    setGivenName: setGivenName,
    setEmail: setEmail,
    setUsername: setUsername,
    setmongoId: setmongoId,
    setorganisation: setorganisation,
    setorganisationGroupe: setorganisationGroupe,
    setActeur: setActeur,
    setGroupe: setGroupe,
  }

})();

export default UserProfile;