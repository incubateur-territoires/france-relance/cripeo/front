import { createTheme } from '@mui/material/styles';
import { frFR } from '@mui/material/locale';

const theme = createTheme(({
  palette: {
    primary: {
      main: '#033878',
      mainGradient: "linear-gradient(to right, #ffffff, #3867cf, #3867cf)",
      mainGradient2: "linear-gradient(to right, #ffffff, #033878, #3867cf)",
      mainGradient3: "linear-gradient(52deg, rgba(255,255,255,1) 12%, rgba(3,56,120,1) 41%, rgba(56,103,207,1) 100%)",
    },
    secondary: {
      main: '#ff5252',
    },
    action: {
      // disabledBackground: 'set color of background here',
      disabled: '#000000',
    },
    text: {
      color: 'rgb(33, 150, 243)',
      disabled: '#000000',
    },
    input: {
      color: "rgb(33, 150, 243)"
    },
  },
  titleCard: {
    accueil: {
      color: "#ffffff",
    }
  },
  typography: {
    useNextVariants: true,
  },
}, frFR));

export default theme;
