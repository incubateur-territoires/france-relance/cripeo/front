import React, { Component } from 'react';
import './App.css';
import { SnackbarProvider } from 'notistack';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { ThemeProvider, StyledEngineProvider } from '@mui/material/styles';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import frLocale from 'date-fns/locale/fr';
import { LocalizationProvider } from '@mui/x-date-pickers';
import theme from './ui/theme/index';
import connect from "react-redux/es/connect/connect";
import PageError from "./components/PageError/PageError";
import Liste from "./components/Screens/Liste";
import MonParametrage from "./components/Screens/MonParametrage";
import Dossier from "./components/Screens/Dossier";
import Acteurs from "./components/Screens/Acteurs";
import Bandeau from './components/Bandeau/SimpleHeader';
import Organisation from './components/Screens/Organisation';
import Alerts from "./components/Commun/Alerte/Alerts";
import { fetchUser, getRoles, getMasks } from "./actions/home";
import { getCivilites } from "./actions/dossier";
import { getUtilisateurKc } from "./actions/utilisateur";
import { getActeur } from "./actions/acteur";

class App extends Component {

  componentDidMount = async () => {
    await this.props.fetchUser();
    this.props.getCivilites();
    if (this.props.keycloak) {
      this.props.getUtilisateurKc(this.props.keycloak.tokenParsed.sub);
    }

    if (this.props.utilisateur && this.props.utilisateur.organisation && this.props.utilisateur.organisation.acteur_id) {
      this.props.getActeur(this.props.utilisateur?.organisation?.acteur_id);
    }

    this.props.getRoles();
    this.props.getMasks();
  }

  componentDidUpdate = async (prevProps) => {
    if (prevProps.keycloak !== this.props.keycloak) {
      await this.props.fetchUser();
      await this.props.getUtilisateurKc(this.props.keycloak.tokenParsed.sub);
    }

    if (prevProps.utilisateur !== this.props.utilisateur && this.props.utilisateur && this.props.utilisateur.organisation && this.props.utilisateur.organisation.acteur_id) {
      await this.props.getActeur(this.props.utilisateur?.organisation?.acteur_id);
    }
  }

  render() {
    return (
      <StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>
          <div className="App">
            <SnackbarProvider maxSnack={3}>
              <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={frLocale}>
                {this.props.utilisateur ? (
                  <Router>
                    <Alerts />
                    <div>
                      <Bandeau />
                      <Switch>
                        <Route exact path="/" component={Liste} />
                        <Route exact path="/liste" component={Liste} />
                        <Route exact path='/dossier/:id' component={Dossier} />
                        <Route exact path='/monparametrage' component={MonParametrage} />
                        <Route exact path='/acteurs' component={Acteurs} />
                        <Route exact path='/organisation/:id' component={Organisation} />
                        <Route path='*' component={PageError} />
                      </Switch>
                    </div>
                  </Router>
                )
                  : "Vous n'avez pas les droits d'accès à cette application"
                }
              </LocalizationProvider>
            </SnackbarProvider>
          </div>
        </ThemeProvider>
      </StyledEngineProvider>
    );
  }
}

function mapStateToProps(state) {
  return {
    keycloak: state.keycloak,
    utilisateur: state.utilisateur,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCivilites: () => dispatch(getCivilites()),
    getUtilisateurKc: (id) => dispatch(getUtilisateurKc(id)),
    getActeur: (id) => dispatch(getActeur(id)),
    getRoles: () => dispatch(getRoles()),
    getMasks: () => dispatch(getMasks()),
    fetchUser: () => dispatch(fetchUser()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
