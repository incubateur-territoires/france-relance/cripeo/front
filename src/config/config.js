module.exports = {
  SERVEUR: process.env.REACT_APP_API_URL ? process.env.REACT_APP_API_URL : "http://localhost:4583",
  WEBSERVICEORACLE: process.env.REACT_APP_WEB_SERVICE_ORACLE ? process.env.REACT_APP_WEB_SERVICE_ORACLE : "https://rec-api-ws-oracle.calvados.fr",
  ENV: process.env.REAC_APP_ENV ? process.env.REAC_APP_ENV : "recette",
  KEYCLOAK_CONFIG: {
    "realm": process.env.REACT_APP_KEYCLOAK_REALM,
    "auth-server-url": process.env.REACT_APP_KEYCLOAK_URL,
    "ssl-required": process.env.REACT_APP_KEYCLOAK_SSL,
    "resource": process.env.REACT_APP_KEYCLOAK_RESOURCE,
    "public-client": process.env.REACT_APP_KEYCLOAK_PUBLIC_CLIENT
  },
  GED: process.env.GEDFRONT ? process.env.GEDFRONT : "https://rec-geddevs.calvados.fr"
};
