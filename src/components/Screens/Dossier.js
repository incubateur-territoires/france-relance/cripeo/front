import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import clsx from 'clsx';
import { getDossier, deleteOneDossier } from "../../actions/dossier";
import { deleteFile, estActeurEchange, estDestinataire, getEchangesByDossierByUtilisateur } from "../../actions/echange";
import { getDocumentsByDossierByActeur, getAllDocumentsByDossier } from "../../actions/document";
import { getHistoriquesByDossier } from "../../actions/historique";
import { getActeurs } from "../../actions/acteur";
import { getCategorieByActeur } from "../../actions/categorie";
import { WrapperCreateEchange } from '../Echange/AjoutEchange/WrapperCreateEchange';
import { PrintOutlined, AttachFile, FileDownload, Delete, ExpandMore, Add } from "@mui/icons-material";
import {
  Grid, Accordion, AccordionSummary, AccordionDetails, Tab, Tabs, Typography,
  Box, Button, DialogContent, DialogActions, Tooltip, CircularProgress
} from '@mui/material';
import { LoadingButton } from '@mui/lab';
import BlocRenseignement from "../Dossier/BlocRenseignement";
import BlocActions from '../Dossier/BlocActions';
import BlocDocuments from '../Dossier/BlocDocuments';
import BlocHistorique from '../Dossier/BlocHistorique/BlocHistorique';
import BlocEchangeCompact from '../Dossier/BlocEchangeCompact';
import ButtonExport from '../Dossier/BlocExport/BlocExport'
import { Prompt, ButtonText } from '../composants_internes';
import { grey } from '@mui/material/colors';
import PageError from "../../components/PageError/PageError";


const styles = theme => ({
  root: {
    width: '100%',
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  expansion: {
    marginTop: 16,
    marginBottom: 16
  },
  header: {
    display: "flex",
    justifyContent: "left",
    width: "100%"
  },
  tiers: {
    fontSize: "0.9em",
    color: "#762020"
  },
  dataPanel: {
    backgroundColor: theme.palette.background.default
  },
  title: {
    textAlign: "left",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    paddingLeft: 15,
    color: "#2F80ED",
    fontWeight: "bold",
  },
  AccordionTitle: {
    color: theme.palette.getContrastText(theme.palette.background.default),
    marginRight: 35
  },
  editButton: {
    color: "#2f8a20",
    border: "solid",
    marginLeft: theme.spacing(3),
    marginTop: theme.spacing(-1),
  },
  tabs: {
    backgroundColor: "#fefefe"
  },
  tabsRoot: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    display: 'flex',
    height: 240,
  },
  tabsTab: {
    borderRight: `1px solid ${theme.palette.divider}`,
    padding: 5
  },
  history: {
    position: "fixed",
    bottom: "20px",
    left: "15px",
    fontSize: "10px",
    color: grey[400],
    width: 200
  },
  historyExpanded: {
    position: "relative",
    top: "350px",
    right: "1300px",
    fontSize: "10px",
    color: grey[400],
    width: 200
  },
  historyClicked: {
    position: "fixed",
    bottom: "20px",
    left: "15px",
    fontSize: "10px",
    color: grey[400],
    width: 200,
    borderRight: `2px solid #2F80ED`
  },
  historyClickedExpanded: {
    position: "relative",
    top: "350px",
    right: "195px",
    fontSize: "10px",
    color: grey[400],
    width: 250,
    height: 45,
    borderRight: `2px solid #2F80ED`
  },
  action: {
    color: "#BA3F1D"
  },
  creation: {
    color: '#A77E58'
  },
  enCours: {
    color: "#5FAD56"
  },
  clos: {
    color: "#263D42"
  }
});


const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
}


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      style={{ width: "100%", padding: 5 }}
      {...other}
    >
      {value === index &&
        <Box p={3}>
          {children}
        </Box>
      }
    </div>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

class Dossier extends Component {
  constructor(props) {
    super(props);
    this.state = {
      openHistory: false,
      value: 0,
      openEchange: false,
      createdEchange: null,
      zipLoading: false,
      openDialog: false,
      openDialogSupr: false,
      expanded: false
    };
  }

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    this.props.getDossier(id);
    await this.props.getActeurs();
    await this.props.getHistoriquesByDossier(id);

    if (this.props.auth) {
      this.props.getEchangesByDossierByUtilisateur(id, this.props.auth.organisation.acteur_id);
      this.props.getDocumentsByDossierByActeur(id, this.props.auth.organisation.acteur_id);
      let acteur_id = this.props.auth.organisation.acteur_id;
      if (acteur_id) {
        this.props.getCategorieByActeur(acteur_id);
      }
    }
  };

  componentDidUpdate(prevProps, _prevState) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.props.getDossier(this.props.match.params.id);
      this.props.getHistoriquesByDossier(this.props.match.params.id);
      if (this.props.auth) {
        this.props.getEchangesByDossierByUtilisateur(this.props.match.params.id, this.props.auth.organisation.acteur_id);
        this.props.getDocumentsByDossierByActeur(this.props.match.params.id, this.props.auth.organisation.acteur_id);

      }
    }
    if (this.props.auth && !prevProps.auth && this.props.dossier) {
      this.props.getHistoriquesByDossier(this.props.match.params.id);
      this.props.getEchangesByDossierByUtilisateur(this.props.match.params.id, this.props.auth.organisation.acteur_id);
      this.props.getDocumentsByDossierByActeur(this.props.match.params.id, this.props.auth.organisation.acteur_id);
      let acteur_id = this.props.auth.organisation.acteur_id;
      if (acteur_id) {
        this.props.getCategorieByActeur(acteur_id);
      }
    }
  }

  handleChange = (_event, newValue) => {
    this.setState({ value: newValue, openHistory: false });
  };

  handleOpenEchange = () => {
    this.setState({ openEchange: true });
  }

  handleCloseEchange = (echange = null) => {
    this.setState({ value: 0, createdEchange: echange });
    this.setState({ openEchange: false });
  }

  handleExpandedChange = () => {
    this.setState({ expanded: false })
  }

  deletefile = async (id) => {
    await this.props.deleteFile(id);
  }

  getAllDocument = async (dossier_id) => {

    this.setState({ zipLoading: true })
    const file = await getAllDocumentsByDossier(dossier_id)
    const blob = b64toBlob(file, 'application/zip')
    const fileUrl = await window.URL.createObjectURL(blob);
    const link = document.createElement('a');
    link.href = fileUrl;
    link.download = `Export.zip`;
    document.body.appendChild(link);
    link.click();
    this.setState({ zipLoading: false })

  }
  openDialog = () => {
    this.setState({ openDialog: !this.state.openDialog })
  }

  handleClose = () => {
    this.setState({ openDialog: !this.state.openDialog })
  }

  openDialogSupr = () => {
    this.setState({ openDialogSupr: !this.state.openDialogSupr })
  }

  handleCloseSupr = () => {
    this.setState({ openDialogSupr: !this.state.openDialogSupr })
  }

  handleDelete = async (event) => {
    event.preventDefault();
    const id = this.props.dossier._id
    const acteur = this.props.dossier.acteur_id
    await this.props.deleteOneDossier({ id, acteur });
    this.handleCloseSupr()
    this.props.history.push("/")

  }

  handleOpenHistorique = () => {
    this.setState({ openHistory: true, value: this.props.echanges.length !== 0 ? this.props.documents.length !== 0 ? 3 : 2 : 0 });
  }

  identite = (individu) => {
    if (!individu) return "";
    return `${transformCivilite(individu.civilite)?.toUpperCase()} ${individu.nom ? individu.nom?.toUpperCase() : ""} ${individu.prenom ? individu.prenom?.toUpperCase() : ""}`
  }

  render() {

    const { classes, dossier, echanges, auth, acteurs, documents, historiques, categories } = this.props;
    const { value, openHistory } = this.state;

    if (!dossier || !auth)
      return <div><CircularProgress /></div>;

    if (dossier.error)
      return <PageError />

    const forAction = echanges && echanges.reduce((acc, ech) => {
      if (acc === true) return true;
      let haveToSee = estActeurEchange(ech, auth.organisation.acteur_id, auth.equipes);
      if (haveToSee) {
        let actionSorted = ech.action.sort((a, b) => new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1);
        if (actionSorted && actionSorted[0]) {
          haveToSee = estDestinataire(actionSorted[0].destinataire_id, auth.organisation.acteur_id, auth.equipes);
          return haveToSee;
        }
      }
      return acc;
    }, 0);

    let titrePage = "CRÉATION DU DOSSIER";
    let coulTitre = 'creation';
    if (dossier.clos) {
      titrePage = "DOSSIER CLOS";
      coulTitre = 'clos';
    } else if (echanges && echanges[0]) {
      if (forAction) {
        titrePage = "POUR ACTION";
        coulTitre = "action";
      } else {
        titrePage = "DOSSIER EN COURS";
        coulTitre = 'enCours';
      }
    }

    return (

      <Grid container item xs={12} className={classes.root}>
        <Grid container item sx={{ paddingLeft: 1, paddingRight: 1 }} alignItems="center" justifyContent="space-between"
          spacing={3}>
          {!dossier &&
            <Grid item>
              <Typography variant="button" className={classes.title}>
                Création d'un dossier
              </Typography>
            </Grid>
          }
          {dossier && dossier._id &&
            <Grid item >
              <Typography variant="h5" className={clsx(classes.title, classes[coulTitre])}>
                {titrePage}
              </Typography>
            </Grid>
          }
          <Grid item>
            {echanges && echanges.length === 0 &&
              <Button
                startIcon={<Delete />}
                color={"error"}
                onClick={this.openDialogSupr}
              >
                SUPPRESSION
              </Button>
            }
            <Prompt
              titre={"Confirmer la Supression?"}
              open={this.state.openDialogSupr}
              onClose={this.handleCloseSupr}
              actions={
                <DialogActions>
                  {echanges && echanges.length === 0 &&
                    <Button
                      startIcon={<Delete />}
                      color={"error"}
                      onClick={this.handleDelete}
                    >
                      SUPPRESSION
                    </Button>
                  }
                  <Button onClick={this.handleCloseSupr} color="primary">
                    Fermer
                  </Button>
                </DialogActions>
              }
            >
            </Prompt>

            <Button
              onClick={this.openDialog}
              startIcon={<FileDownload />}
            >
              Export
            </Button>


            {this.state.openDialog &&
              <Prompt
                titre={"Export du Dossier"}
                open={this.state.openDialog}
                onClose={this.handleClose}
                sx={{ padding: "40px" }}
                actions={
                  <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                      Fermer
                    </Button>
                  </DialogActions>
                }
              >
                <DialogContent key={dossier._id} sx={{ display: "flex" }}>
                  <Grid item sx={{ margin: "10px" }}>
                    {echanges && echanges.length > 0 ?
                      <ButtonExport key={dossier._id} dossier={dossier} echanges={echanges} acteurs={acteurs} />
                      :
                      !echanges ?
                        <Tooltip title="En cours de chargement" arrow >
                          <span>
                            <ButtonText
                              variant="text"
                              icon={<PrintOutlined />}
                              texte="Export PDF"
                              disable={true}
                            />
                          </span>
                        </Tooltip>
                        :
                        <Tooltip title="Pas d'échanges sur le dossier" arrow >
                          <span>
                            <ButtonText
                              variant="text"
                              icon={<PrintOutlined />}
                              texte="Export PDF"
                              disable={true}
                            />
                          </span>
                        </Tooltip>
                    }
                  </Grid>
                  <Grid item sx={{ margin: "10px" }}>
                    <LoadingButton loading={this.state.zipLoading} onClick={(event) => {
                      event.stopPropagation();
                      this.getAllDocument(dossier._id)
                    }}
                      endIcon={<AttachFile />}
                    >
                      Export Piéces jointes
                    </LoadingButton>
                  </Grid>
                </DialogContent>
              </Prompt>
            }
          </Grid>
        </Grid>
        <Accordion key={1} defaultExpanded={true} className={classes.root} expanded={this.state.expanded} onChange={() => this.setState({ expanded: !this.state.expanded })}>
          <AccordionSummary
            expandIcon={<ExpandMore />}
            aria-label="Expand"
            className={classes.dataPanel}
          >
            <Grid item container xs={12}>
              <Grid item container xs={8} className={classes.header} alignItems="center">
                <Grid item>
                  <Typography variant="button" className={classes.AccordionTitle}>Etat civil</Typography>
                </Grid>
                <Grid item>
                  <Typography
                    variant="overline"
                  >
                    {dossier ?
                      this.identite(dossier.individuPrincipal)
                      :
                      ""
                    }
                  </Typography>
                </Grid>
              </Grid>

              <Grid container item xs={4} alignItems="center" justifyContent="flex-end">
                <Typography variant='caption'>Voir en détail</Typography>
              </Grid>
            </Grid>
          </AccordionSummary>
          <AccordionDetails>
            <BlocRenseignement
              dossier={dossier || null}
              id={(dossier && dossier._id) || null}
              history={this.props.history}
            />
            <ButtonText
              color="primary"
              action={this.handleExpandedChange}
              texte="FERMER"
              variant="text"
            />
          </AccordionDetails>
        </Accordion>
        {(dossier && dossier._id) &&
          <div className={classes.tabsRoot}>
            {this.state.openEchange &&
              <WrapperCreateEchange
                acteurId={this.props.auth.organisation.acteur_id}
                open={this.state.openEchange}
                onClose={this.handleCloseEchange}
                dossier_id={dossier._id}
                dossier={dossier}
                deleteFile={this.deletefile.bind(this)}
              />
            }
            <Tabs
              value={value}
              onChange={this.handleChange}
              orientation="vertical"
              aria-label="Menu de navigation des échanges"
              className={classes.tabsTab}
              TabIndicatorProps={{
                style: openHistory ? { display: 'none' } : ""
              }}
            >
              {echanges && Array.isArray(echanges) && echanges.length > 0 &&
                <Tab label="ECHANGE(S)" {...a11yProps(0)} />
              }
              {echanges && Array.isArray(echanges) && echanges.length > 0 &&
                <Tab label="ACTION(S)" {...a11yProps(1)} />
              }
              {documents && Array.isArray(documents) && documents.length > 0 &&
                <Tab label="DOCUMENT(S)" {...a11yProps(2)} />
              }
              <Tab label={
                <Tooltip
                  title={categories.length === 0 ?
                    <>
                      <Typography>Aucune Catégorie</Typography>
                      <Typography variant='body'>demandez à l'administateur de votre organisation d'en ajouter</Typography>
                    </>
                    :
                    null
                  }
                  arrow
                >
                  <span>
                    <ButtonText
                      color="primary"
                      aria-label="add"
                      action={this.handleOpenEchange}
                      texte="AJOUT D'UN ECHANGE"
                      icon={<Add />}
                      disable={categories.length === 0}
                    />
                  </span>
                </Tooltip>
              }
              />
            </Tabs>
            <Fragment>
              {echanges && Array.isArray(echanges) && echanges.length > 0 &&
                <TabPanel value={value} index={0}>
                  <BlocEchangeCompact
                    createdEchange={this.state.createdEchange || null}
                    echanges={[...echanges]}
                    dossier={dossier}
                    documents={documents || []}
                    acteurId={this.props.auth.organisation.acteur_id}
                  />
                </TabPanel>
              }
              {echanges && Array.isArray(echanges) && echanges.length > 0 &&
                <TabPanel value={value} index={1}>
                  <Grid container item xs={12}>
                    <BlocActions
                      acteurId={this.props.auth.organisation.acteur_id}
                      echanges={echanges}
                      key={dossier._id}
                      documents={documents || []}
                      delete={this.deletefile.bind(this)}
                    />
                  </Grid>
                </TabPanel>
              }
              {documents && Array.isArray(documents) && documents.length > 0 &&
                <TabPanel value={value} index={2}>
                  <Grid container item xs={12}>
                    <BlocDocuments
                      acteurId={this.props.auth.organisation.acteur_id}
                      key={dossier._id}
                      dossier={dossier}
                      documents={documents || []}
                      handleDelete={this.deletefile.bind(this)}
                    />
                  </Grid>
                </TabPanel>
              }
            </Fragment>
            {historiques && Array.isArray(historiques) && historiques.length > 0 &&
              <Grid className={openHistory ? this.state.expanded ? classes.historyClickedExpanded : classes.historyClicked : this.state.expanded ? classes.historyExpanded : classes.history}>
                <ButtonText
                  color={openHistory ? "primary" : "inherit"}
                  variant="text"
                  aria-label="historique"
                  action={this.handleOpenHistorique}
                  texte="HISTORIQUE(S)"
                />
              </Grid>
            }
            {openHistory && historiques && Array.isArray(historiques) && historiques.length > 0 &&
              <Grid container item xs={12} key={dossier._id + historiques.length}>
                <BlocHistorique
                  key={dossier._id + historiques.length}
                  historiques={historiques}
                  dossier={dossier}
                  acteurId={this.props.auth.organisation.acteur_id}
                />
              </Grid>
            }
          </div>
        }
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    dossier: state.dossier,
    echanges: state.echanges,
    documents: state.documents,
    auth: state.auth,
    acteurs: state.acteurs,
    historiques: state.historiques,
    categories: state.categories,
  };
}

function transformCivilite(civilite) {
  switch (civilite) {
    case "M":
      return "Monsieur";
    case "Mme":
      return "Madame";
    case "Mlle":
      return "Mademoiselle";
    default:
      return "";
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDocumentsByDossierByActeur: (id, acteur) => dispatch(getDocumentsByDossierByActeur(id, acteur)),
    getDossier: (data) => dispatch(getDossier(data)),
    getHistoriquesByDossier: (id) => dispatch(getHistoriquesByDossier(id)),
    getEchangesByDossierByUtilisateur: (dossier_id) => dispatch(getEchangesByDossierByUtilisateur(dossier_id)),
    deleteFile: (id) => dispatch(deleteFile(id)),
    getActeurs: () => dispatch(getActeurs()),
    getCategorieByActeur: (id) => dispatch(getCategorieByActeur(id)),
    deleteOneDossier: (id) => dispatch(deleteOneDossier(id)),
    getAllDocumentsByDossier: (id) => dispatch(getAllDocumentsByDossier(id))
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Dossier))