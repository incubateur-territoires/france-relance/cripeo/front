import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'
import withStyles from '@mui/styles/withStyles';
import { Grid, Button } from '@mui/material';
import ActeursListe from "../Acteur/ActeursListe";
import CategoriesListe from "../Categorie/CategorieListe";
import { TH } from '../composants_internes';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  button: {
    backgroundColor: '#3867cf',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: '#033878'
    }
  },
});


class Admin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  handleClose = () => {
    this.setState({ open: false, dossier: null })
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  toRoute = route => (event) => {
    event.stopPropagation();
    this.props.history.push(`/${route}`);
  };

  handleClick = (event) => {
    this.setState({ open: event.currentTarget.name })
  }

  render() {
    const { classes, keycloak, roles } = this.props;
    const { open } = this.state;

    if (!roles || roles.length === 0) return <div>Aucuns droits (rôles)</div>;
    const administrateur = keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.GESTION_ORGA) !== -1;
    if (!administrateur) return <Redirect to='/' />;

    return (
      <Grid container item xs={12} justifyContent="space-evenly" alignItems="center" direction="row" spacing={2}>
        <Grid item xs={12}>
          <TH variant="h5" >
            Interface Administration
          </TH>
        </Grid>
        <Grid container item>
          <Grid item xs={3}>
            <Button onClick={this.handleClick} name="acteur" className={classes.button}>Acteur</Button>
          </Grid>
          <Grid item xs={3}>
            <Button onClick={this.handleClick} name="categorie" className={classes.button}>Gestion des échanges</Button>
          </Grid>
          <Grid item xs={3}>
            <Button>Acteur</Button>
          </Grid>
        </Grid>
        {open === "acteur" &&
          <Grid container item xs={12} style={{ marginTop: 50 }}>
            <ActeursListe />
          </Grid>
        }
        {open === "categorie" &&
          <Grid container item xs={12} style={{ marginTop: 50 }}>
            <CategoriesListe />
          </Grid>
        }
      </Grid>
    );
  }

}

function mapStateToProps(state) {
  return {
    keycloak: state.keycloak,
    roles: state.roles
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Admin));