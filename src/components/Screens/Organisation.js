import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'
import withStyles from '@mui/styles/withStyles';
import { Grid, CircularProgress } from '@mui/material';
import { getActeur, setActeur } from "../../actions/acteur";
import Acteur from "../Acteur/Acteur";
import { green } from '@mui/material/colors';
import { TH } from '../composants_internes';
import PageError from "../../components/PageError/PageError";


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    backgroundColor: "#B6174B",
    textTransform: "uppercase",
    textAlign: "center"
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
});

class Organisation extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modeEdition: this.props.enabled || false,
      actif: props.acteur && props.acteur._id ? props.acteur.actif : true,
    }
  }

  componentDidMount = async () => {
    const id = this.props.match.params.id;
    await this.props.getActeur(id);
  }

  componentDidUpdate() {
    if ((this.props.acteur && this.props.acteur._id !== this.props.match.params.id) || !this.props.acteur) {
      this.props.getActeur(this.props.match.params.id);
    }
  }

  handleSave = async (data) => {
    await this.props.setActeur(data);
    window.location.reload();
  }

  render() {
    const { classes, acteur, keycloak, roles, auth } = this.props;

    if (!roles || roles.length === 0) return <div>Aucuns droits (rôles)</div>;
    if (!auth) return <div>Aucuns droits (auth)</div>;
    const administrateur = keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.GESTION_ORGA) !== -1;
    const administrateurFonc = keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.CREATION_ORGA) !== -1;
    if (!administrateur) return <Redirect to='/' />;
    if (!acteur) return <CircularProgress size={34} className={classes.fabProgress} />;
    if (!administrateurFonc && acteur._id !== auth.organisation.acteur_id) return <Redirect to='/' />;

    if (acteur === null) return <PageError />;

    return (
      <Grid container item xs={12} justifyContent="space-evenly" alignItems="center" direction="row">
        <Grid item xs={12} className={classes.title}>
          <TH
            color="white"
            align="center"
            variant="h6"
          >
            Interface Administration
          </TH>
        </Grid>
        <Grid item xs={12}>
          <Acteur
            handleClick={this.handleSave.bind(this)}
            acteur={acteur}
            isOrga={true}
            creation={false}
          />
        </Grid>
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    acteur: state.acteur,
    keycloak: state.keycloak,
    roles: state.roles,
    auth: state.auth
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getActeur: (id) => dispatch(getActeur(id)),
    setActeur: (data) => dispatch(setActeur(data)),
  }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Organisation));