import { Backdrop, Box, CircularProgress, Grid, Typography } from '@mui/material';
import React, { useState, useCallback, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import BlocFiltre from '../Liste/BlocFiltre/BlocFiltre';
import { ButtonText } from '../composants_internes';
import { setParametrageFiltres, getFiltres } from '../../actions/filtres';
import { getCategorieByActeurEmeDes } from "../../actions/categorie";


export default function MonParametrage({ user, closeFiltrePerso }) {

  const dispatch = useDispatch();
  const utili = useSelector(state => state.utilisateur);
  const categories = useSelector(state => state.categories);
  const defautsFiltres = useSelector(state => state.defautsFiltres);
  const utilisateur = user && user !== undefined && user !== null ? user : utili;

  const [defauts, setDefauts] = useState({
    secteurGeoSelectValue: "all",
    secteurAdmSelectValue: "all",
    filterReferenceExist: false,
    filterUrgence: false,
    filterUrgenceCreation: false,
    recherche: "",
    categoryFilter: "",
  });

  const cbSetFilter = useCallback(
    (id, filtres) => dispatch(setParametrageFiltres(id, filtres)),
    [dispatch]
  );

  const cbgetFiltres = useCallback(
    (id) => dispatch(getFiltres(id)),
    [dispatch]
  );

  const cbGetCategories = useCallback(
    () => dispatch(getCategorieByActeurEmeDes()),
    [dispatch]
  );

  useEffect(() => {
    setDefauts(defautsFiltres);
  }, [defautsFiltres]);

  useEffect(() => {
    cbgetFiltres(utilisateur._id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [utilisateur]);

  useEffect(() => {
    if (!categories && utilisateur) {
      cbGetCategories();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [categories, utilisateur]);

  if (!categories || !utilisateur) return <Backdrop open> <CircularProgress /></Backdrop>

  const submitFormulaire = () => {
    cbSetFilter(utilisateur._id, defauts);
    if (user !== undefined) {
      closeFiltrePerso();
    }
  };

  return (
    <Grid item container>
      <Box margin={5} flexGrow={3} sx={{ boxShadow: 2 }} >
        <Grid container alignItems="center" justifyContent="flex-start" spacing={2} margin={2} >
          <Typography variant='h5' color='primary' >Paramétrage des filtres par défaut</Typography>
          <BlocFiltre form defautsFiltres={defautsFiltres} defauts={defauts} setDefauts={setDefauts} />
        </Grid>
        <Grid item container justifyContent="flex-end" margin={5} xs={11}>
          <ButtonText
            color="primary"
            action={submitFormulaire}
            texte="valider"
          />
        </Grid>
      </Box>
    </Grid>
  )
}