import React, {Component} from 'react';
import {connect} from "react-redux";
import withStyles from '@mui/styles/withStyles';
import {getDossiers} from "../../actions/dossier";
import {Grid, Typography} from '@mui/material';
import {RateReview, Pageview} from "@mui/icons-material";
import CardSimple from "../Commun/Card/CardSimple";
import CreateDossier from "../Dossier/CreateDossier";


const styles = theme => { 
  return ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootCards : {
    padding: 50
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  }
})};


class Accueil extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      dossier: null
    };
  }

  componentDidMount = async () => {
    await this.props.getDossiers();
  };

  handleClose = () => {
    this.setState({open: false, dossier: null})
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  toRoute = route => (event) => {
    event.stopPropagation();
    this.props.history.push(`/${route}`);
  };

  render() {
    const {classes} = this.props;

    return (
      <Grid container item xs={12} justifyContent="space-evenly" alignItems="center" direction="row" className={classes.rootCards}>
        <CreateDossier open={this.state.open} handleClose={this.handleClose.bind(this)} />
        <Grid item xs={3}>
          <CardSimple         
            handleClic={this.handleOpen}
            center
            couleur={"teal"}
          >
            <Grid container direction="column" justifyContent="center" alignItems="center" spacing={4}>
              <Grid item>
                <Typography  gutterBottom>
                  <RateReview />
                </Typography>
              </Grid>
              <Grid item>
                <Typography color="textSecondary" gutterBottom>
                  CREER UN DOSSIER
                </Typography>
              </Grid>
            </Grid>
          </CardSimple>
        </Grid>
        <Grid item xs={3} >
          <CardSimple
            handleClic={this.toRoute("liste")}
            center
            couleur={"rgb(49,107,55)"}
          >
            <Grid container  direction="column" justifyContent="center" alignItems="center" spacing={4}>
              <Grid item>
                <Typography  gutterBottom>
                  <Pageview /> 
                </Typography>
              </Grid>
              <Grid item>
                <Typography color="textSecondary" gutterBottom>
                  CONSULTER SES DOSSIERS
                </Typography>
              </Grid>
            </Grid>
          </CardSimple>
        </Grid>
      </Grid>
    );
  }

}

function mapStateToProps(state) {
  return {
    dossiers: state.dossiers,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getDossiers: () => dispatch(getDossiers()),
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Accueil));