import React, { Component } from 'react';
import { Grid } from '@mui/material';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getDossiersByActeur, clearDossier } from "../../actions/dossier";
import { getUtilisateurs } from "../../actions/utilisateur";
import { changeFiltre, getFiltres } from "../../actions/filtres";
import { getCategorieByActeurEmeDes } from "../../actions/categorie";
import { Add } from '@mui/icons-material';
import BlocAll from "../Liste/BlocAll";
import CreateDossier from "../Dossier/CreateDossier";
import WrapperTab from '../Liste/WrapperTab/WrapperTab';
import { multiRechercheChamps, majRetourMultiRecherche } from '../Recherche/recherche';
import BlocFiltre from '../Liste/BlocFiltre/BlocFiltre';
import { ButtonText } from '../composants_internes';

const styles = theme => {
    return ({
        root: {
            width: '100%',
            marginTop: theme.spacing(3),
            marginBottom: theme.spacing(3),
        },
        tablecell: {
            textAlign: 'center'
        },
        title: {
            color: '#2F80ED',
            fontSize: 'xx-large',
            textAlign: "left"
        },
        card: {
            marginBottom: 20
        },
        overline: {
            fontSize: 7
        },
        fab: {
            backgroundColor: '#2F80ED',
        }
    })
};

class Liste extends Component {
    state = {
        recherche: "",
        open: false,
        dossier: null,
        modal: null,
        openModal: false,
        secteurGeoSelectValue: "all",
        secteurAdmSelectValue: "all",
        filterReferenceExist: false,
        filterUrgence: false,

    }

    componentDidMount = async () => {
        const { utilisateur } = this.props;
        if (utilisateur) {
            await this.props.getDossiersByActeur();
            await this.props.getCategorieByActeurEmeDes();
            await this.props.getFiltres(this.props.utilisateur._id);
            await this.props.getUtilisateurs();
            this.props.changeFiltre("init", this.props.defautsFiltres || null);
        }
        this.props.clearDossier();
    };

    componentDidUpdate = (prevProps) => {
        if (this.props.utilisateur && this.props.utilisateur !== prevProps.utilisateur) {
            this.props.getDossiersByActeur();
            this.props.getCategorieByActeurEmeDes();
            this.props.getFiltres(this.props.utilisateur._id);
            this.props.getUtilisateurs();
            this.props.changeFiltre("init", this.props.defautsFiltres || null);
        }
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({ [name]: value });
    };

    filtrerRecherche = (params, data) => {
        if (data) {
            if (this.state.recherche && this.state.recherche !== "") {
                const recherches = this.state.recherche.trim().split(" ");
                const inFine = params.champs.map((tabChamp, key) => {
                    const filtre = multiRechercheChamps(data, recherches, tabChamp.more, tabChamp.noSpace, tabChamp.exceptions, tabChamp.importance);
                    return majRetourMultiRecherche(data, filtre, params.champRetour[key]);
                });
                if (inFine && Array.isArray(inFine)) {
                    return (inFine[0].data);
                }
            } else {
                return (data);
            }
        }
    };

    handleClose = () => {
        this.setState({ open: false, dossier: null })
    };

    handleOpen = () => {
        this.setState({ open: true });
    };

    handleSecteurGeoSelectValue = (value) => {
        this.setState({ secteurGeoSelectValue: value });
    }

    handleSecteurAdmSelectValue = (value) => {
        this.setState({ secteurAdmSelectValue: value });
    }

    handleChangeReferenceExist = () => {
        this.setState({ filterReferenceExist: !this.state.filterReferenceExist });
    }
    handleChangeUrgence = () => {
        this.setState({ filterUrgence: !this.state.filterUrgence });
    }

    toRoute = route => (event) => {
        event.stopPropagation();
        this.props.history.push(`/${route}`);
    };

    setOpenModal = (dossiers, libelle, colonnes, couleur) => () => {
        this.setState({ modal: { dossiers, libelle, colonnes, couleur }, openModal: true })
    }

    handleCloseModal = () => {
        this.setState({ modal: null, openModal: false });
    }

    handleAutoCompleteCategory = (event, value) => {
        let category = '';
        if (value) category = value.libelle
        this.setState({ categoryFilter: category });
    }

    filterCategories = (dos) => {
        return dos.categories.findIndex(cat => {
            if (cat) return cat.libelle === this.state.categoryFilter
            return false;
        }) !== -1;
    }

    filterSecteurGeographique = (dos) => {
        if (dos.secteurGeo && dos.secteurGeo.length > 0) {
            return dos.secteurGeo.findIndex(sect => {
                if (sect) return sect.code === this.state.secteurGeoSelectValue && sect.acteur_id === this.props.utilisateur.organisation.acteur_id
                return false;
            }) !== -1;
        }
        return false;
    }

    filterNumeroDossierReference = (dos) => {
        if (dos.numeroApplication && dos.numeroApplication.length > 0) {
            return dos.numeroApplication.findIndex(num => {
                if (num) return String(num.acteur_id) === String(this.props.utilisateur.organisation.acteur_id)
                return false;
            }) !== -1;
        }
        return false;
    }

    filterSecteursAdministratif = (dos) => {
        if (dos.secteurOrganisation && dos.secteurOrganisation.length > 0) {
            return dos.secteurOrganisation.findIndex(sect => {
                if (sect) return String(sect.secteur_id) === String(this.state.secteurAdmSelectValue) && String(sect.acteur_id) === String(this.props.utilisateur.organisation.acteur_id)
                return false;
            }) !== -1;
        }
        return false;
    }
    filterUrgence = (dos) => {
        return dos.urgent
    }


    render() {
        const { classes, dossiersEnCours, dossiersATraiter, dossiersEnCreation, dossiersClos, categories, filtres, defautsFiltres } = this.props;
        const { modal, openModal } = this.state;

        const enteteALL = [
            { id: 'individuPrincipalNom', numeric: false, disablePadding: true, label: 'Nom Prénom', type: "string", disableAlign: false, disableSize: false },
            { id: 'nombreEchanges', numeric: false, disablePadding: true, label: 'Éch', type: "numero", disableAlign: false, disableSize: false },
            { id: 'created_at', numeric: false, disablePadding: false, label: 'Date de création', type: "date", disableAlign: false, disableSize: false },
            { id: 'updated_at', numeric: false, disablePadding: false, label: 'Dernière mise à jour', type: "date", disableAlign: false, disableSize: false },
            { id: 'dateSuppressionDossier', numeric: false, disablePadding: false, label: 'Date de supression', type: "date", disableAlign: false, disableSize: false },
            { id: 'action', numeric: false, disablePadding: false, label: 'Action', type: "button", labelButton: "Ouvrir", disableAlign: true, disableSize: false },
        ];

        const enteteATraiter = [
            { id: 'urgent', numeric: true, disablePadding: true, label: 'Urg', type: "boolean", disableAlign: false, disableSize: true },
            { id: 'individuPrincipalNom', numeric: false, disablePadding: true, label: 'Nom Prénom', type: "string", disableAlign: false, disableSize: false },
            { id: 'nombreEchanges', numeric: false, disablePadding: true, label: 'Éch', type: "numero", disableAlign: false, disableSize: false },
            { id: 'created_at', numeric: false, disablePadding: false, label: 'Date de création', type: "date", disableAlign: false, disableSize: false },
            { id: 'updated_at', numeric: false, disablePadding: false, label: 'Dernière mise à jour', type: "date", disableAlign: false, disableSize: false },
            { id: 'action', numeric: false, disablePadding: false, label: 'Action', type: "button", labelButton: "Ouvrir", disableAlign: true, disableSize: false },
        ];

        const enteteEnCours = [
            { id: 'acteurEstInfo', numeric: true, disablePadding: true, label: 'Info', type: "info", disableAlign: false, disableSize: true },
            { id: 'urgent', numeric: true, disablePadding: false, label: 'Urg', type: "boolean", disableAlign: true, disableSize: true },
            { id: 'individuPrincipalNom', numeric: false, disablePadding: true, label: 'Nom Prénom', type: "string", disableAlign: false, disableSize: false },
            { id: 'nombreEchanges', numeric: false, disablePadding: true, label: 'Éch', type: "numero", disableAlign: false, disableSize: false },
            { id: 'created_at', numeric: false, disablePadding: false, label: 'Date de création', type: "date", disableAlign: false, disableSize: false },
            { id: 'updated_at', numeric: false, disablePadding: false, label: 'Dernière mise à jour', type: "date", disableAlign: false, disableSize: false },
            { id: 'action', numeric: false, disablePadding: false, label: 'Action', type: "button", labelButton: "Ouvrir", disableAlign: true, disableSize: false },
        ];
        return (
            <Grid container >
                <CreateDossier open={this.state.open} handleClose={this.handleClose.bind(this)} />
                <WrapperTab open={openModal} modal={modal} handleClose={this.handleCloseModal} />
                <Grid container item alignItems="center" xs={12} spacing={2} sx={{ margin: "auto" }}>
                    <Grid item container xs={12} alignItems="center" justifyContent="flex-start" spacing={2} >
                        {categories &&
                            <BlocFiltre defauts={defautsFiltres} loading={!dossiersEnCours || !dossiersATraiter || !dossiersEnCreation || !dossiersClos} />
                        }
                    </Grid>

                    <Grid container item xs={3}>
                        <ButtonText
                            color="primary"
                            aria-label="add"
                            action={this.handleOpen}
                            texte="Créer un dossier"
                            icon={<Add />}
                        />
                    </Grid>
                </Grid>

                <Grid container item sm={12} md={6} className={classes.card} direction="column">
                    <BlocAll
                        key={"bloccreation"}
                        dossiers={filtres?.dossiers?.creations || []}
                        titre={"EN COURS DE CREATION"}
                        celluleEntete={[{ id: 'individuPrincipalNom', numeric: false, disablePadding: true, label: 'Nom Prénom', type: "string", disableAlign: false, disableSize: false },
                        { id: 'created_at', numeric: false, disablePadding: false, label: 'Date de création', type: "date", disableAlign: false, disableSize: false },
                        { id: 'circo', numeric: false, disablePadding: false, label: 'Secteur', type: "string", disableAlign: false, disableSize: false },
                        { id: 'action', numeric: false, disablePadding: false, label: 'Action', type: "button", labelButton: "Ouvrir", disableAlign: true, disableSize: false }]}
                        couleurEntete={'#A77E58'}
                        buttonModal
                    />
                </Grid>
                <Grid container item sm={12} md={6} className={classes.card} direction="column" >
                    <BlocAll
                        key={"blocatraiter"}
                        dossiers={filtres?.dossiers?.aTraiter || []}
                        titre={"A TRAITER"}
                        celluleEntete={enteteATraiter}
                        couleurEntete={'#BA3F1D'}
                        buttonModal
                        urgentFilter={true}
                    />
                </Grid>
                <Grid container item sm={12} md={6} className={classes.card} direction="column" >
                    <BlocAll
                        key={"blocencours"}
                        dossiers={filtres?.dossiers?.enCours || []}
                        titre={"DOSSIERS EN COURS"}
                        celluleEntete={enteteEnCours}
                        couleurEntete={'#5FAD56'}
                        buttonModal
                    />
                </Grid>
                <Grid container item sm={12} md={6} className={classes.card} direction="column">
                    <BlocAll
                        key={"blocclos"}
                        dossiers={filtres?.dossiers?.clos || []}
                        titre={"DOSSIERS CLOS"}
                        celluleEntete={enteteALL}
                        couleurEntete={'#263D42'}
                        buttonModal
                    />
                </Grid>
            </Grid>
        );
    }

}

function mapStateToProps(state) {
    return {
        dossiers: state.dossiers,
        dossiersATraiter: state.dossiersATraiter,
        dossiersEnCours: state.dossiersEnCours,
        dossiersEnCreation: state.dossiersEnCreation,
        dossiersClos: state.dossiersClos,
        keycloak: state.keycloak,
        utilisateur: state.utilisateur,
        utilisateurs: state.utilisateurs,
        categories: state.categories,
        acteur: state.acteur,
        filtres: state.filtres,
        defautsFiltres: state.defautsFiltres,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getDossiersByActeur: () => dispatch(getDossiersByActeur()),
        getCategorieByActeurEmeDes: () => dispatch(getCategorieByActeurEmeDes()),
        clearDossier: () => dispatch(clearDossier()),
        changeFiltre: (cle, valeur) => dispatch(changeFiltre(cle, valeur)),
        getFiltres: (id) => dispatch(getFiltres(id)),
        getUtilisateurs: () => dispatch(getUtilisateurs())
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Liste));