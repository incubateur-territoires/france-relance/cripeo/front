import React, { Component } from 'react';
import { connect } from "react-redux";
import { Redirect } from 'react-router-dom'
import withStyles from '@mui/styles/withStyles';
import { Grid } from '@mui/material';
import ActeursListe from "../Acteur/ActeursListe";
import { TH } from '../composants_internes';


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  title: {
    backgroundColor: "#B6174B",
    textTransform: "uppercase",
    textAlign: "center"
  },
  button: {
    backgroundColor: '#3867cf',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: '#033878'
    }
  },
});


class Acteurs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }

  render() {
    const { classes, keycloak, roles } = this.props;

    if (!roles || roles.length === 0) return <div>Aucuns droits (rôles)</div>;
    const administrateur = keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.LISTE_ORGA) !== -1;
    if (!administrateur) return <Redirect to='/' />;

    return (
      <Grid container item xs={12} justifyContent="space-evenly" alignItems="center" direction="row">
        <Grid item xs={12} className={classes.title}>
          <TH
            color="white"
            align="center"
            variant="h6"
          >
            Interface Administration
          </TH>
        </Grid>
        <Grid item xs={12} className={classes.title}>
          <TH
            color="white"
            align="center"
            variant="h6"
          >
            Gestion des équipes
          </TH>
        </Grid>
        <Grid container item xs={12} style={{ marginTop: 50 }}>
          <ActeursListe />
        </Grid>
      </Grid>
    );
  }

}

function mapStateToProps(state) {
  return {
    keycloak: state.keycloak,
    roles: state.roles
  };
}


export default withStyles(styles)(connect(mapStateToProps, null)(Acteurs));