
import { Error404 } from "../composants_internes";

export default function PageError() {
    return (
        <Error404
            color="#0e3c7a"
            variant="pomme"
        />
    )
}