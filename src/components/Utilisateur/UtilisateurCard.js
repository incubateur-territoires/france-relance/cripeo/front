
import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import withStyles from '@mui/styles/withStyles';
import { Grid, Typography, Skeleton, CircularProgress } from "@mui/material";
import { searchByKeycloakId } from "../../actions/keycloak";
import EquipeRow from "../Acteur/EquipeRow";
import Editer from "../Commun/Bouton/Editer";
import Utilisateur from "./Utilisateur";
import { getGroupes } from '../../actions/home';
import { getEquipes, getActeur } from "../../actions/acteur";
import { setUtilisateur, getUtilisateur } from "../../actions/utilisateur";
import { green, blue } from '@mui/material/colors';


const styles = theme => ({
  title: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 'small',
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(1),
    borderBottom: 'solid thin',
    borderBottomColor: blue[500],
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
  carte: {
    borderColor: blue[500],
    borderWidth: 'thin',
  }
});

class UtilisateurCard extends Component {

  state = {
    open: false,
    utilisateurKeycloak: '',
    modeEdition: false,
    utilisateur: this.props.utilisateur ? this.props.utilisateur : null,
  }

  componentDidMount = async () => {
    await this.props.getGroupes();
    if (this.props.utilisateur && this.props.utilisateur.k_userId) {
      const resp = await this.props.searchByKeycloakId(this.props.utilisateur.k_userId);
      this.setState({ utilisateurKeycloak: resp.data });
      await this.props.getEquipes(this.props.utilisateur.organisation.acteur_id);
    }
  };

  componentDidUpdate(prevState) {
    if (this.state.utilisateur && this.state.utilisateur?._id !== prevState.utilisateur?._id && this.props.utilisateur) {
      this.props.getUtilisateur(this.props.utilisateur._id);
    }
  }

  changeEdition = () => {
    this.setState({ modeEdition: !this.state.modeEdition });
  }

  handleCancelEdit = () => {
    this.setState({ modeEdition: false });
  }

  handleClick = async (data) => {
    const res = await this.props.setUtilisateur(data);
    if (res.success) {
      this.setState({ modeEdition: false, utilisateur: data });
      if (this.props.utilisateur && this.props.utilisateur.organisation) {
        this.props.getActeur(this.props.utilisateur.organisation.acteur_id)
      }
    }
  }

  render() {
    const { classes } = this.props;
    const { utilisateurKeycloak, modeEdition, utilisateur } = this.state;

    if (!utilisateur) return <CircularProgress size={34} className={classes.fabProgress} />;

    const equipesRender = (utilisateur && utilisateur.equipes && utilisateur.equipes.length > 0) ? Object.keys(utilisateur.equipes).map(key => {
      return <EquipeRow equipe={utilisateur.equipes[key]} key={"ligneeq-" + utilisateur.equipes[key]._id} />
    }) : [];

    return (
      <Grid container item xs={12} className={classes.carte}>
        <Grid item xs={12} container className={classes.title}>
          <Grid container item xs={11} alignContent={"flex-start"}>
            {!utilisateurKeycloak ?
              <Skeleton sx={{ width: "100%" }} />
              :
              <Typography>
                {utilisateurKeycloak.lastName + ' ' + utilisateurKeycloak.firstName}
              </Typography>
            }
          </Grid>
          <Grid container item xs={1} alignContent={"flex-end"} >
            <Editer action={this.changeEdition} texte={"Editer l'utilisateur"} disable={this.state.modeEdition} size={15} />
          </Grid>
        </Grid>
        <Grid container item xs={12} justifyContent="space-between" >
          {equipesRender}
        </Grid>
        {modeEdition &&
          <Utilisateur
            utilisateur={utilisateur}
            groupes={this.props.groupes}
            equipes={this.props.acteurs}
            handleCancel={this.handleCancelEdit}
            handleClick={this.handleClick}
            open={modeEdition}
          />
        }
      </Grid>
    );
  }
}

function mapStateToProps(state) {
  return {
    groupes: state.groupes,
    acteurs: state.acteurs,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchByKeycloakId: (id) => dispatch(searchByKeycloakId(id)),
    setUtilisateur: (data) => dispatch(setUtilisateur(data)),
    getGroupes: () => dispatch(getGroupes()),
    getEquipes: (id) => dispatch(getEquipes(id)),
    getUtilisateur: (id) => dispatch(getUtilisateur(id)),
    getActeur: (id) => dispatch(getActeur(id)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(withRouter(UtilisateurCard)));