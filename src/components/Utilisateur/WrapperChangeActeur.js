import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import { setUtilisateur } from "../../actions/utilisateur";
import { getActeurs } from "../../actions/acteur";
import { addNotification } from "../../actions/notifications";
import connect from "react-redux/es/connect/connect";
import SelectUI from "../Commun/Input/SelectUI";
import { Button, MenuItem, DialogActions, DialogContent } from "@mui/material";
import { blue } from '@mui/material/colors';
import { Prompt } from '../composants_internes';


const styles = theme => ({
    avatar: {
        backgroundColor: blue[100],
        color: blue[600],
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 150,
    }
});

class WrapperChangeActeur extends Component {

    state = {
        open: false,
        organisation: this.props.utilisateur.organisation,
    };

    handleClose = () => {
        this.props.onClose();
    };

    componentDidMount() {
        this.props.getActeurs();
    }

    handleChange = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    };

    handleSubmit = async () => {
        const { login, libelle, actif, role, _id } = this.props.utilisateur;
        let data = {
            login,
            libelle,
            actif,
            role,
            _id,
            'organisation.acteur_id': this.state.organisation.acteur_id
        };
        const res = await this.props.setUtilisateur(data);
        if (res.success) {
            this.props.addNotification({ variant: "success", message: "L'utilisateur est modifié" });
            this.handleClose();
        } else {
            this.props.addNotification({
                variant: "error",
                message: "Erreur lors de la sauvegarde de l'individu",
                details: res.error[0].msg
            });
        }
    };

    render() {
        const { utilisateur, acteurs, open } = this.props;
        const { organisation } = this.state;

        if (!acteurs || !utilisateur) return <div />

        const acteurMenuItems = acteurs ? Object.keys(acteurs).map(key =>
            <MenuItem
                key={key}
                value={acteurs[key]._id}
            >
                {acteurs[key].entite}
            </MenuItem>
        ) : [];
        return (
            <Prompt
                texte={`Modification de l'acteur: ${utilisateur.libelle}`}
                onClose={this.handleClose}
                aria-labelledby="simple-dialog-title"
                open={open}
                actions={
                    <DialogActions>
                        <Button onClick={this.handleClose} color="secondary">
                            Annuler
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Enregistrer les modifications
                        </Button>
                    </DialogActions>
                }
            >
                <DialogContent>
                    <SelectUI items={acteurMenuItems} value={organisation.acteur_id} handleChange={this.handleChange} name={"acteur_id"} label={"Acteur"} />
                </DialogContent>
            </Prompt>
        )
    }
}

WrapperChangeActeur.propTypes = {
    classes: PropTypes.object.isRequired,
    onClose: PropTypes.func,
    open: PropTypes.bool
};

function mapStateToProps(state) {
    return {
        acteurs: state.acteurs
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUtilisateur: (obj) => dispatch(setUtilisateur(obj)),
        addNotification: (obj) => dispatch(addNotification(obj)),
        getActeurs: () => dispatch(getActeurs())
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(WrapperChangeActeur));