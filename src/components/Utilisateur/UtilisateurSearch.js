import React, { Component } from "react";
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import Input from "../Commun/Input/Input";
import { addNotification } from "../../actions/notifications";
import { searchUserByMail } from "../../actions/keycloak";
import { searchByKeycloakId } from "../../actions/keycloak";
import Recherche from "../Commun/Bouton/Recherche";
import { Sync } from '@mui/icons-material';
import { Grid, Paper, IconButton, Tooltip } from "@mui/material";
import { refreshDataUtilisateur } from "../../actions/utilisateur"

const styles = theme => ({
    paper: {
        padding: theme.spacing(3, 2),
        margin: theme.spacing(3),
        width: '100%',
        textAlign: 'left'
    }
});

class UtilisateurSearch extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: "",
            nouvelUtilisateur: !this.props.disabled
        };
    }

    componentDidMount = async () => {

        if (this.props.k_userId) {
            const res = await this.props.searchByKeycloakId(this.props.k_userId);
            if (res && res.data) {
                this.setState({
                    utilisateur: res.data, email: res.data.email,
                    nom: res.data.lastName, prenom: res.data.firstName, login: res.data.username
                })
                this.props.setUtilisateur(res.data);
            }
        }
    }

    handleSearch = async () => {
        const res = await this.props.searchUserByMail(this.state.email);
        if (res.success && res.data.length === 1) {
            this.setState({
                utilisateur: res.data[0]
            });
            this.props.selectUtilisateur(res.data[0]);
            this.props.addNotification({ variant: "success", message: "Utilisateur trouvé" })
        } else {
            this.setState({ utilisateur: false, nom: "", prenom: "" })
        }
    }

    handleCancel = (event) => {
        event.stopPropagation();
        this.props.handleCancel();
    }

    handleChange = async (event) => {
        await this.setState({ [event.target.name]: event.target.value })
        this.props.setUtilisateur(this.state);
    }

    handleRefresh = async () => {
        await this.props.refreshDataUtilisateur(this.state.utilisateur);
    }

    render() {
        const { disabled, classes } = this.props;
        const { email, utilisateur, nouvelUtilisateur, nom, prenom } = this.state;

        let notEdit = false;
        if (nouvelUtilisateur && utilisateur) notEdit = true;

        return (
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <Input
                        label="Email"
                        name="email"
                        value={email}
                        handleChange={this.handleChange}
                        disabled={!nouvelUtilisateur}
                    />
                </Grid>
                <Grid item xs>
                    {!disabled &&
                        <Recherche action={this.handleSearch} texte={"Rechercher"} />
                    }
                </Grid>
                <Grid item xs>
                    <Tooltip title="Raffraichissement des données de l'utilisateur">
                        <IconButton color="primary" onClick={this.handleRefresh}>
                            <Sync />
                        </IconButton>
                    </Tooltip>
                </Grid>

                <Paper className={classes.paper} variant="outlined" square>
                    <Grid item container xs={12} spacing={2}>

                        <Grid item xs={12} lg={12}>
                            <Input
                                disabled={true}
                                label="Login"
                                name="login"
                                value={(utilisateur && utilisateur.username) || ""}
                                handleChange={this.handleChange}
                            />
                        </Grid>

                        <Grid item xs={5} lg={6}>
                            <Input
                                disabled={notEdit || !!disabled}
                                label="Nom"
                                name="nom"
                                value={(utilisateur && utilisateur.lastName.toUpperCase()) || nom || ""}
                                handleChange={this.handleChange}
                            />
                        </Grid>

                        <Grid item xs={6} lg={6}>
                            <Input
                                disabled={notEdit || !!disabled}
                                label="Prénom"
                                name="prenom"
                                value={(utilisateur && utilisateur.firstName.toUpperCase()) || prenom || ""}
                                handleChange={this.handleChange}
                            />
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        addNotification: (obj) => dispatch(addNotification(obj)),
        searchUserByMail: (mail) => dispatch(searchUserByMail(mail)),
        searchByKeycloakId: (id) => dispatch(searchByKeycloakId(id)),
        refreshDataUtilisateur: (utilisateur) => dispatch(refreshDataUtilisateur(utilisateur))
    }
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(UtilisateurSearch));