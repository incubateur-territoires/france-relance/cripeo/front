import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getUtilisateursByActeur } from "../../actions/utilisateur";
import { Grid } from '@mui/material';
import UtilisateurCard from "./UtilisateurCard";


const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    rootPaper: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(2),
    },
    padding: {
        padding: `0 ${theme.spacing(2)}`,
    },
    chip: {
        margin: theme.spacing(1),
    },
    card: {
        minHeight: 75,
        marginTop: theme.spacing(20),
    },
    paper: {
        padding: theme.spacing(3, 2),
        margin: theme.spacing(1),
        width: '100%',
        textAlign: 'left'
    }
});


class UtilisateursCardListe extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount = async () => {
        if (this.props.acteur_id) await this.props.getUtilisateursByActeur(this.props.acteur_id);
    };

    componentDidUpdate(prevProps) {
        if (this.props.acteur_id !== prevProps.acteur_id) this.props.getUtilisateursByActeur(this.props.acteur_id);
    }

    handleClickUtilisateur = (utilisateur) => {
        this.setState({ utilisateur })
    }

    handleCancelEdit = () => {
        this.setState({ utilisateur: false });
    }

    render() {
        const { utilisateurs } = this.props;

        const utilisateursRender = utilisateurs ? Object.keys(utilisateurs).map(key => {
            return <Grid item xs={4} key={"card-" + utilisateurs[key]._id}>
                <UtilisateurCard
                    key={utilisateurs[key]._id}
                    utilisateur={utilisateurs[key]}
                    handleClick={this.handleClickUtilisateur}
                />
            </Grid>
        }) : [];

        return (
            <Grid container item key={"UtilisateursCardListe"} xs={12} spacing={4}>
                {utilisateursRender}
            </Grid>
        )
    }

}

function mapStateToProps(state) {
    return {
        utilisateurs: state.utilisateurs,
        groupes: state.groupes
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUtilisateursByActeur: (id) => dispatch(getUtilisateursByActeur(id)),
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(UtilisateursCardListe));