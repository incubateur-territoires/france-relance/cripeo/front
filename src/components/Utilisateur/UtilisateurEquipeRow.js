import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { searchByKeycloakId } from '../../actions/keycloak';
import { Grid, CircularProgress } from "@mui/material";
import { green } from '@mui/material/colors';

const styles = theme => ({
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
  ligneUser: {
    paddingLeft: theme.spacing(2),
  }
});


class UtilisateurEquipeRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showActeur: false
    };
  }

  componentDidMount = async () => {
    if (this.props.utilisateur && this.props.utilisateur.k_userId) {
      const resp = await this.props.searchByKeycloakId(this.props.utilisateur.k_userId);
      this.setState({ utilisateurKeycloak: resp.data });
    }
  };

  handleClickShow = (event) => {
    event.stopPropagation();
    this.props.handleClick(this.props.utilisateur);
  }

  handleClickGroupe = () => {
    this.setState({ showActeur: !this.state.showActeur });
  }

  render() {
    const { classes, utilisateur, key } = this.props;
    const { utilisateurKeycloak } = this.state;

    if (!utilisateur) return <CircularProgress size={34} className={classes.fabProgress} />;

    return (
      <Grid container item xs={6} key={"user-" + key} alignContent={"flex-start"} className={classes.ligneUser}>
        {utilisateurKeycloak && '- ' + utilisateurKeycloak.lastName + ' ' + utilisateurKeycloak.firstName}
      </Grid>
    )
  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    searchByKeycloakId: (id) => dispatch(searchByKeycloakId(id))
  }
};


export default withStyles(styles)(connect(null, mapDispatchToProps)(UtilisateurEquipeRow));