import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { TableCell, TableRow } from "@mui/material";
import Groupe from "../Commun/Bouton/Groupe";
import Editer from "../Commun/Bouton/Editer";
import WrapperChangeActeur from "./WrapperChangeActeur";
import { searchByKeycloakId } from '../../actions/keycloak';


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  }
});


class UtilisateurRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showActeur: false
    };
  }

  componentDidMount = async () => {
    if (this.props.utilisateur && this.props.utilisateur.k_userId) {
      const resp = await this.props.searchByKeycloakId(this.props.utilisateur.k_userId);
      this.setState({ utilisateurKeycloak: resp.data });
    }
  };

  handleClickShow = (event) => {
    event.stopPropagation();
    this.props.handleClick(this.props.utilisateur);
  }

  handleClickGroupe = () => {
    this.setState({ showActeur: !this.state.showActeur });
  }

  render() {
    const { utilisateur } = this.props;
    const { showActeur, utilisateurKeycloak } = this.state;
    if (!utilisateur) return <div />
    return (
      <TableRow key={utilisateur._id}>
        <TableCell component="th" scope="row">
          {utilisateurKeycloak && utilisateurKeycloak.email}
        </TableCell>
        <TableCell component="th" scope="row">
          {utilisateurKeycloak && utilisateurKeycloak.lastName}
        </TableCell>
        <TableCell component="th" scope="row">
          {utilisateurKeycloak && utilisateurKeycloak.firstName}
        </TableCell>
        <TableCell component="th" scope="row">
          {utilisateur.organisation.groupe}
        </TableCell>
        <TableCell component="th" scope="row">
          {utilisateur.libelle}
        </TableCell>
        <TableCell component="th" scope="row">
          {utilisateur.actif ? "Oui" : "Non"}
        </TableCell>
        <TableCell component="th" scope="row">
          <Editer texte="Editer" action={this.handleClickShow} />
          <Groupe texte="Change de groupe" action={this.handleClickGroupe} />
          <WrapperChangeActeur open={showActeur} onClose={this.handleClickGroupe} utilisateur={utilisateur} />
        </TableCell>
      </TableRow>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchByKeycloakId: (id) => dispatch(searchByKeycloakId(id))
  }
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(UtilisateurRow));