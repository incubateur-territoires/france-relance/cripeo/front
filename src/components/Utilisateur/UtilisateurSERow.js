import React, { useEffect, useState } from 'react';
import { directSearchByKeycloakId } from '../../actions/keycloak';
import { Skeleton, ListItem, ListItemText, ListItemIcon, Tooltip, Typography } from '@mui/material'
import { SyncProblem, CloudSync, Badge } from '@mui/icons-material';
import { format } from 'date-fns'
import { fr } from 'date-fns/locale';


export default function UtilisateurSERow({ utilisateur }) {
  const [utilisateurKeycloak, setUtilisateurKeycloak] = useState(null);

  const getUserKC = async () => {
    const user = await directSearchByKeycloakId(utilisateur.k_userId)
    setUtilisateurKeycloak(user.data);
  }

  useEffect(() => {
    getUserKC()
    // eslint-disable-next-line
  }, []);

  if (!utilisateur) return <Skeleton />

  return (
    <ListItem>
      <ListItemText primary={utilisateur.libelle || utilisateur.login} secondary={`Membre depuis le : ${format(new Date(utilisateur.created_at), "d MMMM yyyy", { locale: fr })}`} />
      <ListItemIcon>

        {!utilisateurKeycloak &&
          <Tooltip
            title={
              <React.Fragment>
                <Typography variant="subtitle1">Problème de synchronisation du compte</Typography>
                <Typography variant="caption">k_userId: {utilisateur.k_userId}</Typography>
              </React.Fragment>
            }
          >
            <SyncProblem color="error" />
          </Tooltip>
        }

        {utilisateurKeycloak &&
          <Tooltip
            title="Synchronisation du compte valide"
          >
            <CloudSync color="success" />
          </Tooltip>
        }

        {!utilisateur.login &&
          <Tooltip
            title="Aucun login pour l'utilisateur"
          >
            <Badge color="error" />
          </Tooltip>
        }

      </ListItemIcon>
    </ListItem>
  )
}