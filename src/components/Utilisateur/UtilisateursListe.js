import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getUtilisateursByActeur, setUtilisateur } from "../../actions/utilisateur";
import { getGroupes } from '../../actions/home'
import { Grid, Typography, TableCell, TableRow, Table, TableHead, TableBody } from '@mui/material';
import UtilisateurRow from "./UtilisateurRow";
import Utilisateur from "./Utilisateur";
import SelectPersonne from "../Commun/Bouton/SelectPersonne";


const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    rootPaper: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(2),
    },
    padding: {
        padding: `0 ${theme.spacing(2)}`,
    },
    chip: {
        margin: theme.spacing(1),
    },
    card: {
        minHeight: 75,
        marginTop: theme.spacing(20),
    },
    paper: {
        padding: theme.spacing(3, 2),
        margin: theme.spacing(1),
        width: '100%',
        textAlign: 'left'
    }
});


class UtilisateursListe extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addUtilisateur: false
        };
    }

    componentDidMount = async () => {
        if (this.props.acteurId) await this.props.getUtilisateursByActeur(this.props.acteurId);
        this.props.getGroupes();
    };

    componentDidUpdate(prevProps) {
        if (this.props.acteurId !== prevProps.acteurId) this.props.getUtilisateursByActeur(this.props.acteurId);
    }

    handleAddUtilisateur = () => {
        this.setState({ addUtilisateur: !this.state.addUtilisateur })
    }

    handleClick = async (data) => {
        const res = await this.props.setUtilisateur(data);
        if (res.success) {
            this.setState({ utilisateur: false, addUtilisateur: false });
            this.props.getUtilisateursByActeur(this.props.acteurId);
        }
    }

    handleClickUtilisateur = (utilisateur) => {
        this.setState({ utilisateur })
    }

    handleCancelEdit = () => {
        this.setState({ utilisateur: false, addUtilisateur: false });
    }

    render() {
        const { utilisateurs, groupes, organisation } = this.props;
        const { utilisateur, addUtilisateur } = this.state;
        const utilisateursRender = utilisateurs ? Object.keys(utilisateurs).map(key => {
            return <UtilisateurRow
                key={utilisateurs[key]._id}
                utilisateur={utilisateurs[key]}
                handleClick={this.handleClickUtilisateur}
            />
        }) : [];

        return (
            <Grid container key={"UtilisateursListe"}>
                <Grid container item xs={12} justifyContent="space-between" alignItems="center">
                    <Grid item >
                        <Typography>Liste des utilisateurs</Typography>
                    </Grid>
                    <Grid item >
                        <SelectPersonne
                            action={this.handleAddUtilisateur}
                            texte={"Ajouter un utilisateur"}
                            disable={false}
                        />
                    </Grid>
                </Grid>
                {utilisateur &&
                    <Utilisateur
                        utilisateur={utilisateur}
                        groupes={groupes}
                        handleClick={this.handleClick}
                        handleCancel={this.handleCancelEdit}
                        open={!!utilisateur}
                    />
                }
                {addUtilisateur &&
                    <Utilisateur
                        organisation={organisation}
                        groupes={groupes}
                        handleClick={this.handleClick}
                        handleCancel={this.handleCancelEdit}
                        open={addUtilisateur}
                    />
                }

                <Grid item xs={12}>
                    <Table>
                        <TableHead>
                            <TableRow>
                                <TableCell>Email</TableCell>
                                <TableCell>Nom</TableCell>
                                <TableCell>Prénom</TableCell>
                                <TableCell>Groupe</TableCell>
                                <TableCell>Libelle</TableCell>
                                <TableCell>Actif</TableCell>
                                <TableCell>Action</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {utilisateursRender}
                        </TableBody>
                    </Table>
                </Grid>
            </Grid>
        );
    }

}

function mapStateToProps(state) {
    return {
        utilisateurs: state.utilisateurs,
        groupes: state.groupes
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getUtilisateursByActeur: (id) => dispatch(getUtilisateursByActeur(id)),
        setUtilisateur: (data) => dispatch(setUtilisateur(data)),
        getGroupes: () => dispatch(getGroupes())
    }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(UtilisateursListe));