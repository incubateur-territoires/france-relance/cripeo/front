import React, { Component } from "react";
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { deleteUtilisateur } from "../../actions/utilisateur"
import { Grid, DialogActions, DialogContent, DialogContentText, TextField, Switch, FormControlLabel, Autocomplete } from "@mui/material";
import UtilisateurSearch from "./UtilisateurSearch";
import MonParametrage from "../Screens/MonParametrage";
import { ButtonText, Prompt, SelectDefault } from '../composants_internes';
import { Delete } from '@mui/icons-material';

const styles = theme => ({
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  groupes: {
    marginBottom: 15
  }
});

class Utilisateur extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.utilisateur && props.utilisateur._id ? props.utilisateur._id : "",
      login: props.utilisateur && props.utilisateur.login ? props.utilisateur.login : "",
      k_userId: props.utilisateur && props.utilisateur.k_userId ? props.utilisateur.k_userId : "",
      nom: props.utilisateur && props.utilisateur.nom ? props.utilisateur.nom : "",
      prenom: props.utilisateur && props.utilisateur.prenom ? props.utilisateur.prenom : "",
      libelle: props.utilisateur && props.utilisateur.libelle ? props.utilisateur.libelle : "",
      email: props.utilisateur && props.utilisateur.email ? props.utilisateur.email : "",
      groupe: props.utilisateur && props.utilisateur.groupe ? props.utilisateur.groupe : "Utilisateur",
      k_groupId: props.utilisateur && props.utilisateur.k_groupId ? props.utilisateur.k_groupId : null,
      disable: !!(props.utilisateur && props.utilisateur._id),
      actif: props.utilisateur && props.utilisateur._id ? props.utilisateur.actif : true,
      organisation: props.utilisateur && props.utilisateur.organisation ? props.utilisateur.organisation : props.organisation,
      equipes: props.utilisateur && props.utilisateur.equipes ? props.utilisateur.equipes : [],
      creation: false,
      openSupprimer: false,
      openFiltrePerso: false,
    };
  }

  componentDidMount() {
    if (!this.state.k_groupId) {
      const groupe = this.props.groupes?.find(item => item.name === "Utilisateur")
      this.setState({ k_groupId: groupe?.id })
    }
  }

  handleClick = () => {
    const {
      nom,
      prenom,
      email,
      actif,
      groupe,
      organisation,
      k_userId,
      k_groupId,
      id,
      equipes,
      libelle
    } = this.state;
    const login = this.props.utilisateur && this.props.utilisateur.login ? this.props.utilisateur.login : "";

    if (id) {
      this.props.handleClick({ login, k_groupId, k_userId, actif, libelle, _id: id, groupe, email, organisation, equipes });
      return;
    }
    const lib = nom + " " + prenom;
    let data = {
      login,
      k_userId,
      nom,
      prenom,
      email,
      actif,
      groupe,
      k_groupId,
      organisation: { acteur_id: organisation },
      equipes,
      libelle: lib
    };
    this.props.handleClick(data);
  }

  selectUtilisateur = (data) => {
    this.setState({ selectUtilisateur: data })
    this.setState({
      login: data.username,
      k_userId: data.id,
      nom: data.lastName,
      prenom: data.firstName,
      email: data.email,
    });
  }

  setUtilisateur = (data) => {
    this.setState({ selectUtilisateur: data })
    this.setState({
      login: data.login,
      nom: data.nom,
      prenom: data.prenom,
      email: data.email,
    });
  }

  handleCancel = (event) => {
    event.stopPropagation();
    this.props.handleCancel();
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value })
  }

  handleSwitch = (event) => {
    event.stopPropagation();
    this.setState({ [event.target.name]: event.target.checked })
  }

  handleSelect = async (role) => {
    if (this.props.groupes && this.props.groupes.length > 0) {
      for (let i = 0; i < this.props.groupes.length; i++) {
        if (this.props.groupes[i].name === role) {
          await this.setState({
            k_groupId: this.props.groupes[i].id,
            groupe: role
          })
        }
      }
    }
  }

  deleteUtilisateur = () => {
    this.setState({ openSupprimer: true });
  }

  handleChangeEquipe = async (event, value) => {
    event.stopPropagation();
    let equipes = value.map(equipe => {
      return { acteur_id: equipe._id }
    });
    this.setState({ equipes });
  }

  handleCloseConfirmSupprimer = () => {
    this.setState({ openSupprimer: false });
  }

  submitSuppresion = async () => {
    await this.props.deleteUtilisateur(this.props.utilisateur._id, this.props.utilisateur.organisation.acteur_id);
    this.handleCloseConfirmSupprimer();
    this.props.handleCancel();
  }

  handleOpenFiltrePerso = () => {
    this.setState({ openFiltrePerso: true });
  }

  handleCloseFiltrePerso = () => {
    this.setState({ openFiltrePerso: false });
  }

  render() {
    const { classes, groupes, open, handleCancel, equipes, utilisateur, keycloak, masks, roles, auth } = this.props;
    const { actif, groupe, id, k_userId, openSupprimer, openFiltrePerso } = this.state;
    const groupeSelectDefault = groupes?.map(role => { return { label: role.name, value: role.name } });

    let equipe = [];
    if (utilisateur && utilisateur.equipes && utilisateur.equipes.length > 0) {
      for (let i = 0; i < equipes.length; i++) {
        for (let j = 0; j < utilisateur.equipes.length; j++) {
          if (utilisateur.equipes[j].acteur_id === equipes[i]._id) {
            equipe.push(equipes[i]);
          }
        }
      }
    }

    let administrateurOrg;
    let administrateurFonc;
    let utilisateurIsFonc;
    let isProtected;
    if (utilisateur && keycloak && keycloak.tokenParsed && keycloak.tokenParsed.realm_access && keycloak.tokenParsed.realm_access.roles) {
      administrateurOrg = roles ? keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.GESTION_ORGA) !== -1 : false;
      administrateurFonc = roles ? keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.LISTE_ORGA) !== -1 : false;
      utilisateurIsFonc = masks ? utilisateur.groupe === masks.ADMIN_FONCTIONNEL : false;
      isProtected = (administrateurOrg && !administrateurFonc && utilisateurIsFonc);
    } else {
      isProtected = false;
    }

    let hasPermi = false;
    if (utilisateur && utilisateur !== undefined && utilisateur?.login !== auth.username) {
      if (auth.groupe === "Administrateur Fonctionnel") {
        hasPermi = true;
      } else if (auth.groupe === "Administrateur Organisationnel" &&
        utilisateur.groupe !== "Administrateur Fonctionnel" &&
        auth.organisation.acteur_id.toString() === utilisateur.organisation.acteur_id.toString()) {
        hasPermi = true;
      }
    }

    // désactiver la button de valider, s'il n'y a pas des changement
    let disableButton = true;
    if (utilisateur) {
      if (utilisateur.actif !== actif || utilisateur.groupe !== groupe || utilisateur.equipes !== this.state.equipes) {
        disableButton = false;
      }
    } else {
      if (this.state.email !== "" && this.state.nom !== "" && this.state.prenom !== "") {
        disableButton = false;
      }
    }

    return (
      <Prompt
        titre={id && id.length > 0 ? "Modification d'un utilisateur" : "Ajout d'un utilisateur"}
        open={open}
        onClose={handleCancel}
        aria-describedby="modal-description"
        fullWidth={true}
        maxWidth={"lg"}
        actions={
          <DialogActions sx={{ marginRight: 2 }}>
            <ButtonText
              action={handleCancel}
              color="info"
              texte="Annuler"
              variant="text"
            />
            <ButtonText
              action={this.handleClick}
              color="primary"
              texte="Valider"
              disable={disableButton}
            />
          </DialogActions>
        }
      >
        <DialogContent>
          <Grid item container alignItems="center" spacing={2} marginTop={(id && id.length > 0) ? 0 : 2}>
            {id && id.length > 0 && hasPermi &&
              <Grid container item justifyContent="flex-end" marginBottom={2}>
                <ButtonText
                  action={this.deleteUtilisateur}
                  color="error"
                  icon={<Delete />}
                  texte="Supprimer"
                  variant="contained"
                />
              </Grid>
            }
            <UtilisateurSearch
              selectUtilisateur={this.selectUtilisateur}
              setUtilisateur={this.setUtilisateur}
              disabled={(id && id.length > 0)}
              k_userId={k_userId}
            />
            <Grid container item className={classes.groupes}>
              <Grid item xs={6}>
                {!isProtected &&
                  <SelectDefault label="Groupe dans l'organisation" value={groupe} options={groupeSelectDefault} setValue={this.handleSelect} />
                }
              </Grid>
              <Grid container item xs={2} sx={{ alignItems: 'center' }}>
                <FormControlLabel
                  control={<Switch
                    checked={actif}
                    onChange={this.handleSwitch}
                    color="primary"
                    name="actif"
                    inputProps={{ 'aria-label': 'utilisateur actif checkbox' }}
                  />}
                  label="actif"
                  labelPlacement="start"
                  required={true}
                />
              </Grid>
              {utilisateur &&
                <Grid container item xs={4} sx={{ justifyContent: 'flex-end', alignItems: 'center' }}>
                  <ButtonText
                    action={this.handleOpenFiltrePerso}
                    color="info"
                    texte="filtre Personnelle d'utilisateur"
                    variant="outlined"
                  />
                </Grid>
              }
            </Grid>
            <Grid item xs={12}>
              <Autocomplete
                multiple
                id="tags-standard"
                options={equipes}
                getOptionLabel={(option) =>
                  option.entite
                }
                defaultValue={equipe}
                fullWidth
                filterSelectedOptions
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="standard"
                    label="Sélection des équipes"
                    placeholder="Equipes"
                  />
                )}
                onChange={this.handleChangeEquipe}
              />
            </Grid>
          </Grid>

          {utilisateur && openFiltrePerso &&
            <Prompt
              titre={" "}
              open={openFiltrePerso}
              onClose={this.handleCloseFiltrePerso}
              aria-describedby="modal-description"
              fullWidth={true}
              maxWidth={"lg"}
            >
              <DialogContent>
                <MonParametrage
                  user={utilisateur}
                  closeFiltrePerso={this.handleCloseFiltrePerso}
                />
              </DialogContent>
            </Prompt>
          }
          {openSupprimer &&
            <Prompt
              titre={"Suppresion d'un Utilisateur"}
              open={openSupprimer}
              keepMounted
              onClose={this.handleCloseConfirmSupprimer.bind(this)}
              actions={
                <DialogActions>
                  <ButtonText
                    action={this.handleCloseConfirmSupprimer.bind(this)}
                    color="info"
                    texte="Annuler"
                    variant="text"
                  />
                  <ButtonText
                    action={this.submitSuppresion}
                    color="primary"
                    texte="Valider"
                  />
                </DialogActions>
              }
            >
              <DialogContent>
                <DialogContentText id="Suppresion d'un Utilisateur">
                  Êtes-vous sûr(e) de suppression d'utilisateur ?
                </DialogContentText>
              </DialogContent>
            </Prompt>
          }
        </DialogContent>
      </Prompt>
    )
  }
}

function mapStateToProps(state) {
  return {
    keycloak: state.keycloak,
    roles: state.roles,
    masks: state.masks,
    auth: state.auth
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteUtilisateur: (idUtilisateur, idActeur) => dispatch(deleteUtilisateur(idUtilisateur, idActeur))
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Utilisateur));