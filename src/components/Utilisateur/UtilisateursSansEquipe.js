import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { getUtilisateursByActeur } from "../../actions/utilisateur";
import { Grid, List } from "@mui/material";
import UtilisateurSERow from "./UtilisateurSERow";
import { TH } from "../composants_internes";


export default function UtilisateursSansEquipe({ acteurId }) {
    // eslint-disable-next-line
    const [utilisateur, setUtilisateur] = useState(null);
    const utilisateurs = useSelector(state => state.utilisateurs)
    const dispatch = useDispatch();

    const cbGetUtilisateursByActeur = useCallback(
        (id) => dispatch(getUtilisateursByActeur(id))
        ,
        [dispatch]);

    useEffect(() => {
        cbGetUtilisateursByActeur(acteurId);
        // eslint-disable-next-line
    }, [])

    const handleClickUtilisateur = (newUtilisateur) => {
        setUtilisateur(newUtilisateur);
    }

    if (!utilisateurs || !Array.isArray(utilisateurs)) return <div />

    const utilisateursFiltered = utilisateurs?.filter((user) => user?.equipes?.length === 0 || user?.equipes === null);

    const utilisateursRender = utilisateursFiltered?.map(user => {
        return <UtilisateurSERow
            key={user._id}
            utilisateur={user}
            handleClick={handleClickUtilisateur}
        />
    });

    return (
        <Grid container key={"UtilisateursListe"}>
            <Grid container item xs={12} justifyContent="space-between" alignItems="center">
                <Grid item >
                    <TH variant="h6">Utilisateurs sans équipe</TH>
                </Grid>
            </Grid>
            <Grid container item>
                <List sx={{ width: '100%', maxWidth: 460, bgcolor: 'background.paper' }}
                    aria-label="utilisateurs">
                    {utilisateursRender}
                </List>
            </Grid>
        </Grid>
    )
}