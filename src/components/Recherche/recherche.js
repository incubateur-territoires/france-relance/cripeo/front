const concatRechercheChamps = (prevData, data, uniqueKey) => {
    return prevData.reduce((concatOut, value) => {
        if (value[uniqueKey].toString().toLowerCase().indexOf(prevData[uniqueKey]) === -1) {
            return concatOut.concat([value]);
        }
        return concatOut;
    }, data);
};

const multiRechercheChamps = (data, recherches, champs, noSpace, exceptions, importance) => {
    const filteredWords = [];
    const recherchesLower = recherches.map(recherche => sansAccent(recherche.toString()).toLowerCase());
    for (let i = 0; i < data.length; i++) {
        let bool = true;
        let score = 0;
        for (let j = 0; j < recherchesLower.length && bool; j++) {
            // eslint-disable-next-line no-loop-func
            bool = bool && champs.reduce((find, champ, index) => {
                const elem = parseChampToGetValue(champ, data && data[i] ? (Array.isArray(data[i]) ? data[i][0] : data[i]) : null);
                if (elem) {
                    if (!noSpace[index]) {
                        if (sansAccent(elem.toString()).toLowerCase().indexOf(recherchesLower[j]) !== -1)
                            score += importance[index] + (recherchesLower[j] === sansAccent(elem).toString().toLowerCase() ? 1 : 0);
                        return find || sansAccent(elem.toString()).toLowerCase().indexOf(recherchesLower[j]) !== -1;
                    }
                    if (sansAccent(elem.toString()).toLowerCase().replace(/\s/g, "").indexOf(recherchesLower[j]) !== -1) {
                        score += importance[index] + (recherchesLower[j].replace(/\s/g, "") === sansAccent(elem).toString().toLowerCase() ? 1 : 0);
                    }
                    return find || sansAccent(elem.toString()).toLowerCase().replace(/\s/g, "").indexOf(recherchesLower[j]) !== -1;
                }
                return find;
            }, false);
        }
        if (bool) {
            const returnValue = data[i];
            returnValue.score = score;
            filteredWords.push(returnValue);
        }
    }
    return filteredWords
};

const parseChampToGetValue = (champ, object) => {
    const sousAttr = champ.split(".");
    return sousAttr.reduce((tab, sAttr) => {
        if (tab && tab[sAttr])
            return tab[sAttr];
        return null;
    }, object);
};

const majRetourMultiRecherche = (retourObj, data, uniqueKey) => {
    if (data.length > 0 && retourObj.filtre) {
        const concatFiltre = concatRechercheChamps(data, retourObj.data, uniqueKey);
        return { data: concatFiltre, filtre: true };
    }
    if (data.length > 0) {
        return { data, filtre: true };
    }
    if (!retourObj.filtre) {
        return { data: [], filtre: true }
    }
    return retourObj;
};


const sansAccent = (str) => {
    const accent = [
        /[\300-\306]/g, /[\340-\346]/g, // A, a
        /[\310-\313]/g, /[\350-\353]/g, // E, e
        /[\314-\317]/g, /[\354-\357]/g, // I, i
        /[\322-\330]/g, /[\362-\370]/g, // O, o
        /[\331-\334]/g, /[\371-\374]/g, // U, u
        /[\321]/g, /[\361]/g, // N, n
        /[\307]/g, /[\347]/g, // C, c
    ];
    const noaccent = ['A', 'a', 'E', 'e', 'I', 'i', 'O', 'o', 'U', 'u', 'N', 'n', 'C', 'c'];

    for (let i = 0; i < accent.length; i++) {
        str = str.replace(accent[i], noaccent[i]);
    }

    return str;
};

module.exports = {
    concatRechercheChamps,
    multiRechercheChamps,
    parseChampToGetValue,
    majRetourMultiRecherche,
    sansAccent
};