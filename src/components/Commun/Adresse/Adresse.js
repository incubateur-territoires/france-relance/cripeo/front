import React, { Component } from 'react';
import { connect } from "react-redux";
import { TextValidator, ValidatorForm } from "react-material-ui-form-validator";
import { Grid, TextField, Paper, MenuItem } from "@mui/material";
import withStyles from '@mui/styles/withStyles';
import { addNotification } from "../../../actions/notifications";
import PropTypes from 'prop-types';
import axios from 'axios';
import Downshift from 'downshift';
import { updateAdresse } from "../../../actions/dossier";
import Editer from "../Bouton/Editer";
import Annuler from "../Bouton/Annuler";
import Valider from "../Bouton/Valider";

const styles = _theme => ({
  root: {
    display: 'flex',
    flexGrow: 1,
  },
  formControl: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    width: "100%"
  },
  group: {
    display: 'inline-table',
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
  },
  container: {
    flexGrow: 1,
    position: 'relative',
  },
  paper: {
    position: 'absolute',
    zIndex: 1,
    left: 0,
    right: 0,
  },
  chip: {},
  inputRoot: {
    flexWrap: 'wrap',
  },
  inputInput: {
    marginLeft: 0,
    marginRight: 0,
    marginTop: 0,
    marginBottom: 0,
    width: '100%',
  },
  divider: {},
  removeButton: {
    color: "#9d0a3a",
    border: "solid",
  },
  editButton: {
    color: "#2f8a20",
    border: "solid",
  },
  input: {
    color: 'rgb(33, 150, 243)'
  },
});

function renderSuggestion({ suggestion, index, itemProps, highlightedIndex, selectedItem, label, statename }) {
  const isHighlighted = highlightedIndex === index;
  const isSelected = (selectedItem || '').indexOf(suggestion.label) > -1;
  return (
    <MenuItem
      {...itemProps}
      key={suggestion.properties.id}
      item={JSON.stringify(suggestion)}
      statename={statename}
      selected={isHighlighted}
      component="div"
      style={{
        fontWeight: isSelected ? 500 : 400,
      }}
    >
      {label}
    </MenuItem>
  );
}

renderSuggestion.propTypes = {
  highlightedIndex: PropTypes.number,
  index: PropTypes.number,
  itemProps: PropTypes.object,
  selectedItem: PropTypes.string,
  suggestion: PropTypes.shape({ label: PropTypes.string }).isRequired,
};

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <TextField
      autoComplete="off"
      InputProps={{
        className: classes.input,
        autoComplete: "off",
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput,
        },
      }}
      InputLabelProps={{
        shrink: true,
      }}
      {...InputProps}
      {...other}
    />
  );
}

function renderInputValidator(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps;

  return (
    <TextValidator
      InputProps={{
        className: classes.input,
      }}
      {...InputProps}
      {...other}
    />
  );
}

const cpBano = async (cp) => {
  if (cp.length >= 3) {
    const url = 'https://api-adresse.data.gouv.fr/search/?q=' + cp + '&limit=10';
    return await callBano(url, "municipality");
  }
  return [];
};
const communeBano = async (commune) => {
  if (commune.length >= 3) {
    const url = 'https://api-adresse.data.gouv.fr/search/?q=' + commune + '&limit=5';
    return await callBano(url, "municipality");
  }
  return [];
};

const adresseBano = async (voie, cp) => {
  if (voie.length >= 3) {
    const url = 'https://api-adresse.data.gouv.fr/search/?q=' + voie + '&postcode=' + cp + '&limit=5';
    return await callBano(url, "all");
  }
  return [];
};

//Todo traiter le filtre des codes postaux, non fonctionnel
const callBano = async (url, filter) => {
  const instance = axios.create();
  const res = await instance.get(url);
  let result = [];
  if (res.data.features.length > 0) {
    result = res.data.features.filter((item) => {
      if (filter === 'all') {
        return true;
      } else if (item.properties.type === filter) {
        return true;
      }
      return false;
    });
  }
  return result;
};

class Adresse extends Component {
  _isMounted = false;
  choiceBano = async (choice, data) => {
    switch (choice) {
      case 'cp':
        this.setState({ res: await cpBano(data) });
        break;
      case 'commune':
        this.setState({ res: await communeBano(data) });
        break;
      case 'voie':
        this.setState({ res: await adresseBano(data, this.state.cp) });
        break;
      default:
        break;
    }
  };

  componentDidMount = async () => {
    this._isMounted = true;
    if (this.state.cp && this.state.commune && this.state.voie && this.state.coordonnees) {
      this.props.handleAdresse(this.state);
    } else if (this.state.cp && this.state.commune && this.state.voie) {
      const pos = await adresseBano(this.state.voie, this.state.cp);
      if (pos.length > 0) {
        this.props.handleAdresse(this.state);
      }
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
    this.setState = (state, callback) => {
      return;
    };
  }

  state = {
    cp: this.props.data && this.props.data.cp ? this.props.data.cp : '',
    commune: this.props.data && this.props.data.commune ? this.props.data.commune : '',
    voie: this.props.data && this.props.data.voie ? this.props.data.voie : '',
    lieudit: this.props.data && this.props.data.lieudit ? this.props.data.lieudit : '',
    complement: this.props.data && this.props.data.complement ? this.props.data.complement : '',
    coordonnees: this.props.data && this.props.data.coordonnees ? {
      lat: this.props.data && this.props.data.coordonnees.lat,
      lon: this.props.data && this.props.data.coordonnees.lon
    } : null,
    cpInitial: this.props.data && this.props.data.cp ? this.props.data.cp : '',
    communeInitial: this.props.data && this.props.data.commune ? this.props.data.commune : '',
    voieInitial: this.props.data && this.props.data.voie ? this.props.data.voie : '',
    lieuditInitial: this.props.data && this.props.data.lieudit ? this.props.data.lieudit : '',
    complementInitial: this.props.data && this.props.data.complement ? this.props.data.complement : '',
    coordonneesInitial: this.props.data && this.props.data.coordonnees ? {
      lat: this.props.data && this.props.data.coordonnees.lat,
      lon: this.props.data && this.props.data.coordonnees.lon
    } : null,
    res: [],
    modeEdition: false,
  };

  handleEvent = async (event) => {
    event.stopPropagation();
    event.persist();
    if (event.target && event.target.name) {
      await this.setState({ [event.target.name]: event.target.value });
      await this.choiceBano(event.target.name, event.target.value);
      this.props.handleAdresse(this.state);
    }
  };

  handleClick = async (event) => {
    event.stopPropagation();
    const obj = JSON.parse(event.target.getAttribute("item"));
    switch (event.target.getAttribute("statename")) {
      case "cp":
        await this.setState({ cp: obj.properties.postcode });
        await this.setState({ commune: obj.properties.city });
        this.props.handleAdresse(this.state);
        break;
      case "commune":
        await this.setState({ commune: obj.properties.city });
        await this.setState({ cp: obj.properties.postcode });
        this.props.handleAdresse(this.state);
        break;
      case "voie":
        await this.setState({
          voie: obj.properties.name,
          lieudit: obj.properties.oldcity ? obj.properties.oldcity : ''
        });
        await this.setState({
          coordonnees: {
            lon: obj.geometry.coordinates[0],
            lat: obj.geometry.coordinates[1]
          }
        });
        this.props.handleAdresse(this.state);
        break;
      default:
        break;
    }

  };

  componentDidUpdate = async (prevProps, prevState) => {
    if ((!prevProps.data && this.props.data) || (prevProps.data !== this.props.data &&
      (
        (this.props.data && prevProps.data && this.props.data.cp && prevProps.data.cp && prevProps.data.cp !== this.props.data.cp)
        ||
        (this.props.data && prevProps.data && this.props.data.commune && prevProps.data.commune && prevProps.data.commune !== this.props.data.commune)
        ||
        (this.props.data && prevProps.data && this.props.data.voie && prevProps.data.voie && prevProps.data.voie !== this.props.data.voie)
        ||
        (!this.props.data)
      )
    )
    ) {
      await this.setState({
        cp: this.props.data && this.props.data.cp ? this.props.data.cp : null,
        commune: this.props.data && this.props.data.commune ? this.props.data.commune : null,
        voie: this.props.data && this.props.data.voie ? this.props.data.voie : null,
        lieudit: this.props.data && this.props.data.lieudit ? this.props.data.lieudit : null,
        complement: this.props.data && this.props.data.complement ? this.props.data.complement : null,
        coordonnees: this.props.data && this.props.data.coordonnees ? {
          lat: this.props.data && this.props.data.coordonnees.lat,
          lon: this.props.data && this.props.data.coordonnees.lon
        } : null,
      })

      if (this.state.cp && this.state.commune && this.state.voie && this.state.coordonnees) {
        this.props.handleAdresse(this.state);
      } else if (this.state.cp && this.state.commune && this.state.voie) {
        const pos = await adresseBano(this.state.voie, this.state.cp);
        if (pos.length > 0) {
          this.props.handleAdresse(this.state);
        }
      }
    }
  };

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({
      modeEdition: !this.state.modeEdition
    });
  }

  cancelEdit = async () => {
    await this.setState({
      cp: this.state.cpInitial,
      commune: this.state.communeInitial,
      voie: this.state.voieInitial,
      lieudit: this.state.lieuditInitial,
      complement: this.state.complementInitial,
      coordonnees: this.state.coordonneesInitial ? {
        lat: this.state.coordonneesInitial.lat,
        lon: this.state.props.data.coordonneesInitial.lon
      } : null,
    })
    await this.setState({ modeEdition: false })
  }

  updateinfosAdresse = async () => {

    let obj = {
      id: this.props.dossier._id,
      adresse: {
        voie: (this.state.voie) ? this.state.voie : " ",
        cp: (this.state.cp) ? this.state.cp.toString() : " ",
        commune: (this.state.commune) ? this.state.commune : " ",
        complement: (this.state.complement) ? this.state.complement : " ",
        lieudit: (this.state.lieudit) ? this.state.lieudit : " ",
      }
    };

    try {
      const traitement = await this.props.updateAdresse(obj);
      if (traitement) {
        this.props.addNotification({ variant: "success", message: "La modification s'est bien déroulée" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la modification du dossier",
          details: traitement.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification du dossier" });
    }
    await this.setState({ modeEdition: false })
  };


  render = () => {
    const { classes } = this.props;
    return (
      <Grid container item xs={12}>
        <Grid container item xs={11}>
          <ValidatorForm component="fieldset" className={classes.formControl}
            ref="form"
            onSubmit={this.updateinfosAdresse.bind(this)}
            onError={errors => console.log(errors)}
          >
            <Grid container >
              <Grid container item xs={8}>
                <Grid container item spacing={2}>
                  <Grid item xs={12}>
                    <Downshift>
                      {({
                        getInputProps,
                        getItemProps,
                        getMenuProps,
                        highlightedIndex,
                        inputValue,
                        isOpen,
                        selectedItem,
                      }) => (
                        <div className={classes.container} style={{}}>
                          {renderInput({
                            fullWidth: true,
                            classes,
                            InputProps: getInputProps({
                              id: "downshift-voie",
                              disabled: !this.state.modeEdition,
                              name: "voie",
                              label: "Rue",
                              placeholder: 'Rue',
                              value: (this.state.voie) ? this.state.voie.toUpperCase() : "",
                              onChange: this.handleEvent
                            }),
                          })}
                          <div {...getMenuProps()}>
                            {isOpen ? (
                              <Paper className={classes.paper} square>
                                {this.state.res.map((suggestion, index) =>
                                  renderSuggestion({
                                    suggestion,
                                    index,
                                    itemProps: getItemProps({
                                      item: suggestion.properties.label,
                                      onClick: this.handleClick
                                    }),
                                    highlightedIndex,
                                    selectedItem,
                                    statename: "voie",
                                    label: suggestion.properties.oldcity ? suggestion.properties.label + '(' + suggestion.properties.oldcity + ')' : suggestion.properties.label,
                                  }),
                                )}
                              </Paper>
                            ) : null}
                          </div>
                        </div>
                      )}
                    </Downshift>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      id="lieudit"
                      label="Lieu Dit"
                      name="lieudit"
                      className={classes.textField}
                      value={this.state.lieudit ? this.state.lieudit.toUpperCase() : ''}
                      onChange={this.handleEvent}
                      margin="normal"
                      fullWidth
                      disabled={!this.state.modeEdition}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      InputProps={{
                        className: classes.input,
                      }}
                    />
                  </Grid>
                  <Grid item xs={4}>
                    <Downshift>
                      {({
                        getInputProps,
                        getItemProps,
                        getMenuProps,
                        highlightedIndex,
                        isOpen,
                        selectedItem,
                      }) => (
                        <div className={classes.container} style={{ margin: 0 }}>
                          {renderInputValidator({
                            fullWidth: true,
                            InputLabelProps: { "shrink": true },
                            classes,
                            InputProps: getInputProps({
                              disabled: !this.state.modeEdition,
                              autoComplete: "off",
                              placeholder: 'Code postal',
                              id: "downshift-cp",
                              label: "Code Postal",
                              name: "cp",
                              value: this.state.cp || "",
                              onChange: this.handleEvent,
                              validators:
                                [
                                  'isNumber',
                                  'minStringLength:5',
                                  'maxStringLength:5'
                                ],
                              errorMessages: [
                                'Doit être numérique',
                                'Le code postal doit être de 5 chiffres',
                                'Le code postal doit être de 5 chiffres'
                              ],

                            }),
                          }
                          )}
                          <div {...getMenuProps()}>
                            {isOpen ? (
                              <Paper className={classes.paper} style={{ width: 400 }} square>
                                {this.state.res.map((suggestion, index) =>
                                  renderSuggestion({
                                    suggestion,
                                    index,
                                    itemProps: getItemProps({
                                      item: suggestion.properties.postcode,
                                      onClick: this.handleClick

                                    }),
                                    highlightedIndex,
                                    selectedItem,
                                    statename: "cp",
                                    label: suggestion.properties.label + " " + suggestion.properties.postcode,
                                  }),
                                )}
                              </Paper>
                            ) : null}
                          </div>
                        </div>
                      )}
                    </Downshift>
                  </Grid>
                  <Grid item xs={8}>
                    <Downshift>
                      {({
                        getInputProps,
                        getItemProps,
                        getMenuProps,
                        highlightedIndex,
                        isOpen,
                        selectedItem,
                      }) => (
                        <div className={classes.container}>
                          {renderInput({
                            fullWidth: true,
                            classes,
                            InputProps: getInputProps({
                              disabled: !this.state.modeEdition,
                              id: "downshift-commune",
                              name: "commune",
                              label: "Ville",
                              placeholder: 'Ville',
                              value: this.state.commune ? this.state.commune.toUpperCase() : "",
                              onChange: this.handleEvent
                            }),
                          })}
                          <div {...getMenuProps()}>
                            {isOpen ? (
                              <Paper className={classes.paper} style={{ width: 400 }} square>
                                {this.state.res.map((suggestion, index) =>
                                  renderSuggestion({
                                    suggestion,
                                    index,
                                    itemProps: getItemProps({
                                      item: suggestion.properties.label,
                                      onClick: this.handleClick
                                    }),
                                    highlightedIndex,
                                    selectedItem,
                                    statename: "commune",
                                    label: suggestion.properties.label + " " + suggestion.properties.postcode,
                                  }),
                                )}
                              </Paper>
                            ) : null}
                          </div>
                        </div>
                      )}
                    </Downshift>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      id="complement"
                      label="Complément Adresse"
                      name="complement"
                      className={classes.textField}
                      value={this.state.complement ? this.state.complement.toUpperCase() : ''}
                      onChange={this.handleEvent}
                      margin="normal"
                      fullWidth
                      disabled={!this.state.modeEdition}
                      InputLabelProps={{
                        shrink: true,
                      }}
                      InputProps={{
                        className: classes.input,
                      }}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </ValidatorForm>
        </Grid>
        <Grid container item xs={1} style={{ justifyContent: "flex-end" }}>
          <Grid item xs={6}>
            <Editer action={this.changeEdition} texte={"Editer les informations"} disable={this.state.modeEdition} />
          </Grid>
          <Grid item xs={6}></Grid>
          <Grid item xs={6}>
            {this.state.modeEdition &&
              <Annuler texte="Annuler" action={this.cancelEdit.bind(this)} />
            }
          </Grid>
          <Grid item xs={6}>
            {this.state.modeEdition &&
              <Valider texte="Valider" action={this.updateinfosAdresse.bind(this)} />
            }
          </Grid>
        </Grid>
      </Grid>
    )
  }

}

function mapStateToProps(state) {
  return {
    dossier: state.dossier,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    updateAdresse: (obj) => dispatch(updateAdresse(obj)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Adresse))
