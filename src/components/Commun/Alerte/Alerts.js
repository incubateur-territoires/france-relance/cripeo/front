import React, { Component } from 'react';
import Alert from './Alert';

import { connect } from "react-redux";

class Alerts extends Component {

    state = {
        notifications: []
    };

    UNSAFE_componentWillUpdate(prevProps, prevState) {
        if (prevProps !== this.props) {
            this.setState({ notifications: this.props.notifications });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps !== this.props) {
            this.setState({ notifications: this.props.notifications });
        }
    }

    render() {
        const notifications = Object
            .keys(this.state.notifications)
            .map(key => <Alert key={key}
                variant={this.state.notifications[key].variant}
                message={this.state.notifications[key].message}
                details={this.state.notifications[key].details || false}
            />);

        return (
            <div>
                {notifications}
            </div>
        )
    }
}

//gestion des données
function mapStateToProps(state) {

    return {
        notifications: state.notifications,
    };
}


export default connect(mapStateToProps)(Alerts)
