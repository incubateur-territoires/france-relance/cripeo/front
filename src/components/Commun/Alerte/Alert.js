import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import classNames from 'classnames';
import { Snackbar, SnackbarContent, Portal } from '@mui/material';
import { Warning, Error, Info, CheckCircle } from '@mui/icons-material';
import { withRouter } from "react-router-dom";
import { removeNotification } from "../../../actions/notifications";
import { connect } from "react-redux";
import { green, amber } from '@mui/material/colors';


const variantIcon = {
    success: CheckCircle,
    warning: Warning,
    error: Error,
    info: Info,
};

const styles1 = theme => ({
    onTop: {
        zIndex: 1350
    },
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.dark,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    blocMessage: {
        display: 'flex',
        flexFlow: "column wrap"
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    details: {
        lineHeight: 0.74
    },
    detail: {
        marginTop: 15,
        lineHeight: 1.3
    }
});

class Alert extends Component {
    state = { open: true };

    handleClose = (reason) => {
        if (reason === 'clickaway') {
            return;
        }
        this.setState({ open: false });
        this.props.removeNotification();
    };

    render() {
        const { classes, className, message, variant, details } = this.props;
        const Icon = variantIcon[variant];
        return (
            <Portal>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'right',
                    }}
                    className={classes.onTop}
                    open={this.state.open}
                    autoHideDuration={5000}
                    onClose={this.handleClose}
                    ContentProps={{
                        'aria-describedby': 'message-id',
                    }}
                >
                    <SnackbarContent
                        className={classNames(classes[variant], className)}
                        aria-describedby="client-snackbar"
                        message={
                            <div id="client-snackbar" className={classes.blocMessage}>
                                <div className={classNames(classes.message)}>
                                    <Icon className={classNames(classes.icon, classes.iconVariant)} />
                                    {message}
                                </div>
                                {details && Array.isArray(details) && (
                                    <div className={classNames(classes.details)}>
                                        {
                                            details.map((detail, key) =>
                                                <p className={classNames(classes.detail)} key={key}>{detail}</p>
                                            )
                                        }
                                    </div>
                                )}
                            </div>
                        }
                    />
                </Snackbar>
            </Portal>
        )
    }
}

Alert.propTypes = {
    classes: PropTypes.object.isRequired,
    className: PropTypes.string,
    message: PropTypes.node,
    onClose: PropTypes.func,
    variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeNotification: () => dispatch(removeNotification()),
    }
};

export default withStyles(styles1)(connect(null, mapDispatchToProps)(withRouter(Alert)));
