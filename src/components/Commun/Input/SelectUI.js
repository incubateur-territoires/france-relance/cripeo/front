import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { FormControl, InputLabel, Select, MenuItem } from '@mui/material';


const useStyles = makeStyles(theme => ({
  selectEmpty: {
  },
  input: {
    color: 'rgb(33, 150, 243)',
    "&.Mui-disabled": {
      color: "#000000",
    },
  },
  label: {
    color: 'rgb(33, 150, 243)',
    "&.Mui-disabled": {
      color: "#000000",
    },
  }
}));

export default function SelectUI({ value, label, name, handleChange, items, disabled, required }) {
  const classes = useStyles();

  return (
    <FormControl required={required} fullWidth className={classes.formControl} disabled={disabled}>
      <InputLabel id={`${name}-select`} className={classes.label}>{label}</InputLabel>
      <Select
        labelId={`${name}-select`}
        inputProps={{
          name,
          id: name,
          className: classes.input,
        }}
        margin="none"
        fullWidth
        value={value}
        onChange={handleChange}
        className={classes.selectEmpty}
      >
        <MenuItem value="">
          <em></em>
        </MenuItem>
        {items}
      </Select>
    </FormControl>
  )
}




