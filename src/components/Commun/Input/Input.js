import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { TextField } from '@mui/material';

const useStyles = makeStyles(theme => ({
  textField: {
    minWidth: 120,
  },
  input: {
    color: 'rgb(33, 150, 243)',
    "&.MuiInputBase-root.Mui-disabled": {
      color: "#a8a8a8"
    }
  },

}));

export default function Input({ value, label, name, handleChange, required, disabled, multiline, placeholder, type, min, max, step }) {
  const classes = useStyles();

  return (
    <TextField
      id={name}
      label={label}
      name={name}
      required={!!required}
      className={classes.textField}
      value={value || ""}
      onChange={handleChange}
      margin="none"
      fullWidth
      disabled={disabled}
      placeholder={placeholder}
      type={type ? type : 'text'}
      InputLabelProps={{
        shrink: true,
        className: classes.input,
      }}
      maxRows={max}
      minRows={min}
      InputProps={{
        inputProps: {
          className: classes.input,
          min: min,
          max: max
        }
      }}
      multiline={multiline}
    />
  )
}
