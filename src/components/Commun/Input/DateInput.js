import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { TextField } from '@mui/material';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import {LocalizationProvider} from '@mui/x-date-pickers';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';

const useStyles = makeStyles(theme => ({
  textField: {
    minWidth: 120,
  },
  input: {
    color: 'rgb(33, 150, 243)'
  },
}));

export default function DateInput({ value, label, name, handleChange, disabled }) {
  const classes = useStyles();

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <DatePicker
        label={label}
        value={value}
        initialFocusedDate={new Date()}
        autoOk
        className={classes.textField}
        name={name}
        onChange={(date) => {
          handleChange({ target: { name, value: date } })
        }}
        fullWidth
        animateYearScrolling
        format="dd/MM/yyyy"
        clearLabel="vider"
        cancelLabel="annuler"
        clearable
        disabled={disabled}
        InputProps={{
          className: classes.input,
        }}
        renderInput={(params) => <TextField {...params} />}
        InputLabelProps={{
          shrink: true,
          className: classes.input,
        }}
      />
    </LocalizationProvider>
  )
}
















