import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import { blue } from "@mui/material/colors";
import { IconButton } from "@mui/material";

export default function NonVisible({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: blue[500] }}
            component="span"
            size="large"
        >
            <VisibilityOffIcon style={{ fontSize: 30 }} />
        </IconButton>
    );

}