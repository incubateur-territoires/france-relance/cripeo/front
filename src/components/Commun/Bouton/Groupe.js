import GroupIcon from '@mui/icons-material/Group';
import { blue, grey } from "@mui/material/colors";
import { Button } from "@mui/material";

export default function Groupe({
  action,
  texte,
  disable,
  actif
}) {
  let style = actif ? { color: blue[500] } : { color: grey[500] };
  return (
    <Button
      onClick={action}
      title={texte}
      disabled={disable}
      style={style}
      component="span"
      endIcon={<GroupIcon style={{ fontSize: 30 }} />}
    >
      Equipes
    </Button>
  )

}