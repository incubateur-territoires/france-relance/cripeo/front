import LockIcon from '@mui/icons-material/Lock';
import { IconButton } from "@mui/material";
import { blue } from "@mui/material/colors";


export default function Droit({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: blue[500] }}
            component="span"
            size="large">
            <LockIcon style={{ fontSize: 30 }} />

        </IconButton>
    );


}