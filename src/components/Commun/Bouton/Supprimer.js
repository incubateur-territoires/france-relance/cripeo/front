import { IconButton } from "@mui/material";
import { red } from "@mui/material/colors";
import DeleteIcon from "@mui/icons-material/Delete";


export default function Supprimer({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: red[500] }}
            component="span"
            size="large"
        >
            <DeleteIcon style={{ fontSize: 30 }} />

        </IconButton>
    );


}