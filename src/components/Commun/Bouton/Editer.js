import EditIcon from '@mui/icons-material/Edit';
import { IconButton } from "@mui/material";
import { blue } from "@mui/material/colors";


export default function Editer({
  action,
  texte,
  disable,
  size
}) {

  let style = size ? { fontSize: size } : { fontSize: 30 };
  return (
    <IconButton
      onClick={action}
      title={texte}
      disabled={disable}
      style={{ color: blue[500] }}
      component="span"
      size="large"
    >
      <EditIcon style={style} />

    </IconButton>
  );
}