import AddCircleIcon from '@mui/icons-material/AddCircle';
import { blue } from "@mui/material/colors";
import { IconButton } from "@mui/material";

export default function Ajouter({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: blue[500] }}
            component="span"
            size="large">
            <AddCircleIcon style={{ fontSize: 30 }} />
        </IconButton>
    );

}