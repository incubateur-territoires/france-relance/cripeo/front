import GroupAddIcon from '@mui/icons-material/GroupAdd';
import { blue } from "@mui/material/colors";
import { Button } from "@mui/material";

export default function GroupAdd({
    action,
    texte,
    disable,
    actif
}) {
    let style = { color: blue[500] };
    return (
        <Button
            onClick={action}
            variant="outlined"
            title={texte}
            style={style}
            component="span"
            endIcon={<GroupAddIcon style={{ fontSize: 30 }} />}>
            Ajout Equipe
        </Button>
    );

}