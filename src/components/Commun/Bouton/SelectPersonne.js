import PersonAddIcon from '@mui/icons-material/PersonAdd';
import { blue } from "@mui/material/colors";
import { Button } from "@mui/material";

export default function SelectPersonne({
    action,
    texte,
}) {
    let style = { color: blue[500] };
    return (
        <Button
            onClick={action}
            variant="outlined"
            title={texte}
            style={style}
            component="span"
            endIcon={<PersonAddIcon style={{ fontSize: 30 }} />}
        >
            Ajout Utilisateur
        </Button>
    );

}