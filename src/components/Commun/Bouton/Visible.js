import VisibilityIcon from '@mui/icons-material/Visibility';
import { blue } from "@mui/material/colors";
import { IconButton } from "@mui/material";

export default function Visible({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: blue[500] }}
            component="span"
            size="large"
        >
            <VisibilityIcon style={{ fontSize: 30 }} />
        </IconButton>
    );

}