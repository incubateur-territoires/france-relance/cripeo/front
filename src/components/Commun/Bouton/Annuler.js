import CancelIcon from '@mui/icons-material/Cancel';
import { IconButton } from "@mui/material";
import { red } from "@mui/material/colors";


export default function Annuler({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: red[500] }}
            component="span"
            size="large">
            <CancelIcon style={{ fontSize: 30 }} />

        </IconButton>
    );


}