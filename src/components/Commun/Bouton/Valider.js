import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { IconButton } from "@mui/material";

import { green, grey } from '@mui/material/colors';

export default function Valider({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: disable ? grey[400] : green[500] }}
            component="span"
            size="large"
        >
            <CheckCircleIcon style={{ fontSize: 30 }} />

        </IconButton>
    );

}