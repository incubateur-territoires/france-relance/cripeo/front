import Search from '@mui/icons-material/Search';
import { blue } from "@mui/material/colors";
import { IconButton } from "@mui/material";

export default function Recherche({
    action,
    texte,
    disable
}) {

    return (
        <IconButton
            onClick={action}
            title={texte}
            disabled={disable}
            style={{ color: blue[500] }}
            component="span"
            size="large"
        >
            <Search style={{ fontSize: 30 }} />
        </IconButton>
    );

}