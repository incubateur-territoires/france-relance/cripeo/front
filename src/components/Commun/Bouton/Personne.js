import PersonIcon from '@mui/icons-material/Person';
import { blue, grey } from "@mui/material/colors";
import { Button } from "@mui/material";

export default function Person({
  action,
  texte,
  disable,
  actif
}) {
  let style = actif ? { color: blue[500] } : { color: grey[500] };
  return (
    <Button
      onClick={action}
      title={texte}
      disabled={disable}
      style={style}
      component="span"
      endIcon={<PersonIcon style={{ fontSize: 30 }} />}
    >
      Utilisateurs
    </Button>
  )

}