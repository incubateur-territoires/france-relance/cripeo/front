import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@mui/styles';
import { DialogTitle, DialogContent, DialogContentText, DialogActions, Button, Dialog } from "@mui/material";

const styles = theme => ({});

class ConfirmDialog extends Component {

  render() {
    const { open, handleCloseFunction, valideFunction, question, titre } = this.props;
    return (
      <Dialog
        open={open}
        keepMounted
        onClose={handleCloseFunction}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {titre || "Suppression d'élément"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            {question}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleCloseFunction} color="secondary">
            Annuler
          </Button>
          <Button onClick={valideFunction}
            color="primary">
            Valider
          </Button>
        </DialogActions>
      </Dialog>
    )
  }
}

ConfirmDialog.propTypes = {
  classes: PropTypes.object.isRequired,
  question: PropTypes.string.isRequired,
  open: PropTypes.bool.isRequired,
  handleCloseFunction: PropTypes.func.isRequired,
  valideFunction: PropTypes.func.isRequired

};

export default withStyles(styles)(ConfirmDialog);
