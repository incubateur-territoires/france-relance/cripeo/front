import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Typography, Card, CardHeader, CardMedia, CardContent, CardActions } from '@mui/material';
import { grey } from '@mui/material/colors';

const backGroundColor = grey[100];
const fontHeadingColor = grey[800];
const fontColor = grey[700];

const useStyles = makeStyles(theme => ({
  card: {
    margin: 5,
    height: 200,
    transition: "0.3s",
    boxShadow: "0 8px 15px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 10px 25px -12.125px rgba(0,0,0,0.3)",
      cursor: "pointer",
      filter: "brightness(85%)"
    }
  },
  media: {
    paddingTop: "56.25%"
  },
  contentDefault: {
    textAlign: "left",
    padding: theme.spacing(3),
    color: fontColor
  },
  contentCenter: {
    textAlign: "lcentereft",
    padding: theme.spacing(3),
    color: fontColor
  },
  divider: {
    margin: `${theme.spacing(3)} 0`
  },
  heading: {
    fontWeight: 650,
    color: fontHeadingColor,
    overflow: "auto",
    maxHeight: "60px"
  },
  subheading: {
    lineHeight: 1.8
  },
}));

export default function CardComplexe({
  header,
  media,
  actions,
  children,
  handleClic,
  couleur,
  center,
  selected
}) {
  const classes = useStyles();
  let style = {
    backgroundColor: couleur ? backGroundColor : backGroundColor
  };

  if (selected) {
    style.boxShadow = "0 10px 25px -12.125px rgba(0,0,0,0.3)";
    style.cursor = "pointer";
    style.filter = "brightness(85%)";
  }

  return (
    <Card
      className={classes.card}
      style={style}
      onClick={handleClic}
    >
      {media &&
        <CardMedia
          className={classes.media || false}
          image={media.img || false}
          title={media.title || false}
        />
      }
      {header &&
        <CardHeader
          avatar={header.avatar || false}
          action={header.action || false}
          title={
            <Typography
              className={classes.heading}
              variant={"subtitle1"}
              gutterBottom
            >
              {header.titre || false}
            </Typography>
          }
          subheader={
            <Typography
              className={classes.heading}
              variant={"subtitle2"}
              gutterBottom
            >
              {header.sousTitre || false}
            </Typography>
          }
        />
      }

      <CardContent className={center ? classes.contentCenter : classes.contentDefault}>
        {children}
      </CardContent>
      {actions &&
        <CardActions disableSpacing>
          {actions}
        </CardActions>
      }
    </Card>
  )
}
