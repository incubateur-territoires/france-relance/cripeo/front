import React from 'react';
import CardLibelle from './CardLibelle';

export default function CardSimple({
  titre,
  sousTitre,
  action,
  children,
  handleClic,
  couleur,
  center,
  noHoover,
  selected,
  actif
}) {
  return (
    <CardLibelle
      header={{ titre, sousTitre, actif }}
      handleClic={handleClic}
      couleur={couleur}
      center={center}
      noHoover={noHoover}
      selected={selected}
      action={action}
    >
      {children}
    </CardLibelle>

  )
}
