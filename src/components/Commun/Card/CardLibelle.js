import React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Grid, Card, CardHeader, CardMedia, CardContent, CardActions } from '@mui/material'
import { grey } from '@mui/material/colors';

const backGroundColor = grey[100];
const fontHeadingColor = grey[800];

const useStyles = makeStyles(theme => ({
  card: {
    margin: 5,
    transition: "0.3s",
    boxShadow: "0 8px 15px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 10px 25px -12.125px rgba(0,0,0,0.3)",
      filter: "brightness(85%)"
    }
  },
  media: {
    paddingTop: "56.25%"
  },
  contentDefault: {
    textAlign: "left",
  },
  contentCenter: {
    textAlign: "lcentereft",
  },
  divider: {
    margin: `${theme.spacing(3)} 0`
  },
  heading: {
    fontWeight: 650,
    color: fontHeadingColor,

  },
  subheading: {
    lineHeight: 1.8,
    fontWeight: 450,
    color: fontHeadingColor,

  },
  content: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between"
  },

}));

export default function CardLibelle({
  header,
  media,
  action,
  handleClic,
  center,
  selected
}) {
  const classes = useStyles();
  let style = {
    backgroundColor: header.actif ? backGroundColor : grey[400]
  };

  if (selected) {
    style.boxShadow = "0 10px 25px -12.125px rgba(0,0,0,0.3)";
    style.filter = "brightness(85%)";
  }

  return (
    <Grid item sx={{ display: 'flex', flex: 1, height: '95%', margin: " 10px 10px" }} >
      <Card sx={{
        flex: 1,
        display: 'flex', justifyContent: 'space-between', flexDirection: 'column', height: '100%'
      }}

        className={classes.card}
        style={style}
      >
        {media &&
          <CardMedia
            className={classes.media || false}
            image={media.img || false}
            title={media.title || false}
          />
        }
        <CardActions
          disableSpacing
        >
          {action}
        </CardActions>
        {header &&
          <CardHeader
            style={{ height: "100%", cursor: "pointer" }}
            classes={{ content: classes.content }}
            title={header.titre || false}
            subheader={header.sousTitre || false}
            titleTypographyProps={{ className: classes.heading, variant: "subtitle1", gutterBottom: true }}
            subheaderTypographyProps={{ className: classes.subheading, variant: "subtitle2" }}
            onClick={handleClic}
          />
        }
        <CardContent className={center ? classes.contentCenter : classes.contentDefault}
          sx={{ color: "red", padding: "8px", fontWeight: 800 }}
        >
          {header.actif ? "" : "Inactive"}
        </CardContent>
      </Card>
    </Grid>
  )
}
