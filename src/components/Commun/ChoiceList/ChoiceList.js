import React, { useEffect } from "react";
import makeStyles from '@mui/styles/makeStyles';
import { Grid, Typography, List, ListItem, ListItemIcon, ListItemText, Checkbox, Button, Paper } from "@mui/material";

const useStyles = makeStyles((theme) => ({
  root: {
    margin: 'auto',
  },
  paper: {
    width: 200,
    height: 230,
    overflow: 'auto',
  },
  button: {
    margin: theme.spacing(0.5, 0),
  },
}));

function not(a, b) {
  return a.filter((value) => b.indexOf(value) === -1);
}

function intersection(a, b) {
  return a.filter((value) => b.indexOf(value) !== -1);
}

export default function ChoiceList({ acteurs, echange, handleSubmit, categorie, edit = false, stepper = false }) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState([]);
  const [left, setLeft] = React.useState([]);
  const [right, setRight] = React.useState([]);

  React.useEffect(() => {
    if (acteurs) {
      let acteursReduced = acteurs.reduce((acc, acteur) => {
        if (echange) {
          if (acteur._id === echange.categorie.destinataire_id || acteur._id === echange.categorie.emetteur_id) {
            return acc;
          }

          if (echange.information.some((information) => information.acteur_id === acteur._id)) {
            acc.right.push(acteur)
          } else {
            acc.left.push(acteur)
          }
        } else if (categorie) {
          if (acteur._id === categorie.destinataire.id || acteur._id === categorie.emetteur.id) {
            return acc;
          } else if (categorie.information.length > 0 &&
            categorie.information.findIndex(info => info.acteur_id === acteur._id) !== -1) {
            acc.right.push(acteur)
          } else {
            acc.left.push(acteur)
          }
        }
        return acc;
      }, { left: [], right: [] })

      setLeft(acteursReduced.left);
      setRight(acteursReduced.right);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [acteurs, echange, categorie])

  const leftChecked = intersection(checked, left);
  const rightChecked = intersection(checked, right);

  useEffect(() => {
    if (stepper) {
      handleSubmit(left, right);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [left, right, stepper])

  const handleToggle = (acteur) => () => {
    const currentIndex = checked.findIndex((check => check._id === acteur._id));
    const newChecked = [...checked];

    if (currentIndex === -1) {
      newChecked.push(acteur);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setChecked(newChecked);
  };

  const handleCheckedRight = () => {
    setRight(right.concat(leftChecked));
    setLeft(not(left, leftChecked));
    setChecked(not(checked, leftChecked));
  };

  const handleCheckedLeft = () => {
    setLeft(left.concat(rightChecked));
    setRight(not(right, rightChecked));
    setChecked(not(checked, rightChecked));
  };

  const handleSubmitChoiceList = () => {
    handleSubmit(left, right);
  }

  const customList = (items) => (
    <Paper className={classes.paper}>
      <List dense component="div" role="list">
        {items.map((acteur) => {
          const labelId = `transfer-list-item-${acteur.entite}-label`;

          return (
            <ListItem key={acteur._id} role="listitem" button onClick={handleToggle(acteur)}>
              <ListItemIcon>
                <Checkbox
                  checked={checked.indexOf(acteur) !== -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText id={labelId} primary={`${acteur.entite}`} />
            </ListItem>
          );
        })}
        <ListItem />
      </List>
    </Paper>
  );



  return (
    <div >
      <Grid
        item
        container
        justifyContent="center"
        alignItems="center"
        className={classes.root}
      >
        <Grid container item xs={5} direction="column" justifyContent="center" alignItems="center">
          <Grid item container justifyContent="center">
            <Typography variant="h6">
              Non visible
            </Typography>
          </Grid>
          <Grid item>{customList(left)}</Grid>
        </Grid>
        <Grid item xs={2}>
          <Grid item container direction="column" alignItems="center">
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={handleCheckedRight}
              disabled={leftChecked.length === 0}
              aria-label="move selected right"
            >
              &gt;
            </Button>
            <Button
              variant="outlined"
              size="small"
              className={classes.button}
              onClick={handleCheckedLeft}
              disabled={rightChecked.length === 0}
              aria-label="move selected left"
            >
              &lt;
            </Button>
          </Grid>
        </Grid>
        <Grid container item xs={5} direction="column" justifyContent="center" alignItems="center" >
          <Grid item container justifyContent="center">
            <Typography variant="h6">
              En Information
            </Typography>
          </Grid>
          <Grid item>{customList(right)}</Grid>
        </Grid>
      </Grid>

      {!stepper &&
        <Grid item container justifyContent='flex-end' spacing={2} style={{ marginTop: 5 }}>
          {!edit && <Grid item>
            <Button variant="text" onClick={handleSubmitChoiceList} >
              Fermer
            </Button>
          </Grid>
          }
          <Grid item container={edit} justifyContent={edit ? "center" : "flex-start"}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleSubmitChoiceList}
            >
              {!edit ? "Terminer" : "Valider les changements"}
            </Button>
          </Grid>
        </Grid>
      }
    </div>
  );
}