import React, { Component } from 'react';
import withStyles from '@mui/styles/withStyles';
import 'react-pdf/dist/esm/Page/TextLayer.css';
import {
  Page,
  Document,
  pdfjs
} from 'react-pdf';
import { Folder, Add, Delete, Lock, Visibility } from '@mui/icons-material';
import {
  Tooltip, Grid, Button, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText,
  Avatar, IconButton, DialogActions, DialogContent, DialogContentText, CircularProgress
} from '@mui/material';
import DroitsFile from "./DroitsFile";
import { green, grey } from '@mui/material/colors';
import { Prompt } from "../../composants_internes";

const styles = theme => ({
  list: {
    marginLeft: 5,
    width: '100%',
  },
  item: {
    padding: 9,
    paddingRight: 38,
    display: "flex",
    justifyContent: "space-between",
    alignContent: 'space-between',
    backgroundColor: grey[50],
  },
  delete: {
    marginLeft: 10,
  },
  primaryAvatar: {
    color: '#fff',
    backgroundColor: theme.palette.primary.main,
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
  droits: {
    color: "#2F80ED"
  },
  visualisation: {
    width: '100%',
    height: '100%',
    justifyContent: "center"
  }
});


const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

class Uploader extends Component {

  state = {
    open: false,
    openDroits: false,
    fichier: null,
    uploading: false,
    openVisualisation: false,
    fileDataURL: "",
    typeFileDocument: "",
    numPages: null
  }

  update = (event) => {
    event.stopPropagation();
  }

  accessFile = (fichier) => async event => {
    event.stopPropagation();
    if (this.props.getFile) {
      const file = await this.props.getFile(fichier._id);
      const Filetype = fichier.extension;
      const mimType = Filetype === 'pdf' ? 'application/pdf' : Filetype === 'xlsx' ? 'application/xlsx' :
        Filetype === 'pptx' ? 'application/pptx' : Filetype === 'csv' ? 'application/csv' : Filetype === 'docx' ? 'application/docx' :
          Filetype === 'jpg' ? 'application/jpg' : Filetype === 'png' ? 'application/png' : '';

      const blob = b64toBlob(file, mimType);

      const fileUrl = await window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = fileUrl;
      link.download = `${fichier.titre}`;
      document.body.appendChild(link);
      link.click();
    }
  }

  // visualisation de document
  visualisationFile = (fichier) => async event => {
    event.stopPropagation();

    pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;
    const Filetype = fichier.extension;
    const mimType = Filetype === 'pdf' ? 'application/pdf' :
      Filetype === 'jpg' ? 'application/jpg' : Filetype === 'png' ? 'application/png' : '';

    if (this.props.getFile && (Filetype === 'pdf' || Filetype === 'jpg' || Filetype === 'png')) {
      const file = await this.props.getFile(fichier._id);
      const blob = b64toBlob(file, mimType);
      const fileUrl = await window.URL.createObjectURL(blob);
      this.setState({ typeFileDocument: Filetype, fileDataURL: fileUrl, openVisualisation: true });
    }
  }

  closeVisualisation = () => {
    this.setState({ openVisualisation: false });
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  }

  deletefile = id => event => {
    event.stopPropagation();
    this.props.delete(id);
    this.handleClose();
  }

  onSelect = async (event) => {
    await this.setState({ uploading: true });
    event.stopPropagation();
    const formData = new FormData();
    formData.append('filedata', event.target.files[0]); // appending file
    await this.props.upload(this.props.id, formData);
    await this.setState({ uploading: false });
  }

  openTraitementDialog = fichier => (event) => {
    event.stopPropagation();
    this.setState({ open: true, fichier });
  }

  openDroitsDialog = fichier => (event) => {
    event.stopPropagation();
    this.setState({ openDroits: true, fichier });
  }

  handleClose = () => {
    this.setState({ open: false, openDroits: false, fichier: null });
  }

  validDroit = (data) => {
    this.props.gestiondroits(this.state.fichier._id, data);
    this.handleClose();
  }

  render() {
    const { classes, documents, gestiondroits, clos, acteurId } = this.props;
    const { uploading, openDroits, openVisualisation, fileDataURL, typeFileDocument, numPages } = this.state;
    let fichiers = documents?.map(fichier => {
      let newDoc = { ...fichier };
      newDoc.acteurIsInformation = false;
      if (fichier.informationActeurs_id && fichier.informationActeurs_id.length > 0) {
        if (fichier.informationActeurs_id?.findIndex((element) => element.toString() === acteurId.toString()) !== -1) {
          newDoc.acteurIsInformation = true;
        }
      }
      if (fichier.mainActeurId.toString() !== acteurId.toString()) {
        newDoc.canDelete = false;
      }
      return newDoc;
    });

    const fichiersRender = fichiers ? Object
      .keys(fichiers)
      .map(key => (
        <ListItem className={classes.item} button key={key} sx={{ marginBottom: 1 }}>
          <ListItemAvatar onClick={this.accessFile(fichiers[key])}>
            <Avatar>
              <Folder />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={fichiers[key] ? fichiers[key]?.titre || "Nom du fichier" : "Nom du fichier"}
            onClick={this.accessFile(fichiers[key])}
          />
          <ListItemSecondaryAction className={classes.delete}>
            {(fichiers[key].extension === 'pdf' || fichiers[key].extension === 'jpg' || fichiers[key].extension === 'png') &&
              <Tooltip title='Visualisation de document'>
                <IconButton
                  className={classes.droits}
                  aria-label="Visualisation"
                  onClick={this.visualisationFile(fichiers[key])}
                  size="large">
                  <Visibility />
                </IconButton>
              </Tooltip>
            }
            {gestiondroits && !fichiers[key].acteurIsInformation &&
              <Tooltip title='Les droits sur le document'>
                <IconButton
                  className={classes.droits}
                  aria-label="Droits"
                  onClick={this.openDroitsDialog(fichiers[key])}
                  size="large">
                  <Lock />
                </IconButton>
              </Tooltip>
            }
            {!clos && !fichiers[key].acteurIsInformation && fichiers[key].canDelete &&
              <IconButton
                color="error"
                aria-label="Delete"
                onClick={this.openTraitementDialog(fichiers[key]?._id)}
                size="large">
                <Delete />
              </IconButton>
            }
            {openDroits && !fichiers[key].acteurIsInformation &&
              <DroitsFile
                key={'listedroitsfichier' + fichiers[key]?._id}
                open={openDroits}
                document={this.state.fichier}
                handleClose={this.handleClose}
                validedroits={this.validDroit.bind(this)}
              />
            }
          </ListItemSecondaryAction>
        </ListItem>
      )) : []

    const typeFile = `type-${this.props.type}`;

    return (
      <Grid container item height={'100%'}>
        <List dense className={classes.list}>
          {fichiersRender}
          {this.props.upload &&
            <label htmlFor={`addFile-${typeFile}-${this.props.id}`}>
              <ListItem
                className={classes.item}
                button
              >
                <ListItemAvatar>
                  <Avatar className={classes.primaryAvatar}>
                    <Add />
                    {uploading && <CircularProgress size={34} className={classes.fabProgress} />}
                  </Avatar>

                </ListItemAvatar>
                <ListItemText primary="Ajouter un document" />
                {this.props.upload && <input
                  id={`addFile-${typeFile}-${this.props.id}`}
                  type="file"
                  style={{ display: "none" }}
                  name="filedata"
                  onChange={this.onSelect}
                />
                }
              </ListItem>
            </label>}
        </List>

        <Prompt
          titre={"Suppression de document"}
          open={this.state.open}
          keepMounted
          onClose={this.handleClose}
          aria-describedby="alert-dialog-slide-description"
          actions={
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Annuler
              </Button>
              <Button onClick={this.deletefile(this.state.fichier)}
                color="primary">
                Valider
              </Button>
            </DialogActions>
          }
        >
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              Voulez-vous supprimer ce document ?
            </DialogContentText>
          </DialogContent>
        </Prompt>

        <Prompt
          titre={"visualisation de document"}
          open={openVisualisation}
          keepMounted
          onClose={this.closeVisualisation}
          maxWidth={"md"}
          aria-describedby="alert-dialog-slide-description"
          actions={
            <DialogActions>
              <Button onClick={this.closeVisualisation} color="primary">
                Fermer
              </Button>
            </DialogActions>
          }
        >
          <DialogContent className={classes.visualisation} >
            {typeFileDocument === 'jpg' || typeFileDocument === 'png' ?
              <img className={classes.visualisation} src={fileDataURL} alt="preview" />
              :
              <Document file={fileDataURL} onLoadSuccess={this.onDocumentLoadSuccess}>
                {Array.from(
                  new Array(numPages),
                  (el, index) => (
                    <Page
                      width={780}
                      key={`page_${index + 1}`}
                      pageNumber={index + 1}
                    />
                  ),
                )}
              </Document>
            }
          </DialogContent>
        </Prompt>
      </Grid>
    )
  }
}

export default withStyles(styles)(Uploader);
