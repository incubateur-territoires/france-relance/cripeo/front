import React, { useState } from 'react';
import { makeStyles, withStyles } from '@mui/styles';
import { useSelector } from 'react-redux'
import { Prompt } from '../../composants_internes';
import {
    Grid, Typography, FormLabel, FormControl, FormGroup,
    FormControlLabel, Checkbox, DialogContent, DialogActions, Button
} from '@mui/material';

const GreenCheckbox = withStyles({
    root: {
        color: "#8e8e8e",
        '&$checked': {
            color: "#2F80ED",
        },
    },
    checked: {},
})((props) => <Checkbox color="default" {...props} />);


const useStyles = makeStyles(theme => ({
    dialog: {
        padding: 30
    },
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(3),
    },
    nomDocument: {
        color: "#2F80ED",
    },
    checkedIcon: {
        '&$checked': {
            color: "#2F80ED",
        },
    },
    checked: {}
}));



export default function DroitsFile(props) {
    const classes = useStyles();
    const { document, open, handleClose, validedroits } = props;
    const acteurs = useSelector(state => state.acteurs);
    const [selectActeurs, setSelectActeurs] = useState(document.acteurs.map(act => act.acteur_id));

    const handleChange = (event) => {
        event.stopPropagation();
        const check = selectActeurs.findIndex(act => act === event.target.name);
        if (check === -1) {
            setSelectActeurs(selectActeurs.concat([event.target.name]));
        } else {
            const newArray = selectActeurs.filter((act) => act !== event.target.name);
            setSelectActeurs(newArray);
        }
    };

    const validation = () => {
        validedroits(selectActeurs);
    }

    const checkBoxActeurs = acteurs ? acteurs.map((acteur) =>
        <FormControlLabel
            key={'labelGestionDroitFichier' + acteur._id}
            control={<GreenCheckbox checked={selectActeurs ? selectActeurs.findIndex(act => act === acteur._id) !== -1 : false} onChange={handleChange} name={acteur._id} />}
            label={acteur.entite}
        />
    ) : [];

    return (
        <Prompt
            titre={
                <Grid>
                    <Typography variant="h6">Droits appliqués à <span className={classes.nomDocument}>
                        {document.titre}
                    </span>
                    </Typography>
                </Grid>
            }
            onClose={handleClose}
            aria-labelledby="droits-document-dialog"
            open={open}
            className={classes.dialog}
            maxWidth={"xs"}
            actions={
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Fermer
                    </Button>
                    <Button onClick={validation} color="primary">
                        Valider
                    </Button>
                </DialogActions>
            }
        >
            <DialogContent>
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend">Selection des acteurs :</FormLabel>
                    <FormGroup>
                        {checkBoxActeurs}
                    </FormGroup>
                </FormControl>
            </DialogContent>
        </Prompt>
    );
}
