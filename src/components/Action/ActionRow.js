import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import Editer from "../Commun/Bouton/Editer";
import { getActeurs } from "../../actions/acteur";
import { Grid, Alert, CircularProgress } from "@mui/material";
import { addNotification } from "../../actions/notifications";
import { green, grey } from '@mui/material/colors';
import { InputDefault, ButtonText } from '../composants_internes';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
});


class ActionRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      libelle: props.action ? props.action.libelle : "",
      modeEdition: props.modeEdition || false,
      errerMessage: false
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.action && this.props.action !== prevProps.action) {
      this.setState({ libelle: this.props.action.libelle || "" })
    }

    if (prevState.libelle !== this.state.libelle) {
      this.props.handleClickAction({ libelle: this.state.libelle });
    }
  }

  componentDidMount = async () => {
    await this.props.getActeurs();
  };

  handleLibelle = (value) => {
    if (value === "") {
      this.setState({ errerMessage: true });
    }

    this.setState({ libelle: value });
  }

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({
      errerMessage: false,
      modeEdition: !this.state.modeEdition
    });
  }

  cancelEdit = () => {
    this.setState({
      errerMessage: false,
      modeEdition: false,
      libelle: this.props.action ? this.props.action.libelle : "",
    });
    if (this.props.lineCancel) this.props.lineCancel();
  }

  handleClick = async (event) => {
    event.stopPropagation();
    this.setState({ errerMessage: false });
    if (!this.props.addLine) {
      this.props.editLine(this.props.indice, {
        libelle: this.state.libelle,
        entite: this.props.categorie.emetteur.entite,
        acteur_id: this.props.categorie.emetteur.id,
      });
    } else {
      this.props.addLine({
        libelle: this.state.libelle,
        entite: this.props.categorie.emetteur.entite,
        acteur_id: this.props.categorie.emetteur.id,
        categorie_id: this.props.categorie._id
      });
    }
    this.cancelEdit();
  }

  handleDelete = async (event) => {
    event.stopPropagation();
    this.setState({ errerMessage: false });
    this.props.handleDelete(this.props.indice);
    this.cancelEdit();
  }

  render() {
    const { classes, addLine } = this.props;
    const { libelle, errerMessage } = this.state;

    if (!this.props.acteurs) {
      return <CircularProgress size={34} className={classes.fabProgress} />;
    }

    return (
      <Grid container item xs={12} spacing={2} alignItems='center' marginTop={addLine ? 0 : 2} borderBottom={addLine ? 0 : 1} sx={{ borderColor: grey[400] }}>
        <Grid container item xs={addLine ? 12 : 8} marginBottom={addLine ? 0 : 2}>
          <Grid item xs={12} >
            <InputDefault
              label="Libellé de l'action"
              value={libelle}
              setValue={this.handleLibelle}
              disable={addLine ? this.state.modeEdition : !this.state.modeEdition}
              fullWidth
            />
          </Grid>
          {errerMessage &&
            <Alert severity="error">
              Le libellé ne devrait pas être <strong>vide!</strong>
            </Alert>
          }
        </Grid>

        {!this.state.modeEdition ?
          <Grid container item xs={4} justifyContent="flex-end">
            {!addLine &&
              <Editer action={this.changeEdition} texte={"Editer l'action"} disable={this.state.modeEdition} />
            }
          </Grid>
          :
          <Grid item xs={addLine ? 12 : 4}>
            {!addLine &&
              <Grid marginBottom={2}>
                <Grid container item justifyContent="flex-end">
                  <ButtonText
                    variant="text"
                    texte="Supprimer"
                    color="error"
                    action={this.handleDelete}
                  />
                </Grid>
                <Grid item container justifyContent="flex-end">
                  <ButtonText
                    action={this.cancelEdit}
                    texte="Annuler"
                    color="info"
                    variant='text'
                  />
                  <ButtonText
                    action={this.handleClick}
                    texte="Sauvegarder"
                    color="primary"
                    variant='outlined'
                    disable={errerMessage}
                  />
                </Grid>
              </Grid>
            }
          </Grid>
        }
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return {
    acteurs: state.acteurs
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    getActeurs: () => dispatch(getActeurs()),
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ActionRow));