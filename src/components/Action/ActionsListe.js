import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { addNotification } from "../../actions/notifications";
import { setAction } from "../../actions/action";
import { Typography, Grid, TableCell, TableRow, Table, TableHead, TableBody } from "@mui/material";
import Ajouter from "../Commun/Bouton/Ajouter";
import ActionRow from "./ActionRow";
import WrapperAjoutAction from "./WrapperAjoutAction";

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  }
});


class ActionsListe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      addAction: false
    };
  }

  handleAddAction = (_event) => {
    this.setState({ addAction: !this.state.addAction });
  }

  handleClickAction = (action) => {
    this.setState({ action });
  }

  handleCancelEdit = () => {
    this.setState({ action: false });
  }

  handleClose = () => {
    this.setState({ addAction: false });
  };

  render() {
    const { categorie } = this.props;
    const { addAction } = this.state;
    const actionsRender = categorie ? Object.keys(categorie.actions).map(key => {
      return <ActionRow key={categorie.actions[key]._id} action={categorie.actions[key]}
        categorie={categorie}
        handleClick={this.handleClickAction} />
    }) : [];

    return (
      <Grid container key={"ActionsListe"}>
        <Grid item xs={10}>
          <Typography>
            Liste des actions associées
            <Ajouter action={this.handleAddAction} texte={"Ajouter une action"} />
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Libellé</TableCell>
                <TableCell>Entité</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {actionsRender}
            </TableBody>
          </Table>
        </Grid>
        <WrapperAjoutAction
          categorieid={categorie._id}
          open={addAction}
          handleClose={this.handleClose.bind(this)}
        />
      </Grid>
    )
  }

}

function mapStateToProps(state) {
  return {
    actions: state.actions,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAction: (data) => dispatch(setAction(data)),
    addNotification: (obj) => dispatch(addNotification((obj)))
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ActionsListe));