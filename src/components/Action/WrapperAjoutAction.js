import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import connect from "react-redux/es/connect/connect";
import { DialogContent, DialogActions, Button, CircularProgress, MenuItem } from '@mui/material';
import { getActeurs } from "../../actions/acteur";
import Input from "../Commun/Input/Input";
import SelectUI from "../Commun/Input/SelectUI";
import { blue, green } from '@mui/material/colors';
import { Prompt } from '../composants_internes';

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 150,
  },
  cancelButton: {
    marginRight: 50
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
});

class WrapperAjoutAction extends Component {

  state = {
    libelle: "",
    entite: "",
    acteur_id: "",
  };

  handleClose = () => {
    if (this.props.handleClose) {
      this.props.handleClose();
    }
  };

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value.toUpperCase()
    });
  };

  handleSelect = async (event) => {
    event.stopPropagation();
    if (this.props.acteurs && this.props.acteurs.length > 0) {
      for (const element of this.props.acteurs) {
        if (element.entite === event.target.value) {
          await this.setState({
            acteur_id: element._id,
            [event.target.name]: event.target.value
          })
        }
      }
    }
  }

  handleSubmitAction = async (_obj) => {
    const objet = {
      id: this.props.categorieid,
      actions: {
        entite: this.state.entite,
        acteur_id: this.state.acteur_id,
        libelle: this.state.libelle,
      }
    }
    await this.props.addAction(objet);
    this.handleClose();
  };

  render() {
    const { classes, onClose, acteurs, ...other } = this.props;
    const { libelle, entite } = this.state;

    if (!this.props.acteurs) {
      return <CircularProgress size={34} className={classes.fabProgress} />;
    }

    const acteursMenuitem = acteurs ? Object.keys(acteurs).map(key =>
      <MenuItem key={key} value={acteurs[key].entite}>{acteurs[key].entite}</MenuItem>
    ) : [];

    return (
      <Prompt
        titre={"Ajout d'une action"}
        onClose={this.handleClose.bind(this)}
        fullWidth={true}
        maxWidth={"lg"}
        aria-labelledby="simple-dialog-title" {...other}
        actions={
          <DialogActions>
            <Button onClick={this.handleClose.bind(this)} color="secondary">
              Annuler
            </Button>
            <Button onClick={this.handleSubmitAction} color="primary">
              Enregistrer
            </Button>
          </DialogActions>
        }
      >
        <DialogContent>
          <Input
            label="Libellé"
            name="libelle"
            value={libelle}
            handleChange={this.handleChange}
          />
          <SelectUI required={true} handleChange={this.handleSelect}
            items={acteursMenuitem}
            label="Entité" value={entite} name="entite" />
        </DialogContent>

      </Prompt>
    )
  }
}

WrapperAjoutAction.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    categorie: state.categorie,
    acteurs: state.acteurs
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getActeurs: () => dispatch(getActeurs()),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(WrapperAjoutAction));