import React, { Component } from "react";
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import {
  Grid,
  FormControlLabel,
  Switch,
  CircularProgress,
  Typography,
  DialogActions,
  DialogContent,
  InputLabel,
  MenuItem,
  FormControl,
  Box,
  Chip,
  Select
} from '@mui/material';
import { addAction, removeAction, updateAction } from "../../actions/categorie";
import { addNotification } from "../../actions/notifications";
import { getActeurs } from "../../actions/acteur";
import ActionRow from "../Action/ActionRow";
import { Cancel, Add } from '@mui/icons-material';
import { green, blue, grey } from '@mui/material/colors';
import { InputDefault, SelectDefault, ButtonText, Prompt } from "../composants_internes";


const styles = () => ({
  title: {
    color: '#2F80ED',
    fontSize: 'xx-large',
    textAlign: "left"
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
});

class Categorie extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.categorie && props.categorie._id ? props.categorie._id : "",
      libelle: props.categorie && props.categorie.libelle ? props.categorie.libelle : "",
      emetteur: props.categorie && props.categorie.emetteur ? props.categorie.emetteur.entite : props.organisation.entite,
      destinataire: props?.categorie?.destinataire?.id || "",
      information: props?.categorie && props.categorie.information ? props.categorie.information : [],
      modeEdition: this.props.enabled || false,
      actions: props.categorie && props.categorie.actions ? props.categorie.actions : [],
      actif: props.categorie && props.categorie._id ? props.categorie.actif : true,
      urgent: props.categorie && props.categorie._id ? props.categorie.urgent : true,
      openAdd: false,
      nonEchange: false,
      libelleAction: "",
      handleEditCategorie: () => { },
      clickAction: () => { },
      labelInfo: 'Acteurs en Information',
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if ((prevProps.categorie && this.props.categorie && this.props.categorie._id && prevProps.categorie._id && prevProps.categorie._id !== this.props.categorie._id)
      ||
      (prevProps.categorie && this.props.categorie && (prevProps.categorie._id === this.props.categorie._id) && prevProps.categorie.actions?.length !== this.props.categorie.actions?.length)


    ) {
      this.setState({
        id: this.props.categorie && this.props.categorie._id ? this.props.categorie._id : "",
        libelle: this.props.categorie && this.props.categorie.libelle ? this.props.categorie.libelle : "",
        emetteur: this.props.categorie && this.props.categorie.emetteur ? this.props.categorie.emetteur.entite : this.props.organisation.entite,
        destinataire: this.props.categorie?.destinataire?.id || "",
        information: this.props.categorie && this.props.categorie.information ? this.props.categorie.information : [],
        actions: this.props.categorie && this.props.categorie.actions ? this.props.categorie.actions : [],
        actif: this.props.categorie && this.props.categorie._id ? this.props.categorie.actif : true,
        urgent: this.props.categorie && this.props.categorie._id ? this.props.categorie.urgent : true,
        openAdd: false,
        nonEchange: false,
        libelleAction: "",
        clickAction: this.clickAction,
        labelInfo: 'Acteurs en Information',
      });
    }

    let infoEstDifferent = false;
    if (this.state.information && this.state.information.length > 0) {
      this.state.information.forEach(inform => {
        if (prevState.information && prevState.information.length > 0) {
          prevState.information.forEach(info => {
            if (prevState.information.findIndex(infoState => infoState.acteur_id === inform.acteur_id) === -1 ||
              this.state.information.findIndex(infoState => infoState.acteur_id === info.acteur_id) === -1) {
              infoEstDifferent = true;
            }
          })
        } else if (this.state.information && this.state.information.length > 0) {
          infoEstDifferent = true;
        }
      })
    } else if (prevState.information && prevState.information.length > 0) {
      infoEstDifferent = true;
    }

    if (prevState.libelle !== this.state.libelle || prevState.destinataire !== this.state.destinataire || prevState.actif !== this.state.actif || prevState.urgent !== this.state.urgent || infoEstDifferent) {
      this.handleEditCategorie({
        libelle: this.state.libelle,
        destinataire: this.state.destinataire,
        actif: this.state.actif,
        urgent: this.state.urgent,
        information: this.state.information,
      });
    }
  }

  componentDidMount = async () => {
    await this.props.getActeurs();
  };

  handleEditCategorie = (edited) => {
    let destinataire = null;
    for (const element of this.props.acteurs) {
      if (edited.destinataire === element._id) {
        destinataire = {
          entite: element.entite,
          id: element._id
        };
      }
    }
    let editedCategorie = {
      libelle: edited.libelle,
      destinataire: destinataire,
      actions: this.state.actions,
      actif: edited.actif,
      urgent: edited.urgent,
      information: edited.information
    };
    if (this.state.id && this.state.id.length > 0) editedCategorie._id = this.state.id;
    editedCategorie.emetteur = {
      entite: this.props.organisation.entite,
      id: this.props.organisation._id
    };
    this.props.getDataEdited(editedCategorie);
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value.toUpperCase() });
  }

  handleSetLibelle = (value) => {
    this.setState({ libelle: value });
  }

  handleSetDestinataire = (value) => {
    this.setState({ destinataire: value });
  }

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({ modeEdition: !this.state.modeEdition });
  }

  cancelEdit = () => {
    this.setState({ modeEdition: false });
    this.props.wrapperClose();

  }

  handleSwitch = (event) => {
    event.stopPropagation();
    this.setState({ [event.target.name]: event.target.checked });
  }

  handleDisabled = (event) => {
    event.stopPropagation();
    this.cancelEdit();
  }

  handleClose = () => {
    this.setState({ openAdd: false })
  };

  handleChangeInfo = (event) => {
    event.stopPropagation();
    let inform = this.state.information.length > 0 ? this.state.information : [];
    const newInfo = event.target.value[event.target.value.length - 1];
    if (this.state.information.length === 0 || (this.state.information.length > 0 && this.state.information.findIndex(info => info.acteur_id === newInfo.id) === -1)) {
      inform = inform.concat({ acteur_id: newInfo.id, entite: newInfo.value });
    } else {
      inform = inform.filter(data => data.acteur_id !== newInfo.id);
    }
    this.setState({ information: inform });
  }

  handleDeleteInfo = (value) => {
    let inform = this.state.information.length > 0 ? this.state.information : [];
    const newInfo = value;
    inform = inform.filter(data => data.entite !== newInfo);
    this.setState({ information: inform });
  }

  handleAction = async () => {
    this.setState({ openAdd: !this.state.openAdd });
  }

  handleDelete = async (indice) => {
    let actions = this.state.actions;
    let obj = actions[indice]._id;
    try {
      const res = await this.props.removeAction(obj);
      if (res.success) {
        this.props.addNotification({ variant: "success", message: "L'action a bien été supprimée" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la suppression de l'action",
          details: res.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la suppression de l'action" });
    }
    actions.splice(indice, 1);
    this.setState({ actions: actions });
  };

  editAction = async (indice, data) => {
    let actions = this.state.actions;
    actions[indice].libelle = data.libelle;
    actions[indice].entite = data.entite;
    actions[indice].acteur_id = data.acteur_id;
    await this.setState({ actions: actions });
    try {
      const res = await this.props.updateAction(actions[indice]);
      if (res.success) {
        this.props.addNotification({ variant: "success", message: "L'action a bien été modifié" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la modification de l'action",
          details: res.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification de l'action" });
    }
  };

  addAction = async (data) => {
    if (data !== null && data !== "") {
      try {
        const res = await this.props.addAction(this.props.categorie._id, { libelle: data, entite: this.props.categorie.emetteur.entite, acteur_id: this.props.categorie.emetteur.id });
        if (res.success) {
          this.props.addNotification({ variant: "success", message: "L'action a bien été ajouté" });
        } else {
          this.props.addNotification({
            variant: "error",
            message: "Erreur lors de l'ajout de l'action",
            details: res.error[0].msg
          });
        }
      } catch (err) {
        console.log(err);
        this.props.addNotification({ variant: "error", message: "Erreur lors de l'ajout de l'action" });
      }
    }
  };

  clickAction = (objAction) => {
    this.setState({ libelleAction: objAction.libelle });
  }

  handleValiderAction = () => {
    let data = this.state.libelleAction;
    this.addAction(data);
  }

  render() {
    const { classes, acteurs, categorie, organisation } = this.props;
    const { libelle, destinataire, actif, urgent, information, actions, openAdd, labelInfo } = this.state;

    if (!acteurs) {
      return <CircularProgress size={34} className={classes.fabProgress} />;
    }

    const filterNoOrganisation = acteurs?.filter(act => act._id !== organisation._id);

    const acteursOptions = filterNoOrganisation?.map(act => {
      return {
        value: act._id,
        label: act.entite
      }
    });

    const filterNoDestinataire = filterNoOrganisation?.filter(act => act._id !== destinataire);

    const acteursInfoOptions = filterNoDestinataire?.map(act => {
      return {
        value: act.entite,
        id: act._id
      }
    });

    const valueSelectInfo = information.length > 0 ? information?.map(info => { return info.entite }) : [];

    let actionsListe;
    if (categorie) {
      actionsListe = actions.map((action, index) => {
        return (
          <ActionRow
            key={`action-${index}`}
            nextkey={`action-${index}`}
            action={action}
            indice={index}
            name={`action-${index}`}
            handleDelete={this.handleDelete.bind(this)}
            editLine={this.editAction.bind(this)}
            categorie={categorie}
            handleClickAction={this.clickAction.bind(this)}
          />
        )
      });
    }

    return (
      <div className={classes.paper}>
        {openAdd && categorie &&
          <Prompt
            titre={"Ajouter une action"}
            onClose={this.handleClose}
            open={openAdd}
            fullWidth
            actions={
              <DialogActions sx={{ marginRight: 2 }}>
                <ButtonText
                  action={this.handleClose}
                  color="info"
                  texte="Annuler"
                  variant="text"
                />
                <ButtonText
                  action={this.handleValiderAction}
                  color="primary"
                  texte="Valider"
                />
              </DialogActions>
            }
          >
            <DialogContent>
              <ActionRow
                key={"ajoutligne"}
                addLine={this.addAction.bind(this)}
                lineCancel={this.handleClose}
                categorie={categorie}
                handleClickAction={this.clickAction.bind(this)}
              />
            </DialogContent>
          </Prompt>}
        <Grid container spacing={2}>
          <Grid container item xs={12} justifyContent="space-between" alignItems="center" spacing={1}>
            <Grid item xs={6}>
              <InputDefault
                label={"Libellé Catégorie"}
                value={libelle}
                setValue={this.handleSetLibelle}
                fullWidth
              />
            </Grid>
            <Grid container item xs={3} justifyContent="flex-end">
              <FormControlLabel
                control={<Switch
                  checked={urgent}
                  onChange={this.handleSwitch}
                  color="warning"
                  name="urgent"
                  inputProps={{ 'aria-label': 'Catégorie urgent checkbox' }}
                />}
                label="Catégorie urgence"
                labelPlacement="start"
              />
            </Grid>
            {categorie &&
              <Grid container item xs={3} justifyContent="flex-end" >
                <FormControlLabel
                  control={<Switch
                    checked={actif}
                    onChange={this.handleSwitch}
                    color="primary"
                    name="actif"
                    inputProps={{ 'aria-label': 'Catégorie active checkbox' }}
                  />}
                  label="Catégorie active"
                  labelPlacement="start"
                />
              </Grid>
            }
          </Grid>
          <Grid container item xs={12} justifyContent="space-between" alignItems="center">
            <Grid item xs={4} marginRight={1}>
              <SelectDefault
                label={'Destinataire'}
                value={destinataire}
                setValue={this.handleSetDestinataire}
                options={acteursOptions}

              />
            </Grid>
            <Grid item xs={7}>
              <FormControl sx={{ minWidth: 120, width: "100%" }}>
                <InputLabel id={`${labelInfo.split(' ').join('')}-label`}>{labelInfo}</InputLabel>
                <Select
                  labelId={`${labelInfo.split(' ').join('')}-label`}
                  label={labelInfo}
                  multiple
                  autoWidth
                  value={valueSelectInfo}
                  onChange={this.handleChangeInfo}
                  renderValue={selected => (
                    <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                      {selected.map(value =>
                        <Chip
                          key={value}
                          label={value}
                          deleteIcon={
                            <Cancel
                              onMouseDown={(event) => event.stopPropagation()}
                            />
                          }
                          onDelete={this.handleDeleteInfo.bind(this, value)}
                        />
                      )}
                    </Box>
                  )}
                >
                  {acteursInfoOptions.map((name, index) => (
                    <MenuItem key={`${name._id}${index}`} value={name}>
                      {information.length > 0 ?
                        information?.findIndex(info => info.acteur_id === name.id) === -1 ?
                          <Typography variant="body1" sx={{ color: blue['700'] }}>
                            {name.value}
                          </Typography>
                          :
                          <Typography variant="caption" sx={{ color: grey['700'] }}>
                            {name.value}
                          </Typography>
                        :
                        <Typography variant="body1" sx={{ color: blue['700'] }}>
                          {name.value}
                        </Typography>
                      }
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
          </Grid>
          {categorie && actionsListe &&
            <Grid container item marginTop={1} xs={12}>
              <Grid container item xs={6}>
                <Typography>
                  Liste des actions associées
                </Typography>
              </Grid>
              <Grid container item xs={6} justifyContent="flex-end">
                <ButtonText
                  action={this.handleAction}
                  color="primary"
                  texte="Ajouter une action"
                  icon={<Add />}
                />
              </Grid>
              {actionsListe}
            </Grid>
          }
        </Grid>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    acteurs: state.acteurs,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getActeurs: () => dispatch(getActeurs()),
    addAction: (idCategorie, data) => dispatch(addAction(idCategorie, data)),
    removeAction: (data) => dispatch(removeAction(data)),
    updateAction: (data) => dispatch(updateAction(data)),
    addNotification: (obj) => dispatch(addNotification(obj)),

  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Categorie));
