import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getCategories, setCategorie, removeCategorie } from "../../actions/categorie";
import { Grid, Button } from '@mui/material';
import CategorieCard from "../Categorie/CategorieCard";
import WrapperCategorie from "./WrapperCategorie";
import { SearchInput } from "../composants_internes";

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  button: {
    backgroundColor: '#3867cf',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: '#033878'
    }
  },
});


class CategoriesListe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      categorie: false,
      categorieForm: false,
      filterCategorie: ""
    };
  }

  componentDidMount = async () => {
    await this.props.getCategories(this.props.organisation._id);
  };

  handleClick = (categorie) => {
    this.setState({ categorie, categorieForm: false, openCategorie: true });
  }

  handleSaveCategorie = async (categorie) => {
    const res = await this.props.setCategorie(categorie);
    if (res && res.success) {
      this.setState({ categorieForm: false, categorie: false });
      this.props.getCategories(this.props.organisation._id);
    }
  }

  handleShowForm = () => {
    this.setState({ categorieForm: !this.state.categorieForm, categorie: false });
  }

  handleChangeFilterCategorie = (value) => {
    this.setState({ filterCategorie: value });
  }

  handleClear = () => {
    this.setState({ filterCategorie: "" });
  }

  handleDeleteCategorie = async (data) => {
    let id = data._id;
    let idActeur = this.props.organisation._id;
    await this.props.removeCategorie(id, idActeur);
  }

  render() {
    const { classes, organisation, categories } = this.props;
    const { categorieForm, filterCategorie } = this.state;

    if (!categories || !Array.isArray(categories)) return <div />;

    const categoriesFiltered = filterCategorie?.length > 0 && categories ? categories?.filter(categ => categ.libelle.toUpperCase().indexOf(filterCategorie.toUpperCase()) !== -1) : categories;

    const sortedCategories = categoriesFiltered?.sort(function (a, b) {
      if (a.libelle && b.libelle && a.libelle.toUpperCase() < b.libelle.toUpperCase()) return -1;
      if (a.libelle && b.libelle && a.libelle.toUpperCase() > b.libelle.toUpperCase()) return 1;
      return 0
    })?.sort((a, b) => b.actif - a.actif)

    const categoriesRender = sortedCategories ? Object.keys(categoriesFiltered).map(key => {
      return <Grid item lg={2} xs={3} key={"categorie-" + categoriesFiltered[key]._id}>
        <CategorieCard categorie={categoriesFiltered[key]}
          handleClick={this.handleClick}
          handleSave={this.handleSaveCategorie}
          organisation={organisation}
          handleDeleteCategorie={this.handleDeleteCategorie}
        />
      </Grid>
    }) : [];


    return (
      <React.Fragment>
        <Grid container item xs={12} justifyContent="center" alignItems="center" spacing={2} >
          <Grid item xs={4} marginTop={1}>
            <SearchInput
              label="Recherche une catégorie"
              value={filterCategorie?.toUpperCase()}
              setValue={this.handleChangeFilterCategorie}
              handleClear={this.handleClear}
              fullWidth
            />
          </Grid>
          <Grid item>
            <Button onClick={this.handleShowForm} className={classes.button}>Ajouter une catégorie</Button>
          </Grid>
        </Grid>
        <Grid item container xs={12}>
          {categoriesRender}
        </Grid>
        <Grid item xs={12}>
          <WrapperCategorie
            handleSave={this.handleSaveCategorie}
            handleClose={this.handleShowForm}
            enabled={true}
            open={categorieForm}
            organisation={organisation}
          />
        </Grid>
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    categories: state.categories,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: (idOrganisation) => dispatch(getCategories(idOrganisation)),
    setCategorie: (data) => dispatch(setCategorie(data)),
    removeCategorie: (idCategorie, idActeur) => dispatch(removeCategorie(idCategorie, idActeur)),
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(CategoriesListe));