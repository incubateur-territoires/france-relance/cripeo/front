import CardSimple from "../Commun/Card/CardSimple";
import React, { Component, Fragment } from "react";
import WrapperCategorie from "./WrapperCategorie";
import { ButtonIcon, ButtonText, Prompt } from "../composants_internes";
import { Delete, Notifications } from '@mui/icons-material';
import { DialogContentText, DialogActions, DialogContent, Grid, Tooltip, CircularProgress } from "@mui/material";
import { grey, red } from '@mui/material/colors';


class CategorieCard extends Component {
  state = {
    openDelete: false,
    open: false
  }

  handleClick = (event) => {
    event.stopPropagation();
    this.props.handleClick(this.props.categorie)
  }

  handleSave = (data) => {
    this.props.handleSave(data);
  }

  handleOpen = (event) => {
    event.stopPropagation();
    this.setState({ open: true });
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  handleDeleteCategorie = () => {
    this.props.handleDeleteCategorie(this.props.categorie);
  }

  handleConfirmDelete = (event) => {
    event.stopPropagation();
    this.setState({ openDelete: true });
  }
  handleCloseConfirmDelete = () => {
    this.setState({ openDelete: false });
  }

  render() {
    const { classes, categorie, selected, organisation } = this.props;
    const { open, openDelete } = this.state;

    if (!categorie) return <CircularProgress size={34} className={classes.fabProgress} />

    return (
      <Fragment>
        <CardSimple
          handleClic={this.handleOpen}
          handleSave={this.handleSave}
          center
          selected={selected}
          titre={categorie.libelle ? categorie.libelle.toUpperCase() : ""}
          sousTitre={categorie.emetteur && categorie.emetteur.entite && (categorie.emetteur.entite.toUpperCase() + " - " + categorie?.destinataire?.entite?.toUpperCase())}
          actif={categorie.actif}
          action={
            <Grid item container alignItems='center' xs={12}>

              <Grid item container xs={6} justifyContent='flex-start'>
                <Tooltip title={categorie.urgent ? "Urgence" : "Non Urgence"}>
                  <Notifications sx={{ color: categorie.urgent ? red[700] : grey[400], margin: 1 }} />
                </Tooltip>
              </Grid>
              <Grid item container xs={6} justifyContent='flex-end'>
                {categorie.noEchange &&
                  <ButtonIcon
                    action={this.handleConfirmDelete}
                    color="error"
                    icon={<Delete />}
                    texte="Supprimer"
                  />
                }
              </Grid>
            </Grid>
          }
        >
        </CardSimple>
        {categorie.noEchange && openDelete &&
          <Prompt
            titre={"Supression d'une categorie"}
            open={openDelete}
            keepMounted
            onClose={this.handleCloseConfirmDelete.bind(this)}
            actions={
              <DialogActions>
                <ButtonText
                  action={this.handleCloseConfirmDelete.bind(this)}
                  color="info"
                  texte="Annuler"
                  variant="text"
                />
                <ButtonText
                  action={this.handleDeleteCategorie.bind(this)}
                  color="primary"
                  texte="Valider"
                />

              </DialogActions>
            }
          >
            <DialogContent>
              <DialogContentText id="Supression d'un categorie">
                Voulez-vous supprimer cette categorie ?
              </DialogContentText>
            </DialogContent>

          </Prompt>
        }
        <WrapperCategorie
          open={open}
          handleClose={this.handleClose}
          categorie={categorie}
          handleSave={this.handleSave}
          organisation={organisation}
        />
      </Fragment>
    )
  }
}



export default CategorieCard;