import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";
import withStyles from '@mui/styles/withStyles';
import { DialogContent, DialogActions } from '@mui/material';
import Categorie from "../Categorie";
import { getActeurs } from "../../../actions/acteur";
import { getCategorie } from '../../../actions/categorie';
import { Prompt, ButtonText } from "../../composants_internes";

const styles = () => ({
  root: {
    padding: 50
  }
});

class WrapperCategorie extends Component {

  constructor(props) {
    super(props);
    this.state = {
      editedCategorie: null,
    };
  }

  componentDidMount = async () => {
    if (this.props.categorie && this.props.categorie._id) this.props.getCategorie(this.props.categorie._id);
    await this.props.getActeurs();
  }

  handleClose = () => {
    this.setState({ editedCategorie: null });
    this.props.handleClose();
  }

  handleSave = () => {
    let obj = this.state.editedCategorie;
    if (obj.destinataire && obj.libelle) {
      this.props.handleSave(obj);
      this.handleClose();
    } else {
      alert("saisie obligatoire");
    };

  }

  getDataEdited = (data) => {
    this.setState({ editedCategorie: data });
  }

  render() {
    const { classes, open, categorie, enabled, organisation } = this.props;

    // désactiver la button de valider, s'il n'y a pas des changement
    let disableButton = true;
    if (categorie) {

      let infoEstDifferent = false;
      if (this.state.editedCategorie && this.state.editedCategorie.information && this.state.editedCategorie.information.length !== 0) {
        this.state.editedCategorie.information.forEach(inform => {
          if (categorie.information !== null && categorie.information && categorie.information.length !== 0) {
            categorie.information.forEach(info => {
              if (categorie.information.findIndex(infoState => infoState.acteur_id === inform.acteur_id) === -1 ||
                this.state.editedCategorie.information.findIndex(infoState => infoState.acteur_id === info.acteur_id) === -1) {
                infoEstDifferent = true;
              }
            })
          } else if (this.state.editedCategorie.information && this.state.editedCategorie.information.length !== 0) {
            infoEstDifferent = true;
          }
        })
      } else if (categorie.information !== null && categorie.information && categorie.information.length !== 0) {
        infoEstDifferent = true;
      }

      if (this.state.editedCategorie && (
        (categorie.destinataire && categorie.destinataire.id.toString() !== this.state.editedCategorie.destinataire.id.toString()) ||
        (!categorie.destinataire && this.state.editedCategorie.destinataire) ||
        categorie.libelle !== this.state.editedCategorie.libelle ||
        categorie.urgent !== this.state.editedCategorie.urgent ||
        categorie.actif !== this.state.editedCategorie.actif ||
        infoEstDifferent)
      ) {
        disableButton = false;
      }
    } else {
      if (this.state.editedCategorie && this.state.editedCategorie.libelle !== "" && this.state.editedCategorie.destinataire) {
        disableButton = false;
      }
    }

    return (
      <Prompt
        titre={categorie ? "Catégorie" : "Nouvelle catégorie"}
        onClose={this.handleClose}
        fullWidth={true}
        maxWidth={"lg"}
        scroll={"body"}
        open={open}
        actions={
          <DialogActions sx={{ marginRight: 2 }}>
            <ButtonText
              action={this.handleClose}
              color="info"
              texte="Annuler"
              variant="text"
            />
            <ButtonText
              action={this.handleSave}
              color="primary"
              texte="Valider"
              disable={disableButton}
            />
          </DialogActions>
        }
      >
        <DialogContent className={classes.root}>
          {open &&
            <Categorie
              categorie={categorie}
              enabled={enabled}
              getDataEdited={this.getDataEdited}
              handleValider={this.handleValider}
              organisation={organisation}
              wrapperClose={this.handleClose}
            />
          }
        </DialogContent>
      </Prompt>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    getCategorie: (id) => dispatch(getCategorie(id)),
    getActeurs: () => dispatch(getActeurs())
  }
}

export default withStyles(styles)(connect(null, mapDispatchToProps)(WrapperCategorie));