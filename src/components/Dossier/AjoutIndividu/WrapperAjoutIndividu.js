import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import { addIndividu } from "../../../actions/dossier";
import { Button, Grid, DialogActions, DialogContent, TextField, Box, Paper } from "@mui/material";
import connect from "react-redux/es/connect/connect";
import { FormIndividu, Prompt } from "../../composants_internes";
import { blue } from '@mui/material/colors';

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 150,
  },
  cancelButton: {
    marginRight: 50
  }
});

class WrapperAjoutIndividu extends Component {

  state = {
    open: false,
    civilite: "",
    nom: "",
    prenom: "",
    dateNaissance: null,
    communeNaissance: "",
    paysNaissance: "FRANCE",
    commentaire: "",
    individu: null,
    lienParent: "",
    refIndividu: ""
  };

  handleClose = () => {
    this.props.onClose();
  };

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  };

  handleSetForm = (form) => {
    this.setState({ individu: form });
  }

  wrappingState = (newState) => {
    this.setState({ individu: newState });
  }

  handleSubmitIndividu = async () => {
    this.handleClose();
    const obj = this.state.individu;
    const lienParent = this.state.lienParent
    const refIndividu = { ref: this.state.refIndividu.toString(), orga: this.props.auth?.organisation?.acteur_id }

    const nouvDateNaiss = obj.dateNaissance ? new Date((new Date(obj.dateNaissance)).setHours(13)) : ""
    const objet = {
      id: this.props.dossier._id,
      individuSecondaire: {
        civilite: obj.civilite || "",
        nom: obj.nom || "",
        prenom: obj.prenom || "",
        dateNaissance: nouvDateNaiss,
        communeNaissance: obj.lieuNaissance || "",
        paysNaissance: obj.nationalite || "",
        lienParent: lienParent || "",
        refIndividu: refIndividu || ""
      }
    }
    await this.props.addIndividu(objet);
    this.handleClose();
  };

  render() {
    const { classes, onClose, dossier, addIndividu, ...other } = this.props;


    return (
      <Prompt
        titre={"Ajout d'un individu"}
        onClose={this.handleClose}
        fullWidth={true}
        maxWidth={"lg"}
        aria-labelledby="simple-dialog-title" {...other}
        actions={
          <DialogActions>
            <Grid container justifyContent='flex-end' spacing={2}>
              <Grid item>
                <Button onClick={this.handleClose} className={classes.cancelButton}>
                  Annuler
                </Button>
              </Grid>
              <Grid item>
                <Button onClick={this.handleSubmitIndividu} color="primary" variant='contained' className={classes.cancelButton}>
                  Ajout de l'individu
                </Button>
              </Grid>
            </Grid>
          </DialogActions>
        }
      >
        <DialogContent>
          <FormIndividu
            setForm={this.handleSetForm.bind(this)}
            stepper
          />
          <Box>
            <Paper elevation={1} sx={{ marginTop: 2, padding: 2 }}>
              <Grid container item xs={12} spacing={2} padding={2}>
                <TextField label="Lien de parenté" variant="outlined" name="lienParent" onChange={this.handleChange}></TextField>
              </Grid>
            </Paper>
          </Box>
        </DialogContent>
      </Prompt>
    )
  }
}

WrapperAjoutIndividu.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,

};

function mapStateToProps(state) {
  return {
    dossier: state.dossier,
    auth: state.auth
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addIndividu: (obj) => dispatch(addIndividu(obj)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(WrapperAjoutIndividu));