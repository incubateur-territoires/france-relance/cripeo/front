import React, { useEffect, useCallback } from 'react';
import { Grid, Typography } from "@mui/material";
import { Timeline, TimelineItem, timelineItemClasses, TimelineSeparator, TimelineContent, TimelineDot } from "@mui/lab";
import moment from 'moment';
import { useDispatch } from 'react-redux';
import { getHistoriquesByDossier } from "../../../actions/historique";
import { grey } from '@mui/material/colors';


export default function BlocHistorique({ historiques, dossier }) {

    const dispatch = useDispatch();

    const cbGetHistoriques = useCallback((id) => {
        return dispatch(getHistoriquesByDossier(id));
    }, [dispatch]);

    useEffect(() => {
        cbGetHistoriques(dossier._id);
        // eslint-disable-next-line
    }, [dossier]);

    const historiquesSorted = historiques.sort((a, b) => new Date(a.created_at) > new Date(b.created_at) ? -1 : 1);
    const historique = historiquesSorted.map((history, index) => {
        return (
            <TimelineItem key={history._id + index}>
                <TimelineSeparator>
                    <TimelineDot />
                </TimelineSeparator>
                <TimelineContent >
                    <Grid container alignItems={"center"} spacing={1}>
                        <Grid item>
                            <Typography sx={{ fontSize: "11px", color: grey[600] }}>
                                Le {moment(history.created_at).format('DD/MM/YYYY')} à {moment(history.created_at).format('HH:mm')} :
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ fontSize: "13px" }}>
                                {history.message}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ fontSize: "13px", fontWeight: "bold" }}>
                                "{history.tag === "RÉFÉRENCE" ? history.tag + " DE " + history.acteur.toUpperCase() : history.tag}"
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Typography sx={{ fontSize: "13px" }}>
                                par &nbsp;{history.createdBy}&nbsp;( {history.acteur} )
                            </Typography>
                        </Grid>
                    </Grid>
                </TimelineContent>
            </TimelineItem>
        )
    })

    return (
        <Grid sx={{ paddingLeft: 5 }} >
            <Typography variant="h6" margin={3} sx={{ textAlign: "left", color: "#2F80ED" }}>HISTORIQUE D'ÉVÈNEMENT</Typography>
            <Timeline sx={{ [`& .${timelineItemClasses.root}:before`]: { flex: 0, padding: 0, }, }}>
                {historique}
            </Timeline>
        </Grid>
    )
}