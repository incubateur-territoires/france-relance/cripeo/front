import React, { Component } from 'react';
import { connect } from "react-redux";
import { Box, CircularProgress } from "@mui/material";
import withStyles from '@mui/styles/withStyles';
import { Timeline } from "@mui/lab";
import LigneAction from "../../Echange/LigneAction";

const styles = theme => ({
  root: {
    flex: 1
  }
});

class BlocActions extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  delete = async (id) => {
    await this.props.delete(id);
  }

  render = () => {
    const { classes, echanges, utilisateur, documents, acteurId } = this.props;

    if (!echanges)
      return <div><CircularProgress /></div>;

    let ligneActions;
    let listeActions = [];
    for (const element of echanges) {
      if (element.action) {
        for (const action of element.action) {
          let data = { ...action };
          data.echange = element;
          listeActions.push(data);
        }
      }
    }

    let actionsSorted = listeActions.sort((a, b) => new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1);
    ligneActions = Object
      .keys(actionsSorted)
      .map(key => {
        const docactions = documents && documents.length > 0 ? documents.filter((doc) => actionsSorted[key].document.some(actDoc => actDoc.document_id === doc._id)) : [];
        return <LigneAction
          acteurId={acteurId}
          key={actionsSorted[key].action_id + actionsSorted[key].dateRealisation}
          action={actionsSorted[key]}
          max={actionsSorted.length}
          numeroOrdre={key}
          acteur={utilisateur && utilisateur.acteur_id ? utilisateur.acteur_id : null}
          alternatif={true}
          documents={docactions}
          delete={this.delete.bind(this)}
        />
      })

    return (
      <Box className={classes.root}>
        <Timeline >
          {ligneActions}
        </Timeline>
      </Box>
    )
  }
}

function mapStateToProps(state) {
  return {
    utilisateur: state.utilisateur
  };
}

export default withStyles(styles)(connect(mapStateToProps, null)(BlocActions));