import React, { Component } from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import { Grid, CircularProgress } from "@mui/material";
import { addNotification } from "../../../actions/notifications";
import BlocIdentite from "../BlocIdentite";
import BlocAdresse from "../BlocAdresse";
import BlocReference from "../BlocReference";
import WrapperAjoutIndividu from "../AjoutIndividu";
import BlocSecteurs from '../BlocSecteurs';
import { ButtonText } from  '../../composants_internes';
import { Add } from '@mui/icons-material';

const styles = theme => ({
  button: {
    backgroundColor: '#22671d',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: 'orange'
    }
  },
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  },
  fab: {
    height: 50,
    minHeight: 50,
    width: 50
  },
  pointligne: {
    marginLeft: 70
  },
});

class BlocRenseignement extends Component {
  state = {
    add: false,
    adresse: (this.props.dossier && this.props.dossier.adresse) || null,
    open: false,
    modeEdition: false,
  };

  ajouter = () => {
    this.setState({ add: true, open: true });
  }

  endAdd = () => {
    this.setState({
      add: false,
      open: false,
    });
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleAdresse = (adresse) => {
    this.setState({ adresse });
  };

  handleClose = value => {
    this.setState({ open: false });
  };


  render() {
    const { classes, dossier, creation } = this.props;
    const { add, open } = this.state;

    if (!dossier && !creation)
      return <div><CircularProgress /></div>;

    let blocSecondaire = "";
    if (dossier.individuSecondaire.length > 0) {
      blocSecondaire = Object
        .keys(dossier.individuSecondaire)
        .map(key =>
          <div key={dossier.individuSecondaire[key]._id}>
            <BlocIdentite
              key={dossier.individuSecondaire[key]._id}
              endAdd={this.endAdd.bind(this)}
              occurence={dossier.individuSecondaire[key]}
              retrait={true}
            />
          </div>
        );
    }

    return (
      <Grid item container spacing={1} xs={12} justifyContent="center">
        {add && open &&
          <WrapperAjoutIndividu
            open={open}
            onClose={this.handleClose}
            dossier={dossier}
          />
        }
        <Grid item xs={11}>
          <BlocReference key={dossier._id} />
          <BlocSecteurs dossier={dossier} />
        </Grid>
        <Grid container item spacing={2} alignItems="center" className={classes.pointligne} marginTop={5}>
          <ButtonText
            color="primary"
            aria-label="add"
            action={this.ajouter}
            texte="Ajouter un individu"
            icon={<Add />}
          />
        </Grid>
        <Grid item xs={11}>
          <BlocIdentite
            key={dossier.individuPrincipal.nom + dossier.individuPrincipal.prenom}
            endAdd={this.endAdd.bind(this)}
            occurence={dossier.individuPrincipal}
            dossierId={dossier._id}
            retrait={false}
          />
        </Grid>
        <Grid item xs={11}>
          {blocSecondaire}
        </Grid>
        <Grid item xs={11}>
          <BlocAdresse
            handleChange={this.handleChange}
            handleAdresse={this.handleAdresse}
            adresse={this.state.adresse}
            dossier_id={dossier._id}
          />
        </Grid>
      </Grid>
    )
  }
}

BlocRenseignement.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
  }
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(BlocRenseignement));