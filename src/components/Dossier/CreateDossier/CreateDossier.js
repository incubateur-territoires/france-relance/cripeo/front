import React, { Component } from 'react';
import withStyles from '@mui/styles/withStyles';
import { connect } from "react-redux";
import { withRouter } from 'react-router-dom';
import { Box, DialogContentText, DialogActions, DialogContent, Paper, Grid } from '@mui/material';
import { Adresse, FormIndividu, StepperDefault, TH, InputDefault, Prompt, ButtonText } from '../../composants_internes';
import { addDossier, updateAdresse, updateReference, clearDossier } from "../../../actions/dossier";
import { addNotification } from "../../../actions/notifications";
import BlocReferenceHook from '../BlocReference';

const styles = theme => ({
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  },
  button: {
    backgroundColor: '#22671d',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: 'orange'
    }
  },
});


class CreateDossier extends Component {
  constructor(props) {
    super(props);
    this.state = {
      commentaire: "",
      adresse: null,
      openVerif: false,
      individu: null,
      dossier: null,
      reference: null,
      openCreate: false,
    }
  }

  handleClearState = () => {
    this.setState({
      commentaire: "",
      adresse: null,
      individu: null,
      dossier: null,
      reference: null,
    })
  }

  setFormAdresse = (adresse) => {
    this.setState({ adresse });
  }

  setFormCreateIndividu = (individu) => {
    this.setState({ individu });
  }

  setFormReference = (reference) => {
    this.setState({ reference });
  }

  submitCreateIndividu = async () => {
    const { individu } = this.state;
    const { refIndividu, commentaire } = individu;
    const { civilite, dateNaissance, lieuNaissance, nationalite, nom, prenom } = individu.formIndividu;

    const obj = {
      individuPrincipal: {
        civilite: civilite || "",
        nom: nom || "",
        prenom: prenom || "",
        dateNaissance: dateNaissance || "",
        communeNaissance: lieuNaissance || "",
        paysNaissance: nationalite || "",
        commentaire: commentaire || "",
        refIndividu: { ref: refIndividu, orga: this.props.auth ? this.props.auth.organisation.acteur_id : null }
      },
      individuPrincipalNom: nom + " " + prenom || "",
      acteur_id: this.props.auth ? this.props.auth.organisation.acteur_id : null
    };
    const dossier = await this.props.addDossier(obj);
    if (dossier) {
      this.setState({ dossier });
      this.props.addNotification({ variant: "success", message: "La création de l'individu est terminée" });
    }
  }

  submitAdresse = async () => {
    await this.props.updateAdresse({ id: this.state.dossier._id, adresse: this.state.adresse });
  }

  submitReferences = async () => {
    const obj = {
      id: this.state.dossier._id
    }
    obj.acteur_id = this.props.auth?.organisation?.acteur_id;
    obj.numero = this.state.reference?.referenceDossier;
    try {
      const traitement = await this.props.updateReference(obj);
      if (traitement) {
        this.props.addNotification({ variant: "success", message: "Les références sont sauvegardées, la création du dossier est complète" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la modification du dossier",
          details: traitement.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification du dossier" });
    }

    this.props.history.push("/dossier/" + this.state.dossier._id);
  }

  handleChangeCivilite = (retour) => {
    this.setState({ civilite: retour });
  }

  handleAdresse = (adresse) => {
    this.setState({ adresse });
  };

  handleClose = async () => {
    if (this.props.handleClose) {
      this.handleClearState();
      await this.props.clearDossier();
      await this.props.handleClose();
    }
  };

  handleSubmit = async () => {
    const obj = {
      individuPrincipal: {
        civilite: this.state.civilite.toUpperCase() || "",
        nom: this.state.nom.toUpperCase() || "",
        prenom: this.state.prenom.toUpperCase() || "",
        dateNaissance: this.state.dateNaissance || "1800-10-05 11:00:00.000Z",
        communeNaissance: this.state.communeNaissance.toUpperCase() || "",
        paysNaissance: this.state.paysNaissance.toUpperCase() || "",
        commentaire: this.state.commentaire.toUpperCase() || "",
        refIndividu: { ref: this.state.referenceDossier, orga: this.props.auth ? this.props.auth.organisation.acteur_id : null }
      },
      individuPrincipalNom: this.state.nom.toUpperCase() + " " + this.state.prenom.toUpperCase() || "",
      acteur_id: this.props.auth ? this.props.auth.organisation.acteur_id : null
    };

    const dossier = await this.props.addDossier(obj);
    if (dossier) {
      this.props.addNotification({ variant: "success", message: "L'opération s'est bien déroulée" });
      this.props.history.push("/dossier/" + dossier._id);
    }
  };

  submitForm = async () => {
    this.setState({ openCreate: false });
    this.handleClose();

    const { individu } = this.state;
    const { refIndividu, commentaire } = individu;
    const { civilite, dateNaissance, lieuNaissance, nationalite, nom, prenom } = individu.formIndividu;
    const dataDossier = {
      individuPrincipal: {
        civilite: civilite || "",
        nom: nom || "",
        prenom: prenom || "",
        dateNaissance: dateNaissance || "",
        communeNaissance: lieuNaissance || "",
        paysNaissance: nationalite || "",
        commentaire: commentaire || "",
        refIndividu: { ref: refIndividu, orga: this.props.auth ? this.props.auth.organisation.acteur_id : null },
        clos: false,
      },
      individuPrincipalNom: (nom || "") + " " + (prenom || "") || "",
      acteur_id: this.props.auth ? this.props.auth.organisation.acteur_id : null
    };

    dataDossier.adresse = this.state.adresse;

    const obj = {};
    obj.acteur_id = this.props.auth?.organisation?.acteur_id;
    obj.numero = this.state.reference ? this.state.reference?.referenceDossier : "";

    if (this.state.reference || this.props.auth) {
      dataDossier.numeroApplication = [].concat(obj);
    }
    const dossier = await this.props.addDossier(dataDossier);
    if (dossier) {
      this.setState({ dossier });
      this.props.history.push("/dossier/" + dossier._id);
      this.setState({ openCreate: false });
      this.handleClose();
    }
  }

  handleConfirmCreate = () => {
    this.setState({ openCreate: true });
  }
  handleCloseConfirmCreate = () => {
    this.setState({ openCreate: false });
  }

  render() {
    const { openCreate } = this.state;

    return (
      <Prompt
        titre={"Création d'un dossier"}
        open={this.props.open}
        onClose={this.handleClose}
        fullWidth
        maxWidth={"lg"}
      >
        <DialogContent>
          {this.props.open &&
            <StepperDefault
              steps={[
                {
                  label: "Création de l'individu",
                  component: <FormIndividuSolis
                    valuesForm={this.state.individu}
                    setForm={this.setFormCreateIndividu}
                  />,
                },
                {
                  label: "Adresse de l'individu",
                  component: <Adresse
                    label="Adresse de l'individu"
                    valuesForm={this.state.adresse}
                    setForm={this.setFormAdresse}
                    stepper
                  />,
                  prec: true
                },
                {
                  label: "Référence dossier",
                  component: <BlocReferenceHook
                    stepper
                    valuesForm={this.state.reference}
                    setForm={this.setFormReference}
                  />,
                  submitFunction: this.handleConfirmCreate,
                  prec: true
                }
              ]}
              handleClose={this.handleClose}
              labelEnd="Enregistrer le dossier"
            />
          }

          {openCreate &&
            <Prompt
              titre={"Création d'un dossier"}
              open={openCreate}
              keepMounted
              onClose={this.handleCloseConfirmCreate.bind(this)}
              actions={
                <DialogActions>
                  <ButtonText
                    action={this.handleCloseConfirmCreate.bind(this)}
                    color="info"
                    texte="Annuler"
                    variant="text"
                  />
                  <ButtonText
                    action={this.submitForm}
                    color="primary"
                    texte="Valider"
                  />
                </DialogActions>
              }
            >
              <DialogContent>
                <DialogContentText id="Création d'un dossier">
                  Êtes-vous sûr(e) des informations saisies?
                </DialogContentText>
              </DialogContent>

            </Prompt>
          }
        </DialogContent>

      </Prompt>
    )
  }
}

const FormIndividuSolis = ({ setForm, valuesForm }) => {
  const [commentaire, setCommentaire] = React.useState(valuesForm?.commentaire || '');
  const [formIndividu, setFormIndividu] = React.useState(valuesForm?.formIndividu || {});
  // eslint-disable-next-line
  const [referenceDossier, setReferenceDossier] = React.useState(valuesForm?.referenceDossier || '');
  // eslint-disable-next-line
  const [refIndividu, setRefIndividu] = React.useState(valuesForm?.refIndividu || '')

  const setFormIndi = (data) => {
    setFormIndividu({ ...data });
  }

  React.useEffect(() => {
    setForm({
      commentaire,
      referenceDossier,
      formIndividu,
      refIndividu
    });
  }, [formIndividu, referenceDossier, commentaire, refIndividu, setForm]);



  return (
    <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
      <Box sx={{ marginBottom: 1 }}>
        <FormIndividu
          valuesForm={formIndividu}
          setForm={setFormIndi}
          label={"Identité"}
          stepper
        />
      </Box>
      <TH>Informations supplémentaires</TH>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
        <Grid container item xs={12} spacing={1}>
          <Grid container item spacing={2} xs={12}>
            <Grid item xs={12} md={12} >
              <InputDefault
                fullWidth
                setValue={setCommentaire}
                value={commentaire}
                label='Commentaire'
                multiline
              />
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Paper>
  )
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    addDossier: (dossier) => dispatch(addDossier(dossier)),
    updateAdresse: (obj) => dispatch(updateAdresse(obj)),
    updateReference: (obj) => dispatch(updateReference(obj)),
    clearDossier: () => dispatch(clearDossier()),
  }

}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateDossier)));