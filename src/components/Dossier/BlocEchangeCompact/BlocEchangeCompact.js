import React, { useEffect, useState, useCallback } from 'react';
import moment from 'moment';
import makeStyles from '@mui/styles/makeStyles';
import { useSelector, useDispatch } from 'react-redux';
import {
  Card,
  CardContent,
  CardHeader,
  Typography,
  CardActionArea,
  Divider,
  Grid,
  Tooltip
} from '@mui/material';
import { Info, Lock, Notifications } from '@mui/icons-material';
import { blue, grey, red } from '@mui/material/colors';
import { estDestinataire, echangeEstLu } from '../../../actions/echange';
import WrapperEchange from '../../Echange/WrapperEchange';
import { getActeurs } from "../../../actions/acteur";

const useStyles = makeStyles(theme => ({
  cardTitle: {
    display: "flex",
    fontSize: '1.8rem',
    fontWeight: 'bold',
    color: grey['800'],
    alignItems: "center",
    minHeight: '80px',
  },
  title: {
    fontWeight: 'bold',
    color: blue['700'],
  },
  NonLu: {
    color: blue['700'],
    fontWeight: 'bold',
    justifyContent: 'flex-start',
    padding: '7px 0 0 0',
  },
  cardLabel: { padding: '7px 0 7px 0', fontWeight: '200', color: grey['600'], fontSize: '0.8rem', },
  cardContent: {
    padding: '7px 0 7px 0',
    fontSize: '0.8rem',
    fontWeight: '600',
    color: grey['800'],
  },
  cardContentWarning: {
    padding: '7px 0 7px 0',
    fontSize: '0.8rem',
    fontWeight: '600',
    color: red['800'],
  },
  notificationUrgente: {
    paddingRight: 10,
    paddingTop: 10,
    color: red[400],
  },
  pourInfo: {
    paddingRight: 10,
    paddingTop: 10,
    color: grey['600']
  },
  pourInfoToolTipLi: {
    listStyle: 'none',
    fontSize: '0.8rem',
  },
  cloture: {
    paddingLeft: 10,
    paddingTop: 10,
    color: grey[800],
  },
}));

export default function BlocEchangeCompact({ echanges, dossier, documents, acteurId }) {

  const createdEchange = useSelector(state => state.echangeCreated);
  const [echange, setEchange] = useState(createdEchange || null);
  const [openEchange, setOpenEchange] = useState(!!createdEchange);
  const classes = useStyles();
  const auth = useSelector(state => state.auth);
  const acteurs = useSelector(state => state.acteurs);

  const dispatch = useDispatch();

  const cbGetActeurs = useCallback(
    () => dispatch(getActeurs()),
    [dispatch]
  );

  const cbEchangeEstLu = useCallback((data) => {
    return dispatch(echangeEstLu(data));
  }, [dispatch]);

  useEffect(() => {
    cbGetActeurs();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (echange && !createdEchange && echanges && Array.isArray(echanges)) {
      if (echanges.findIndex(e => e._id === echange._id) !== -1) {
        const ech = echanges[echanges.findIndex(e => e._id === echange._id)];
        let actionSorted = ech.action.sort((a, b) =>
          new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1,
        );
        let nextResponse;
        if (actionSorted && actionSorted[0]) {
          nextResponse = actionSorted[0];
        }
        if (nextResponse) ech.urgence = nextResponse.urgent;
        setEchange(ech);
      }
    } else if (echange && createdEchange) {
      let actionSorted = createdEchange.action.sort((a, b) =>
        new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1,
      );
      let nextResponse;

      if (actionSorted && actionSorted[0]) {
        nextResponse = actionSorted[0];
      }

      if (nextResponse) createdEchange.urgence = nextResponse.urgent;
      setEchange(createdEchange);
    }
  }, [echange, createdEchange, echanges]);

  const handleOpenEchange = _echange => async _event => {
    _event.stopPropagation();
    if (_echange.estLu === undefined || _echange.estLu.toString() === "2020-01-01T00:00:00.000Z") {
      if (_echange.action[0].destinataire_id.toString() === auth.organisation.acteur_id.toString()) {
        const data = { id: _echange._id, date: new Date() };
        await cbEchangeEstLu(data);
      }
    }
    setEchange(_echange);
    setOpenEchange(true);
  };

  const handleCloseEchange = () => {
    setOpenEchange(false);
    setEchange(null);
  };

  const echangeAction = echanges.map(ech => {
    let echAction = { ...ech };
    let actionSorted = echAction.action.sort((a, b) =>
      new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1,
    );
    let nextResponse;
    if (actionSorted && actionSorted[0]) {
      nextResponse = actionSorted[0];
    }

    if (!nextResponse) nextResponse = null;
    else {
      const doitRepondre = estDestinataire(
        nextResponse.destinataire_id,
        auth.organisation.acteur_id,
        auth.equipes,
      );
      echAction.urgence = nextResponse.urgent;
      echAction.doitRepondre = doitRepondre;
    }
    echAction.nextResponse = nextResponse;
    return echAction;
  });

  // trie date update + repondre + clos
  const echangesTri = echangeAction.sort((a, b) => {
    let bonusA = a.clos ? -5 : 5;
    bonusA += a.doitRepondre ? 3 : 0;
    let bonusB = b.clos ? -5 : 5;
    bonusB += b.doitRepondre ? 3 : 0;
    bonusA += a.updated_at > b.updated_at ? 1 : -1;
    return bonusA > bonusB ? -1 : 1;
  });


  const cards = echangesTri.map(echange => {
    const acteurList = echange.information.map(acteur => {
      return (
        <li key={acteur.entite + acteur._id} className={classes.pourInfoToolTipLi}>
          {acteur.entite}
        </li>
      );
    });

    const estLu = (!echange.estLu || echange.estLu.toString() === "2020-01-01T00:00:00.000Z") &&
      (echange.action[0].destinataire_id.toString() === auth.organisation.acteur_id.toString());

    return (
      <Grid item xs={6} lg={4} xl={3} key={`echange_${echange._id}`}>
        <Card sx={{ borderLeft: estLu ? 3 : "", borderLeftColor: estLu ? blue[600] : "", backgroundColor: echange.clos ? grey[200] : "" }}>
          <CardActionArea onClick={handleOpenEchange(echange)}>
            <CardHeader
              title={<Typography className={estLu ? classes.title : ""}>{echange.categorie.libelle}</Typography>}
              avatar={!!echange.clos ? <Lock className={classes.cloture} fontSize="small" /> : <div />}
              className={classes.cardTitle}
              action={
                <React.Fragment>
                  {echange.urgence ?
                    <Notifications className={classes.notificationUrgente} /> : <div />
                  }
                  {echange.information && echange.information.length > 0 ?
                    <Tooltip
                      title={
                        <div>
                          <Typography>Pour information</Typography>
                          <ul>
                            {acteurList}
                          </ul>
                        </div>
                      }
                    >
                      <Info className={classes.pourInfo} />
                    </Tooltip>
                    :
                    <div />
                  }
                </React.Fragment>
              }
            />
            <Divider />
            <CardContent>
              <Grid container direction="column" minHeight={'235px'} >
                {CardItem(classes, { title: "Emetteur", value: echange.categorie.emetteur })}
                {CardItem(classes, { title: "Destinataire", value: echange.categorie.destinataire })}
                {CardItem(classes, { title: "Echange créé le", value: moment(echange.created_at).format('DD-MM-YYYY HH:mm') })}
                {CardItem(classes, { title: "Dernière mise à jour", value: moment(echange.updated_at).format('DD-MM-YYYY HH:mm') })}
                <Divider />
                {CardItem(classes, { title: "Actions", value: echange.action.length })}
                {auth && echange.nextResponse && !echange.clos && (
                  <Grid container item>
                    <Grid item>
                      <Typography align="left" className={classes.cardLabel}>
                        A effectuer :&nbsp;
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Typography
                        className={
                          echange.doitRepondre
                            ? classes.cardContentWarning
                            : classes.cardContent
                        }>
                        {!echange.clos && echange.nextResponse && echange.nextResponse.destinataire}{' '}
                        doit répondre
                      </Typography>
                    </Grid>
                  </Grid>
                )
                }
                {echange.estLu && echange.estLu.toString() !== "2020-01-01T00:00:00.000Z" ?
                  CardItem(classes, { title: "Lu le", value: echange.estLu ? moment(echange.estLu).format('DD-MM-YYYY') : "" })
                  :
                  echange.action[0].destinataire_id.toString() === auth.organisation.acteur_id.toString() ?
                    <Grid item>
                      <Typography align="left" className={classes.NonLu}>
                        Non Lu
                      </Typography>
                    </Grid>
                    :
                    ""
                }
              </Grid>
            </CardContent>
          </CardActionArea>
        </Card>
      </Grid>
    );
  });

  return (
    <Grid container spacing={3} >
      {cards}
      {echange && (
        <WrapperEchange
          dossierId={dossier._id}
          acteurId={acteurId}
          open={openEchange}
          echange={echange}
          onClose={handleCloseEchange}
          acteurs={acteurs}
          documents={documents}
        />
      )}
    </Grid>
  );
}

function CardItem(classes, values) {
  return <Grid container item>
    <Grid item>
      <Typography align="left" className={classes.cardLabel}>
        {values.title} :&nbsp;
      </Typography>
    </Grid>
    <Grid item>
      <Typography className={classes.cardContent}>
        {values.value}
      </Typography>
    </Grid>
  </Grid>;
}

