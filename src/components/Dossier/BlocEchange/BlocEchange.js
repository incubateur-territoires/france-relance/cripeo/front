import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { Grid, Typography, AccordionActions, CircularProgress } from "@mui/material";
import Echange from "../../Echange";
import Ajouter from "../../Commun/Bouton/Ajouter";
import AjoutEchange from "../../Echange/AjoutEchange/AjoutEchange";
import { getCategorieByActeur } from "../../../actions/categorie";
import { getActeurs } from "../../../actions/acteur";
import { estActeurAction } from '../../../actions/action';
import { estActeurInfosEchange } from '../../../actions/echange';

const styles = theme => ({
    paper: {
        padding: theme.spacing(3, 2),
        margin: theme.spacing(1),
        width: '100%',
        textAlign: 'left'
    },
    button: {
        backgroundColor: '#22671d',
        color: '#ffffff',
        '&:hover': {
            backgroundColor: 'orange'
        }
    },
});

class BlocEchange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openEchange: false,
        };
    }

    componentDidMount = async () => {
        if (this.props.auth) {
            let acteur_id = this.props.auth.organisation.acteur_id;
            if (acteur_id) {
                this.props.getCategorieByActeur(acteur_id);
            }
        }
        await this.props.getActeurs();
    };

    handleOpenEchange = () => {
        this.setState({ openEchange: true });
    };

    handleClose = () => {
        this.setState({ openEchange: false });
    };

    delete = async (id) => {
        await this.props.delete(id);
    }

    render = () => {
        const { classes, echanges, dossier, utilisateur, documents, auth } = this.props;

        if (!echanges || !auth)
            return <div><CircularProgress /></div>;

        let blocEchange = "";
        let droitAjoutEchange = false;
        const acteur_id = utilisateur && utilisateur.acteur_id ? utilisateur.acteur_id : false;
        //todo droitAjout ne fonctionne pas
        if (echanges.length > 0) {
            blocEchange = Object
                .keys(echanges.reverse())
                .map(key => {
                    if (!droitAjoutEchange && echanges[key].action && echanges[key].action[0] && (estActeurAction(echanges[key].action[0], auth.organisation.acteur_id, auth.equipes) ||
                        estActeurInfosEchange(echanges[key].information, auth.organisation.acteur_id, auth.equipes)
                    )) droitAjoutEchange = true;
                    return <Grid container item xs={12} key={echanges[key]._id}>
                        <Echange
                            key={echanges[key]._id}
                            echange={echanges[key]}
                            dossier={dossier}
                            documents={documents}
                            delete={this.delete.bind(this)}
                            acteurs={this.props.acteurs}
                        />
                    </Grid>
                }
                );
        }
        droitAjoutEchange = true;

        return (
            <Grid container>
                {droitAjoutEchange &&
                    <AjoutEchange
                        open={this.state.openEchange}
                        onClose={this.handleClose}
                        dossier_id={this.props.key}
                        dossier={dossier}
                        deleteFile={this.delete.bind(this)}
                    />
                }
                <Grid container item>
                    <AccordionActions>
                        <Grid container item xs={12} className={classes.root}>
                            <Grid container item spacing={2} alignItems="center" className={classes.pointligne}>
                                <Grid item onClick={this.handleOpenEchange}>
                                    <Ajouter action={this.ajouter} texte={"Ajouter un échange"} />
                                </Grid>
                                <Grid item>
                                    <Typography variant="overline">Ajouter un échange</Typography>
                                </Grid>
                            </Grid>
                            <Grid item xs={12}>
                                {blocEchange}
                            </Grid>
                        </Grid>
                    </AccordionActions>
                </Grid>
            </Grid>
        )
    }
}

function mapStateToProps(state) {
    return {
        categories: state.categories,
        acteurs: state.acteurs,
        utilisateur: state.utilisateur,
        auth: state.auth
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCategorieByActeur: (id) => dispatch(getCategorieByActeur(id)),
        getActeurs: () => dispatch(getActeurs()),
    }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BlocEchange));