import React from "react";
import { ButtonText } from "../../composants_internes";
import PrintOutlinedIcon from "@mui/icons-material/PrintOutlined";
import { useSelector } from 'react-redux';
import {
    usePDF,
    Page,
    Text,
    View,
    Document,
    StyleSheet,
} from '@react-pdf/renderer';


export default function ButtonExport(props) {
    const styles = StyleSheet.create({

        h1: {
            fontSize: "24px",
            height: "40px",
        },
        h2: {
            fontSize: "18px",
            textAlign: "center",
            height: "36px"
        },
        refdossier: {
            fontSize: "14px",
            border: "1px solid black"
        },
        row: {
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottom: '1px solid #979797',
            width: '100%',
            minHeight: '15px',
        },
        cell: {
            display: 'flex',
            textAlign: 'center',
            flex: 1,
            justifyContent: 'space-around',
            alignItems: 'center',
        },
        comment_cell: {
            display: 'flex',
            textAlign: 'center',
            flex: 3,
            justifyContent: 'space-around',
            alignItems: 'center',
        },
        sender_cell: {
            display: 'flex',
            textAlign: 'center',
            flex: 2,
            justifyContent: 'space-around',
            alignItems: 'center',
        }


    });

    const { dossier, echanges, acteurs } = props;
    const auth = useSelector(state => state.auth);

    const myDoc = (dossier, echanges) => {
        return (
            <Document>
                <Page size="A4" style={styles.page}>
                    <View style={{ textAlign: "center" }}>
                        <Text style={styles.h1}>
                            BORDEREAU RECAPITULATIF
                        </Text>
                        <Text style={styles.tableau}>
                            SITUATION : {dossier.individuPrincipal.civilite ? dossier.individuPrincipal.civilite : "" } {" "}
                            {dossier.individuPrincipal.nom ? dossier.individuPrincipal.nom.toUpperCase() : ""}
                            {" "}
                            {dossier.individuPrincipal.prenom ? dossier.individuPrincipal.prenom : ""}
                        </Text>
                        <Text style={{ fontSize: "14px", marginTop: 5 }}>
                            {dossier.individuPrincipal.dateNaissance ?
                                "Né le " + new Date(dossier.individuPrincipal.dateNaissance).toLocaleDateString("fr-FR") : ""}
                            {dossier.individuPrincipal.communeNaissance ? " à " + dossier.individuPrincipal.communeNaissance : ""}
                        </Text>
                        <Text style={{ fontSize: "14px" }}>
                            Adresse : {dossier.adresse.voie}
                            {dossier.adresse.commune ? ", " + dossier.adresse.commune : ""}
                            {dossier.adresse.cp ? " " + dossier.adresse.cp : ""}
                            {dossier.adresse.lieudit ? " (" + dossier.adresse.lieudit + ")" : " "}
                        </Text>
                        {acteurs.map((acteur, index) => acteur.secteurGeo.actif && acteur._id.toString() === auth.organisation.acteur_id.toString() &&
                            <Text key={index+"secteurGeo"} style={{ fontSize: "14px" }}>
                                {dossier.secteurGeo.length > 0 ? dossier.secteurGeo[0].libelle ? "Secteur Géographique : " + dossier.secteurGeo[0].libelle : "" : ""}
                            </Text>
                        )}
                        {dossier.numeroApplication.map((ref, index) => {
                            const entite = acteurs.find(acteur => acteur._id === ref.acteur_id)?.entite
                            if (auth.organisation.acteur_id.toString() === ref.acteur_id.toString() && ref.numero !== "") {
                                return (
                                    <Text style={{ marginTop: "3px" }} key={index+"numeroApplication"}>
                                        <Text style={styles.refdossier}>Ref Dossier {entite ? entite : ""} : </Text>
                                        <Text style={styles.refdossier}> {ref.numero}</Text>
                                    </Text>
                                );
                            }
                            return <Text key={index+"numeroApplication"}> </Text>;
                        })}
                    </View>
                    <View style={{ marginTop: "5px" }}>
                        {echanges.map(echange => <View key={`echange_${echange._id}`} >
                            <View  style={{ borderTop: "1px solid black", borderBottom: "1px solid black" }}>
                                <Text style={{ marginTop: 20, padding: "3px", fontSize: "20px", fontWeight: "900", textAlign: "center" }}>
                                    Echange {echange.categorie?.libelle} du {new Date(echange.created_at).toLocaleDateString("fr-FR")}
                                </Text>
                                <Text style={{ marginTop: "3px", padding: "3px", fontSize: "16px", fontWeight: "900", textAlign: "center" }}>
                                    Commentaires de l'échange : {echange.commentairePublic.toUpperCase()}
                                </Text>
                            </View>
                            <View style={[styles.row, { marginTop: "5px", fontSize: "12px", }]}>
                                <Text style={styles.sender_cell}>Service Emetteur :</Text>
                                <Text style={styles.cell}>Date :</Text>
                                <Text style={styles.comment_cell}>Commentaires :</Text>
                            </View>

                            {echange.action.map(action => <View style={{ diplay: "flex" }} key={`action${action._id}`}>
                                <View style={[styles.row, { fontSize: "12px", }]} >
                                    <Text style={styles.sender_cell}>{action.emetteur}</Text>
                                    <Text style={styles.cell}>{new Date(action.dateRealisation).toLocaleDateString("fr-FR")}</Text>
                                    <Text style={styles.comment_cell} hyphenationCallback={word => [word]}>{action.commentairePublic ? action.commentairePublic : `action : ${action.libelle}`}</Text>
                                </View>
                            </View>)}
                        </View>
                        )}
                    </View>
                </Page>
            </Document>
        )
    }

    const [instance] = usePDF({ document: myDoc(dossier, echanges) });

    const onDownload = () => {
        const link = document.createElement('a');
        link.download = `historique.pdf`;
        link.href = instance.url || '';
        link.click();
    };

    return (

        <ButtonText
            variant="text"
            icon={<PrintOutlinedIcon />}
            texte="Export PDF"
            action={onDownload}
        />

    )

}
