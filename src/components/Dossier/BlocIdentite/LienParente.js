import React from 'react';
import Input from "../../Commun/Input/Input";

export default function LienParente({ value, handleChange, disabled }) {
    return (
        <Input
            label="Lien de Parenté"
            name="lienParent"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
            multiline={true}
            fullWidth={true}
        />
    )
}



