import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import { Grid, Paper, Tooltip, Box } from '@mui/material';
import { addNotification } from "../../../actions/notifications";
import ConfirmDialog from "../../Commun/Confirmation/ConfirmDialog";
import Commentaire from "./Commentaire";
import LienParente from './LienParente';
import { removeIndividu, updateIndividu } from "../../../actions/dossier";
import Annuler from "../../Commun/Bouton/Annuler";
import Supprimer from "../../Commun/Bouton/Supprimer";
import { ButtonDelete, ButtonEdit, ButtonValidForm, FormIndividu, TH, ButtonText } from "../../composants_internes";


const styles = theme => ({
  "&MuiInput-root": {
    color: "#5775ec",
  },
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    flex: 1,
    textAlign: 'left'
  },
  addButton: {
    color: "#5775ec",
    marginTop: theme.spacing(-20),
  },
  removeButton: {
    color: "#9d0a3a",
    border: "solid",
  },
  editButton: {
    color: "#2f8a20",
    border: "solid",
  },
  validButton: {
    bottom: 0,
    position: "absolute"
  },
  title: {
    textAlign: "left",
    marginBottom: theme.spacing(3),
    color: "#2F80ED",
    fontWeight: "bold",
  },
});



class BlocIdentite extends Component {

  state = {
    openDelete: false,
    civilite: !this.props.add ? this.props.occurence.civilite : "",
    nom: !this.props.add ? this.props.occurence.nom : "",
    prenom: !this.props.add ? this.props.occurence.prenom : "",
    dateNaissance: !this.props.add ? this.props.occurence.dateNaissance : "",
    communeNaissance: !this.props.add ? this.props.occurence.communeNaissance : "",
    paysNaissance: !this.props.add ? this.props.occurence.paysNaissance : "",
    refIndividu: (!this.props.add && this.props.occurence.refIndividu) ? this.props.occurence.refIndividu : "",
    commentaire: !this.props.add ? this.props.occurence.commentaire : " ",
    lienParent: !this.props.add ? this.props.occurence.lienParent : "",
    civiliteInitial: !this.props.add ? this.props.occurence.civilite : "",
    nomInitial: !this.props.add ? this.props.occurence.nom : "",
    prenomInitial: !this.props.add ? this.props.occurence.prenom : "",
    dateNaissanceInitial: !this.props.add ? this.props.occurence.dateNaissance : "",
    communeNaissanceInitial: !this.props.add ? this.props.occurence.communeNaissance : "",
    paysNaissanceInitial: !this.props.add ? this.props.occurence.paysNaissance : "",
    refIndividuInitial: (!this.props.add && this.props.occurence.refIndividu) ? this.props.occurence.refIndividu.toString() : "",
    commentaireInitial: !this.props.add ? this.props.occurence.commentaire : " ",
    modeEdition: this.props.enabled || false,
    formIdentite: {
      civilite: !this.props.add ? this.props.occurence.civilite : "",
      nom: !this.props.add ? this.props.occurence.nom : "",
      prenom: !this.props.add ? this.props.occurence.prenom : "",
      dateNaissance: !this.props.add ? this.props.occurence.dateNaissance : "",
      lieuNaissance: !this.props.add ? this.props.occurence.communeNaissance : "",
      nationalite: !this.props.add ? this.props.occurence.paysNaissance : "Française",
    }
  }
  componentDidMount = () => {
    const refIndividu = this.state.refIndividu
    if (refIndividu.findIndex(elem => elem.acteur_id === this.props.auth.organisation.acteur_id) !== -1) {
      const numero = refIndividu[refIndividu.findIndex(elem => elem.acteur_id === this.props.auth.organisation.acteur_id)].numero;
      this.setState({ refIndividu: numero, refIndividuInitial: numero });
    }
    else {
      this.setState({ refIndividu: "", refIndividuInitial: "" });
    }
  }

  componentDidUpdate = (prevProps) => {
    if (!this.props.add && this.props.dossierId !== prevProps.dossierId) {
      this.setState({
        civilite: !this.props.add ? this.props.occurence.civilite : "",
        nom: !this.props.add ? this.props.occurence.nom : "",
        prenom: !this.props.add ? this.props.occurence.prenom : "",
        dateNaissance: !this.props.add ? this.props.occurence.dateNaissance : "",
        communeNaissance: !this.props.add ? this.props.occurence.communeNaissance : "",
        paysNaissance: !this.props.add ? this.props.occurence.paysNaissance : "",
        refIndividu: (!this.props.add && this.props.occurence.refIndividu) ? this.props.occurence.refIndividu.toString() : "",
        commentaire: !this.props.add ? this.props.occurence.commentaire : "",
      });
    }
  }

  handleChange = async (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    await this.setState({ [name]: value });
    if (this.props.wrapping) {
      this.props.wrapping(this.state);
    }
  };

  openConfirmDeleteDialog = (event) => {
    event.stopPropagation();
    this.setState({ openDelete: true });
  }

  closeConfirmDeleteDialog = () => {
    this.setState({ openDelete: false });
  }

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({ modeEdition: !this.state.modeEdition });
  }

  cancelEdit = async () => {
    await this.setState({
      civilite: this.state.civiliteInitial,
      nom: this.state.nomInitial,
      prenom: this.state.prenomInitial,
      dateNaissance: this.state.dateNaissanceInitial,
      communeNaissance: this.state.communeNaissanceInitial,
      paysNaissance: this.state.paysNaissanceInitial,
      refIndividu: this.state.refIndividuInitial,
      commentaire: this.state.commentaireInitial,
    });
    await this.setState({ modeEdition: false });
  }

  handleSubmitIndividu = async () => {
    let objet = {
      civilite: this.state.civilite,
      nom: this.state.nom,
      prenom: this.state.prenom,
      dateNaissance: this.state.dateNaissance,
      communeNaissance: this.state.communeNaissance,
      paysNaissance: this.state.paysNaissance,
      refIndividu: [this.state.refIndividu, this.props.auth ? this.props.auth.organisation.acteur_id : null],
      commentaire: this.state.commentaire,
    }
    await this.props.handleSubmitIndividu(objet);
  }

  setFormIdentite = (form) => {
    this.setState({ formIdentite: form });
  }

  handleCloseIndividu = async () => {
    await this.props.handleCloseIndividu();
  }

  deleteIndividu = async () => {
    const obj = {
      id: this.props.dossier._id
    };
    obj.individu_id = this.props.occurence._id;
    try {
      const traitement = await this.props.removeIndividu(obj);
      if (traitement.success) {
        this.props.addNotification({ variant: "success", message: "L'individu a bien été supprimé." });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la suppression",
          details: traitement.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la suppression" });
    }
  }

  updateinfosIndividu = async () => {
    const obj = {
      id: this.props.dossier._id
    }
    const nouvDateNaiss = !this.state.formIdentite.dateNaissance ? "" : (new Date(new Date(this.state.formIdentite.dateNaissance).setHours(13)))
    obj.individu_id = this.props.occurence._id;
    obj.civilite = this.state.formIdentite.civilite ? this.state.formIdentite.civilite : "";
    obj.nom = this.state.formIdentite.nom ? this.state.formIdentite.nom : "";
    obj.prenom = this.state.formIdentite.prenom ? this.state.formIdentite.prenom : " ";
    obj.dateNaissance = nouvDateNaiss ? nouvDateNaiss : "";
    obj.communeNaissance = this.state.formIdentite.lieuNaissance ? this.state.formIdentite.lieuNaissance : "";
    obj.paysNaissance = this.state.formIdentite.nationalite ? this.state.formIdentite.nationalite : "";
    obj.refIndividu = { ref: this.state.refIndividu.toString(), orga: this.props.auth?.organisation?.acteur_id };
    obj.lienParent = this.state.lienParent ? this.state.lienParent : "";
    obj.commentaire = this.state.commentaire ? this.state.commentaire : "";
    if (this.props.retrait) {
      // individu secondaire
      obj.cible = "individuSecondaire";
    } else {
      // individu principal
      obj.cible = "individuPrincipal";
    }
    try {
      await this.props.updateIndividu(obj);
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification du dossier" });
    }
    await this.setState({ modeEdition: false })
  }

  render() {

    const { retrait } = this.props;
    const {
      civilite,
      nom,
      prenom,
      dateNaissance,
      communeNaissance,
      paysNaissance,
      commentaire,
      openDelete,
      lienParent
    } = this.state;

    return (
      <Box sx={{ m: 2 }}>
        <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
          <Grid container item xs={12} spacing={2} justifyContent="flex-end" marginTop={5} >
            {!this.state.modeEdition &&
              <ButtonEdit action={this.changeEdition} />
            }
            {this.state.modeEdition && this.props.retrait &&
              <>
                <ButtonDelete action={this.openConfirmDeleteDialog.bind(this)} />
              </>
            }
          </Grid>
          <Box sx={{ marginBottom: 1 }}>
            <FormIndividu
              setForm={this.setFormIdentite}
              label={"Identité"}
              valuesForm={{
                civilite,
                nom,
                prenom,
                dateNaissance: dateNaissance ? new Date(dateNaissance) : "",
                lieuNaissance: communeNaissance,
                nationalite: paysNaissance,
              }}
              stepper
              disable={!this.state.modeEdition}
            />
          </Box>
          <TH>Informations supplémentaires</TH>
          <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
            <Grid container item xs={12} spacing={1}>
              <Grid container item spacing={1} xs={11}>
                {this.props.retrait && <Grid item xs={12} md={4} >
                  <LienParente
                    handleChange={this.handleChange.bind(this)}
                    value={lienParent}
                    disabled={!this.state.modeEdition}>
                  </LienParente>
                </Grid>
                }
                <Grid container item spacing={1} xs={12}>
                  <Grid item xs={12} md={12} >
                    <Commentaire
                      handleChange={this.handleChange.bind(this)}
                      value={commentaire}
                      disabled={!this.state.modeEdition}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
          <Grid container item xs={12} spacing={2} justifyContent="flex-end" marginTop={5}>
            {this.state.modeEdition &&
              <>
                <ButtonText
                  action={this.cancelEdit.bind(this)}
                  color="primary"
                  texte="Annuler"
                  variant="text"
                />
                <ButtonText
                  action={this.updateinfosIndividu.bind(this)}
                  color="primary"
                  texte="Valider"
                  variant="outlined"
                />
              </>
            }
          </Grid>
        </Paper>

        {!this.props.add && <Grid container item xs={1}
          style={{ justifyContent: "flex-end" }}
        >
          <Grid item xs={6}>
          </Grid>
          <Grid item xs={6}>
            {retrait && false &&
              <Tooltip title="Supprimer cet individu">
                <Supprimer texte="Effacer cet individu" disable={!this.state.modeEdition} action={this.openConfirmDeleteDialog.bind(this)} />
              </Tooltip>
            }
          </Grid>
          <Grid item xs={6}>
            {this.state.modeEdition && false &&
              <Annuler texte="Annuler" action={this.cancelEdit.bind(this)} />
            }
          </Grid>
          <Grid item xs={6}>
            {this.state.modeEdition && false &&
              <ButtonValidForm action={this.updateinfosIndividu.bind(this)} />
            }
          </Grid>
        </Grid>
        }
        {!this.props.add &&
          <ConfirmDialog
            open={openDelete}
            handleCloseFunction={this.closeConfirmDeleteDialog.bind(this)}
            valideFunction={this.deleteIndividu.bind(this)}
            question="Voulez-vous supprimer ce individu ?"
            titre="Supression d'un individu"
          />
        }
      </Box>
    )
  }
}

BlocIdentite.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    dossier: state.dossier,
    auth: state.auth
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    removeIndividu: (obj) => dispatch(removeIndividu(obj)),
    updateIndividu: (obj) => dispatch(updateIndividu(obj)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BlocIdentite));