import React from 'react';
import Input from "../../Commun/Input/Input";

export default function Prenom({ value, handleChange, disabled }) {
    return (
        <Input
            label="Prénom"
            name="prenom"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



