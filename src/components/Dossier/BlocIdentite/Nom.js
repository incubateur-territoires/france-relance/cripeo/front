import React from 'react';
import Input from "../../Commun/Input/Input";

export default function Nom({ value, handleChange, disabled }) {
    return (
        <Input
            label="Nom"
            name="nom"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



