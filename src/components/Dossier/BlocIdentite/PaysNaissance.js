import React from 'react';
import Input from "../../Commun/Input/Input";

export default function PaysNaissance({ value, handleChange, disabled }) {
    return (
        <Input
            label="Pays de naissance"
            name="paysNaissance"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



