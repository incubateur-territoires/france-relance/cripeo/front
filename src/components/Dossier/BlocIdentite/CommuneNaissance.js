import React from 'react';
import Input from "../../Commun/Input/Input";

export default function CommuneNaissance({ value, handleChange, disabled }) {
    return (
        <Input
            label="Lieu de naissance"
            name="communeNaissance"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



