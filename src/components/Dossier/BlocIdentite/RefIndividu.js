import React from 'react';
import Input from "../../Commun/Input/Input";


export default function RefIndividu({ value, handleChange, disabled, props }) {
    return (
        <Input
            label="Réf. Individu"
            name="refIndividu"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
            fullWidth
        />
    )
}



