import React from 'react';
import Input from "../../Commun/Input/Input";

export default function Commentaire({ value, handleChange, disabled }) {
    return (
        <Input
            label="Commentaire"
            name="commentaire"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
            multiline={true}
            fullWidth={true}
        />
    )
}



