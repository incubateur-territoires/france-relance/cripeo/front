import React from 'react';
import { Grid } from '@mui/material';
import { useSelector } from 'react-redux'
import { SelectDefault } from '../../composants_internes';

export default function Civilite({ civilite, handleChange}) {
    const civilites = useSelector(state => state.civilites);

    const civiliteOptions = civilites.map(civilite => {
        return {
            value: civilite,
            label: civilite
        }
    })

    return (
        <Grid item xs={12} >
            <SelectDefault
                setValue={handleChange}
                value={civilite}
                label="Civilité"
                options={civiliteOptions}
            />
        </Grid>

    )
}