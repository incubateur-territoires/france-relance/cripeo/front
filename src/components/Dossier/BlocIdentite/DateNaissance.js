import React from 'react';
import DateInput from "../../Commun/Input/DateInput";

export default function DateNaissance({ value, handleChange, disabled }) {
    return (
        <DateInput
            label="Date de naissance"
            name="dateNaissance"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



