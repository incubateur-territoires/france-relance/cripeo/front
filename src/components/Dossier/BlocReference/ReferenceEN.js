import React from 'react';
import Input from "../../Commun/Input/Input";

export default function ReferenceEN({ value, handleChange, disabled }) {
    return (
        <Input
            label="Référence EN"
            name="numeroEN"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



