import React from 'react';
import Input from "../../Commun/Input/Input";

export default function ReferenceTPE({ value, handleChange, disabled }) {
  return (
    <Input
      label="Référence TPE"
      name="numeroTPE"
      value={value}
      handleChange={handleChange}
      disabled={disabled}
    />
  )
}



