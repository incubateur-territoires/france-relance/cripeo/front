import React from 'react';
import Input from "../../Commun/Input/Input";

export default function ReferenceCassiopee({ value, handleChange, disabled }) {
    return (
        <Input
            label="Référence CASSIOPEE"
            name="numeroCassiope"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



