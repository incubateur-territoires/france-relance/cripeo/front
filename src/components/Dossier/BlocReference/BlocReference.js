import React, { Component, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { Grid, Paper, Box } from '@mui/material';
import { addNotification } from "../../../actions/notifications";
import { updateReference } from "../../../actions/dossier";
import { InputDefault, TH, ButtonCancel, ButtonEdit, ButtonValidForm, ButtonText } from '../../composants_internes';
import { useSelector, useDispatch } from 'react-redux';


export default function BlocReferenceHook({ stepper, setForm }) {
  const dossier = useSelector(state => state.dossier);
  const acteur = useSelector(state => state.auth?.organisation?.acteur_id);
  const dispatch = useDispatch();
  const numAppli = dossier?.numeroApplication.filter(num => {
    return num.acteur_id === acteur;
  });

  const [referenceDossier, setReferenceDossier] = React.useState(numAppli?.[0]?.numero || '');
  const [modeEdition, setModeEdition] = React.useState(!!stepper);
  const cbUpdateReference = useCallback((obj) => {
    return dispatch(updateReference(obj));
  }, [dispatch]);

  const cbAddNotification = useCallback((obj) => {
    dispatch(addNotification(obj));
  }, [dispatch]);

  useEffect(() => {
    if (stepper && setForm) {
      setForm({ referenceDossier });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [stepper, referenceDossier, setForm]);

  const updateinfosReference = async () => {
    const obj = {
      id: dossier._id
    }
    obj.acteur_id = acteur;
    obj.numero = referenceDossier;

    try {
      const traitement = await cbUpdateReference(obj);
      if (traitement) {
        cbAddNotification({ variant: "success", message: "La modification s'est bien déroulée" });
      } else {
        cbAddNotification({
          variant: "error",
          message: "Erreur lors de la modification du dossier",
          details: traitement.error[0].msg
        });
      }
    } catch (err) {
      cbAddNotification({ variant: "error", message: "Erreur lors de la modification du dossier" });
    }
    setModeEdition(false)
  }
  const cancelEdit = async () => {
    await setReferenceDossier(referenceDossier || '')
    await setModeEdition(false);
  }

  return (
    <Box sx={{ m: 2 }}>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
        <Grid container item xs={12}>
          <Grid item xs={6}>
            <TH>Référence dossier</TH>
          </Grid>
          {!stepper && !modeEdition &&
            <Grid container item xs={6} justifyContent="flex-end">
              <ButtonEdit action={() => setModeEdition(!modeEdition)} />
            </Grid>
          }
        </Grid>
        <Grid container item xs={12} spacing={1}>
          <Grid item md={3} xs={6}>
            <InputDefault
              label="Référence Dossier"
              disable={!modeEdition}
              value={referenceDossier}
              setValue={setReferenceDossier}
              fullWidth
            />
          </Grid>
        </Grid>
        <Grid container item xs={12} spacing={2} display='inline-block'>
          {!stepper && modeEdition &&
            <Grid container item spacing={2} justifyContent="flex-end">
              <ButtonText
                action={cancelEdit}
                color="primary"
                texte="Annuler"
                variant="text"
              />
              <ButtonText
                action={updateinfosReference}
                color="primary"
                texte="Valider"
                variant="outlined"
              />
            </Grid>
          }
        </Grid>
      </Paper>
    </Box>
  )
}

class BlocReference extends Component {
  state = {
    referenceDossier: (this.props.dossier && this.props.dossier.numeroApplication && this.props.dossier.numeroApplication.numero) || "",
    referenceDossierInitial: (this.props.dossier && this.props.dossier.numeroApplication && this.props.dossier.numeroApplication.numero) || "",
    modeEdition: false,
  }

  setReferenceDossier = (referenceDossier) => {
    this.setState({
      referenceDossier,
    });
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({
      modeEdition: !this.state.modeEdition
    });
  }

  cancelEdit = async () => {
    await this.setState({
      referenceDossier: this.state.referenceDossierInitial,
    })
    await this.setState({ modeEdition: false })
  }

  updateinfosReference = async () => {

    const obj = {
      id: this.props.dossier._id
    }
    obj.numeroSolis = (this.state.numeroSolis) ? this.state.numeroSolis : " ";
    obj.numeroCassiope = this.state.numeroCassiope ? this.state.numeroCassiope : " ";
    obj.numeroTPE = this.state.numeroTPE ? this.state.numeroTPE : " ";
    obj.numeroEN = this.state.numeroEN ? this.state.numeroEN : " ";
    try {
      const traitement = await this.props.updateReference(obj);
      if (traitement) {
        this.props.addNotification({ variant: "success", message: "La modification s'est bien déroulée" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la modification du dossier",
          details: traitement.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification du dossier" });
    }
    await this.setState({ modeEdition: false })
  }

  render() {

    const { referenceDossier } = this.state;

    return (
      <Box sx={{ m: 2 }}>
        <TH>Références</TH>
        <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
          <Grid container item xs={12} spacing={1}>
            <Grid item md={3} xs={6}>
              <InputDefault
                label="Rérérence dossier"
                disable={!this.state.modeEdition}
                value={referenceDossier}
                setValue={this.setReferenceDossier}
                fullWidth
              />
            </Grid>
          </Grid>
        </Paper>
        <Grid container item xs={12} spacing={2} justifyContent="flex-end" sx={{ marginTop: 1 }}>
          {!this.state.modeEdition &&
            <ButtonEdit action={this.changeEdition} />
          }
          {this.state.modeEdition &&
            <>
              <ButtonCancel action={this.cancelEdit.bind(this)} />
              <ButtonValidForm action={this.updateinfosReference.bind(this)} />
            </>
          }
        </Grid>
      </Box>
    )
  }
}

BlocReference.propTypes = {
  classes: PropTypes.object.isRequired,
};
/* eslint-disable */
function mapStateToProps(state) {
  return {
    dossier: state.dossier,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    updateReference: (obj) => dispatch(updateReference(obj)),
  }
};

/* eslint-enable */