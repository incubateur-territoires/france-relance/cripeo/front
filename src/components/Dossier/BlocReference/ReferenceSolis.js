import React from 'react';
import Input from "../../Commun/Input/Input";

export default function ReferenceSolis({ value, handleChange, disabled }) {
    return (
        <Input
            label="Référence SOLIS"
            name="numeroSolis"
            value={value}
            handleChange={handleChange}
            disabled={disabled}
        />
    )
}



