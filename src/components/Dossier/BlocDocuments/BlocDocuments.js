import React, { useCallback } from 'react';
import { Box, Typography, CircularProgress } from "@mui/material";
import Uploader from "../../Commun/Uploader/Uploader";
import { getFile, gestiondroitsDocuments } from "../../../actions/document";
import { useDispatch } from 'react-redux';


export default function BlocDocuments({ handleDelete, documents, dossier, acteurId }) {
  const dispatch = useDispatch();

  const cbGetFile = useCallback((id) => {
    return dispatch(getFile(id));
  }, [dispatch]);

  const cbGestiondroitsDocuments = useCallback((data) => {
    return dispatch(gestiondroitsDocuments(data));
  }, [dispatch]);

  const validedroits = async (id, data) => {
    let donnees = {
      mainActeur: acteurId,
      dossierId: dossier._id,
      document_id: id,
      acteurs: data.map(act => { return { acteur_id: act } })
    }
    await cbGestiondroitsDocuments(donnees);
  };

  const del = async (id) => {
    await handleDelete(id);
  };

  if (!documents) {
    return <div><CircularProgress /></div>;
  };

  return (
    <Box sx={{ width: "95%" }}>
      <Typography variant="h6" marginBottom={3} sx={{ textAlign: "left", color: "#2F80ED", fontWeight: "bold" }}>Documents</Typography>
      <Uploader
        acteurId={acteurId}
        documents={documents || []}
        id={dossier._id}
        delete={del}
        getFile={cbGetFile}
        type="doc"
        gestiondroits={validedroits}
      />
    </Box>
  )
}
