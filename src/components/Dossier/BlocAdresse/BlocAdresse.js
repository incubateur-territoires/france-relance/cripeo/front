import React, { useCallback } from 'react';
import makeStyles from '@mui/styles/makeStyles';
import { Box, Grid, Paper } from '@mui/material';
import { useDispatch } from 'react-redux';
import { updateAdresse } from '../../../actions/dossier';
import { Adresse, ButtonEdit, ButtonText } from '../../composants_internes';

const useStyles = makeStyles(theme => ({
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  },
  title: {
    textAlign: "left",
    marginBottom: theme.spacing(3),
    color: "#2F80ED",
    fontWeight: "bold",
  },
}));

export default function BlocAdresse({ adresse, dossier_id }) {
  const [modeEdition, setModeEdition] = React.useState(false);
  const [formAdresse, setFormAdresse] = React.useState(adresse);
  const classes = useStyles() // eslint-disable-line
  const dispatch = useDispatch();

  const handleAdresse = useCallback(async () => {
    await dispatch(updateAdresse({ id: dossier_id, adresse: formAdresse }));
    setModeEdition(value => !value)
  }, [dispatch, dossier_id, formAdresse]);


  return (

    <Box sx={{ m: 2 }}>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
        {!modeEdition &&
          <Grid container item xs={12} spacing={2} justifyContent="flex-end" marginTop={5}>
            <ButtonEdit action={() => setModeEdition(value => !value)} />
          </Grid>
        }
        <Adresse
          setForm={setFormAdresse}
          label="Adresse de vie"
          valuesForm={adresse}
          stepper
          disable={!modeEdition}
        />
        <Grid container item xs={12} spacing={2} justifyContent="flex-end" marginTop={5}>
          {modeEdition && <>
            <ButtonText
              action={() => setModeEdition(value => !value)}
              color="primary"
              texte="Annuler"
              variant="text"
            />
            <ButtonText
              action={handleAdresse}
              color="primary"
              texte="Valider"
              variant="outlined"
            />
          </>
          }
        </Grid>
      </Paper>
    </Box>
  )
}
