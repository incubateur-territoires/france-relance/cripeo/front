import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Box, Paper, Grid, Typography } from '@mui/material';
import { TH, SelectDefault, ButtonEdit, ButtonText } from '../../composants_internes';
import { updateSecteurDossier } from '../../../actions/dossier';
import { getActeurs } from '../../../actions/acteur';


export default function BlocSecteurs({ dossier, stepper }) {
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth);
  const acteurs = useSelector(state => state.acteurs);

  useEffect(() => {
    if (!acteurs)
      dispatch(getActeurs());
  }, [acteurs, dispatch]);

  const secteurs = acteurs?.find(act => String(act._id) === String(auth.organisation.acteur_id))?.secteurs;
  const secteurGeo = dossier?.secteurGeo?.find(secteur => String(secteur.acteur_id) === String(auth.organisation.acteur_id));

  const [secteurAdministratif, setSecteurAdministratif] = useState(dossier?.secteurOrganisation?.find(secteur => String(secteur.acteur_id) === String(auth.organisation.acteur_id))?.secteur_id);
  const [modeEdition, setModeEdition] = useState(false);


  const options = secteurs?.map(sec => {
    return {
      label: sec.valeur,
      value: sec._id
    }
  }) || [];

  const cancelEdit = () => {
    setModeEdition(false);
    setSecteurAdministratif(dossier?.secteurOrganisation?.find(secteur => String(secteur.acteur_id) === String(auth.organisation.acteur_id))?.secteur_id);
  }

  const handleSubmit = () => {
    dispatch(updateSecteurDossier(dossier._id, auth.organisation.acteur_id, secteurAdministratif));
    setModeEdition(false);
  }

  return (
    <Box sx={{ m: 2, textAlign: "left" }}>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
        <Grid container item xs={12} marginTop={5}>
          <Grid item xs={6}>
            <TH>Secteur Administratif</TH>
          </Grid>
          {!stepper && !modeEdition &&
            <Grid container item xs={6} justifyContent="flex-end">
              <ButtonEdit action={() => setModeEdition(!modeEdition)} />
            </Grid>
          }
        </Grid>
        <Grid container item xs={12} spacing={1} marginBottom={secteurGeo ? 4 : 1} >
          <Grid item md={3} xs={6}>
            <SelectDefault
              label="Secteur Administratif"
              disable={!modeEdition}
              value={secteurAdministratif}
              setValue={setSecteurAdministratif}
              options={options}
            />
          </Grid>
        </Grid>
        {secteurGeo &&
          <TH>Secteur Géographique</TH>
        }
        <Grid container item xs={12} display='inline-block' >
          {secteurGeo &&
            <Grid item md={3} xs={5}>
              <Typography>
                {secteurGeo?.libelle}
              </Typography>
            </Grid>
          }
          {!stepper &&
            <Grid item xs={12}>
              {modeEdition &&
                <Grid container item spacing={2} justifyContent="flex-end">
                  <ButtonText
                    action={cancelEdit}
                    color="primary"
                    texte="Annuler"
                    variant="text"
                  />
                  <ButtonText
                    action={handleSubmit}
                    color="primary"
                    texte="Valider"
                    variant="outlined"
                  />
                </Grid>
              }
            </Grid>
          }
        </Grid>
      </Paper>
    </Box>
  )
}


