import React from 'react';
import Input from "../Commun/Input/Input";

export default function CommentairePublic({ value, handleChange, disabled, multiline, rows, required }) {
    return (
        <Input
            label="Commentaire"
            name="commentairePublic"
            value={value.toUpperCase()}
            handleChange={handleChange}
            disabled={disabled}
            multiline={multiline}
            rows={rows}
            required={required}
        />
    )
}



