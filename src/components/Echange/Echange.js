import React, { Component, Fragment } from 'react';
import { connect } from "react-redux";
import makeStyles from '@mui/styles/makeStyles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import {
  Grid,
  StepConnector,
  Typography,
  Paper,
  Switch,
  FormControlLabel,
  Box,
  IconButton,
  CircularProgress
} from "@mui/material";
import withStyles from '@mui/styles/withStyles';
import CommentairePublic from "./CommentairePublic";
import CommentairePrive from "./CommentairePrive";
import Editer from "../Commun/Bouton/Editer";
import Timeline from '@mui/lab/Timeline';
import { Accessibility, AccountBalance, MoreHoriz } from '@mui/icons-material';
import 'moment/locale/fr';
import LigneAction from "./LigneAction";
import AjoutAction from "./AjoutAction";
import {
  setEchange,
  uploadDocument,
  switchCloture,
  gestionInformationEchange,
  estActeurEchange,
  estDestinataire,
  estEmetteur,
  deleteFile
} from "../../actions/echange";
import { getFile } from "../../actions/document";
import { addNotification } from "../../actions/notifications";
import { estActeurAction } from '../../actions/action';
import { red, grey } from '@mui/material/colors';
import WrapperOptions from './WrapperOptions'
import { ButtonAdd, ButtonText } from '../composants_internes';


const backGroundClosPaper = grey[100];
const backGroundClosAction = grey[200];

// eslint-disable-next-line
const ColorLibConnector = withStyles({
  alternativeLabel: {
    top: 5,
  },
  active: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg, rgb(32,163,158) 0%, rgb(47,128,237) 50%, rgb(38,61,66) 100%)',
    },
  },
  completed: {
    '& $line': {
      backgroundImage:
        'linear-gradient( 95deg, rgb(32,163,158) 0%, rgb(47,128,237) 50%, rgb(38,61,66) 100%)',
    },
  },
  line: {
    height: 3,
    border: 0,
    backgroundColor: '#eaeaf0',
    borderRadius: 1,
  },
})(StepConnector);

const useColorLibStepIconStyles = makeStyles({
  root: {
    backgroundColor: '#ccc',
    zIndex: 1,
    color: '#fff',
    width: 20,
    height: 20,
    display: 'flex',
    borderRadius: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  active: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(32,163,158) 0%, rgb(47,128,237) 50%, rgb(38,61,66) 100%)',
    boxShadow: '0 4px 10px 0 rgba(0,0,0,.25)',
  },
  completed: {
    backgroundImage:
      'linear-gradient( 136deg, rgb(32,163,158) 0%, rgb(47,128,237) 50%, rgb(38,61,66) 100%)',
  },
});

function ColorLibStepIcon(props) {
  const classes = useColorLibStepIconStyles();
  const { active, completed } = props;

  const icons = {
    1: <Accessibility />,
    2: <AccountBalance />
  };

  return (
    <div
      className={clsx(classes.root, {
        [classes.active]: active,
        [classes.completed]: completed,
      })}
    >
      {icons[String(props.icon)]}
    </div>
  );
}

ColorLibStepIcon.propTypes = {
  /**
   * Whether this step is active.
   */
  active: PropTypes.bool,
  /**
   * Mark the step as completed. Is passed to child components.
   */
  completed: PropTypes.bool,
  /**
   * The label displayed in the step icon.
   */
  icon: PropTypes.node,
};


const styles = theme => ({
  paper: {
    padding: theme.spacing(4),
    margin: theme.spacing(1),
    width: '95%',
    textAlign: 'left',

  },
  paperToSee: {
    padding: theme.spacing(4),
    margin: theme.spacing(1),
    width: '95%',
  },
  button: {
    backgroundColor: '#22671d',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: 'orange'
    }
  },
  categorie: {
    display: 'flex',
    justifyContent: 'center',
    fontWeight: 800,
    flexWrap: 'wrap',
    '& > *': {
      margin: theme.spacing(0.5),
    },
  },
  information: {
    color: '#139113',
  },
  blocDate: {
    margin: 'auto',
  },
  title: {
    textAlign: "left",
    color: "#2F80ED",
    fontWeight: "bold",
  },
  divider: {
    marginTop: 20
  },
  paperClosed: {
    backgroundColor: backGroundClosPaper
  },
  stepperDisable: {
    backgroundColor: backGroundClosPaper
  },
  repondre: {
    backgroundColor: "#5FAD56",
    color: theme.palette.getContrastText("#498A42"),
    borderRadius: 20,
    '&:hover': {
      backgroundColor: "#498A42",
    },
  },
  informationActeur: {
    marginBottom: 10
  },
  actionsClos: {
    backgroundColor: backGroundClosAction
  },
  doitRepondre: {
    color: red[300],
    fontWeight: 500
  }
});

class Echange extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emetteur: (!this.props.add && this.props.echange && this.props.echange.categorie && this.props.echange.categorie.emetteur) ? this.props.echange.categorie.emetteur : "",
      destinataire: (!this.props.add && this.props.echange && this.props.echange.categorie && this.props.echange.categorie.destinataire) ? this.props.echange.categorie.destinataire : "",
      commentairePublic: (!this.props.add && this.props.echange) ? this.props.echange.commentairePublic : "",
      commentairePrive: (!this.props.add && this.props.echange) ? this.props.echange.commentairePrive : "",
      Prioritaire: (!this.props.add && this.props.echange) ? !!this.props.echange.action[this.props.echange.action.length - 1].urgent : false,
      modeEdition: this.props.enabled || false,
      clos: !!this.props.echange.clos,
      openAction: false,
      openDroits: false,
    };
  }

  componentDidMount = async () => { }

  componentDidUpdate = async (prevProps, prevState) => {
    if (!this.props.add && this.props.echange._id !== prevProps.echange._id) {
      this.setState({
        emetteur: (!this.props.add && this.props.echange) ? this.props.echange.categorie.emetteur : "",
        destinataire: (!this.props.add && this.props.echange) ? this.props.echange.categorie.destinataire : "",
        commentairePublic: (!this.props.add && this.props.echange) ? this.props.echange.commentairePublic : "",
        commentairePrive: (!this.props.add && this.props.echange) ? this.props.echange.commentairePrive : "",
        Prioritaire: (!this.props.add && this.props.echange) ? !!this.props.echange.action[this.props.echange.action.length - 1].urgent : false,
        clos: !!this.props.echange.clos,
      })
    }
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({ [name]: value });
  };

  handleChangePrioritaire = (event) => {
    this.setState({ Prioritaire: event.target.checked });
  }

  handleSwitch = async (event) => {
    this.setState({ [event.target.name]: event.target.checked });
    const retour = await this.props.switchCloture(this.props.echange._id);
    if (retour) {
      this.props.addNotification({ variant: retour.success ? "success" : "error", message: retour.msg });
    }
  };

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({ modeEdition: !this.state.modeEdition });
  }

  cancelEdit = () => {
    this.setState({ modeEdition: false });
  }

  handleEditeEchange = async () => {
    let data = {
      _id: this.props.echange._id,
      commentairePublic: this.state.commentairePublic,
      commentairePrive: this.state.commentairePrive,
      Prioritaire: this.state.Prioritaire,
    };
    await this.props.setEchange(data);
    this.setState({ modeEdition: false });
  }

  handleOpenAction = () => {
    this.setState({ openAction: true });
  };

  handleClose = async () => {
    await this.setState({ openAction: false, openDroits: false });
  };


  handleSubmitActeursEchange = async (left, right) => {
    let donnees = [];
    let docNewActeur = [];
    let deleteActeur = [];
    let docsInfo = [];

    for (const element of right) {
      donnees.push({ acteur_id: element._id, entite: element.entite });
      docNewActeur.push({ acteur_id: element._id });
    }

    // Récupérer tous les acteurs dans left (celle qu'on ne veut pas être dans l'info) pour une protection dans les droits documents
    for (const elem of left) {
      deleteActeur.push({ acteur_id: elem._id });
    }

    let envoi = {
      id: this.props.echange._id,
      information: donnees
    }

    // Récupérer toutes les documents de dossier
    let docs = this.props.documents;

    // Filtre tous les documents de cet échange sauf si l'acteur n'a pas créé ce document
    let docsEchange = docs.length > 0 ? docs.filter(doc => doc.echange_id.toString() === this.props.echange._id.toString()) : [];

    // S'il y a les documents dans cet échange, on ajoute l'acteur d'info dans les droits de document
    if (docsEchange && docsEchange.length > 0) {
      let docActeurs = [].concat({ acteur_id: this.props.echange.categorie.emetteur_id }, { acteur_id: this.props.echange.categorie.destinataire_id }, docNewActeur);
      for (const doc of docsEchange) {
        let dataDoc = {
          deleteActeur: deleteActeur,
          echangeId: this.props.echange._id,
          mainActeur: this.props.acteurId,
          dossierId: this.props.dossierId,
          document_id: doc._id,
          acteurs: docActeurs
        }
        docsInfo.push(dataDoc);
      }
    }
    await this.props.gestionInformationEchange(envoi, docsInfo);
  }

  upload = async (id, data) => {
    await this.props.uploadDocument(id, data);
  }

  delete = async (id) => {
    try {
      const res = await this.props.deleteFile(id);
      if (res.success) {
        this.props.addNotification({ variant: "success", message: "Le document a bien été supprimé" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la suppression du document",
          details: res.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la suppression du document" });
    }
  }

  openDroitsDialog = (event) => {
    event.stopPropagation();
    this.setState({ openDroits: true });
  }

  validDroit = async (data) => {
    let donnees = [];
    for (let i = 0; i < data.length; i++) {
      for (let j = 0; j < this.props.acteurs.length; j++) {
        if (this.props.acteurs[j]._id === data[i]) {
          donnees.push({ acteur_id: data[i], entite: this.props.acteurs[j].entite });
        }
      }
    }
    let envoi = {
      id: this.props.echange._id,
      information: donnees
    }
    await this.props.gestionInformationEchange(envoi);
    await this.handleClose();
  }

  render = () => {
    const { classes, echange, auth, documents, acteurs, handleClose, acteurId } = this.props;
    const { modeEdition, emetteur, destinataire, commentairePublic, commentairePrive, clos, openDroits, Prioritaire } = this.state;

    if (!auth || !echange) return <div>...</div>;

    // Récupérer la liste des Orga + equipe de l'utilisateur (ids) et vérifier qu'au moins l'un de ces acteurs est un emetteur ou destinataire
    let haveToSee = estActeurEchange(echange, auth.organisation.acteur_id, auth.equipes);
    const acteur_id = auth && auth.organisation.acteur_id ? auth.organisation.acteur_id : false;


    let userIsInCategorie = haveToSee;

    if (!echange)
      return <div><CircularProgress /></div>;

    let droitInformation = false;
    let droitCloture = false;
    let droitEdition;
  
    let ligneActions = [];
    let actionSorted = [];
    if (echange.action && echange.action.length > 0) {
      actionSorted = echange.action.sort((a, b) => new Date(a.dateRealisation) > new Date(b.dateRealisation) ? -1 : 1);
      if (actionSorted && actionSorted[0]) {
        haveToSee = estDestinataire(actionSorted[actionSorted.length - 1].destinataire_id, auth.organisation.acteur_id, auth.equipes);
        droitEdition = estEmetteur(actionSorted[actionSorted.length - 1].emetteur_id, auth.organisation.acteur_id, auth.equipes);
      }
      ligneActions = Object
        .keys(actionSorted)
        .map(key => {
          let action = actionSorted[key];
          if (!droitCloture) {
            if (estActeurAction(action, auth.organisation.acteur_id, auth.equipes)) droitCloture = true;
          }
          const docactions = documents && documents.length > 0 ? documents.filter((doc) => (action.document.findIndex(d => d.document_id === doc._id) !== -1)) : [];
          return <LigneAction
            acteurId={acteurId}
            key={action.action_id + key}
            action={action}
            max={actionSorted.length - 1}
            numeroOrdre={key}
            acteur={acteur_id}
            documents={docactions}
            delete={this.delete.bind(this)}
            clos={clos}
          />
        }
        )
    }
    const droitEditeCommentaire = modeEdition && droitEdition;

    let nextResponse;
    if (actionSorted && actionSorted[0]) {
      nextResponse = actionSorted[0];
    }

    // désactiver la button de valider, s'il n'y a pas des changement
    let disableButton = true;
    if (this.props.echange) {
      if (this.props.echange.commentairePublic !== this.state.commentairePublic ||
        this.props.echange.commentairePrive !== this.state.commentairePrive ||
        (!!this.props.echange.action[this.props.echange.action.length - 1].urgent) !== this.state.Prioritaire
      ) {
        disableButton = false;
      }
    }

    return (
      <Fragment>
        {openDroits && acteurs &&
          <WrapperOptions
            clos={clos}
            echange={echange}
            acteurs={acteurs}
            handleSubmitActeursEchange={this.handleSubmitActeursEchange}
            handleSwitch={this.handleSwitch}
            open={openDroits}
            handleClose={this.handleClose}
            closeEchange={handleClose}
          />
        }
        {this.state.openAction &&
          <AjoutAction
            utilisateur={auth}
            echange={echange}
            open={this.state.openAction}
            onClose={this.handleClose.bind(this)}
            delete={this.delete.bind(this)}
          />
        }
        <Grid container justifyContent="space-between" alignItems="center" spacing={4}>
          <Grid container item xs={8}>
            <Grid container item spacing={3}>
              <Grid item>
                Emetteur :
              </Grid>
              <Grid item>
                {emetteur}
              </Grid>
            </Grid>
            <Grid container item spacing={3}>
              <Grid item>
                Destinataire :
              </Grid>
              <Grid item>
                {destinataire}
              </Grid>
            </Grid>
          </Grid>

          {droitCloture &&

            <Grid container justifyContent="flex-end" item xs={4}>
              <IconButton
                className={classes.droits}
                aria-label="Droits"
                onClick={this.openDroitsDialog}
                title={"Options de l'échange"}
                size="large">
                <MoreHoriz />
              </IconButton>
            </Grid>
          }
          {!clos && nextResponse &&
            <Grid container item marginBottom={1}>
              <Grid item>
                En attente de réponse de : <span
                  className={estDestinataire(nextResponse.destinataire_id, auth.organisation.acteur_id, auth.equipes) ? classes.doitRepondre : null}>{nextResponse.destinataire}</span>
              </Grid>
            </Grid>
          }
          <Grid container item xs={12} marginBottom={3}>
            <Box flexGrow={3} sx={{ boxShadow: 2 }} >
              <Grid container alignItems="center">
                <Grid container item xs={5} justifyContent="flex-start">
                  <FormControlLabel
                    control={<Switch
                      checked={Prioritaire}
                      onChange={this.handleChangePrioritaire}
                      color="warning"
                      name="Prioritaire"
                      inputProps={{ 'aria-label': "Prioritaire de l'échange" }}
                    />}
                    label="Prioritaire"
                    labelPlacement="start"
                    disabled={!modeEdition}
                  />
                </Grid>
                <Grid item container xs={7} justifyContent="flex-end" >
                  {!clos && userIsInCategorie && !modeEdition &&
                    <Editer
                      action={this.changeEdition}
                      texte={"Editer les informations"}
                      disable={modeEdition}
                    />
                  }
                </Grid>
              </Grid>
              <Grid container spacing={2} margin={2}>
                <Grid item xs={5.7}>
                  {(haveToSee || droitInformation || droitEdition) &&
                    <CommentairePublic
                      handleChange={this.handleChange.bind(this)}
                      value={commentairePublic}
                      disabled={!droitEditeCommentaire}
                      multiline
                    />
                  }
                </Grid>
                <Grid item xs={5.7}>
                  {(haveToSee || droitEdition) &&
                    <CommentairePrive
                      handleChange={this.handleChange.bind(this)}
                      value={commentairePrive}
                      disabled={!droitEditeCommentaire}
                      multiline
                    />
                  }
                </Grid>
              </Grid>

              <Grid container item>
                {!this.props.add && modeEdition &&
                  <Grid container item justifyContent="flex-end" marginRight={1} margin={2}>
                    <ButtonText
                      action={this.cancelEdit.bind(this)}
                      color="info"
                      texte="Annuler"
                      variant="text"
                    />
                    <ButtonText
                      action={this.handleEditeEchange.bind(this)}
                      color="info"
                      texte="Valider"
                      variant="outlined"
                      disable={disableButton}
                    />
                  </Grid>
                }
              </Grid>
            </Box>
          </Grid>
        </Grid>
        <Grid item container xs={12} className={classes.divider}>
          <Grid item xs={12}>
            <Paper className={clsx(clos ? classes.actionsClos : '')}>
              <Grid>
                <Grid container justifyContent="space-between" alignItems="center">
                  <Typography className="body1">
                    <b>Actions</b>
                  </Typography>
                  {userIsInCategorie && !clos && !modeEdition &&
                    <ButtonAdd
                      action={this.handleOpenAction}
                      texte={"Répondre"}
                    />
                  }
                </Grid>
              </Grid>
              <Grid>
                <Timeline position="left">
                  {ligneActions}
                </Timeline>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setEchange: (data) => dispatch(setEchange(data)),
    uploadDocument: (id, data) => dispatch(uploadDocument(id, data)),
    addNotification: (obj) => dispatch(addNotification(obj)),
    getFile: (id) => dispatch(getFile(id)),
    switchCloture: (id) => dispatch(switchCloture(id)),
    gestionInformationEchange: (data, docDroit, acteurId, dossierId) => dispatch(gestionInformationEchange(data, docDroit, acteurId, dossierId)),
    deleteFile: (id) => dispatch(deleteFile(id)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Echange));