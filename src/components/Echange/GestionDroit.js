import React, { useState } from 'react';
import { makeStyles, withStyles } from '@mui/styles';
import {
    Typography, FormControl, FormLabel, FormGroup, FormControlLabel,
    Dialog, DialogContent, DialogActions,
    Button, Checkbox
} from '@mui/material';


const GreenCheckbox = withStyles({
    root: {
        color: "#8e8e8e",
        '&$checked': {
            color: "#2F80ED",
        },
        '&$disabled': {
            color: "#ed2f7b",
        },
    },
    disabled: {},
    checked: {},
})((props) => <Checkbox color="default" {...props} />);

const useStyles = makeStyles(theme => ({
    dialog: {
        padding: 30
    },
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(3),
    },
    nomDocument: {
        color: "#2F80ED",
    },
    checkedIcon: {
        '&$checked': {
            color: "#2F80ED",
        }
    }

}));

export default function GestionDroit(props) {
    const classes = useStyles();
    const { open, echange, acteurs, onClose, validedroits } = props;
    const [selectActeurs, setSelectActeurs] = useState(echange.information.map(act => act.acteur_id));

    const handleChange = (event) => {
        event.stopPropagation();
        const check = selectActeurs.findIndex(act => act === event.target.name);
        if (check === -1) {
            setSelectActeurs(selectActeurs.concat([event.target.name]))
        } else {
            const newArray = selectActeurs.filter((act) => act !== event.target.name);
            setSelectActeurs(newArray);
        }
    };

    const validation = () => {
        validedroits(selectActeurs);
    };

    const acteursListe = acteurs ? acteurs.map((acteur) => {
        const acteurActif = acteur._id === echange.action[0].emetteur_id || acteur._id === echange.action[0].destinataire_id;
        return <FormControlLabel
            label={acteur.entite}
            key={'labelGestionDroit' + acteur._id}
            control={
                <GreenCheckbox
                    onChange={handleChange}
                    name={acteur._id}
                    checked={selectActeurs ? selectActeurs.findIndex(act => act === acteur._id) !== -1 || acteurActif : false}
                    disabled={acteurActif}
                    key={'GestionDroit' + acteur._id}
                />
            }
        />
    }
    ) : [];

    return (
        <Dialog onClose={onClose} aria-labelledby="droits-echange-dialog"
            open={open} className={classes.dialog}>
            <DialogContent>
                <Typography variant="h6">
                    Acteurs pour information
                </Typography>
                <FormControl component="fieldset" className={classes.formControl}>
                    <FormLabel component="legend">Selection des acteurs</FormLabel>
                    <FormGroup>
                        {acteursListe}
                    </FormGroup>
                </FormControl>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose} color="secondary">
                    Fermer
                </Button>
                <Button onClick={validation} color="primary">
                    Valider
                </Button>
            </DialogActions>
        </Dialog>
    );
}
