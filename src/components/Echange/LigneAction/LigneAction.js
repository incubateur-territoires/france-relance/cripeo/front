import React, { Component, Fragment } from "react";
import withStyles from '@mui/styles/withStyles';
import { connect } from "react-redux";
import { Grid, Chip, Paper, Typography } from "@mui/material";
import {
  TimelineItem,
  TimelineDot,
  TimelineSeparator,
  TimelineOppositeContent,
  TimelineConnector,
  TimelineContent
} from "@mui/lab";
import CommentairePublic from "../CommentairePublic";
import CommentairePrive from "../CommentairePrive";
import moment from 'moment';
import WrapperEchange from "../WrapperEchange";
import { grey } from '@mui/material/colors';
import { getFile } from "../../../actions/document";
import { addNotification } from "../../../actions/notifications";
import { estActeurAction, estEmetteurAction } from "../../../actions/action";
import { echangeEstLu } from '../../../actions/echange';
import { ListeFichiers } from '../../composants_internes';


const styles = theme => ({
  paper: {
    padding: theme.spacing(2),
    margin: theme.spacing(1),
    width: '100%',
  },
  nomAction: {
    textAlign: "left",
    textTransform: "capitalize",
  },
  nomCategorie: {
    textAlign: "left",
    textTransform: "capitalize",
    color: theme.palette.primary.main,
  },
  actionZone: {
    "&:hover": {
      cursor: "pointer"
    }
  },
  noOppositeContent: {
    missingOppositeContent: {
      "&:before": {
        display: "none"
      }
    },
    flex: 1
  },
  createdByTitre: {
    textAlign: "left",
    textTransform: "capitalize",
    fontSize: '0.7rem',
    color: grey[500],
  },
  createdBy: {
    textAlign: "left",
    textTransform: "capitalize",
    color: grey[500],
    fontSize: '0.7rem',
  }
});


class LigneAction extends Component {
  state = {
    openAction: false,
    openEchange: false,
  }

  handleOpenAction = async (event) => {
    event.stopPropagation();
    if (this.props.alternatif) {
      this.setState({ openEchange: !this.state.openEchange });
      if (this.props.action.echange.estLu === undefined || this.props.action.echange.estLu.toString() === "2020-01-01T00:00:00.000Z") {
        if (this.props.action.echange.action[0].destinataire_id.toString() === this.props.auth.organisation.acteur_id.toString()) {
          const data = { id: this.props.action.echange._id, date: new Date() };
          await this.props.echangeEstLu(data);
        }
      }
      // eslint-disable-next-line
    } else {
      this.setState({ openAction: !this.state.openAction });
    }
  };

  handleCloseAction = () => {
    this.setState({ openEchange: false })
  };

  checkPrivate = (action, acteur) => {
    return action.emetteur_id === acteur || action.destinataire_id === acteur;
  }

  render() {
    const { action, classes, alternatif, documents, clos, auth, acteurId } = this.props;
    const { openEchange } = this.state;

    if (!action || !auth) return <div />

    const canSeePrivate = estActeurAction(action, auth.organisation.acteur_id, auth.equipes);

    if (action.urgent === 1 && action.echange)
      action.echange.urgence = true

    return (
      <Fragment>
        {alternatif &&
          <WrapperEchange
            open={openEchange}
            echange={action.echange}
            onClose={this.handleCloseAction}
          />}
        <TimelineItem sx={{ flex: 1 }}
          position={!alternatif ? "left" : (estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "left" : "right")}>
          <TimelineOppositeContent display="none" sx={{ display: "none" }}></TimelineOppositeContent>
          <TimelineContent className={classes.noOppositeContent}>
            <Grid wrap="nowrap" container
              direction={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "row" : "row-reverse"}
              columnSpacing={2}>
              <Grid item p={1}>
                <Grid
                  container
                  direction={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "row" : "row-reverse"}
                  columnSpacing={2}
                  alignItems="center"
                  wrap="nowrap"
                >
                  <Grid item sx={{ justifyContent: "center" }}>
                    <Grid container item justifyContent="center">
                      <Typography className={classes.nomCategorie}
                        variant="caption">Le {moment(action.dateRealisation).format('DD-MM-YYYY')}</Typography>
                    </Grid>
                    <Grid container item justifyContent="center">
                      <Typography className={classes.nomCategorie}
                        variant="caption">à {moment(action.dateRealisation).format('HH:mm')}</Typography>
                    </Grid>
                  </Grid>
                  <Grid item>
                    <TimelineSeparator>
                      <TimelineDot />
                      <TimelineConnector />
                    </TimelineSeparator>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container item xs={7} rowSpacing={2} m={1}>
                <Paper sx={{ padding: 2, flex: 1 }} elevation={2}>
                  <Grid
                    sx={{ marginBottom: 1, cursor: alternatif ? "pointer" : "default" }}
                    direction={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "row" : "row-reverse"}
                    container item alignItems="center" spacing={2}
                    justifyContent={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "flex-start" : "flex-start"}
                    onClick={this.handleOpenAction}
                  >
                    <Grid item>
                      <Chip sx={{ cursor: alternatif ? "pointer" : "default" }} label={action.emetteur} variant="outlined"
                        color={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "primary" : "secondary"}
                      />
                      {" vers "}
                      <Chip sx={{ cursor: alternatif ? "pointer" : "default" }} label={action.destinataire} variant="outlined"
                        color={estEmetteurAction(action.emetteur_id, auth.organisation.acteur_id, auth.equipes) ? "primary" : "secondary"}
                      />
                    </Grid>
                    <Grid item className={classes.nomAction}>
                      <Typography className={classes.nomCategorie}
                        variant="overline">{action?.echange?.categorie?.libelle} </Typography>
                      <Typography className={classes.nomAction} variant="overline">{action?.libelle}</Typography>
                    </Grid>
                    {action?.createdBy &&
                      <Grid item container paddingBottom={1} xs={12}>
                        <Typography className={classes.createdByTitre} marginRight={1}>Créé par: </Typography>
                        <Typography className={classes.createdBy} >{action?.createdBy}</Typography>
                      </Grid>
                    }
                  </Grid>
                  <Grid item xs={12}>
                    {ContentAction(action, documents, clos, canSeePrivate, this.props.delete.bind(this), this.props.getFile.bind(this), acteurId)}
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          </TimelineContent>
        </TimelineItem>
      </Fragment>
    );

    function ContentAction(action, documents, clos, canSeePrivate, deleteFile, getFile, acteurId) {

      const accessFile = async (fichier) => {
        if (getFile) {
          const file = await getFile(fichier._id);
          const Filetype = fichier.extension;
          const mimType = Filetype === 'pdf' ? 'application/pdf' : Filetype === 'xlsx' ? 'application/xlsx' :
            Filetype === 'pptx' ? 'application/pptx' : Filetype === 'csv' ? 'application/csv' : Filetype === 'docx' ? 'application/docx' :
              Filetype === 'jpg' ? 'application/jpg' : Filetype === 'png' ? 'application/png' : '';

          const blob = b64toBlob(file, mimType);
          const fileUrl = await window.URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = fileUrl;
          link.download = `${fichier.titre}`;
          document.body.appendChild(link);
          link.click();
        }
      }

      const deleteFunction = async (fichier) => {
        await deleteFile(fichier._id);
        return;
      }

      let docs = [];
      if (documents && documents.length > 0 && acteurId !== undefined) {
        docs = documents?.map(fichier => {

          let newDoc = { ...fichier };
          newDoc.acteurIsInformation = false;

          if (fichier?.informationActeurs_id && fichier?.informationActeurs_id.length > 0) {
            if (fichier?.informationActeurs_id?.findIndex((element) => element?.toString() === acteurId?.toString()) !== -1) {
              newDoc.acteurIsInformation = true;
            }
          }

          if (fichier.mainActeurId.toString() !== acteurId.toString()) {
            newDoc.canDelete = false;
          }
          return newDoc;
        });
      }

      return <Grid container item spacing={2} direction="column">
        {action.commentairePublic &&
          <Grid item>
            <CommentairePublic
              value={(action.commentairePublic && action.commentairePublic.length > 0) ? action.commentairePublic.toUpperCase() : ""}
              disabled={true}
              multiline={true}
            />
          </Grid>}
        {canSeePrivate && action.commentairePrive &&
          <Grid item>
            <CommentairePrive
              value={action.commentairePrive && action.commentairePrive.length > 0 ? action.commentairePrive.toUpperCase() : ""}
              disabled={true}
              multiline={true}
            />
          </Grid>}
        {documents && documents.length > 0 &&
          <Grid item>
            <ListeFichiers
              maxWidth={650}
              fichiers={docs.map(fichier => {
                return {
                  _id: fichier._id,
                  extension: `.${fichier.extension}`,
                  titre: fichier.titre,
                  canDelete: !fichier.acteurIsInformation && fichier.canDelete
                }
              })}
              deleteFunction={deleteFunction}
              downloadFunction={accessFile}
            />
          </Grid>}
      </Grid>;
    }
  }
}

const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
  const byteCharacters = atob(b64Data);
  const byteArrays = [];

  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);

    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }

    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getFile: (id) => dispatch(getFile(id)),
    addNotification: (obj) => dispatch(addNotification(obj)),
    echangeEstLu: (data) => dispatch(echangeEstLu(data)),
  }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(LigneAction));
