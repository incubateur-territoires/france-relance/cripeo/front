import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import connect from "react-redux/es/connect/connect";
import { DialogContent, Grid, Typography, CircularProgress, DialogActions, Button } from '@mui/material';
import Echange from "../Echange";
import { Notifications } from '@mui/icons-material';
import moment from 'moment';
import { red, blue } from '@mui/material/colors';
import { endedCreationEchange } from '../../../actions/echange';
import { Prompt } from '../../composants_internes';


const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 150,
  },
  cancelButton: {
    marginRight: 50
  },
  title: {
    textAlign: "left",
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    paddingLeft: 15,
    color: "#2F80ED",
    fontWeight: "bold",
  },
  notificationUrgente: {
    color: red[400],
    position: 'absolute',
    right: '100px',
    top: '15px',
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});

class WrapperEchange extends Component {

  handleClose = () => {
    this.props.endedCreationEchange();
    this.props.onClose();
  };

  render() {
    const { classes, echange, acteurs, open, acteurId, dossierId } = this.props;

    if (!echange)
      return <div><CircularProgress /></div>;

    return (
      <Prompt
        titre={
          <Grid container item xs={12} justifyContent="space-between" alignItems="center" marginLeft={1}>
            <Grid container item justifyContent="flex-start" alignItems="center" spacing={2}>
              <Typography variant="h6">
                {echange.categorie.libelle}
              </Typography>
              <Typography marginLeft={1} variant="caption">
                - Dernière modification le {moment(echange.created_at).format('DD-MM-YYYY HH:mm')}
              </Typography>
            </Grid>
            <Grid>
              {echange.urgence ?
                <div className={classes.notificationUrgente}><Notifications /> Urgence </div>
                :
                <div />
              }
            </Grid>
          </Grid>
        }
        open={open}
        onClose={this.handleClose.bind(this)}
        fullWidth={true}
        maxWidth={"xl"}
        aria-labelledby="customized-dialog-title"
        actions={
          <DialogActions>
            <Button onClick={this.handleClose.bind(this)} color="primary" sx={{ justifyContent: 'flex-end', marginRight: 4 }}>
              Fermer
            </Button>
          </DialogActions>
        }
      >
        <DialogContent>
          <Grid container item xs={12}>
            <Echange
              dossierId={dossierId}
              acteurId={acteurId}
              key={echange._id}
              echange={echange}
              acteurs={acteurs || []}
              documents={this.props.documents || []}
              handleClose={this.handleClose}
            />
          </Grid>
        </DialogContent>
      </Prompt>
    );
  }
}

WrapperEchange.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,

};

function mapStateToProps(state) {
  return {
    documents: state.documents,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    endedCreationEchange: () => dispatch(endedCreationEchange())
  }
}

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(WrapperEchange));