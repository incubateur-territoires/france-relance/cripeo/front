import React, { useCallback, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Grid, Typography, DialogContentText, Switch, FormControlLabel, DialogActions, Button, Box, DialogContent } from '@mui/material';
import { Delete } from '@mui/icons-material';
import ChoiceList from '../../Commun/ChoiceList/ChoiceList';
import { Prompt } from '../../composants_internes';
import { deleteEchange } from '../../../actions/echange';


function WrapperOptions({ open, acteurs, echange, clos, handleSwitch, handleSubmitActeursEchange, handleClose, closeEchange }) {
  const dispatch = useDispatch();
  const [openDelete, setOpenDelete] = React.useState(false);
  const auth = useSelector(state => state.auth);

  const handleCloseDelete = () => {
    setOpenDelete(false);
  };

  const handleOpenDelete = () => {
    setOpenDelete(true);
  };

  const handleDelete = useCallback(async () => {
    await dispatch(deleteEchange(echange._id));
    await handleClose();
    await closeEchange();
    // eslint-disable-next-line
  }, [dispatch, echange, handleClose]);

  const acteur_id = auth && auth.organisation.acteur_id ? auth.organisation.acteur_id : false;

  return (
    <Fragment>
      <Prompt
        titre={"Options sur l'échange"}
        onClose={handleClose}
        open={open}
        actions={
          <DialogActions>
            <Button onClick={handleClose} sx={{ justifyContent: 'flex-end', marginRight: 4 }} >Fermer</Button>
          </DialogActions>
        }
      >
        <DialogContent>
          <Grid container direction="column">
            <Grid container item style={{ marginBottom: 20 }} spacing={2}>
              <Grid container item direction="column" spacing={2} style={{ padding: 10 }}>
                {(!echange.action || echange.action.length < 2) && (echange.categorie.emetteur_id === acteur_id) &&
                  <Grid item style={{ paddingLeft: 20 }}>
                    <Button
                      variant="contained"
                      color="error"
                      startIcon={<Delete />}
                      onClick={handleOpenDelete}
                    >
                      Suppression de l'échange
                    </Button>

                  </Grid>
                }
                <Grid item >
                  <FormControlLabel
                    control={
                      <Switch
                        checked={clos}
                        onChange={handleSwitch}
                        color="primary"
                        name="clos"
                        inputProps={{ 'aria-label': 'clôture échange checkbox' }}
                      />
                    }
                    label="Clôturer l'échange"
                    labelPlacement="start"
                    required={true}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Box flexGrow={3} sx={{ boxShadow: 2 }} >
              <Grid container item margin={1}>
                <Typography variant="h6">
                  Changements de droits en information
                </Typography>
              </Grid>
              <Grid item margin={1}>
                <ChoiceList
                  acteurs={acteurs}
                  echange={echange}
                  handleSubmit={handleSubmitActeursEchange}
                  edit
                />
              </Grid>
            </Box>
          </Grid>
        </DialogContent>
      </Prompt>

      <Prompt
        titre={"Suppression de l'échange"}
        open={openDelete}
        keepMounted
        onClose={handleCloseDelete}
        aria-describedby="Suppression de l'échange"
        actions={
          <DialogActions>
            <Button onClick={handleCloseDelete} color="primary">
              Annuler
            </Button>
            <Button onClick={handleDelete}
              color="primary">
              Valider
            </Button>
          </DialogActions>
        }
      >
        <DialogContent>
          <DialogContentText id="Suppression de l'échange">
            Voulez-vous supprimer cet échange ?
          </DialogContentText>
        </DialogContent>
      </Prompt>
    </Fragment>
  );
}

export default WrapperOptions;