import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import {
  Grid,
  DialogContent,
  DialogActions,
  MenuItem,
  Switch,
  FormControlLabel,
  Typography,
  Alert
} from "@mui/material";
import connect from "react-redux/es/connect/connect";
import SelectUI from '../../Commun/Input/SelectUI';
import Valider from "../../Commun/Bouton/Valider";
import Annuler from "../../Commun/Bouton/Annuler";
import { getActionsByCategorie } from "../../../actions/action";
import { addActionEchange, uploadDocumentAction, deleteFile, estDestinataire, echangeEstLu } from "../../../actions/echange";
import { getDocumentsByDossier, getDocumentsByDossierByActeur } from "../../../actions/document";
import { addNotification } from "../../../actions/notifications";
import { blue } from '@mui/material/colors';
import { UploaderFormData, InputDefault, SelectDefault, Prompt, ButtonText } from '../../composants_internes';


const TRANSFERT_ACTION = {
  libelle: "Transférer en interne",
  "_id": "6054b715cb45e503transfert"
};
const INFO_COMPLEMENTAIRE = { libelle: "Information", "_id": "6054b715cb45e503infocomp" };

const mods = [
  INFO_COMPLEMENTAIRE,
]

const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 150,
  },
  cancelButton: {
    marginRight: 50
  }
});

class AjoutAction extends Component {
  state = {
    open: false,
    libelle: "",
    commentairePublic: "",
    commentairePrive: "",
    action: INFO_COMPLEMENTAIRE._id,
    fichiers: [],
    urgent: this.props.echange ? this.props.echange.urgence === 1 ? true : false : false,
    displayEquipes: false,
    equipes: [],
    transfertDestinataire: null,
    displayTransfertConfirmationDialog: false,
    errerMessage: false
  };

  componentDidMount = () => {
    if (this.props.echange && this.props.echange.categorie) {
      this.props.getActionsByCategorie(this.props.echange.categorie.categorie_id);
    }
  }

  handleClose = async () => {
    await this.setState({
      fichiers: [],
    });
    this.props.onClose();
  };

  handleCloseConfirmation = async () => {
    this.setState({
      displayTransfertConfirmationDialog: false
    })
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleSelect = async (event) => {
    event.stopPropagation();
    const modsTemp = [...mods];
    const isDestinataire = estDestinataire(this.props.echange.categorie.destinataire_id, this.props.auth.organisation.acteur_id, this.props.auth.equipes);
    if (isDestinataire) modsTemp.push(TRANSFERT_ACTION);
    const actionsMod = this.props.actions ? modsTemp.concat(this.props.actions) : modsTemp;
    if (actionsMod && actionsMod.length > 0) {
      for (const element of actionsMod) {
        if (element._id === event.target.value) {
          await this.setState({
            [event.target.name]: event.target.value,
            displayEquipes: event.target.value === TRANSFERT_ACTION._id,
            transfertDestinataire: event.target.value !== TRANSFERT_ACTION._id ? null : this.state.transfertDestinataire
          })
        }
      }
    }
  }

  handleSelectDestinataire = async (event) => {
    event.stopPropagation();
    await this.setState({ transfertDestinataire: event.target.value })
  }

  handleSwitch = (event) => {
    event.stopPropagation();
    this.setState({ [event.target.name]: event.target.checked })
  }

  handleSubmit = () => {
    if (this.state.displayEquipes && this.state.transfertDestinataire != null) {
      this.setState({
        displayTransfertConfirmationDialog: true
      });
    } else if (this.state.action && (this.state.commentairePrive || this.state.commentairePublic)) {
      this.setState({ errerMessage: false });
      this.validationSubmit();
    } else {
      this.setState({ errerMessage: true });
    }
  }

  validationSubmit = async () => {
    await this.props.onClose();
    const modsTemp = [...mods];
    const isDestinataire = estDestinataire(this.props.echange.categorie.destinataire_id, this.props.auth.organisation.acteur_id, this.props.auth.equipes);
    if (isDestinataire) modsTemp.push(TRANSFERT_ACTION);
    const actionsMod = this.props.actions ? modsTemp.concat(this.props.actions) : modsTemp;
    let act = {};
    for (const element of actionsMod) {
      if (element._id === this.state.action) {
        act = element;
      }
    }

    let obj = {
      echange_id: this.props.echange._id,
      action: {
        urgent: this.state.urgent,
        action_id: act._id,
        libelle: act._id === TRANSFERT_ACTION._id ? "Destinataire transféré en interne vers " + this.getActeurName(this.state.transfertDestinataire, this.props.acteursOrganisation) + " depuis " + this.getActeurName(this.props.echange.categorie.destinataire_id, this.props.acteursOrganisation) : act.libelle,
        commentairePrive: this.state.commentairePrive || "",
        commentairePublic: this.state.commentairePublic || "",
      },
      acteur_id: this.props.auth.organisation.acteur_id,
    }

    if (this.state.displayEquipes && this.state.transfertDestinataire != null) {
      obj.transfertDestinataire = this.state.transfertDestinataire;
    }

    let fichiers = [];
    if (this.state.fichiers) {
      await Promise.all(await this.state.fichiers.map(async (doc) => {

        fichiers.push(await this.props.uploadDocumentAction(this.props.echange._id, doc));
      }));
    }

    if (fichiers.length > 0) {
      obj.action.document = [];
      for (const element of fichiers) {
        obj.action.document.push({
          document_id: element._id
        });
      }
    }

    obj.action.createdBy = this.props.auth.name.given_name + " " + this.props.auth.name.family_name;

    await this.props.addActionEchange(obj);
    const data = { id: this.props.echange._id, date: "2020-01-01" };
    await this.props.echangeEstLu(data);

    await this.props.getDocumentsByDossierByActeur(this.props.echange.dossier_id, this.props.auth.organisation.acteur_id);
    this.handleCloseConfirmation();
    await this.props.onClose();
  }

  upload = async (data) => {
    await this.setState({
      fichiers: [...this.state.fichiers, ...data],
    });
  }

  deletefile = async (key) => {
    await this.setState({
      fichiers: this.state.fichiers.filter((fichier, index) => index !== key),
    });
  }

  getActeurName = (id, acteurs) => {
    let index = acteurs.findIndex(acteur => acteur._id === id);
    return index !== -1 ? acteurs[index].entite : this.props.auth.organisation.entite;
  }

  render() {
    const {
      classes, onClose, actions, getActionsByCategorie, echange, addActionEchange,
      // eslint-disable-next-line no-unused-vars
      getDocumentsByDossier, uploadDocumentAction, deleteFile, addNotification, auth, acteursOrganisation, ...other
    } = this.props;

    const {
      action,
      fichiers,
      displayEquipes,
      transfertDestinataire,
      errerMessage
    } = this.state;

    const acteursOrganisationMenuItems = acteursOrganisation ? Object.keys(acteursOrganisation).map(key => {
      return <MenuItem key={key + acteursOrganisation[key]._id}
        value={acteursOrganisation[key]._id}>{acteursOrganisation[key].entite}
      </MenuItem>
    }) : [];

    const modsTemp = [...mods];

    // vérifier qu'on est destinataire de l'échange
    const isDestinataire = echange && echange.categorie && estDestinataire(echange.categorie.destinataire_id, auth.organisation.acteur_id, auth.equipes);
    if (isDestinataire) modsTemp.push(TRANSFERT_ACTION);

    const actionsMod = actions ? modsTemp.concat(actions) : modsTemp;

    return (
      <React.Fragment>
        <Prompt
          titre={"Ajout d'une action"}
          onClose={this.handleClose}
          open={this.props.open}
          fullWidth={true}
          maxWidth={"lg"}
          actions={
            <DialogActions sx={{ marginRight: 4 }}>
              <ButtonText
                action={this.handleClose}
                color="info"
                texte="Annuler"
                variant="text"
              />
              <ButtonText
                action={this.handleSubmit}
                color="primary"
                texte="Enregistrer l'Action"
              />
            </DialogActions>
          }
        >
          <DialogContent>
            <Grid container sx={{ padding: 1 }}>
              <Grid container item xs={12} spacing={2} alignItems="center">
                <Grid item xs={10}>
                  <SelectDefault
                    label="Action"
                    value={action}
                    options={actionsMod.map(act => {
                      return { label: act.libelle, value: act._id }
                    })}
                    setValue={(value) => this.setState({ action: value })}
                  />
                </Grid>
                <Grid item xs={2}>
                  <FormControlLabel
                    control={<Switch
                      checked={this.state.urgent}
                      onChange={this.handleSwitch}
                      color="warning"
                      name="urgent"
                      inputProps={{ 'aria-label': 'Urgent ?' }}
                    />}
                    label="Urgence"
                    labelPlacement="start"
                  />
                </Grid>
                {displayEquipes &&
                  <Grid item xs={12}>
                    <SelectUI required={false} handleChange={this.handleSelectDestinataire}
                      items={acteursOrganisationMenuItems}
                      label="Equipe destinataire du transfert" value={transfertDestinataire}
                      name="transfertDestinataire" />
                  </Grid>
                }
                <Grid item xs={6}>
                  <InputDefault
                    label="Commentaire"
                    fullWidth
                    value={this.state.commentairePublic}
                    setValue={(value) => {
                      if (value !== "") {
                        this.setState({ errerMessage: false });
                      };
                      this.setState({ commentairePublic: value });
                    }}
                    multiline
                  />
                </Grid>
                <Grid item xs={6}>
                  <InputDefault
                    label="Commentaire Privé"
                    fullWidth
                    value={this.state.commentairePrive}
                    setValue={(value) => {
                      if (value !== "") {
                        this.setState({ errerMessage: false });
                      };
                      this.setState({ commentairePrive: value })
                    }}
                    multiline
                  />
                </Grid>
                {errerMessage &&
                  <Alert severity="error">
                    Vous devez remplir au moins <strong>une des commentaires</strong>
                  </Alert>
                }
                <Grid item xs={12}>
                  <UploaderFormData
                    uploadFile={this.upload.bind(this)}
                    deleteFile={this.deletefile.bind(this)}
                    fichiers={fichiers || []}
                    multiple
                    maxFileSize={53000000}
                    maxNbFiles={10}
                    allowedFileTypes={["text/plain", "application/vnd.oasis.opendocument.text", "image/jpeg", "image/png", "application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"]}
                  />
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
        </Prompt>

        <Prompt
          titre={"Confirmation du transfert"}
          onClose={this.handleCloseConfirmation}
          open={this.state.displayTransfertConfirmationDialog}
          fullWidth={false} maxWidth={"md"}
          actions={
            <DialogActions>
              <Annuler action={this.handleCloseConfirmation} texte={"Annuler"} />
              <Valider action={this.validationSubmit} texte={"Transférer"} />
            </DialogActions>
          }
        >
          <DialogContent>
            <Grid container>
              <Grid container item xs={12}>
                <Typography variant="body1">
                  Vous allez transférer cet échange
                  vers <b>{this.getActeurName(transfertDestinataire, acteursOrganisation)}</b>, souhaitez-vous continuer
                  ?
                </Typography>
              </Grid>
            </Grid>
          </DialogContent>
        </Prompt>
      </React.Fragment>
    )
  }
}

AjoutAction.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,

};

function mapStateToProps(state) {
  return {
    actions: state.actions,
    documentsactions: state.documentsactions,
    auth: state.auth,
    acteursOrganisation: state.acteursOrganisation
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getActionsByCategorie: (categorie_id) => dispatch(getActionsByCategorie(categorie_id)),
    addActionEchange: (obj) => dispatch(addActionEchange(obj)),
    uploadDocumentAction: (id, data) => dispatch(uploadDocumentAction(id, data)),
    deleteFile: (id) => dispatch(deleteFile(id)),
    getDocumentsByDossier: (id) => dispatch(getDocumentsByDossier(id)),
    getDocumentsByDossierByActeur: (dossierId, acteurId) => dispatch(getDocumentsByDossierByActeur(dossierId, acteurId)),
    addNotification: (obj) => dispatch(addNotification(obj)),
    echangeEstLu: (data) => dispatch(echangeEstLu(data)),

  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(AjoutAction));