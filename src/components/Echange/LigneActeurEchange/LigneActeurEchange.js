import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import connect from "react-redux/es/connect/connect";
import { Grid, Checkbox } from '@mui/material';
import { Warning, Help } from "@mui/icons-material";
import { green, red } from '@mui/material/colors';


const styles = theme => ({
  root: {
    display: "flex",
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    '&$checked': {
      color: green[500],
    },
    alignItems: "center"
  },
  desti: {
    display: "flex",
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    '&$checked': {
      color: red[500],
    },
    alignItems: "center"
  },
  checked: {},
  label: {
    alignSelf: 'center',
  },
});

class LigneActeurEchange extends Component {
  state = {
    checked: false,
    checkedDest: false,
  };

  handleClick = () => {
    this.setState({ checked: !this.state.checked });
    this.props.handleClick(!this.state.checked);
  };

  render() {
    const { classes, acteur, destinataire, emetteur, ordre } = this.props;
    const { checked } = this.state;

    let destiCheck = false;
    if (acteur._id === destinataire) {
      destiCheck = true;
    }
    if (acteur._id === emetteur) {
      destiCheck = true;
    }

    return (
      <Grid container item xs={12}>
        {/*eslint-disable-next-line*/}
        {ordre == 0 &&
          <Grid container item xs={12}>
            <Grid item xs={4} style={{ textAlign: "center", color: "#f00" }}>
              Pour Action <Warning color={"secondary"} />
            </Grid>
            <Grid item xs={4}>
            </Grid>
            <Grid item xs={4} style={{ textAlign: "center", color: "#18b01a" }}>
              Pour Information <Help htmlColor={"#18b01a"} />
            </Grid>
          </Grid>
        }
        <Grid item xs={4}>
          <Checkbox
            checked={destiCheck}
            onChange={this.handleClick('checked')}
            value="checked"
            classes={{
              root: classes.desti,
              checked: classes.checked,
            }}
            disabled
          />
        </Grid>
        <Grid item xs={4} className={classes.label}>
          {acteur.entite}
        </Grid>
        <Grid item xs={4}>
          <Checkbox
            checked={checked}
            onChange={this.handleClick('checked')}
            value="checked"
            classes={{
              root: classes.root,
              checked: classes.checked,
            }}
            disabled={destiCheck}
          />
        </Grid>
      </Grid>
    )
  }
}

LigneActeurEchange.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
  };
}

export default withStyles(styles)(connect(mapStateToProps, null)(LigneActeurEchange));