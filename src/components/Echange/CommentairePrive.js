import React from 'react';
import Input from "../Commun/Input/Input";

export default function CommentairePrive({ value, handleChange, disabled, multiline, rows, required }) {
    return (
        <Input
            label="Commentaire Privé"
            name="commentairePrive"
            value={value.toUpperCase()}
            handleChange={handleChange}
            disabled={disabled}
            multiline={multiline}
            rows={rows}
            required={required}
        />
    )
}



