import React, { useEffect, useCallback } from "react";
import ChoiceList from "../../Commun/ChoiceList/ChoiceList";

export function FormActeursInfo({ acteurs, categorie, setForm, valuesForm }) {
  const [infos, setInfos] = React.useState(valuesForm || []);

  const handleSubmit = useCallback((left, right) => {
    let donnees = [];
    for (const element of right) {
      donnees.push({ acteur_id: element._id, entite: element.entite });
    }
    setInfos(donnees);
  }, [])

  useEffect(() => {
    setForm(infos);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [infos]);


  return (
    <ChoiceList acteurs={acteurs} categorie={categorie} handleSubmit={handleSubmit} stepper>

    </ChoiceList>
  )
}