import React, { useCallback, useEffect } from "react";
import { DialogContent } from "@mui/material";
import { StepperCreate } from "./StepperCreate";
import { addNotification } from "../../../actions/notifications";
import { useSelector, useDispatch } from "react-redux";
import { getEquipeOrganisation } from "../../../actions/acteur";
import { setEchange, estUrgent, informationEchange, uploadDocumentAction, integreDocumentsAction } from "../../../actions/echange";
import { Prompt } from "../../composants_internes";


export function WrapperCreateEchange({ open, onClose, dossier_id, acteurId }) {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  const cbActeursOrganisation = useCallback(
    (id) => dispatch(getEquipeOrganisation(id)),
    [dispatch]
  );

  useEffect(() => {
    cbActeursOrganisation(auth.organisation.acteur_id);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSetEchange = useCallback(
    async ({ formEchange, formSelectionDocument, formEchangeDocuments, formEchangeActeurs }) => {
      onClose();
      let obj = {
        dossier_id: dossier_id,
        commentairePrive: formEchange.commentairePrive,
        commentairePublic: formEchange.commentairePublic,
        etat: [{
          libelle: 'Création ',
          user: {
            nom: auth.name.family_name,
            prenom: auth.name.given_name,
            username: auth.username,
          }
        }],
        categorie: {
          categorie_id: formEchange.categorie,
          emetteur_id: formEchange.emetteur,
          destinataire_id: formEchange.destinataire,
          libelle: ""
        },
        action: [],
        createdBy: auth.name.given_name + " " + auth.name.family_name,
      };
      let echange = await dispatch(setEchange(obj));
      if (!echange.success) return false;

      echange = echange.echange;

      if (formEchange.urgent) {
        await dispatch(estUrgent(echange._id));
      }
      let fichiers = [];
      if (formEchangeDocuments) {
        await Promise.all(await formEchangeDocuments.map(async (doc) => {

          fichiers.push(await dispatch(uploadDocumentAction(echange._id, doc)));
        }));

        let obj = {
          echange_id: echange._id,
          acteur_id: echange.categorie.emetteur_id,
          action: {
            document: []
          },
        }

        if (fichiers.length > 0) {
          obj.action.document = [];
          for (const element of fichiers) {
            if (element && element._id) {
              obj.action.document.push({
                _id: element._id,
                acteurs: [{ acteur_id: echange.categorie.emetteur_id }, { acteur_id: echange.categorie.destinataire_id }],
              });
            }
          }
        }

        await dispatch(integreDocumentsAction(echange._id, [...formSelectionDocument, ...obj?.action?.document]));

      }
      if (formEchangeActeurs) {
        let docNewActeur = [];
        for (const info of formEchangeActeurs) {
          docNewActeur.push({ acteur_id: info.acteur_id })
        }
        let docsInfo = [].concat({ acteur_id: echange.categorie.emetteur_id }, { acteur_id: echange.categorie.destinataire_id }, docNewActeur)
        let document = [];
        if (fichiers.length > 0) {
          for (const element of fichiers) {
            if (element && element._id) {
              document.push({
                echangeId: echange._id,
                dossierId: dossier_id,
                mainActeur: acteurId,
                document_id: element._id,
                acteurs: docsInfo,
              });

            }
          }
        }
        await dispatch(informationEchange({ id: echange._id, information: formEchangeActeurs }, document));
      }

      if (echange) {
        await dispatch(addNotification({ variant: "success", message: "L'échange a bien été ajouté." }));
      } else {
        await dispatch(addNotification({ variant: "error", message: "Erreur lors de l'Enregistrement d'échange" }));
      }

      onClose(echange);
      // eslint-disable-next-line
    }, [dispatch, dossier_id, onClose]);

  return (
    <Prompt
      titre={"Ajout d'un échange"}
      onClose={onClose}
      fullWidth={true} maxWidth={"lg"}
      open={open}
    >
      <DialogContent>
        <StepperCreate
          setechange={handleSetEchange}
          handleClose={onClose}
        />
      </DialogContent>
    </Prompt>
  );
}