import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import connect from "react-redux/es/connect/connect";
import { Grid, Typography, Button, DialogContent, MenuItem, CircularProgress, Stepper, Step, StepLabel } from '@mui/material';
import { ArrowForwardIos } from '@mui/icons-material';
import { grey, blue } from '@mui/material/colors';
import SelectUI from "../../Commun/Input/SelectUI";
import CommentairePublic from "../CommentairePublic";
import CommentairePrive from "../CommentairePrive";
import LigneActeurEchange from "../LigneActeurEchange";
import { setEchange } from "../../../actions/echange";
import { getEquipeOrganisation } from "../../../actions/acteur";
import { AjoutDocument } from './AjoutDocument';
import { SelectionDocuments } from './SelectionDocuments';
import ActeursInfo from './ActeursInfo';
import { Prompt } from '../../composants_internes';


const styles = theme => ({
  avatar: {
    backgroundColor: blue[100],
    color: blue[600],
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 150,
  },
  cancelButton: {
    marginRight: 50
  }
});

function getSteps(documentsEmpty) {
  if (documentsEmpty) return [{ libelle: 'Création de l\'échange', id: 0 }, {
    libelle: 'Ajout de documents',
    id: 2
  }, { libelle: 'Organismes en information', id: 3 }];
  return [{ libelle: 'Création de l\'échange', id: 0 }, {
    libelle: 'Sélections des documents sur le dossier',
    id: 1
  }, { libelle: 'Ajout de documents', id: 2 }, { libelle: 'Organismes en information', id: 3 }];
}


class AjoutEchange extends Component {

  state = {
    open: false,
    dossier_id: this.props.dossier._id,
    documentsTransfert: [],
    libelle: '',
    commentairePublic: "",
    commentairePrive: "",
    emetteur: "",
    destinataire: "",
    activeStep: 0,
    echangeCreated: null,
    loading: false,
  }

  loadEmetteurSelect = () => {
    this.setState({ loading: true });
    const emetteur = this.props.auth ? this.props.auth.organisation.acteur_id : null;
    this.setState({ emetteur, loading: false });
  }

  loadDestinataireSelect = () => {
    this.setState({ loading: true });
    const categorie = this.props.categories ? this.props.categories[0] : null;
    const destinataire = categorie ? categorie.destinataire.id : null;
    this.setState({ libelle: categorie ? categorie._id : null, destinataire: destinataire, loading: false });
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.auth !== prevProps.auth) {
      this.loadEmetteurSelect();
    }
    if (this.props.categories !== prevProps.categories) {
      this.loadDestinataireSelect();
    }
  }

  componentDidMount = async () => {
    if (this.props.acteurs) {
      for (let i = 0; i < this.props.acteurs.length; i++) {
        this.setState({
          [`info` + i]: false
        });
      }
    }
    if (this.props.auth) {
      this.props.getEquipeOrganisation(this.props.auth.organisation.acteur_id);
    }
    this.loadEmetteurSelect();
    this.loadDestinataireSelect();
  };

  handleNext = () => {
    this.setState(({ activeStep }) => ({ activeStep: activeStep + 1 }));
  };

  handleBack = () => {
    this.setState(({ activeStep }) => activeStep - 1);
  };

  transfertDocuments = (documents) => {
    this.setState({ documentsTransfert: documents });
  }

  handleClose = () => {
    this.props.onClose(this.state.echangeCreated || null);
  };

  handleChange = (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
  };

  handleClick = async (name, isChecked) => {
    await this.setState({
      [`info` + name]: isChecked
    })
  }

  handleSelect = async (event) => {
    event.stopPropagation();
    if (this.props.categories && this.props.categories.length > 0) {
      for (let i = 0; i < this.props.categories.length; i++) {
        if (this.props.categories[i]._id === event.target.value) {
          await this.setState({
            [event.target.name]: event.target.value,
            destinataire: this.props.categories[i].destinataire.id,
            emetteur: this.props.categories[i].emetteur.id,
          })
        }
      }
    }
  }

  handleSelectEmetteur = async (event) => {
    event.stopPropagation();
    this.setState({ emetteur: event.target.value })
  }

  handleSubmitDocuments = async () => {
    this.setState({ activeStep: 3, })
  }

  handleSubmitIndividu = async () => {
    let obj = {
      dossier_id: this.state.dossier_id,
      commentairePrive: this.state.commentairePrive,
      commentairePublic: this.state.commentairePublic,
      etat: [{
        libelle: 'Création ',
        user: {
          nom: this.props.auth.name.family_name,
          prenom: this.props.auth.name.given_name,
          username: this.props.auth.username,
        }
      }],
      categorie: {
        categorie_id: this.state.libelle,
        emetteur_id: this.state.emetteur,
        destinataire_id: this.state.destinataire,
        libelle: ""
      },
      action: []
    };

    const echange = await this.props.setEchange(obj);

    if (echange) {
      this.setState({
        echangeCreated: echange.echange,
        activeStep: 1,
      })
    }
    return echange;
  };

  render() {
    const { classes, onClose, categories, acteurs, setEchange, auth, documents, ...other } = this.props;
    const {
      libelle,
      commentairePublic,
      commentairePrive,
      destinataire,
      emetteur,
      activeStep,
      echangeCreated
    } = this.state;

    const steps = getSteps(documents.length === 0);
    if (!acteurs || !auth) {
      return <CircularProgress size={34} className={classes.fabProgress} />;
    }

    const categoriesMenuitem = categories ? Object.keys(categories).map(key => {
      return <MenuItem key={key} value={categories[key]._id}>
        <Grid container justifyContent="false" direction="column">
          <Typography variant="p">{categories[key].libelle}</Typography>
          <Typography variant="caption" style={{ color: grey['700'] }}>Destinataire
            : {categories[key].destinataire.entite}</Typography>
        </Grid>
      </MenuItem>
    }
    ) : [];

    let emetteursMenuItem = auth.equipes ? Object.keys(auth.equipes).map(key => {
      return <MenuItem key={key + auth.equipes[key]._id} value={auth.equipes[key].acteur_id}>
        <Grid container justifyContent="false" direction="column">
          <Typography variant="p">{auth.equipes[key].entite}</Typography>
          <Typography variant="caption" style={{ color: grey['700'] }}>Equipe interne</Typography>
        </Grid>
      </MenuItem>
    }
    ) : [];


    emetteursMenuItem.push(<MenuItem key={"orga_emetteur"} value={auth.organisation.acteur_id}>
      <Grid container justifyContent="false" direction="column">
        <Typography variant="p">{auth.organisation.entite}</Typography>
        <Typography variant="caption" style={{ color: grey['700'] }}>Organisation</Typography>
      </Grid>
    </MenuItem>)

    // On ajoute en début de liste l'emetteur dans les LigneActeurEchange

    const filterNoOrganisation = acteurs.filter(act => act._id !== auth.organisation.acteur_id);
    const acteursListe = filterNoOrganisation ? Object.keys(filterNoOrganisation).map(key => {
      return <LigneActeurEchange
        key={filterNoOrganisation[key]._id}
        acteur={filterNoOrganisation[key]}
        destinataire={destinataire}
        ordre={key}
        handleClick={this.handleClick.bind(this, key)}
      />
    }
    ) : [];

    return (
      <Prompt
        titre={"Ajout d'un échange"}
        onClose={this.handleClose}
        fullWidth={true} maxWidth={"lg"}
        aria-labelledby="simple-dialog-title" {...other}
      >
        <DialogContent>
          <div container xs={12} style={{ width: "100%", marginBottom: 30 }}>
            <Stepper activeStep={activeStep}>
              {steps.map((label, index) => {
                return <Step key={label.libelle}>
                  <StepLabel>{label.libelle}</StepLabel>
                </Step>
              })}
            </Stepper>
          </div>
          {this.state.loading ?
            <CircularProgress size={34} className={classes.fabProgress} />
            : null
          }
          {steps[activeStep].id === 0 &&
            <Fragment>
              <Grid container spacing={2}>
                <Grid container item alignItems="flex-end" spacing={2}>
                  <Grid item xs={6}>
                    <SelectUI required={true} handleChange={this.handleSelect} items={categoriesMenuitem}
                      label="Libellé" value={libelle} name="libelle" />
                  </Grid>
                  {emetteursMenuItem.length > 1 &&
                    <Grid item xs={6}>
                      <SelectUI required={true} handleChange={this.handleSelectEmetteur} items={emetteursMenuItem}
                        label="Emetteur" value={emetteur} name="emetteur" />
                    </Grid>
                  }
                </Grid>
                <Grid item xs={12}>
                  <CommentairePublic
                    handleChange={this.handleChange.bind(this)}
                    value={commentairePublic}
                    disabled={false}
                    multiline={true}
                  />
                </Grid>
                <Grid item xs={12}>
                  <CommentairePrive
                    handleChange={this.handleChange.bind(this)}
                    value={commentairePrive}
                    disabled={false}
                    multiline={true}
                  />
                </Grid>
              </Grid>

              <Grid container justifyContent='flex-end' spacing={2} sx={{ marginTop: 1 }}>
                <Grid item>
                  <Button variant="text" onClick={this.handleClose}>
                    Fermer
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={this.handleSubmitIndividu}
                    endIcon={<ArrowForwardIos />}
                  >
                    Valider
                  </Button>
                </Grid>
              </Grid>
            </Fragment>
          }
          {steps[activeStep].id === 1 && echangeCreated &&
            <SelectionDocuments
              echange={echangeCreated}
              nextStep={this.handleNext.bind(this)}
              handleStep={this.handleClose.bind(this)}
              transfertDocuments={this.transfertDocuments.bind(this)}
            />
          }
          {steps[activeStep].id === 2 && echangeCreated &&
            <AjoutDocument
              documentsTransfert={this.state.documentsTransfert}
              echange={echangeCreated}
              deleteFile={this.props.deleteFile}
              nextStep={this.handleNext.bind(this)}
              handleClose={this.handleClose.bind(this)}
            />
          }
          {steps[activeStep].id === 3 &&
            <ActeursInfo
              acteurs={filterNoOrganisation}
              echange={echangeCreated}
              handleClose={this.handleClose.bind(this)}
            />
          }
        </DialogContent>
      </Prompt>
    );
  }
}

AjoutEchange.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func,

};

function mapStateToProps(state) {
  return {
    categories: state.categories,
    acteurs: state.acteurs,
    auth: state.auth,
    acteursOrganisation: state.acteursOrganisation,
    documents: state.documents,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    setEchange: (obj) => dispatch(setEchange(obj)),
    getEquipeOrganisation: (idOrganisation) => dispatch(getEquipeOrganisation(idOrganisation)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(AjoutEchange));