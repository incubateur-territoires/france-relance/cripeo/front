import React, { useState, useEffect } from 'react';
import { useSelector } from "react-redux";
import { Checkbox, Grid, List, ListItem, ListItemIcon, ListItemText, Paper, Typography } from '@mui/material';
import makeStyles from '@mui/styles/makeStyles';


// eslint-disable-next-line
const useStyles = makeStyles((theme) => ({
  checkBox: {
    marginLeft: theme.spacing(1.5),
  },
}));


export function FormSelectionDocuments({ setForm, valuesForm }) {
  const documents = useSelector(state => state.documents);
  const [fichiers, setFichiers] = useState(valuesForm || []);

  const saveDocuments = () => {
    setForm(fichiers);
  }
  useEffect(() => {
    if (fichiers.length > 0) {
      saveDocuments();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fichiers]);

  const handleToggle = (value) => (event) => {
    event.stopPropagation();
    const currentIndex = fichiers.findIndex(doc => doc._id === value._id);

    let newFichiers = [...fichiers];

    if (currentIndex === -1) {
      newFichiers.push(value);
    } else {
      newFichiers.splice(currentIndex, 1);
    }
    setFichiers(newFichiers);
  }

  const documentsRender = documents.map(doc =>
    <ListItem key={doc._id + "fichier"} role={undefined} dense button onClick={handleToggle(doc)}>
      <ListItemIcon>
        <Checkbox
          edge="start"
          checked={fichiers ? fichiers.findIndex(newDoc => doc._id === newDoc._id) !== -1 : false}
          tabIndex={-1}
          disableRipple
          inputProps={{ 'aria-labelledby': `checkbox-list-label-${doc._id}` }}
        />
      </ListItemIcon>
      <ListItemText id={`checkbox-list-label-${doc._id}`} primary={doc.titre} />
    </ListItem>
  )

  return (
    <Grid container>
      <Grid container item style={{ padding: 30 }} justifyContent={"center"}>
        <Paper style={{ padding: 5, textAlign: "center" }} >
          <Typography variant={"h6"}>Documents</Typography>
          <List style={{ minWidth: 250 }}>
            {documentsRender}
          </List>
        </Paper>
      </Grid>
    </Grid>
  )
}