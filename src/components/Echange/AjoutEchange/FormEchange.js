import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Switch, Tooltip, FormControlLabel } from '@mui/material';
import CommentairePrive from '../CommentairePrive';
import CommentairePublic from '../CommentairePublic';
import { SelectMultiLine } from '../../composants_internes';


export function FormEchange({ setForm, valuesForm }) {
  const auth = useSelector((state) => state.auth);
  const categories = useSelector((state) => state.categories);
  const [commentairePublic, setCommentairePublic] = React.useState(valuesForm?.commentairePublic || '');

  const sortedCategories = categories.filter(cat => cat.actif).sort(function (a, b) {
    if (a.libelle.toUpperCase() < b.libelle.toUpperCase()) return -1;
    if (a.libelle.toUpperCase() > b.libelle.toUpperCase()) return 1;
    return 0
  });

  const [commentairePrive, setCommentairePrive] = React.useState(valuesForm?.commentairePrive || '');
  const [categorie, setCategorie] = React.useState(valuesForm?.categorie || sortedCategories?.[0]?._id);
  const [emetteur, setEmetteur] = React.useState(valuesForm?.emetteur || auth.organisation.acteur_id);
  const [urgent, setUrgent] = React.useState(categorie ? categories.find(c => c._id === categorie)?.urgent : true);

  useEffect(() => {
    setUrgent(categories.find(c => c._id === categorie)?.urgent)
    // eslint-disable-next-line
  }, [categorie]);

  useEffect(() => {
    setForm({
      commentairePublic,
      commentairePrive,
      categorie,
      emetteur,
      destinataire: categories.find(c => c._id === categorie)?.destinataire.id,
      urgent,
    });
  }
    // eslint-disable-next-line react-hooks/exhaustive-deps
    , [commentairePublic, commentairePrive, categorie, emetteur, urgent]);

  const optionEquipes = auth.equipes.map((equipe) => {
    return {
      value: equipe.acteur_id,
      label: equipe.entite,
      secondLabel: "Equipe interne",
    }
  });
  optionEquipes.push({
    value: auth.organisation.acteur_id,
    label: auth.organisation.entite,
    secondLabel: 'Organisation',
  });

  const optionCategories = sortedCategories.filter(cat => cat.actif).map((categ) => {
    return {
      value: categ._id,
      label: categ.libelle,
      secondLabel: "Destinataire : " + categ.destinataire.entite,
    }
  });

  const handleSetCommentairePublic = async (event) => {
    await setCommentairePublic(event?.target?.value);
  }

  const handleSetCommentairePrive = async (event) => {
    await setCommentairePrive(event?.target?.value);
  }

  const handleChangeUrgence = (event) => {
    setUrgent(event.target.checked);
  };

  return (
    <Grid container spacing={2}>
      <Grid container item alignItems="center" spacing={2} >
        <Grid item xs={6}>
          <SelectMultiLine
            label="Libellé"
            value={categorie}
            setValue={setCategorie}
            options={optionCategories}
          />
        </Grid>
        {optionEquipes.length > 1 &&
          <Grid item xs={6}>
            <SelectMultiLine
              label={"Emetteur"}
              options={optionEquipes}
              value={emetteur}
              setValue={setEmetteur}
            />
          </Grid>
        }
        <Grid container item xs={6} alignItems='center'>
          <FormControlLabel
            control={<Switch
              checked={urgent}
              onChange={handleChangeUrgence}
              color="warning"
              name="urgent"
              inputProps={{ 'aria-label': "Prioritaire de l'échange" }}
            />}
            label="Prioritaire"
            labelPlacement="start"
          />
        </Grid>
      </Grid>

      <Tooltip title="Commentaire visible pour l’émetteur, le destinataire et les organisations en information." arrow sx={{ fontSize: "24px" }}>
        <Grid item xs={12}>
          <CommentairePublic
            handleChange={handleSetCommentairePublic}
            value={commentairePublic}
            disabled={false}
            multiline={true}
          />
        </Grid>
      </Tooltip>

      <Tooltip title="Commentaire visible pour l’émetteur et le destinataire." arrow sx={{ fontSize: "48px" }}>
        <Grid item xs={12}>
          <CommentairePrive
            handleChange={handleSetCommentairePrive}
            value={commentairePrive}
            disabled={false}
            multiline={true}
          />
        </Grid>
      </Tooltip>
    </Grid>
  )
}