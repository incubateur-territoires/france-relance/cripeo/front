import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import { informationEchange } from "../../../actions/echange";
import ChoiceList from "../../Commun/ChoiceList/ChoiceList";

export default function ActeursInfo({ acteurs, echange, handleClose }) {
  const dispatch = useDispatch();

  const cbInformationEchange = useCallback(
    (envoi) => dispatch(informationEchange(envoi)),
    [dispatch]
  );

  const handleSubmit = async (left, right) => {
    let donnees = [];
    for (const element of right) {
      donnees.push({ acteur_id: element._id, entite: element.entite });
    }
    let envoi = {
      id: echange._id,
      information: donnees
    }
    await cbInformationEchange(envoi);
    handleClose();
  }

  return (
    <ChoiceList acteurs={acteurs} echange={echange} handleSubmit={handleSubmit}>

    </ChoiceList>
  )
}