import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Uploader from '../../Commun/Uploader/Uploader';
import { Button, Checkbox, FormControlLabel, Grid } from '@mui/material';
import { ArrowForwardIos } from '@mui/icons-material';
import { uploadDocumentAction, integreDocumentsAction, estUrgent } from '../../../actions/echange';
import { addNotification } from "../../../actions/notifications";
import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme) => ({
  checkBox: {
    marginLeft: theme.spacing(1.5),
  },
}));

export function AjoutDocument({ echange, deleteFile, nextStep, handleClose, documentsTransfert }) {
  const classes = useStyles();
  const [fichiers, setFichiers] = useState([]);
  const [urgent, setUrgent] = useState(false);
  const dispatch = useDispatch();

  const cbUploadDocumentAction = useCallback(
    (id, data) => dispatch(uploadDocumentAction(id, data)),
    [dispatch]
  );

  const cbAddNotification = useCallback(
    (obj) => dispatch(addNotification(obj)),
    [dispatch]
  );

  const cbEstUrgent = useCallback(
    (echange_id) => dispatch(estUrgent(echange_id)),
    [dispatch]
  );

  const cbIntegreDocumentsAction = useCallback(
    (echangeId, documents) => dispatch(integreDocumentsAction(echangeId, documents)),
    [dispatch]
  );

  const documentsactions = useSelector(state => state.documentsactions)

  useEffect(() => {
    if (documentsactions) {
      let temp = [...fichiers];
      temp.push(documentsactions)
      setFichiers(temp);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [documentsactions]);

  const upload = async (id, data) => {
    cbUploadDocumentAction(id, data);
  };

  const saveUpload = async () => {
    let obj = {
      echange_id: echange._id,
      acteur_id: echange.categorie.emetteur_id,
      action: {
        document: []
      },
    }

    if (fichiers.length > 0) {
      obj.action.document = [];
      for (const element of fichiers) {
        obj.action.document.push({
          _id: element._id,
          acteurs: [{ acteur_id: echange.categorie.emetteur_id }, { acteur_id: echange.categorie.destinataire_id }],
        });
      }
    }

    await cbEstUrgent(echange._id);
    cbIntegreDocumentsAction(echange._id, [...documentsTransfert, ...obj?.action?.document]);
    nextStep();
  }

  const deletefile = async (id) => {
    deleteFile(id)
    let fileList = fichiers
    const currentIndex = fileList.findIndex((file => file._id === id));

    if (currentIndex !== -1) {
      fileList.splice(currentIndex, 1);
    }
    setFichiers(fileList);
  }

  return (
    <div className="AjoutDocument">
      <Grid container spacing={2}>
        <Grid item>
          <FormControlLabel
            control={
              <Checkbox
                className={classes.checkBox}
                checked={urgent}
                onChange={() => setUrgent(!urgent)}
              />
            }
            label="Urgent" />
        </Grid>
      </Grid>
      <Uploader
        fichiers={fichiers}
        upload={upload}
        delete={deletefile}
        id={echange._id}
        type="doc"
      />

      <Grid container justifyContent='flex-end' spacing={2}>
        <Grid item>
          <Button variant="text" onClick={handleClose}>
            Fermer
          </Button>
        </Grid>
        <Grid item>
          <Button
            variant="contained"
            color="primary"
            onClick={saveUpload}
            endIcon={<ArrowForwardIos />}
          >
            Valider
          </Button>
        </Grid>
      </Grid>
    </div>
  );
}