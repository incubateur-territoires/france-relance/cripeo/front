import React, { useEffect, useState } from 'react';
import { UploaderFormData } from '../../composants_internes';


export function FormAjoutDocument({ setForm, valuesForm }) {
  const [fichiers, setFichiers] = useState(valuesForm || []);

  const upload = async (data) => {
    setFichiers([...fichiers, ...data]);
  }

  const deleteFic = async (key) => {
    setFichiers(fichiers.filter((fichier, index) => index !== key));
  }

  useEffect(() => {
    setForm(fichiers);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [fichiers]);

  return (
    <>
      <UploaderFormData
        uploadFile={upload}
        deleteFile={deleteFic}
        fichiers={fichiers}
        multiple
        maxFileSize={53000000}
        maxNbFiles={10}
        allowedFileTypes={["text/plain", "application/vnd.oasis.opendocument.text", "image/jpeg", "image/png", "application/pdf", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"]}
      />
    </>
  );
}