import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { StepperDefault } from "../../composants_internes";
import { Grid } from '@mui/material';
import { FormEchange } from "./FormEchange";
import { FormSelectionDocuments } from "./FormSelectionDocuments";
import { FormAjoutDocument } from "./FormAjoutDocument";
import { FormActeursInfo } from "./FormActeursInfo";
import { addNotification } from "../../../actions/notifications";


export function StepperCreate({ setechange, handleClose }) {
  const categories = useSelector((state) => state.categories);
  const acteurs = useSelector((state) => state.acteurs);
  const documents = useSelector((state) => state.documents);

  const [formEchange, setFormEchange] = React.useState({});
  const [formSelectionDocument, setFormSelectionDocument] = React.useState([]);
  const [formEchangeDocuments, setFormEchangeDocuments] = React.useState([]);
  const [formEchangeActeurs, setFormEchangeActeurs] = React.useState([]);
  const [steps, setSteps] = React.useState([]);

  const dispatch = useDispatch();

  const cbNoCategorieSelected= useCallback(() => {
    dispatch(addNotification({ variant: 'warning', message: "Vous devez sélectionner une catégorie, voir le champs 'Libellé', Si aucune catégorie n'est disponible contactez l'administrateur de votre organisation." }))
  }, [dispatch])

  useEffect(() => {
    let stepsArray = [
      {
        label: "Création de l'échange",
        component: (
          <FormEchange
            setForm={setForm}
            valuesForm={formEchange}
          />
        ),
        controlSubmit: () => {
          if(!formEchange.categorie) cbNoCategorieSelected();
          return !!formEchange.categorie
        },
      },
    ];

    if (documents.length > 0) {
      stepsArray.push({
        label: "Ajout des documents du dossier",
        component: (
          <FormSelectionDocuments
            setForm={setFormSelection}
            valuesForm={formSelectionDocument}
          />
        ),
        prec: true,
      });
    }

    stepsArray.push({
      label: "Ajout de documents supplémentaires",
      component: (
        <FormAjoutDocument
          setForm={setFormAjoutDocument}
          valuesForm={formEchangeDocuments}
        />
      ),
      prec: true,
    });

    if (formEchange.categorie) {
      const newCategorie = categories.find((categ) => categ._id === formEchange.categorie);
      if (newCategorie) {
        stepsArray.push({
          label: "Sélection des acteurs en information",
          component: (
            <FormActeursInfo
              setForm={setFormActeursInfo}
              valuesForm={formEchangeActeurs}
              acteurs={acteurs}
              categorie={newCategorie}
            />
          ),
          submitFunction: () => {
            submitForm();
          },
          prec: true
        });
      }
    } else {
      stepsArray.push({
        label: "Sélection des acteurs en information",
        component: (
          <FormActeursInfo
            setForm={setFormActeursInfo}
            valuesForm={formEchangeActeurs}
            acteurs={acteurs}
          />
        ),
      });
    }
    setSteps(stepsArray);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formEchange, formSelectionDocument, formEchangeDocuments, formEchangeActeurs, categories, acteurs]);


  const setForm = useCallback((form) => {
    setFormEchange({ ...form });
  }, [setFormEchange]);

  const setFormSelection = useCallback((form) => {
    setFormSelectionDocument([...form]);
  }, [setFormSelectionDocument]);

  const setFormAjoutDocument = useCallback((form) => {
    setFormEchangeDocuments([...form]);
  }, [setFormEchangeDocuments])

  const setFormActeursInfo = useCallback((form) => {
    setFormEchangeActeurs([...form]);
  }, [setFormEchangeActeurs]);

  const submitForm = useCallback(() => {
    setechange({ formEchange, formSelectionDocument, formEchangeDocuments, formEchangeActeurs });
  }, [formEchange, formSelectionDocument, formEchangeDocuments, formEchangeActeurs, setechange]);


  if (steps.length === 0) {
    return null;
  }
  return (
    <Grid>
      <StepperDefault
        steps={steps}
        handleClose={handleClose}
        labelEnd="Enregistrer l'echange"
      />

    </Grid>
  );
}