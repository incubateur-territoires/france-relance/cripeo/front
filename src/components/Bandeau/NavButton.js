import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import { IconButton, Tooltip, Zoom } from '@mui/material/IconButton';


const iconSize = "medium";

const styles = {
  primary: {
    color: "white"
  },
  secondary: {
    color: "white"
  },
  tooltipMenu: {
    fontSize: 14,
    zIndex: 900,
  }
};

class NavButton extends Component {

  render() {
    const {
      classes,
      IconNav,
      libelle,
      link,
      color,
      title,
      onClickFunction,
      external
    } = this.props;

    const classe = color === "primary" ? classes.primary : classes.secondary;
    const styleTypo = {
      fontSize: "1rem"
    };
    return (
      <Tooltip
        disableFocusListener
        disableTouchListener
        TransitionComponent={Zoom}
        classes={{ tooltip: classes.tooltipMenu }}
        title={title}>
        {
          link ? (
            external ?
              <a target="_blank" rel="noopener noreferrer" href={link} className={classe} >
                <IconButton aria-label={"Formulaire d'incident"} className={classe} name="Feedback" size="large">
                  <IconNav fontSize={iconSize} /><span style={styleTypo}>{libelle}</span>
                </IconButton>
              </a>
              :
              <Link to={link} onClick={onClickFunction} className={classe}>
                <IconButton aria-label={"Formulaire d'incident"} color="inherit" className={classe} name="Feedback" size="large">
                  <IconNav fontSize={iconSize} /><span style={classes.styleTypo}>{libelle}</span>
                </IconButton>
              </Link>
          ) : (
            <IconButton aria-label={"Formulaire d'incident"} onClick={onClickFunction} color="inherit" className={classe} size="large">
              <IconNav fontSize={iconSize} /><span style={classes.styleTypo}>{libelle}</span>
            </IconButton>
          )

        }
      </Tooltip>
    );
  }
}

NavButton.propTypes = {
  classes: PropTypes.object.isRequired,
  libelle: PropTypes.string,
  link: PropTypes.string,
  color: PropTypes.oneOf(['primary', 'secondary']),
  title: PropTypes.string.isRequired,
  onClickFunction: PropTypes.func,
  IconNav: PropTypes.object,
  external: PropTypes.bool
};

export default withStyles(styles)(NavButton)
