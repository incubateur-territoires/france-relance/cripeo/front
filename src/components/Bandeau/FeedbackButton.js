import React from 'react';
import NavButton from "./NavButton";
import ForumIcon from '@mui/icons-material/Forum';

export default function FeedbackButton() {
  return (
    <NavButton
      link="https://kantree.io/f/416fee86-nouvelle-demande"
      title="Je déclare un problème rencontré sur l'application"
      IconNav={ForumIcon}
      color="secondary"
      external
    />
  );
}
