import React from 'react';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import NavButton from "./NavButton";

export default function AccountButton() {
  return (
    <NavButton
      title="Mon compte"
      IconNav={AccountCircleIcon}
      color="secondary"
      external={false}
    />
  );
}
