import React, { Component } from 'react';
import { connect } from 'react-redux';
import SignInIcon from '@mui/icons-material/ExitToApp';
import AccountIcon from '@mui/icons-material/AccountCircle';

import NavButton from "./NavButton";

class SignIn extends Component {

  state = {}

  logUser = (event) => {
    event.stopPropagation();
    if (this.props.keycloak) {
      const kc = this.props.keycloak;
      kc.login();
      this.props.setLoginStatus();
    }
  };


  render() {
    if (!this.props.auth) {
      return (
        <NavButton
          title="Se connecter pour modifier ma fiche"
          IconNav={SignInIcon}
          color="primary"
          onClickFunction={this.logUser}
          link={null}
        />

      )
    }
    else {
      return (
        <NavButton
          title="Modifier ma fiche d'agent"
          IconNav={AccountIcon}
          color="primary"
          link={`/modification/${this.props.auth.username}`}
        />


      )
    }
  }
}


function mapStateToProps(state) {
  return {
    auth: state.auth,
    keycloak: state.keycloak
  };
}

const mapDispatchToProps = (_dispatch) => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
