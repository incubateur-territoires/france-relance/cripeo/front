import React, { Component } from 'react';
import withStyles from '@mui/styles/withStyles';
import { connect } from "react-redux";
import { Grid, Dialog, DialogContent, DialogTitle, List, ListItem } from '@mui/material/Dialog';

const styles = theme => ({
  paper: {
    padding: theme.spacing(3, 2),
    margin: theme.spacing(1),
    width: '100%',
    textAlign: 'left'
  },
  button: {
    backgroundColor: '#22671d',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: 'orange'
    }
  },
});

class WrapperProfile extends Component {

  handleClose = () => {
    if (this.props.onClose) {
      this.props.onClose();
    }
  };

  render() {
    return (
      <Dialog
        open={this.props.open}
        onClose={this.handleClose}
        fullWidth
        maxWidth={"lg"}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">Moi</DialogTitle>
        <DialogContent>
          <List>
            <ListItem>
              <Grid container spacing={3}>
                toto

              </Grid>
            </ListItem>
          </List>
        </DialogContent>
      </Dialog>
    )
  }
}

export default withStyles(styles)(connect(null, null)(WrapperProfile));