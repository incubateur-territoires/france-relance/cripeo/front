import React from 'react';
import SettingsIcon from '@mui/icons-material/Settings';
import NavButton from "./NavButton";

export default function AdminButton() {
  return (
    <NavButton
      link="/administration"
      title="Administration"
      IconNav={SettingsIcon}
      color="secondary"
      external={false}
    />
  );
}
