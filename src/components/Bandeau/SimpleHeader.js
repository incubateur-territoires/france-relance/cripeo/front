import React, { Component } from 'react';
import { Grid, Menu, MenuItem, Typography, Badge, Button, Paper, IconButton, Tooltip, DialogContent } from "@mui/material";
import { grey, red } from '@mui/material/colors';
import withStyles from '@mui/styles/withStyles';
import { Link } from "react-router-dom";
import { testSecteurGeo } from '../../actions/acteur'
import { getUtilisateurKc, luNotif, deleteNotif } from "../../actions/utilisateur";
import { getVideoNames, getVideosByName } from "../../actions/tutoriel";
import { logoutUser } from "../../actions/home";
import { addNotification } from "../../actions/notifications";
import { connect } from "react-redux";
import { AccountCircle, Mail, Notifications, NotificationImportant, Close, ManageAccountsOutlined, FilterAltOutlined, HelpOutline, PlayCircleFilled } from "@mui/icons-material";
import { withRouter } from 'react-router-dom';
import { ApplicationBar, MenuStatus, Prompt, TH } from "../composants_internes";

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: '100%',
    zIndex: 1,
    overflow: 'hidden',
    display: 'flex',
    width: '100%',
  },
  search: {
    width: "80%",
  },
  grow: {
    flexGrow: 1,
  },
  bar: {
    backgroundColor: "#2B75D6",
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  notif: {
    minWidth: 200
  },
  notifMenu: {
    width: "100%",
    flex: 1
  }
});

class SimpleHeader extends Component {

  state = {
    anchorEl: null,
    mobileMoreAnchorEl: null,
    adminEl: null,
    mobileAdminEl: null,
    notifEl: null,
    tutorielEl: null,
    videoNames: "",
    video: "",
    nameVideo: ","
  }

  componentDidMount = async () => {
    if (this.props.keycloak) {
      this.props.getUtilisateurKc(this.props.keycloak.tokenParsed.sub);
    }
  }

  handleProfileMenuOpen = (event) => {
    event.stopPropagation();
    this.setState({ anchorEl: event.currentTarget });
  };

  handleAdminMenuOpen = (event) => {
    event.stopPropagation();
    this.setState({ adminEl: event.currentTarget });
  };

  handleMobileMenuOpen = event => {
    this.setState({ mobileMoreAnchorEl: event.currentTarget });
  };

  handleMenuClose = () => {
    this.setState({ anchorEl: null, adminEl: null, name: null, notifEl: null, tutorielEl: null, video: "", nameVideo: "" });
    this.handleMobileMenuClose();
  };

  handleNotifMenuOpen = event => {
    event.stopPropagation();
    this.setState({ notifEl: event.currentTarget });
  };

  handleMobileMenuClose = () => {
    this.setState({ mobileMoreAnchorEl: null });
  };

  handleNotif = (notif) => event => {
    event.stopPropagation();
    this.props.luNotif(notif._id);
    // Changement de route
    this.props.history.push("/dossier/" + notif.dossier_id);
  }

  deleteNotif = (notif) => event => {
    event.stopPropagation();
    this.props.deleteNotif(notif._id);
  }

  handleOpenTutoriel = async (event) => {
    event.stopPropagation();
    this.setState({ tutorielEl: event.currentTarget });
    const names = await this.props.getVideoNames();
    this.setState({ videoNames: names });
  }

  handleOpenVideo = (name) => async (event) => {
    this.setState({ nameVideo: "", video: "" });
    event.stopPropagation();
    const video = await this.props.getVideosByName(name);
    this.setState({ nameVideo: name, video: video.config.url });
  }

  render() {
    const { classes, auth, roles, keycloak, notificationsAction, acteur } = this.props;
    const { anchorEl, mobileMoreAnchorEl, adminEl, notifEl, tutorielEl, videoNames, video, nameVideo } = this.state;

    const isMenuOpen = Boolean(anchorEl);
    const isAdminOpen = Boolean(adminEl);
    const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
    const isNotifOpen = Boolean(notifEl);
    const isToturielOpen = Boolean(tutorielEl);

    const renderTutoriel = (
      <Prompt
        titre={"Les tutoriels d'utilisation de l'application"}
        open={isToturielOpen}
        onClose={this.handleMenuClose}
        fullWidth
        maxWidth={"lg"}
      >
        <DialogContent>
          <Paper>
            <Grid margin={1}>
              <Typography variant="body1" fontWeight="bold">
                Apprenez comment faire pour :
              </Typography>
            </Grid>
            <Grid item>
              {videoNames && videoNames.length > 0 &&
                videoNames.map((name, index) => {
                  return (nameVideo !== name && <Button
                    sx={{ margin: 1 }}
                    key={name + index}
                    onClick={this.handleOpenVideo(name)}
                    color={nameVideo === name ? "inherit" : "primary"}
                    variant={nameVideo === name ? "contained" : "outlined"}
                    endIcon={<PlayCircleFilled />}
                  >
                    {name.slice(0, -4).replaceAll("_", " ")}
                  </Button>)
                })
              }
            </Grid>
          </Paper>
          <Grid item marginTop={2}>
            {video &&
              <TH  >
                {nameVideo.slice(0, -4).replaceAll("_", " ")}
              </TH>
            }
            {video &&
              /* eslint-disable-next-line jsx-a11y/media-has-caption */
              <video width="1100" id="videoPlayer" controls >
                <source src={video} type="video/mp4" />
              </video>
            }
          </Grid>
        </DialogContent>
      </Prompt>
    );

    const renderMenuProfile = (
      <Menu
        anchorEl={anchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={"primary-account-menu"}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMenuOpen}
        onClose={this.handleMenuClose}
      >
        <Typography sx={{ margin: 1 }} variant="caption">
          Type de compte : </Typography>
        <br />
        <Typography variant="body2" sx={{ ml: 2, fontWeight: "bold" }}>{auth ? auth.groupe : ""}</Typography>
        <br />
        <Typography sx={{ margin: 1 }} variant="caption">Organisation :
        </Typography>
        <br />
        <Typography variant="body2" sx={{ ml: 2, mb: 2, fontWeight: "bold" }}>{auth ? auth.organisation_name.toUpperCase() : ""}</Typography>
        <Link style={{ textDecoration: "none", color: "#000" }} to={"/monparametrage"}>
          <MenuItem>
            <Button
              variant="contained"
              startIcon={<FilterAltOutlined />
              }>
              Gérer mes filtres</Button>
          </MenuItem>
        </Link>
      </Menu>
    );

    const administrateurFonc = roles ? keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.LISTE_ORGA) !== -1 : false;
    const administrateurOrg = roles ? keycloak.tokenParsed.realm_access.roles.findIndex(role => role === roles.GESTION_ORGA) !== -1 : false;

    const renderMenuAdmin = (
      <Menu
        anchorEl={adminEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        id={"primary-admin-menu"}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isAdminOpen}
        onClose={this.handleMenuClose}
      >
        {administrateurFonc &&
          <MenuItem onClick={this.handleMenuClose}>
            <Link style={{ textDecoration: "none", color: "#000" }} to={"/acteurs"}>
              <p>Gestion des organisations</p>
            </Link>
          </MenuItem>
        }
        {
          administrateurOrg && auth &&
          <MenuItem onClick={this.handleMenuClose}>
            <Link style={{ textDecoration: "none", color: "#000" }} to={"/organisation/" + auth.organisation.acteur_id} >
              <p>Gestion de l'organisation</p>
            </Link>
          </MenuItem>
        }
      </Menu>
    );

    const renderMobileMenu = (
      <Menu
        anchorEl={mobileMoreAnchorEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={this.handleMobileMenuClose}
      >
        <MenuItem onClick={this.handleMenuClose}><p>{auth ? auth.groupe : ""}</p></MenuItem>
        <MenuItem onClick={this.handleMenuClose}><p>{auth ? auth.organisation_name : ""}</p></MenuItem>
      </Menu>
    );

    const notifsRender = notificationsAction?.map((not, index) => {
      return <MenuItem key={not.dossier_id + index + not.categorie} onClick={this.handleNotif(not)} >
        <Grid container alignItems="center" justifyContent="space-between" >
          <Grid item>
            {
              !not.urgent ?
                <Notifications style={{ color: grey[300] }} />
                :
                <NotificationImportant style={{ color: red[400] }} />
            }
          </Grid>
          <Grid item xs={4}>
            <Typography variant="caption">{not.individu}</Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography variant="caption" color="primary">{not.categorie}</Typography>
          </Grid>
          <Grid item>
            <IconButton size="small" onClick={this.deleteNotif(not)}>
              <Close />
            </IconButton>
          </Grid>
        </Grid>
      </MenuItem>
    });

    const nbNotif = notificationsAction?.reduce((acc, not) => {
      return acc + (!not.lu ? 1 : 0);
    }, 0);
    const renderNotifsMenu = notificationsAction ? (
      <Menu
        anchorEl={notifEl}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={notificationsAction.length > 0 && isNotifOpen}
        onClose={this.handleMenuClose}
        className={classes.notifMenu}
        PaperProps={{
          style: {
            minWidth: 600,
          },
        }}
      >
        {notifsRender}
      </Menu>
    ) : null;

    let optionStatus = [{ label: 'API Adresse', 'url': 'https://gateway-api.calvados.fr/adresses/?q=8+bd+du+port' }]

    if (acteur?.secteurGeo && acteur?.secteurGeo?.actif && acteur?.secteurGeo?.url) {
      optionStatus.push({ label: 'Secteurs Geographique', callApi: testSecteurGeo })
    }

    const modulesAff = [
      {
        component: (
          <Typography variant="h6">
            {this.props.auth && (this.props.auth.name.family_name + ' ' + this.props.auth.name.given_name)}
          </Typography>
        )
      },
      {
        component: (
          <Tooltip
            title={"Mon compte"}>
            <IconButton onClick={this.handleProfileMenuOpen} color="inherit" size="large">
              <AccountCircle />
            </IconButton>
          </Tooltip>
        )
      },
      {
        component: (
          <Tooltip
            title={"Notifications"}>
            <IconButton color="inherit" onClick={this.handleNotifMenuOpen} size="large">
              <Badge badgeContent={nbNotif} color="warning">
                <Mail />
              </Badge>
            </IconButton>
          </Tooltip>
        )
      },
      {
        component: (
          <>
            {(administrateurFonc || administrateurOrg) &&
              <Tooltip
                title={"Administration"}>
                <IconButton onClick={this.handleAdminMenuOpen} color="inherit" size="large">
                  <ManageAccountsOutlined />
                </IconButton>
              </Tooltip>
            }
          </>
        )
      },
      {
        component: (
          <>
            <MenuStatus services={optionStatus} />
          </>
        )
      },
      {
        component: (
          <Tooltip
            title={"Tutoriels d'utilisation"}>
            <IconButton color="inherit" onClick={this.handleOpenTutoriel} size="large">
              <HelpOutline />
            </IconButton>
          </Tooltip>
        )
      }
    ];

    return (
      <>
        <ApplicationBar
          texte="Cripéo"
          modules={modulesAff}
          link={"/"}
          linkLogo
          linkTitle
          RouterLink={Link}
          keycloak={{ logout: () => this.props.logoutUser() }}
        />
        {renderMenuProfile}
        {renderMenuAdmin}
        {renderMobileMenu}
        {renderNotifsMenu}
        {renderTutoriel}
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {this.props.children}
        </main>
      </>
    )
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    keycloak: state.keycloak,
    utilisateur: state.utilisateur,
    roles: state.roles,
    notificationsAction: state.notificationsAction,
    acteur: state.acteur
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    addNotification: (obj) => dispatch(addNotification(obj)),
    logoutUser: () => dispatch(logoutUser()),
    getUtilisateurKc: (idKc) => dispatch(getUtilisateurKc(idKc)),
    luNotif: (id) => dispatch(luNotif(id)),
    deleteNotif: (id) => dispatch(deleteNotif(id)),
    getVideoNames: () => dispatch(getVideoNames()),
    getVideosByName: (name) => dispatch(getVideosByName(name)),
  }
};

export default withStyles(styles, { withTheme: true })(connect(mapStateToProps, mapDispatchToProps)(withRouter(SimpleHeader)));