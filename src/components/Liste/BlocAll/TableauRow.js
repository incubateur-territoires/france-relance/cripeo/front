import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Link as RouterLink } from "react-router-dom";
import { withStyles } from "@mui/styles";
import moment from 'moment';
import 'moment/locale/fr';
import { Button, TableRow, TableCell, Tooltip, Typography } from '@mui/material';
import { Info, Pageview, NotificationImportant, Notifications } from '@mui/icons-material';
import { red, grey, blue } from "@mui/material/colors";


const styles = theme => ({
    button: {
        margin: theme.spacing(1),
    }
});

class TableauRow extends PureComponent {

    render() {
        const { classes, isSelected, labelId, dossier, celluleEntete, nextkey } = this.props;

        const renderRow = celluleEntete.map((item, index) => {
            let res;
            const resDos = dossier ? Array.isArray(dossier) ? dossier[0] : dossier : null;

            if (!resDos) return <div />;

            let value = resDos[item.id];
            // eslint-disable-next-line

            if (item.id === 'circo' &&
                resDos.secteurGeo &&
                resDos.secteurGeo.findIndex(elem => elem.acteur_id === this.props.auth.organisation.acteur_id) !== -1 &&
                resDos.secteurGeo[resDos.secteurGeo.findIndex(elem => elem.acteur_id === this.props.auth.organisation.acteur_id)].libelle
            ) {
                value = resDos.secteurGeo[resDos.secteurGeo.findIndex(elem => elem.acteur_id === this.props.auth.organisation.acteur_id)].libelle;
            }

            switch (item.type) {
                case "string":
                    res = <TableCell key={nextkey + item.type + index} align='left' padding={item.id === 'circo' ? 'none' : 'normal'} >{value ? value.toUpperCase() : ""}</TableCell>
                    break;
                case "date":
                    res = <TableCell key={nextkey + item.type + index} align='left' padding='none'>{value ? moment(value).format('DD-MM-YYYY') : ""}</TableCell>
                    break;
                case "button":
                    res = <TableCell key={nextkey + item.type + index} align='center' padding='none'>
                        <Button
                            component={RouterLink}
                            variant="contained"
                            title={resDos.labelButton}
                            className={classes.button}
                            to={`/dossier/${resDos._id}`}
                            startIcon={<Pageview />}>
                            {item.labelButton}
                        </Button>
                    </TableCell>
                    break;
                case "id":
                    res =
                        <TableCell key={nextkey + item.type + index} align='left' component="th" id={labelId} scope="row" padding="none">
                            {value}
                        </TableCell>
                    break;
                case "boolean":
                    res =
                        <TableCell key={nextkey + item.type + index} align='left' component="th" scope="row" padding={item.disablePadding ? "normal" : "none"}>
                            {value ?
                                <Tooltip title={<Typography>Urgence</Typography>}><NotificationImportant style={{ color: red[500] }} /></Tooltip>
                                :
                                <Tooltip title={<Typography>Non Urgence</Typography>}><Notifications style={{ color: grey[300] }} /></Tooltip>
                            }
                        </TableCell>
                    break;
                case "info":
                    res =
                        <TableCell key={nextkey + item.type + index} align='left' component="th" scope="row" padding="normal">
                            {value ?
                                <Tooltip title={<Typography>En observatoire</Typography>}><Info style={{ color: blue[500] }} /></Tooltip>
                                :
                                null
                            }
                        </TableCell>
                    break;
                case "numero":
                    res =
                        <TableCell key={nextkey + item.type + index} align='left' padding="normal">
                            {value ?
                                <Tooltip title={<Typography>Nombre d'échanges</Typography>}><Typography>{value}</Typography></Tooltip>
                                :
                                null
                            }
                        </TableCell>
                    break;
                default:
                    res = <TableCell key={nextkey + item.type + index} align='left'>{value}</TableCell>
                    break;
            }
            return res;
        })

        return (
            <TableRow
                hover
                role="checkbox"
                aria-checked={isSelected}
                tabIndex={-1}
                selected={isSelected}
                sx={{ backgroundColor: dossier.acteurEstInfo ? grey[50] : null }}
            >
                {renderRow}

            </TableRow>
        )
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth
    };
}


export default withStyles(styles)(connect(mapStateToProps)(TableauRow));