import React, { Component, useState } from "react";
import { Paper, Avatar, Grid, IconButton, FormControlLabel, Checkbox, Typography, Menu, MenuItem } from "@mui/material";
import withStyles from '@mui/styles/withStyles';
import 'moment/locale/fr';
import Tableau from "../BlocAll/Tableau";
import { AspectRatio, NotificationImportantOutlined, NotificationImportant, CalendarMonth } from "@mui/icons-material";
import WrapperTab from "../WrapperTab/WrapperTab";

const styles = theme => ({
  root: {
    flex: 1,
    padding: 10,
    margin: 'auto',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
    border: "solid",
    flex: 1,
  },
  title: {
    flex: '1 1 100%',
    textAlign: 'left',
    borderBottom: "solid",
    paddingRight: 10
  },
  orange: {
    color: theme.palette.getContrastText("#fff"),
    backgroundColor: "#fff",
    marginLeft: 10
  },
  urgentIcon: {
    color: "#fff",
  },
  FilterIcon: {
    color: "#fff",
  },
});

const EnhancedTableToolbar = (props) => {

  const { classes, titre, couleur, textBadge, buttonModal, dossiers, celluleEntete, urgentCheck, urgentFilter } = props;
  const [open, setOpen] = useState(false);
  const [filtreDate, setFiltreDate] = useState(false);
  const [filtreDateLabel, setFiltreDateLabel] = useState("");
  const [anchorDate, setAnchorDate] = useState(null);
  const openDate = Boolean(anchorDate);

  const setOpenModal = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
  }

  const handleClickDate = (event) => {
    setAnchorDate(event.currentTarget);
  };

  const handleCloseDate = async (event) => {
    setAnchorDate(null);
    switch (event.target.value) {
      case 1:
        setFiltreDate(true);
        setFiltreDateLabel("Aujourd'hui");
        break;
      case 2:
        setFiltreDate(true);
        setFiltreDateLabel("7 derniers jours");
        break;
      case 3:
        setFiltreDate(true);
        setFiltreDateLabel("30 derniers jours");
        break;
      default:
        setFiltreDate(false);
        setFiltreDateLabel("");
        break;
    }
    await props.handleCheckDate(event.target.value);
  }

  return (
    <Grid container className={classes.title} style={{ backgroundColor: couleur, borderColor: couleur }} alignItems="center" justifyContent="space-between">
      <Grid item container xs={7} alignItems="center">
        <Grid item>
          <Typography variant="h6" id="tableTitle"
            style={{ paddingLeft: 10, color: "#FFF" }}>
            {`${titre}`}
          </Typography>
        </Grid>
        <Grid item>
          <Avatar className={classes.orange}>{textBadge}</Avatar>
        </Grid>
      </Grid>
      <Grid item >
        {urgentFilter &&
          <FormControlLabel
            control={
              <Checkbox
                checked={urgentCheck}
                onChange={props.handleCheckUrgent}
                name="CheckUrgentFilter"
                color="primary"
                icon={<NotificationImportantOutlined />}
                checkedIcon={<NotificationImportant className={classes.urgentIcon} />}
                inputProps={{
                  'title': 'Filtrer par urgence',
                }}
              />
            }
          />
        }
        <FormControlLabel
          className={filtreDate ? classes.FilterIcon : undefined}
          control={
            <IconButton
              id="date-button"
              aria-controls={openDate ? 'menu-filtre-date' : undefined}
              aria-expanded={openDate ? 'true' : undefined}
              aria-haspopup="true"
              onClick={handleClickDate}
              aria-label="Filtrer par date"
              className={filtreDate ? classes.FilterIcon : undefined}
            >
              <CalendarMonth />
            </IconButton >
          }
          label={filtreDateLabel}
        />
        <Menu
          id="menu-filtre-date"
          open={openDate}
          onClose={handleCloseDate}
          anchorEl={anchorDate}
          MenuListProps={{
            'aria-labelledby': 'date-button',
          }}
        >
          <MenuItem onClick={handleCloseDate} value={"1"}>Aujourd'hui</MenuItem>
          <MenuItem onClick={handleCloseDate} value={"2"}>Les 7 derniers jours</MenuItem>
          <MenuItem onClick={handleCloseDate} value={"3"}>Les 30 derniers jours</MenuItem>
          <MenuItem onClick={handleCloseDate} value={"0"}>Tous</MenuItem>
        </Menu>
        {buttonModal &&
          <IconButton onClick={setOpenModal} size="large"><AspectRatio /></IconButton>
        }
      </Grid>
      <WrapperTab open={open} handleClose={handleClose} modal={{ couleur, dossiers, colonnes: celluleEntete, libelle: titre, urgentFilter }} />
    </Grid>
  );
};


class BlocAll extends Component {
  state = {
    selectedItems: [],
    urgentCheck: false,
    dateCheck: null,
  };

  handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = this.state.dossiers.map((n) => n._id);
      this.setState({
        selected: newSelecteds
      });
      return;
    }
    this.setState({
      selected: [],
    });
  };

  handleCheckUrgent = () => {
    const urgentCheck = !this.state.urgentCheck;
    this.setState({ urgentCheck });
  };

  handleCheckDate = (event) => {
    let dateCheck;
    switch (event) {
      case 1:
        dateCheck = 1;
        break;
      case 2:
        dateCheck = 7;
        break;
      case 3:
        dateCheck = 30;
        break;
      default:
        dateCheck = null;
        break;
    }
    this.setState({ dateCheck });
  };

  render() {
    const { classes, titre, celluleEntete, couleurEntete, dossiers, buttonModal, urgentFilter } = this.props;
    const { selectedItems, dense, urgentCheck, dateCheck } = this.state;

    let filteredDossiersUrgent = urgentCheck ? dossiers.filter((dossier) => dossier.urgent) : dossiers ? dossiers : [];

    let d = new Date();
    if (dateCheck) {
      d.setDate(d.getDate() - dateCheck);
    }

    let filteredDossiers = dateCheck ? filteredDossiersUrgent.filter((dossier) => dossier.updated_at >= d.toISOString()) : filteredDossiersUrgent ? filteredDossiersUrgent : [];

    return (
      <Grid container className={classes.root} flexDirection="column" justifyContent={"flex-start"}>
        <Paper className={classes.paper} style={{ borderColor: couleurEntete }}>
          <EnhancedTableToolbar
            key={titre + "table"}
            numSelected={selectedItems.length}
            classes={classes}
            titre={titre}
            couleur={couleurEntete}
            textBadge={filteredDossiers.length}
            celluleEntete={celluleEntete}
            dossiers={filteredDossiers}
            buttonModal={buttonModal}
            urgentFilter={urgentFilter}
            urgentCheck={urgentCheck}
            dateCheck={dateCheck}
            handleCheckUrgent={this.handleCheckUrgent}
            handleCheckDate={this.handleCheckDate}
          />
          <Tableau
            key={titre + "tableau"}
            nextkey={titre + "tableau"}
            celluleEntete={celluleEntete}
            dossiers={filteredDossiers}
            dense={dense}
            selectedItems={selectedItems.length}
          />
        </Paper>
      </Grid>
    )
  }
}


export default withStyles(styles)(BlocAll);