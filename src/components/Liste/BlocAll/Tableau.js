import React, { Component } from "react";
import withStyles from '@mui/styles/withStyles';
import TableEntete from "./TableauEntete";
import TableauRow from "./TableauRow";
import { getComparator, stableSort } from "../Commun/fonctionsTris";
import { TablePagination, TableContainer, Table, TableBody } from "@mui/material";

const styles = {
    root: {
        // height: "100%",
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between"
    },
    title: {
        flex: '1 1 100%',
        textAlign: 'left',
        borderBottom: "solid",
    },
    table: {
        marginTop: 10,

    }
};

class Tableau extends Component {
    state = {
        order: 'desc',
        orderBy: 'updated_at',
        selected: [],
        page: 0,
        dense: true,
        rowsPerPage: 5,
        dossiers: this.props.dossiers || [],
    };

    handleChangePage = (newPage) => {
        this.setState({ page: newPage });
    };

    handleChangeRowsPerPage = (event) => {
        this.setState({
            rowsPerPage: (parseInt(event.target.value, 10)),
            page: 0,
        });
    };

    handleRequestSort = (property) => {
        const isAsc = this.state.orderBy === property && this.state.order === 'asc';
        this.setState({
            order: isAsc ? 'desc' : 'asc',
            orderBy: property,
        });
    };

    handleSelectAllClick = (event) => {
        if (event.target.checked) {
            const newSelecteds = this.state.dossiers.map((n) => n._id);
            this.setState({ selected: newSelecteds });
            return;
        }
        this.setState({ selected: [] });
    };

    emptyRows = () => {
        return this.state.rowsPerPage - Math.min(this.state.rowsPerPage, this.state.dossiers.length - this.state.page * this.state.rowsPerPage);
    }
    isSelected = (name) => this.state.selected.indexOf(name) !== -1;

    render() {
        const { classes, celluleEntete, dense, dossiers, selectedItems, nextkey } = this.props;
        const { page, rowsPerPage, order, orderBy } = this.state;
        const renderRow = stableSort(dossiers, getComparator(order, orderBy))
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => {
                const isItemSelected = this.isSelected(row._id);
                const labelId = `enhanced-table-checkbox-${index}`;
                return (
                    <TableauRow key={nextkey + "row-" + index} isSelected={isItemSelected}
                        labelId={labelId} dossier={row} nextkey={nextkey + "row-" + index}
                        celluleEntete={celluleEntete} />
                );
            });

        return (
            <div className={classes.root}>
                <TableContainer >
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size={dense ? 'small' : 'medium'}
                        aria-label="enhanced table"
                    >
                        <TableEntete dense={dense} celluleEntete={celluleEntete}
                            onSelectAllClick={this.handleSelectAllClick.bind(this)}
                            onRequestSort={this.handleRequestSort.bind(this)}
                            selectedItems={selectedItems}
                            dossiers={dossiers} />
                        <TableBody>
                            {renderRow}
                        </TableBody>
                    </Table>
                </TableContainer>

                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={dossiers.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={this.handleChangePage}
                    onRowsPerPageChange={this.handleChangeRowsPerPage}
                />
            </div>
        )
    }
}

export default withStyles(styles)(Tableau);