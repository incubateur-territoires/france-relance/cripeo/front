import React, { Component } from "react";
import withStyles from '@mui/styles/withStyles';
import 'moment/locale/fr';
import { TableHead, TableRow, TableCell, TableSortLabel } from "@mui/material";

const styles = {
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    }
};

function EnhancedTableHead(props) {
    const { classes, order, orderBy, onRequestSort, celluleEntete } = props;
    const createSortHandler = (property) => (event) => {
        onRequestSort(event, property);
    };

    return (
        <TableHead>
            <TableRow>
                {celluleEntete.map(headCell =>
                    <TableCell
                        key={headCell.id}
                        align={headCell.disableAlign ? 'center' : 'left'}
                        padding={headCell.disablePadding ? 'normal' : 'none'}
                        sortDirection={orderBy === headCell.id ? order : false}
                        sx={{ maxWidth: headCell.disableSize ? 15 : 20, fontSize: 13 }}
                    >
                        <TableSortLabel
                            active={orderBy === headCell.id}
                            direction={orderBy === headCell.id ? order : 'asc'}
                            onClick={createSortHandler(headCell.id)}
                        >
                            {headCell.label}
                            {orderBy === headCell.id ?
                                <span className={classes.visuallyHidden}>
                                    {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                                </span>
                                :
                                null
                            }
                        </TableSortLabel>
                    </TableCell>
                )}
            </TableRow>
        </TableHead>
    );
}

class TableauEntete extends Component {
    state = {
        order: 'asc',
        orderBy: 'updated_at',
        selected: [],
        page: 0,
        dense: true,
        rowsPerPage: 5,
        dossiers: this.props.dossiers || [],
    };

    handleRequestSort = (event, property) => {
        this.props.onRequestSort(property);
        this.setState({ orderBy: property, order: this.state.order === 'asc' ? 'desc' : 'asc' });
    };

    handleSelectAllClick = (event) => {
        this.props.onSelectAllClick(event);
    };

    render() {
        const { classes, celluleEntete, dossiers } = this.props;
        const { order, orderBy, selected } = this.state;

        return (
            <EnhancedTableHead
                classes={classes}
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={dossiers.length}
                celluleEntete={celluleEntete}
            />
        )
    }
}

export default withStyles(styles)(TableauEntete);