import React, { useState, useCallback, useEffect } from "react";
import { changeFiltre } from "../../../actions/filtres";
import { useDispatch, useSelector } from "react-redux";
import { Grid, Tooltip, IconButton, Checkbox, FormGroup, FormControlLabel } from "@mui/material";
import { SelectDefault, SearchInput } from "../../composants_internes";
import { FilterAltOff, PersonOutline } from '@mui/icons-material';

export default function BlocFiltre({ loading, form, setDefauts, defauts }) {
    const dispatch = useDispatch();
    const [endLoad, setEndLoad] = useState(false);
    const acteur = useSelector(state => state.acteur);
    const categories = useSelector(state => state.categories);
    const filtres = useSelector(state => state.filtres);
    const [isFiltrePerso, setIsFIltrePerso] = useState(false)


    const cbChangeFilter = useCallback(
        (cle, valeur) => dispatch(changeFiltre(cle, valeur)),
        [dispatch]
    );

    useEffect(() => {
        cbChangeFilter('clean', 'profil');
        // eslint-disable-next-line
    }, [])

    useEffect(() => {
        setEndLoad((end) => !end);
    }, [defauts]);

    useEffect(() => {
        if (!loading && !endLoad) {
            cbChangeFilter("init", !form ? defauts : defauts);
            setEndLoad(true);
        }
        // eslint-disable-next-line
    }, [loading, endLoad, defauts]);

    const handleChange = (value) => {
        cbChangeFilter('recherche', value);
    }

    const handleClear = () => {
        cbChangeFilter('recherche', "");
    }

    const handleSecteurGeoSelectValue = (value) => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.secteurGeoSelectValue = value;
                return newState;
            })
        } else {
            cbChangeFilter('secteurGeoSelectValue', value);
        }
    };

    const handleSecteurAdmSelectValue = (value) => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.secteurAdmSelectValue = value;
                return newState;
            })
        } else {
            cbChangeFilter('secteurAdmSelectValue', value);
        }
    };

    const handleChangeReferenceExist = () => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.filterReferenceExist = !newState.filterReferenceExist;
                return newState;
            })
        } else {
            cbChangeFilter('filterReferenceExist', !filtres?.filterReferenceExist);
        }
    };
    const handleChangeUrgence = () => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.filterUrgence = !newState.filterUrgence;
                return newState;
            })
        } else {
            cbChangeFilter('filterUrgence', !filtres?.filterUrgence);
        }
    };
    const handleChangeUrgenceCreation = (value) => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.filterUrgenceRoot = value;
                return newState;
            })
        } else {
            cbChangeFilter('filterUrgenceRoot', value);
        }
    };

    const handleAutoCompleteCategory = (value) => {
        if (form) {
            setDefauts((state) => {
                const newState = { ...state };
                newState.categoryFilter = value;
                return newState;
            })
        } else {
            cbChangeFilter('categoryFilter', value);
        }
    };

    let optionsSecteursGeo = [];
    if (acteur && acteur.secteurGeo && acteur.secteurGeo.properties && acteur.secteurGeo.properties.length > 0) {
        optionsSecteursGeo.push({ value: 'all', label: 'Tous' });
    }
    optionsSecteursGeo = optionsSecteursGeo.concat(acteur?.secteurGeo?.properties?.map(sect => {
        return {
            label: sect.libelle,
            value: sect.code
        }
    }) || []);

    let sortedOptionsSecteursGeo = [optionsSecteursGeo[0], ...optionsSecteursGeo.slice(1).sort((a, b) => {
        const nameA = a.label.toUpperCase();
        const nameB = b.label.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    })];

    let optionsSecteursAdm = [];
    if (acteur && acteur.secteurs && acteur.secteurs.length > 0) {
        optionsSecteursAdm[0] = { value: 'all', label: 'Tous' };
        optionsSecteursAdm[1] = { value: 'aucun', label: 'Sans Secteur' };
    }
    optionsSecteursAdm = optionsSecteursAdm.concat(acteur?.secteurs?.map(sect => {
        return {
            label: sect.valeur,
            value: sect._id
        }
    }) || []);

    let autoCompleteCat = categories?.map(categ => {
        return {
            label: categ.libelle,
            value: categ.libelle
        }
    }) || [];


    return (
        <Grid item container xs={12} justifyContent="space-between">
            <Grid item container xs={!form ? 8 : 9} alignItems="center" justifyContent="flex-start" spacing={2}>
                {!form &&
                    <Grid item xs={4}>
                        <SearchInput
                            label="Recherche"
                            value={filtres?.recherche?.toUpperCase()}
                            setValue={handleChange}
                            placeholder="Nom du dossier"
                            handleClear={handleClear}
                            fullWidth
                        />
                    </Grid>
                }

                {optionsSecteursGeo && optionsSecteursGeo.length > 0 &&
                    <Grid item xs={3} sx={{ textAlign: "left" }}>
                        <SelectDefault
                            label="Secteur Géographique"
                            value={!form ? filtres?.secteurGeoSelectValue || "1" : defauts.secteurGeoSelectValue}
                            setValue={handleSecteurGeoSelectValue}
                            options={sortedOptionsSecteursGeo}
                        />
                    </Grid>
                }

                {optionsSecteursAdm && optionsSecteursAdm.length > 0 &&
                    <Grid item xs={3} sx={{ textAlign: "left" }}>
                        <SelectDefault
                            label="Secteur Administratif"
                            value={!form ? filtres?.secteurAdmSelectValue || "1" : defauts.secteurAdmSelectValue}
                            setValue={handleSecteurAdmSelectValue}
                            options={optionsSecteursAdm}
                        />
                    </Grid>
                }

                <Grid item xs={4}>
                    <SelectDefault
                        label="Catégories"
                        value={!form ? filtres?.categoryFilter : defauts.categoryFilter}
                        setValue={handleAutoCompleteCategory}
                        options={autoCompleteCat}
                        initialEmpty="Tous"
                    />
                </Grid>
                <Grid item xs={4}>
                    <SelectDefault
                        label="Dossier Prioritaire"
                        value={!form ? filtres?.filterUrgenceRoot : defauts.filterUrgenceRoot}
                        setValue={handleChangeUrgenceCreation}
                        options={[{ label: "Prioritaire", value: "urgent" }, { label: "Non Prioritaire", value: "non_urgent" }, { label: "Tous", value: "tous" }]}

                    />
                </Grid>
            </Grid>

            <Grid container item xs={3}>
                <Grid item marginLeft={2}>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox onChange={handleChangeReferenceExist} checked={!form ? filtres?.filterReferenceExist ? true : false : defauts.filterReferenceExist ? true : false} />} label="Dossier référence saisi" />
                    </FormGroup>
                    <FormGroup>
                        <FormControlLabel control={<Checkbox onChange={handleChangeUrgence} checked={!form ? filtres?.filterUrgence ? true : false : defauts.filterUrgence ? true : false} />} label="Dossier Urgent" />
                    </FormGroup>
                </Grid>
            </Grid>

            {!form &&
                <Grid item xs={1}>
                    {!isFiltrePerso ?
                        <Grid item>
                            <Tooltip title="Désactive les filtres">
                                <IconButton
                                    onClick={(event) => {
                                        event.stopPropagation();
                                        cbChangeFilter('clean');
                                        setIsFIltrePerso(true);
                                    }}
                                >
                                    <FilterAltOff />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                        :
                        <Grid item>
                            <Tooltip title="Utiliser les filtres du profil de l'utilisateur">
                                <IconButton
                                    onClick={(event) => {
                                        event.stopPropagation();
                                        cbChangeFilter('clean', 'profil');
                                        setIsFIltrePerso(false);
                                    }}
                                >
                                    <PersonOutline />
                                </IconButton>
                            </Tooltip>
                        </Grid>
                    }
                </Grid>
            }
        </Grid>
    )
}