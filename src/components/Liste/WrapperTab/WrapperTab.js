import React from 'react';
import { useTheme } from '@mui/material/styles';
import makeStyles from '@mui/styles/makeStyles';
import { Dialog, IconButton, DialogContent } from '@mui/material';
import BlocAll from "../BlocAll";
import { Close } from '@mui/icons-material';


const useStyles = makeStyles(theme => ({
    center: {
        textAlign: "center"
    },
    chiptitre: {
        verticalAlign: 'text-top',
    },
    closeButton: {
        position: "absolute",
        right: 0
    },
    roota: {
        backgroundColor: "rgba(0,0,0,0)",
        padding: 0,
    }
}));

export default function WrapperTab(props) {
    const classes = useStyles();
    const { open, handleClose, modal } = props;
    // eslint-disable-next-line no-unused-vars
    const theme = useTheme();

    if (!modal) return <div />;

    return <Dialog
        fullWidth
        maxWidth={"xl"}
        open={open}
        scroll="body"
        PaperProps={{
            style: {
                backgroundColor: "transparent",
                boxShadow: "none"
            },
        }}
        onClose={handleClose}
        aria-labelledby={`Tableau "${modal.libelle}"`}
        className={classes.roota}
    >
        <IconButton
            aria-label="close"
            onClick={handleClose}
            sx={{
                position: 'absolute',
                right: 34,
                top: 36,
            }}
        >
            <Close fontSize={'small'} />
        </IconButton>

        <DialogContent>
            <BlocAll
                dossiers={modal.dossiers}
                titre={modal.libelle}
                celluleEntete={modal.colonnes}
                couleurEntete={modal.couleur}
                urgentFilter={modal.urgentFilter}
            />
        </DialogContent>
    </Dialog>;
}
