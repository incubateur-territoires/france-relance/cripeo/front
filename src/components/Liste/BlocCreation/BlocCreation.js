import React, { Component } from "react";
import { Paper } from "@mui/material";
import withStyles from '@mui/styles/withStyles';
import 'moment/locale/fr';
import { Typography } from "@mui/material";
import Tableau from "../BlocAll/Tableau";

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    width: '80%',
    margin: 'auto',
  },
  paper: {
    width: '100%',
    marginBottom: theme.spacing(2),
    border: "solid",
  },
  title: {
    flex: '1 1 100%',
    textAlign: 'left',
    borderBottom: "solid",
  },
});

const EnhancedTableToolbar = (props) => {

  const { classes, titre, couleur, textBadge } = props;

  return (
    <Typography className={classes.title} variant="h6" id="tableTitle" component="div"
      style={{ backgroundColor: couleur, borderColor: couleur }}>
      {`${titre} (${textBadge})`}
    </Typography>
  );
};

class BlocCreation extends Component {
  state = {
    selectedItems: [],
  };

  handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelecteds = this.state.dossiers.map((n) => n._id);
      this.setState({
        selected: newSelecteds
      });
      return;
    }
    this.setState({ selected: [] });
  };


  render() {
    const { classes, titre, celluleEntete, couleurEntete, dossiers } = this.props;
    const { selectedItems, dense } = this.state;

    if (!dossiers) return <div></div>
    return (
      <div className={classes.root}>
        <Paper className={classes.paper} style={{ borderColor: couleurEntete }}>
          <EnhancedTableToolbar
            numSelected={selectedItems.length}
            classes={classes}
            titre={titre}
            couleur={couleurEntete}
            textBadge={dossiers.length}
          />
          <Tableau celluleEntete={celluleEntete} dossiers={dossiers} dense={dense} selectedItems={selectedItems.length} />
        </Paper>
      </div>
    )
  }
}

export default withStyles(styles)(BlocCreation);