import React, { useState, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { InputDefault, TH, ButtonText } from '../composants_internes';
import { FormGroup, FormControlLabel, Switch, Grid, Paper, Box, Table, TableBody, TableRow, TableCell, TableHead } from '@mui/material';
import { updateSecteurGeographique } from '../../actions/acteur';


export function ParametrageSecteurGeo({ acteur }) {
  const [url, setUrl] = useState(acteur?.secteurGeo?.url || '');
  const [actif, setActif] = useState(acteur?.secteurGeo?.actif || false);
  const [disableValider, setDisableValider] = useState(true);

  const dispatch = useDispatch();

  const handleSubmit = useCallback(() => {
    dispatch(updateSecteurGeographique(acteur._id, actif, url));
  }, [acteur, url, actif, dispatch]);

  useEffect(() => {
    if ((acteur?.secteurGeo?.url && url === acteur?.secteurGeo?.url) || !actif || url === "") {
      setDisableValider(true);
    } else {
      setDisableValider(false);
    }
    // eslint-disable-next-line
  }, [url, actif]);


  return (
    <Box sx={{ margin: 2, marginBottom: 5 }}>
      <TH>Paramétrage des secteurs Géographique</TH>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
        <Box sx={{ m: 2 }}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <FormGroup>
                <FormControlLabel control={<Switch checked={actif} onChange={() => setActif(!actif)} />} label="Secteur Géographique actif" />
              </FormGroup>
            </Grid>
            <Grid item xs={12}>
              <InputDefault
                label="Adresse URL du flux"
                value={url}
                setValue={setUrl}
                fullWidth
                disable={!actif}
              />
            </Grid>
          </Grid>
          {
            acteur && acteur.secteurGeo && acteur.secteurGeo.properties &&
            <Box sx={{ marginTop: 3 }}>
              <TH>Liste des secteurs Géographique</TH>
              <Grid item container xs={12}>
                <Grid item xs={6}>
                  <Table sx={{ minWidth: 200 }} aria-label="Liste des stecteurs géographique">
                    <TableHead>
                      <TableRow>
                        <TableCell>Nom du Secteur</TableCell>
                        <TableCell>Code du Secteur</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {acteur?.secteurGeo?.properties?.map(properties => (
                        <TableRow
                          key={properties.code}
                          sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                          <TableCell component="th" scope="row">
                            {properties.libelle}
                          </TableCell>
                          <TableCell align="right">{properties.code}</TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </Grid>
              </Grid>
            </Box>
          }
        </Box>
        <Grid container item xs={12} spacing={2} justifyContent="flex-end">
          <ButtonText
            color="primary"
            action={handleSubmit}
            texte="valider"
            disable={disableValider}
          />
          {/* <ButtonValidForm action={handleSubmit} /> */}
        </Grid>
      </Paper>
    </Box>
  );
}