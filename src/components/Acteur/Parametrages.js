import React from 'react';
import { ParametrageSecteurGeo } from "./ParametrageSecteurGeo"
import { ParametrageSecteursAdministratif } from "./ParametrageSecteursAdministratif"
import { Box } from '@mui/material';

export function Parametrages({ acteur }) {


  return (
    <Box sx={{ flex: 1, justifyContent: "flex-start" }}>
      <ParametrageSecteurGeo acteur={acteur} />
      <ParametrageSecteursAdministratif acteur={acteur} />
    </Box>
  )
}