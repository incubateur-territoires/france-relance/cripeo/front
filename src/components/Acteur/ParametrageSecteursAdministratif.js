import React, { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { InputDefault, TH, ButtonIcon, ButtonText, Prompt } from '../composants_internes';
import { Grid, Paper, Box, Typography, Alert, DialogContent, DialogActions, } from '@mui/material';
import { Delete, Add, Edit } from "@mui/icons-material";
import { addSecteurAdministratif, updateSecteurAdministratif, deleteSecteurAdministratif } from '../../actions/acteur';

export function ParametrageSecteursAdministratif({ acteur }) {
  const [secteur, setSecteur] = useState('');
  const [errerMessage, setErrerMessage] = useState(false);
  const [openAjouter, setOpenAjouter] = useState(false);

  const dispatch = useDispatch();
  const handleSubmit = useCallback(() => {
    if (secteur !== "") {
      dispatch(addSecteurAdministratif(acteur._id, secteur));
      setSecteur('');
      setOpenAjouter(false);
    } else {
      setErrerMessage(true);
    }
  }, [acteur, dispatch, secteur]);

  const secteursRender = acteur?.secteurs?.map((secteur) =>
    <LigneSecteur key={secteur._id} secteur={secteur} />
  );

  const handleSecteur = (value) => {
    if (value === "") {
      setErrerMessage(true);
    } else {
      setErrerMessage(false);
      setSecteur(value);
    }
  }

  const handleOpenAjouter = () => {
    setOpenAjouter(!openAjouter);
  }

  return (
    <Box sx={{ margin: 2 }}>
      <TH>Secteurs Administratifs</TH>
      <Paper elevation={1} sx={{ marginBottom: 2, padding: 1 }}>
        <Box sx={{ m: 2 }}>
          <Grid item container justifyContent='flex-end' sx={{ marginBottom: 3 }} >
            <ButtonText
              color="primary"
              action={handleOpenAjouter}
              texte="AJOUTER UN SECTEUR"
              icon={<Add />}
            />
          </Grid>
          <Prompt
            titre={"Ajouter un secteur"}
            onClose={handleOpenAjouter}
            fullWidth={true}
            maxWidth={"lg"}
            scroll={"body"}
            open={openAjouter}
            actions={
              <DialogActions sx={{ marginRight: 2 }}>
                <ButtonText
                  action={handleOpenAjouter}
                  color="info"
                  texte="Annuler"
                  variant="text"
                />
                <ButtonText
                  action={handleSubmit}
                  color="primary"
                  texte="Ajouter"
                  disable={errerMessage}
                />
              </DialogActions>
            }
          >
            <DialogContent>
              <InputDefault
                label="Nom du secteur"
                value={secteur}
                setValue={handleSecteur}
                fullWidth
              />
              {errerMessage &&
                <Alert severity="error">
                  Le secteur devrais pas être <strong>vide!</strong>
                </Alert>
              }
            </DialogContent>
          </Prompt>
          <Grid container sx={{ marginBottom: 2 }}>
            <Typography> Liste des secteurs </Typography>
          </Grid>
          <Grid container spacing={2} sx={{ marginBottom: 3 }} >
            {secteursRender}
          </Grid>
        </Box>
      </Paper>
    </Box>
  );
}

const LigneSecteur = ({ secteur }) => {
  const [valeur, setValeur] = useState(secteur.valeur);
  const [disableSecteur, setDisableSecteur] = useState(true);
  const [errerMessage, setErrerMessage] = useState(false);
  const dispatch = useDispatch();

  const handleSubmit = useCallback(() => {
    dispatch(updateSecteurAdministratif(secteur._id, valeur));
    setDisableSecteur(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secteur, valeur]);

  const handleDelete = useCallback(() => {
    dispatch(deleteSecteurAdministratif(secteur._id));
    setDisableSecteur(true);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [secteur]);

  const editerSecteur = () => {
    setDisableSecteur(!disableSecteur);
    setErrerMessage(true);
  }

  const handleSetValeur = (value) => {
    if (value === "" || value === secteur.valeur) {
      setErrerMessage(true);
    } else {
      setErrerMessage(false);
    }
    setValeur(value);
  }

  const handleAnnuler = () => {
    setValeur(secteur.valeur);
    setDisableSecteur(!disableSecteur);
  }

  return (
    <Grid container item xs={12} spacing={3} alignItems="center" marginBottom={2}>
      <Grid item xs={disableSecteur ? 11 : 9}>
        <InputDefault
          label="Nom du secteur"
          value={valeur}
          setValue={handleSetValeur}
          fullWidth
          disable={disableSecteur}
        />
      </Grid>
      <Grid item container xs={disableSecteur ? 1 : 3} justifyContent='flex-end'>
        {disableSecteur &&
          <ButtonIcon
            action={editerSecteur}
            color="primary"
            icon={<Edit />}
          />
        }
        {!disableSecteur &&
          <Grid item container>
            <Grid item container justifyContent='flex-end'>
              <ButtonIcon
                color="error"
                action={handleDelete}
                icon={<Delete />}
              />
            </Grid>
            <Grid item container justifyContent='flex-end'>

              <Grid item container xs={3} justifyContent='flex-end'>
                <ButtonText
                  color="primary"
                  action={handleAnnuler}
                  texte="Annuler"
                  variant="outlined"
                />
              </Grid>
              <Grid item container xs={4} justifyContent='flex-end' marginLeft={1}>
                <ButtonText
                  color="primary"
                  action={handleSubmit}
                  texte="Enregistrer"
                  variant="outlined"
                  disable={errerMessage}
                />
              </Grid>
            </Grid>
          </Grid>
        }
      </Grid>
    </Grid>
  )
}