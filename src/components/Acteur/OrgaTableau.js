import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getEquipes, setActeur, addEquipe } from "../../actions/acteur";
import { addNotification } from "../../actions/notifications";
import { setUtilisateur } from "../../actions/utilisateur";
import { getGroupes } from '../../actions/home'
import { Grid, Box } from "@mui/material";
import Utilisateur from "../Utilisateur/Utilisateur";
import SelectPersonne from "../Commun/Bouton/SelectPersonne";
import Personne from "../Commun/Bouton/Personne";
import GroupAdd from "../Commun/Bouton/GroupeAdd";
import Groupe from "../Commun/Bouton/Groupe";
import UtilisateursCardListe from "../Utilisateur/UtilisateursCardListe";
import EquipeCardListe from "./EquipeCardListe";
import WrapperActeur from './WrapperActeur';
import { TH } from "../composants_internes";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
    rootPaper: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2),
    },
    margin: {
        margin: theme.spacing(2),
    },
    padding: {
        padding: `0 ${theme.spacing(2)}`,
    },
    chip: {
        margin: theme.spacing(1),
    },
    card: {
        minHeight: 75,
        marginTop: theme.spacing(20),
    },
    title: {
        fontWeight: 'bold',
    },
    paper: {
        padding: theme.spacing(3, 2),
        margin: theme.spacing(1),
        width: '100%',
        textAlign: 'left'
    }
});


class OrgaTableau extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addUtilisateur: false,
            addEquipe: false,
            showEquipe: false,
        };
    }

    componentDidMount = async () => {
        await this.props.getGroupes();
        if (this.props.acteurId) {
            await this.props.getEquipes(this.props.acteurId);
        }
    };

    handleAddUtilisateur = (_event) => {
        this.setState({ addUtilisateur: !this.state.addUtilisateur });
    }

    handleAddEquipe = (_event) => {
        this.setState({ addEquipe: !this.state.addEquipe });
    }

    handleChangeView = async () => {
        this.setState({ showEquipe: !this.state.showEquipe });
    }

    handleClick = async (data) => {
        const res = await this.props.setUtilisateur(data);
        if (res.success) {
            this.setState({ utilisateur: false, addUtilisateur: false });
        }
    }

    handleClickUtilisateur = (utilisateur) => {
        this.setState({ utilisateur });
    }

    handleCancelEditUtilisateur = () => {
        this.setState({ utilisateur: false, addUtilisateur: false });
    }

    handleCancelEditEquipe = () => {
        this.setState({ addEquipe: false });
    }

    handleSaveEquipe = async (equipe) => {
        const res = await this.props.addEquipe(equipe);
        if (res && res.success) {
            this.setState({ acteurs: [] });
            await this.props.getEquipes(equipe.acteur_id);
        }
        this.setState({ addEquipe: false });
    }

    render() {
        const { groupes, acteur_id, acteurs } = this.props;
        const { utilisateur, addUtilisateur, showEquipe } = this.state;

        return (
            <Grid container key={"UtilisateursTableau"}>
                <Grid container item xs={12} justifyContent="space-between" alignItems="center">
                    <Grid container item xs={2} alignItems="flex-start">
                        <TH variant="h6">
                            {!showEquipe ? 'Utilisateurs' : 'Equipes'}
                        </TH>
                    </Grid>
                    <Grid container item xs={10} alignItems="flex-start" spacing={1}>
                        <Grid item>
                            <Personne
                                action={this.handleChangeView}
                                texte={"Liste des utilisateurs"}
                                disable={!showEquipe}
                                actif={showEquipe}
                            />
                        </Grid>
                        <Grid item>
                            <Groupe
                                action={this.handleChangeView}
                                texte={"Liste des équipes"}
                                disable={showEquipe}
                                actif={!showEquipe}
                            />
                        </Grid>
                        <Grid item>
                            <SelectPersonne
                                action={this.handleAddUtilisateur}
                                texte={"Ajouter un utilisateur"}
                                disable={false}
                            />
                        </Grid>
                        <Grid item>
                            <GroupAdd
                                action={this.handleAddEquipe}
                                texte={"Ajouter une équipe"}
                                disable={false}
                            />
                        </Grid>

                    </Grid>
                </Grid>
                <Box py={1} flexGrow={1}>
                    {utilisateur &&
                        <Utilisateur utilisateur={utilisateur} groupes={groupes} handleClick={this.handleClick}
                            equipes={acteurs}
                            handleCancel={this.handleCancelEdit} open={!!utilisateur} />
                    }
                    {addUtilisateur &&
                        <Utilisateur organisation={acteur_id} groupes={groupes} handleClick={this.handleClick}
                            equipes={acteurs}
                            handleCancel={this.handleCancelEditUtilisateur} open={addUtilisateur} />
                    }
                    {this.state.addEquipe &&
                        <WrapperActeur
                            addEquipe={this.state.addEquipe}
                            title={"Ajout d'une équipe"}
                            acteur_id={acteur_id}
                            handleClose={this.handleCancelEditEquipe}
                            handleSave={this.handleSaveEquipe}
                            open={this.state.addEquipe}
                        />
                    }
                    {!showEquipe &&
                        <UtilisateursCardListe acteur_id={acteur_id} />
                    }
                    {showEquipe &&
                        <EquipeCardListe acteur_id={acteur_id} />
                    }
                </Box>
            </Grid>
        )
    }

}

function mapStateToProps(state) {
    return {
        groupes: state.groupes,
        acteurs: state.acteurs,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        setUtilisateur: (data) => dispatch(setUtilisateur(data)),
        setActeur: (data) => dispatch(setActeur(data)),
        addEquipe: (data) => dispatch(addEquipe(data)),
        getGroupes: () => dispatch(getGroupes()),
        addNotification: (obj) => dispatch(addNotification((obj))),
        getEquipes: (id) => dispatch(getEquipes(id)),
    }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(OrgaTableau));