import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { Grid, CircularProgress } from "@mui/material";
import { getActeurNoDispatch } from "../../actions/acteur";
import { green } from '@mui/material/colors';

const styles = theme => ({
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
  ligneUser: {
    paddingLeft: theme.spacing(2),
  }
});

class EquipeRow extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showActeur: false
    };
  }

  componentDidMount = async () => {
    if (this.props.equipe) {
      const reponse = await this.props.getActeurNoDispatch(this.props.equipe.acteur_id);
      this.setState({
        acteur: reponse
      })
    }
  }

  render() {
    const { classes, equipe, key } = this.props;
    const { acteur } = this.state;

    if (!equipe) return <CircularProgress size={34} className={classes.fabProgress} />;

    return (
      <Grid container item xs={12} key={"equipe-" + key} alignContent={"flex-start"} className={classes.ligneUser}>
        {acteur && '- ' + acteur.entite}
      </Grid>
    )
  }

}

const mapDispatchToProps = (dispatch) => {
  return {
    getActeurNoDispatch: (id) => dispatch(getActeurNoDispatch(id)),
  }
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(EquipeRow));