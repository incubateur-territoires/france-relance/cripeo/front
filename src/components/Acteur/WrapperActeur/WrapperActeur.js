import React, { Component } from 'react';
import connect from "react-redux/es/connect/connect";
import withStyles from '@mui/styles/withStyles';
import { DialogActions, DialogContent } from '@mui/material';
import Acteur from '../Acteur';
import { getActeurNoDispatch } from '../../../actions/acteur';
import { ButtonText, Prompt } from '../../composants_internes';

const styles = () => ({
  root: {
    padding: 50
  }
});

class WrapperActeur extends Component {

  constructor(props) {
    super(props);
    this.state = {
      newActeurClicked: false
    };
  }

  componentDidMount = async () => {
    if (this.props.acteur && this.props.acteur._id) this.props.getActeur(this.props.acteur._id);
  }

  handleClose = () => {
    this.props.handleClose();
  }

  setNewActeurClicked = () => {
    this.setState({ newActeurClicked: false });
  }

  handleSave = () => {
    this.setState({ newActeurClicked: true });
  }

  render() {
    const { classes, open, acteur, acteur_id, addEquipe, handleSave, editeEquipe, title } = this.props;

    return (
      <Prompt
        titre={(acteur && acteur.entite) ? acteur.entite : title ? title : "Nouvelle organisation"}
        onClose={this.handleClose}
        fullWidth={true}
        maxWidth={"lg"}
        scroll={"body"}
        open={open}
        actions={
          <DialogActions sx={{ marginRight: 2 }}>
            <ButtonText
              action={this.handleClose}
              color="info"
              texte="Annuler"
              variant="text"
            />
            <ButtonText
              action={this.handleSave}
              color="primary"
              texte="Enregistrer"
            />
          </DialogActions>
        }
      >
        <DialogContent className={classes.root}>
          <Acteur
            acteur={acteur}
            acteur_id={acteur_id}
            enabled={true}
            isOrga={false}
            creation={true}
            newActeurClicked={this.state.newActeurClicked}
            setNewActeurClicked={this.setNewActeurClicked.bind(this)}
            addEquipe={addEquipe}
            handleSaveEquipe={handleSave}
            editeEquipe={editeEquipe}
          />
        </DialogContent>
      </Prompt>
    )
  }
}


const mapDispatchToProps = (dispatch) => {
  return {
    getActeur: (id) => dispatch(getActeurNoDispatch(id)),
  }
}

export default withStyles(styles)(connect(null, mapDispatchToProps)(WrapperActeur));