import React, { Component } from "react";
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import Input from "../../Commun/Input/Input";
import { Checkbox, FormControlLabel, Grid, Paper, Box, Typography, Alert } from "@mui/material";
import { deleteEmail, updateEmail } from "../../../actions/acteur";
import { addNotification } from "../../../actions/notifications";
import { ButtonIcon, ButtonText } from "../../composants_internes";
import { Edit, Delete } from '@mui/icons-material';
import { grey } from '@mui/material/colors';

const styles = () => ({});

class LigneEmail extends Component {

    state = {
        modeEdition: this.props.modeEdition || false,
        adresse: this.props.email ? this.props.email.adresse : "",
        urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'urgente') !== -1 : true,
        classique: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'classique') !== -1 : true,
        creation_urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_urgente') !== -1 : true,
        creation_non_urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_non_urgente') !== -1 : true,
        errerMessageColone1: false,
        errerMessageColone2: false,
        errerMessageInput: false,
    };

    componentDidUpdate(prevProps) {

        if (prevProps.email !== this.props.email && this.props.email) {
            this.setState({
                adresse: this.props.email.adresse || ""
            });
        }
        if (this.props.cliquer) {
            this.handleClick();
        }
    }

    activeEdit = async (event) => {
        event.stopPropagation();
        this.setState({ modeEdition: true });
    }

    cancelEdit = () => {
        this.setState({
            errerMessageColone1: false,
            errerMessageColone2: false,
            errerMessageInput: false,
            modeEdition: false,
            adresse: this.props.email ? this.props.email.adresse : "",
            urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'urgente') !== -1 : true,
            classique: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'classique') !== -1 : true,
            creation_urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_urgente') !== -1 : true,
            creation_non_urgente: this.props.email ? this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_non_urgente') !== -1 : true,
        })
        if (this.props.lineCancel) this.props.lineCancel();
    }

    handleChange = (event) => {

        // verification pour mettre le message d'errer si email est vide
        if (event.target.value === "") {
            this.setState({ errerMessageInput: true });

            // verification pour deactiver le button valider dans la ajouter email
            this.props.handleOptionErrer(true);
        } else {
            this.setState({ errerMessageInput: false });

            // verification pour deactiver le button valider dans la ajouter email
            this.props.handleOptionErrer(this.state.errerMessageColone2 || this.state.errerMessageColone1 ? true : false);
        }
        this.setState({ adresse: event.target.value });
    }

    handleClick = (event) => {
        this.setState({ errerMessageColone1: false, errerMessageColone2: false });
        let boitesEnvoi = [];
        if (this.state.urgente) boitesEnvoi.push('urgente');
        if (this.state.classique) boitesEnvoi.push('classique');
        if (this.state.creation_urgente) boitesEnvoi.push('creation_urgente');
        if (this.state.creation_non_urgente) boitesEnvoi.push('creation_non_urgente');

        if (this.state.adresse === "") {
            this.setState({ errerMessageInput: true });
            this.props.addNotification({ variant: "error", message: "Erreur lors de l'ajout de l'email, l'adresse est vide" });
        } else {
            this.setState({ errerMessageInput: false });
        }

        if (!this.props.addLine) {
            event.stopPropagation();
            this.props.editLine(this.props.indice, {
                adresse: this.state.adresse,
                typeBoiteEnvoi: boitesEnvoi
            });
        } else {
            this.props.addLine({
                adresse: this.state.adresse,
                typeBoiteEnvoi: boitesEnvoi
            });
        }
        this.cancelEdit();
    }

    handleDelete = async (event) => {
        event.stopPropagation();
        this.setState({ errerMessageColone1: false, errerMessageColone2: false, errerMessageInput: false });
        this.props.handleDelete(this.props.indice);
        this.cancelEdit();
    }

    handleSwitch = (event) => {
        event.stopPropagation();

        // verification sur "Vous devez avoir au moins une option cochée par colonne"
        if (event.target.name === "creation_urgente" || event.target.name === "creation_non_urgente") {
            if (this.state.creation_urgente === false || this.state.creation_non_urgente === false) {
                this.setState({ errerMessageColone1: event.target.checked ? false : true });

                // verification pour deactiver le button valider dans la ajouter email
                this.props.handleOptionErrer(!event.target.checked || this.state.errerMessageColone2 || this.state.errerMessageInput || this.state.adresse === "" ? true : false);
            } else {
                this.setState({ errerMessageColone1: false });

                // verification pour deactiver le button valider dans la ajouter email
                this.props.handleOptionErrer(this.state.errerMessageColone2 || this.state.errerMessageInput || this.state.adresse === "" ? true : false);
            }
        } else if (event.target.name === "urgente" || event.target.name === "classique") {
            if (this.state.urgente === false || this.state.classique === false) {
                this.setState({ errerMessageColone2: event.target.checked ? false : true });

                // verification pour deactiver le button valider dans la ajouter email
                this.props.handleOptionErrer(!event.target.checked || this.state.errerMessageColone1 || this.state.errerMessageInput || this.state.adresse === "" ? true : false);
            } else {
                this.setState({ errerMessageColone2: false });

                // verification pour deactiver le button valider dans la ajouter email
                this.props.handleOptionErrer(this.state.errerMessageColone1 || this.state.errerMessageInput || this.state.adresse === "" ? true : false);
            }
        }

        this.setState({ [event.target.name]: event.target.checked });

    }


    render() {
        const { addLine } = this.props;
        const { adresse, name, urgente, classique, creation_urgente, creation_non_urgente, errerMessageColone1, errerMessageColone2, errerMessageInput } = this.state;

        // désactiver la button de valider, s'il n'y a pas des changement
        let disableButtonValider = true;
        if (this.props.email) {
            if (this.state.adresse !== this.props.email.adresse ||
                this.state.creation_urgente !== (this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'urgente') !== -1) ||
                this.state.classique !== (this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'classique') !== -1) ||
                this.state.creation_urgente !== (this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_urgente') !== -1) ||
                this.state.creation_non_urgente !== (this.props.email.typeBoiteEnvoi.findIndex(boite => boite === 'creation_non_urgente') !== -1)
            ) {
                disableButtonValider = false;
            }
        }

        const disableDelete = errerMessageColone1 || errerMessageColone2 || errerMessageInput;
        const disableValider = errerMessageColone1 || errerMessageColone2 || errerMessageInput || disableButtonValider;

        return (
            <Box sx={{ m: 2, textAlign: "left" }}>
                <Paper elevation={1} sx={{ marginBottom: 2, padding: 2 }}>
                    <Grid container item xs={12}>
                        <Grid container item marginBottom={1} xs={addLine ? 12 : 9}>
                            <Grid container item marginBottom={1} xs={12}>
                                <Input
                                    name={name}
                                    value={adresse || ""}
                                    handleChange={this.handleChange}
                                    disabled={!this.state.modeEdition}
                                />
                                {errerMessageInput &&
                                    <Alert severity="error">
                                        L'adresse devrais pas être <strong>vide!</strong>
                                    </Alert>
                                }
                            </Grid>
                            <Grid item marginTop={1} marginRight={10}>
                                <Typography sx={{ color: !this.state.modeEdition ? grey[500] : "black" }}> Urgence création échange: </Typography>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={creation_urgente}
                                            name="creation_urgente"
                                            onClick={this.handleSwitch}
                                            disabled={!this.state.modeEdition}
                                        />
                                    }
                                    label="Oui"
                                />
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={creation_non_urgente}
                                            name="creation_non_urgente"
                                            onClick={this.handleSwitch}
                                            disabled={!this.state.modeEdition}
                                        />
                                    }
                                    label="Non"
                                />
                            </Grid>
                            <Grid item marginTop={1} marginBottom={1} marginRight={10}>
                                <Typography sx={{ color: !this.state.modeEdition ? grey[500] : "black" }}> Urgence dernière action: </Typography>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={urgente}
                                            name="urgente"
                                            onClick={this.handleSwitch}
                                            disabled={!this.state.modeEdition}
                                        />
                                    }
                                    label="Oui"
                                />
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={classique}
                                            name="classique"
                                            onClick={this.handleSwitch}
                                            disabled={!this.state.modeEdition}
                                        />
                                    }
                                    label="Non"
                                />
                            </Grid>
                        </Grid>
                        {!this.state.modeEdition ?
                            <Grid container item alignContent='flex-start' justifyContent="flex-end" xs={3}>
                                <ButtonIcon
                                    texte={"Editer l'email"}
                                    disable={this.state.modeEdition}
                                    action={this.activeEdit}
                                    icon={<Edit />}
                                />
                            </Grid>
                            :
                            <Grid container item justifyContent="flex-end" xs={addLine ? 12 : 3}>
                                {!addLine &&
                                    <Grid>
                                        <Grid container item marginBottom={10} justifyContent="flex-end">
                                            <ButtonText
                                                action={this.handleDelete}
                                                color="error"
                                                icon={<Delete />}
                                                texte="Supprimer"
                                                variant="contained"
                                                disable={disableDelete}
                                            />
                                        </Grid>
                                        <Grid item container justifyContent="flex-end">
                                            <ButtonText
                                                action={this.cancelEdit}
                                                color="primary"
                                                texte="Annuler"
                                                variant="text"
                                            />
                                            <ButtonText
                                                action={this.handleClick}
                                                color="primary"
                                                texte="Sauvegarder"
                                                variant="outlined"
                                                disable={disableValider}
                                            />
                                        </Grid>
                                    </Grid>
                                }
                            </Grid>
                        }

                        {(errerMessageColone1 || errerMessageColone2) &&
                            <Alert severity="error">
                                Vous devez avoir au moins <strong>une option cochée</strong> par colonne
                            </Alert>
                        }
                        {this.state.modeEdition && !(errerMessageColone1 || errerMessageColone2) &&
                            <Typography variant="caption" sx={{ color: grey[500] }}> Vous devez avoir au moins une option cochée par colonne </Typography>
                        }

                    </Grid>
                </Paper>
            </Box>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteEmail: (data) => dispatch(deleteEmail(data)),
        updateEmail: (data) => dispatch(updateEmail(data)),
        addNotification: (obj) => dispatch(addNotification(obj)),
    }
};



export default withStyles(styles)(connect(null, mapDispatchToProps)(LigneEmail));