import React, { Component } from 'react';
import { connect } from "react-redux";
import { Grid } from '@mui/material';
import withStyles from '@mui/styles/withStyles';
import { getEquipes, setActeur } from "../../actions/acteur";
import EquipeCard from "./EquipeCard";

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

class EquipeCardListe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      acteur: false,
      acteurForm: false,
      refreshData: true
    };
  }

  componentDidMount = async () => {
    await this.props.getEquipes(this.props.acteur_id);
  };

  handleClick = (acteur) => {
    this.setState({ acteur, acteurForm: false, openActeur: true });
  }

  render() {
    const { acteurs, acteur_id } = this.props;

    const equipesRender = acteurs ? Object.keys(acteurs).map(key => {
      return <Grid item xs={6} key={"acteur-" + key}><EquipeCard acteur={acteurs[key]}
        acteur_id={acteur_id}
        key={"equipe-" + key}
        handleClick={this.handleClick}
        handleSave={this.handleSaveActeur}
      /></Grid>
    }) : [];

    return (
      <Grid container key={"ActeursListe"} item xs={12} justifyContent="space-between" spacing={2}>
        {equipesRender}
      </Grid>
    )
  }

}

function mapStateToProps(state) {
  return {
    acteurs: state.acteurs,
    auth: state.auth,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getEquipes: (id) => dispatch(getEquipes(id)),
    setActeur: (data) => dispatch(setActeur(data)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(EquipeCardListe));