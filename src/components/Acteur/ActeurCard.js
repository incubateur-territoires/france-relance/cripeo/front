import { Typography, CircularProgress } from "@mui/material";
import CardSimple from "../Commun/Card/CardSimple";
import React, { Component, Fragment } from "react";
import withStyles from '@mui/styles/withStyles';
import { withRouter } from 'react-router-dom';
import WrapperActeur from "./WrapperActeur";
import { green, grey } from '@mui/material/colors';

const fontColor = grey[700];

const styles = () => ({
  title: {
    color: fontColor,
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
});

class ActeurCard extends Component {

  state = {
    open: false
  }

  handleClick = (event) => {
    event.stopPropagation();
    this.props.handleClick(this.props.acteur);
  }

  handleSave = (data) => {
    this.props.handleSave(data);
  }

  handleOpen = (acteur) => event => {
    event.stopPropagation();
    this.props.history.push("/organisation/" + acteur);
  }

  handleClose = () => {
    this.setState({ open: false });
  }

  render() {
    const { classes, acteur, selected } = this.props;
    const { open } = this.state;

    if (!acteur) return <CircularProgress size={34} className={classes.fabProgress} />;

    return (
      <Fragment>
        <CardSimple
          center
          couleur={"teal"}
          titre={acteur.entite}
          actif={acteur.actif}
          sousTitre={acteur.service}
          selected={selected}
          handleClic={this.handleOpen(acteur._id)}
          handleSave={this.handleSave}
        >
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            Lecture des commentaires publics : {acteur.commentairePublic ? "Oui" : "Non"}
          </Typography>
        </CardSimple>
        <WrapperActeur open={open} handleClose={this.handleClose} acteur={acteur} handleSave={this.handleSave} />
      </Fragment>
    )
  }
}

export default withStyles(styles)(withRouter(ActeurCard));