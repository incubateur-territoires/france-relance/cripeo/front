import { Typography, CircularProgress, Grid } from "@mui/material";
import React, { Component } from "react";
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { withRouter } from 'react-router-dom';
import { getUtilisateursByEquipeNoDispatch } from "../../actions/utilisateur";
import { getActeur, getEquipes, setActeur, addEquipe } from "../../actions/acteur";
import UtilisateurEquipeRow from "../Utilisateur/UtilisateurEquipeRow";
import Editer from "../Commun/Bouton/Editer";
import WrapperActeur from "./WrapperActeur";
import { green, blue } from '@mui/material/colors';


const styles = theme => ({
  title: {
    textAlign: 'left',
    fontWeight: 'bold',
    fontSize: 'small',
    paddingLeft: theme.spacing(2),
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  fabProgress: {
    color: green[500],
    position: 'absolute',
    zIndex: 99999,
  },
  carte: {
    border: "solid",
    borderColor: blue[500],
    borderWidth: 'thin',
  }
});

class EquipeCard extends Component {

  state = {
    modeEdition: false,
    utilisateursequipe: null,
    refreshData: true
  }

  componentDidMount = async () => {
    if (this.props.acteur) {
      let utilisateurEquipe = await this.props.getUtilisateursByEquipeNoDispatch(this.props.acteur._id);
      this.setState({ utilisateursequipe: utilisateurEquipe });
    }
  };

  async componentDidUpdate(prevProps, prevState) {
    if (this.props.acteur._id !== prevProps.acteur._id || prevState.refreshData !== this.state.refreshData) {
      let utilisateurEquipe = await this.props.getUtilisateursByEquipeNoDispatch(this.props.acteur._id);
      this.setState({ utilisateursequipe: utilisateurEquipe });
    }
  }

  handleClick = (event) => {
    event.stopPropagation();
    this.props.handleClick(this.props.acteur);
  }

  changeEdition = (_event) => {
    this.setState({ modeEdition: !this.state.modeEdition });
  }

  handleCancelEdit = (_event) => {
    this.setState({ modeEdition: false });
  }

  handleSave = async (acteur) => {
    const { refreshData } = this.state;
    const res = await this.props.addEquipe(acteur);
    if (res && res.success) {
      this.setState({ acteurForm: false, acteur: false, refreshData: !refreshData });
      this.props.getEquipes(acteur.acteur_id);
      this.props.getActeur(acteur.acteur_id);
    }
    this.setState({ modeEdition: false });
  }

  render() {
    const { classes, acteur, acteur_id } = this.props;
    const { modeEdition, utilisateursequipe } = this.state;

    if (!acteur) return <CircularProgress size={34} className={classes.fabProgress} />;

    const utilisateursRender = utilisateursequipe ? Object.keys(utilisateursequipe).map(key => {
      return <UtilisateurEquipeRow utilisateur={utilisateursequipe[key]} key={"ligne-" + utilisateursequipe[key]._id} />
    }) : [];

    return (
      <Grid container item xs={12} className={classes.carte}>
        <Grid item xs={12} container className={classes.title}>
          <Grid container item xs={11} alignContent={"flex-start"}>
            <Typography>
              {acteur.entite}
            </Typography>
          </Grid>
          <Grid container item xs={1} alignContent={"flex-end"} >
            <Editer
              action={this.changeEdition} texte={"Editer l'equipe"} disable={modeEdition} size={15} />
          </Grid>
        </Grid>
        <Grid container item xs={12} justifyContent="space-between" >
          {utilisateursRender}
        </Grid>
        {modeEdition &&
          <WrapperActeur
            acteur_id={acteur_id}
            handleClick={this.handleClick}
            handleClose={this.handleCancelEdit}
            handleSave={this.handleSave}
            open={modeEdition}
            enabled={true}
            acteur={acteur}
            editeEquipe={modeEdition}
          />
        }
      </Grid>
    )
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getUtilisateursByEquipeNoDispatch: (id) => dispatch(getUtilisateursByEquipeNoDispatch(id)),
    setActeur: (data) => dispatch(setActeur(data)),
    getActeur: (id) => dispatch(getActeur(id)),
    getEquipes: (id) => dispatch(getEquipes(id)),
    addEquipe: (data) => dispatch(addEquipe(data))
  }
};

export default withStyles(styles)(connect(null, mapDispatchToProps)(withRouter(EquipeCard)));