import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import Input from "../Commun/Input/Input";
import {
  Typography, FormControl, TextField, AppBar, Tabs, Tab, Box, Grid, FormControlLabel, Switch,
  DialogContent, DialogActions, Autocomplete
} from "@mui/material";
import LigneEmail from "./LigneEmail";
import { deleteEmail, updateEmail, addEmail, changeFrequenceMail, setActeur } from "../../actions/acteur";
import { addNotification } from "../../actions/notifications";
import { getUtilisateursByActeur } from "../../actions/utilisateur";
import { searchByKeycloakId } from "../../actions/keycloak";
import UtilisateursSansEquipe from "../Utilisateur/UtilisateursSansEquipe";
import OrgaTableau from "./OrgaTableau";
import CategorieListe from "../Categorie/CategorieListe";
import { Parametrages } from "./Parametrages";
import { ButtonText, ButtonIcon, Prompt, TH } from "../composants_internes";
import { Edit, Add } from '@mui/icons-material';
import { grey } from '@mui/material/colors';


const styles = theme => ({
  title: {
    color: '#2F80ED',
    fontSize: 'xx-large',
    textAlign: "left"
  },
  paper: {
    padding: 10
  },
  barTabs: {
    backgroundColor: theme.palette.background.paper,
    color: "#090909"
  }
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <Grid
      container item xs={12}
      role="tabpanel"
      hidden={value !== index}
      id={`orga-tabpanel-${index}`}
      aria-labelledby={`orga-tab-${index}`}
      spacing={2}
      {...other}
    >
      {value === index && (
        <Box p={3} flexGrow={1}>
          {children}
        </Box>
      )}
    </Grid>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `orga-tab-${index}`,
    'aria-controls': `orga-tabpanel-${index}`,
  };
}

class Acteur extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: props.acteur && props.acteur._id ? props.acteur._id : "",
      acteur_id: this.props.acteur_id ? this.props.acteur_id : null,
      emails: props.acteur && props.acteur.emails ? props.acteur.emails : [],
      entite: props.acteur && props.acteur.entite ? props.acteur.entite : "",
      service: props.acteur && props.acteur.service ? props.acteur.service : "",
      commentairePublic: props.acteur && props.acteur._id ? props.acteur.commentairePublic : true,
      modeEdition: this.props.enabled || false,
      actif: props.acteur && props.acteur._id ? props.acteur.actif : true,
      openAdd: false,
      frequence: props.acteur && props.acteur.frequenceRappel ? (props.acteur.frequenceRappel === 0 ? "" : props.acteur.frequenceRappel) : "",
      envoiMail: !!(props.acteur && props.acteur.frequenceRappel && props.acteur.frequenceRappel > 0),
      utilisateurs: [],
      selectedUsers: [],
      defaultUsers: [],
      tabsValue: 0,
      cliquer: false,
      optionErrer: true,
    };
  }

  async componentDidMount() {
    if (this.props.acteur_id) {
      await this.props.getUtilisateursByActeur(this.props.acteur_id);
      const utilisateurs = await this.updatedUser(this.props.utilisateurs);
      this.setState({ utilisateurs });
    }
  }

  async componentDidUpdate(prevProps) {
    if (prevProps.acteur !== this.props.acteur) {
      await this.props.getUtilisateursByActeur(this.props.acteur._id);
      this.setState({
        id: this.props.acteur && this.props.acteur._id ? this.props.acteur._id : "",
        acteur_id: this.props.acteur_id ? this.props.acteur_id : null,
        emails: this.props.acteur && this.props.acteur.emails ? this.props.acteur.emails : [],
        entite: this.props.acteur && this.props.acteur.entite ? this.props.acteur.entite : "",
        service: this.props.acteur && this.props.acteur.service ? this.props.acteur.service : "",
        commentairePublic: this.props.acteur && this.props.acteur._id ? this.props.acteur.commentairePublic : true,
        actif: this.props.acteur && this.props.acteur._id ? this.props.acteur.actif : true,
        frequence: this.props.acteur && this.props.acteur.frequenceRappel ? (this.props.acteur.frequenceRappel === 0 ? "" : this.props.acteur.frequenceRappel) : "",
        envoiMail: !!(this.props.acteur && this.props.acteur.frequenceRappel && this.props.acteur.frequenceRappel > 0),
        selectFrequence: (this.props.acteur && this.props.acteur.frequenceMail ? this.props.acteur.frequenceMail.frequence : "0") !== 0,
        openAdd: false,
      });
    }
    if (this.props.newActeurClicked) {
      if ((!this.props.addEquipe && !this.props.editeEquipe) || (this.props.addEquipe === undefined && this.props.editeEquipe === undefined)) {
        this.handleClick();
      } else if (this.props.addEquipe || this.props.editeEquipe) {
        this.handleSaveEquipe();
      }
    }
  }

  updatedUser = async (utilisateurs) => {
    if (utilisateurs) {
      for (const element of utilisateurs) {
        if (element.k_userId) {
          const resp = await this.props.searchByKeycloakId(element.k_userId);
          element.name = resp.data.lastName + ' ' + resp.data.firstName;
        }
      }
      return utilisateurs;
    }
    return [];
  }

  handleSaveEquipe = () => {
    const { entite, service, commentairePublic, actif, frequence, acteur_id, selectedUsers } = this.state;
    let data = {
      entite,
      service,
      commentairePublic,
      actif,
      acteur_id: acteur_id ? acteur_id : "",
      utilisateurs: selectedUsers
    };
    data.frequenceRappel = !data.frequenceRappel ? {} : data.frequenceRappel;
    data.frequenceRappel = frequence;
    if (this.state.id && this.state.id.length > 0) data._id = this.state.id;

    this.props.handleSaveEquipe(data);
  }

  handleClick = async (event) => {
    if (event) event.stopPropagation();
    const { entite, service, commentairePublic, actif, frequence, acteur_id, selectedUsers } = this.state;
    let data = {
      entite,
      service,
      commentairePublic,
      actif,
      acteur_id: acteur_id ? acteur_id : "",
      utilisateurs: selectedUsers
    };
    data.frequenceRappel = !data.frequenceRappel ? {} : data.frequenceRappel;
    data.frequenceRappel = frequence;
    if (this.state.id && this.state.id.length > 0) data._id = this.state.id;

    const retour = await this.props.setActeur(data);
    await this.props.setNewActeurClicked();
    this.cancelEdit();
    if (retour.acteur && retour.acteur._id) await this.props.getUtilisateursByActeur(retour.acteur._id);
    window.location.reload();
  }

  changeEdition = async (event) => {
    event.stopPropagation();
    this.setState({
      modeEdition: !this.state.modeEdition
    });
  }

  handleChangeTabs = (_event, newValue) => {
    this.setState({ tabsValue: newValue });
  };

  cancelEdit = () => {
    this.setState({ modeEdition: false });
  }

  handleChange = (event) => {
    this.setState({ [event.target.name]: event.target.value.toUpperCase() });
  }

  handleSwitch = (event) => {
    event.stopPropagation();
    this.setState({ [event.target.name]: event.target.checked });
    if (event.target.name === 'envoiMail' && event.target.checked === false) {
      this.props.changeFrequenceMail(this.props.acteur._id, parseInt(0));
    }
  }

  handleChangeRadio = (event) => {
    event.stopPropagation();
    this.setState({ frequence: event.target.value });
  }

  handleDisabled = (event) => {
    event.stopPropagation();
    this.cancelEdit();
  }

  handleDelete = async (indice) => {
    let emails = this.state.emails;
    let obj = emails[indice]._id;
    try {
      const res = await this.props.deleteEmail(obj);
      if (res.success) {
        this.props.addNotification({ variant: "success", message: "L'adresse e-mail a bien été supprimée" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la suppression de l'adresse e-mail",
          details: res.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la suppression du mail" });
    }
    emails.splice(indice, 1);
    this.setState({ emails: emails });
  }

  handleNumberChange = (event) => {
    const { value, min, max } = event.target;
    const lowerValue = parseInt(value) < parseInt(min) ? parseInt(min) : parseInt(value);
    const protectValue = parseInt(value) > parseInt(max) ? parseInt(max) : lowerValue;
    this.setState({ [event.target.name]: protectValue });
  }

  handleValidNumberChange = async () => {
    await this.props.changeFrequenceMail(this.props.acteur._id, parseInt(this.state.frequence));
    await this.props.getUtilisateursByActeur(this.props.acteur._id);
  }

  handleChangeUtilisateurs = (event, value) => {
    event.stopPropagation();
    let selectedUsers = value.map(utilisateur => {
      return utilisateur;
    })
    this.setState({ selectedUsers });
  }

  editEmail = async (indice, data) => {
    let emails = this.state.emails;
    emails[indice].adresse = data.adresse;
    emails[indice].typeBoiteEnvoi = data.typeBoiteEnvoi;
    await this.setState({ emails: emails });
    try {
      const res = await this.props.updateEmail(emails[indice]);
      if (res.success) {
        this.props.addNotification({ variant: "success", message: "L'email a bien été modifié" });
      } else {
        this.props.addNotification({
          variant: "error",
          message: "Erreur lors de la modification de l'email",
          details: res.error[0].msg
        });
      }
    } catch (err) {
      this.props.addNotification({ variant: "error", message: "Erreur lors de la modification de l'email" });
    }
  }

  handleEmail = async () => {
    this.setState({ openAdd: !this.state.openAdd });
    this.setState({ optionErrer: true });
  }

  handleCreate = () => {
    this.setState({ cliquer: true });
  }

  addEmail = async (data) => {
    if (data.adresse !== null && data.adresse !== "" && data.adresse !== undefined) {
      try {
        const res = await this.props.addEmail(this.props.acteur._id, { adresse: data.adresse, typeBoiteEnvoi: data.typeBoiteEnvoi });
        if (res.success) {
          this.props.addNotification({ variant: "success", message: "L'email a bien été ajouté" });
        } else {
          this.props.addNotification({
            variant: "error",
            message: "Erreur lors de l'ajout de l'email",
            details: res.error[0].msg
          });
        }
        this.setState({ openAdd: !this.state.openAdd });
        this.setState({ cliquer: false });
      } catch (err) {
        this.props.addNotification({ variant: "error", message: "Erreur lors de l'ajout de l'email" });
      }
    } else {
      this.setState({ cliquer: false });
    }
  }

  handleOptionErrer = (correct) => {
    this.setState({ optionErrer: correct });
  }

  render() {
    const { classes, acteur, isOrga } = this.props;
    const { entite, commentairePublic, actif, emails, openAdd, modeEdition, utilisateurs, defaultUsers, tabsValue, optionErrer } = this.state;

    const emailsList = emails.map((email, index) => {
      return (
        <LigneEmail
          key={`email-${index}`}
          email={email}
          indice={index}
          name={`email-${index}`}
          handleDelete={this.handleDelete.bind(this)}
          editLine={this.editEmail.bind(this)}
          handleOptionErrer={this.handleOptionErrer.bind(this)}
        />
      )
    });

    if (defaultUsers.length === 0 && acteur && utilisateurs && utilisateurs.length > 0) {
      for (const element of utilisateurs) {
        for (let j = 0; j < element?.equipes?.length; j++) {
          if (acteur._id === element.equipes[j].acteur_id && !defaultUsers.includes(element)) {
            defaultUsers.push(element);
          }
        }
      }
    }

    return (
      <Grid container item className={classes.paper}>
        {openAdd &&
          <Prompt
            titre={"Ajouter une adresse e-mail"}
            onClose={this.handleEmail}
            open={openAdd}
            actions={
              <DialogActions>
                <ButtonText
                  action={this.handleEmail}
                  color="info"
                  texte="Annuler"
                  variant="text"
                />
                <ButtonText
                  action={this.handleCreate}
                  color="primary"
                  texte="Valider"
                  disable={optionErrer}
                />
              </DialogActions>
            }
          >
            <DialogContent>
              <LigneEmail
                key={"ajoutligne"}
                addLine={this.addEmail.bind(this)}
                modeEdition={true}
                lineCancel={this.handleEmail.bind(this)}
                cliquer={this.state.cliquer}
                handleOptionErrer={this.handleOptionErrer.bind(this)}
              />
            </DialogContent>
          </Prompt>}
        <Grid container spacing={2}>
          <Grid container item xs={12} justifyContent="space-between" alignItems="flex-start">
            <Typography className={classes.title}>
              {(acteur && acteur.entite) ? acteur.entite : ""}
            </Typography>
            {acteur && acteur._id && acteur._id.length > 0 && !this.props.editeEquipe &&
              <ButtonIcon
                texte={"Editer l'acteur"}
                disable={modeEdition}
                action={this.changeEdition}
                icon={<Edit />}
              />
            }
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          <Grid container item xs={12} justifyContent="space-between" alignItems="center" spacing={1}>
            <Grid item xs={6}>
              <Input
                label="Nom"
                name="entite"
                value={entite}
                handleChange={this.handleChange}
                disabled={!modeEdition}
              />
            </Grid>
            <Grid item xs={3}>
              <FormControlLabel
                control={<Switch
                  checked={commentairePublic || false}
                  onChange={this.handleSwitch}
                  color="primary"
                  name="commentairePublic"
                  inputProps={{ 'aria-label': 'Lecture des commentaires public checkbox' }}
                  disabled={!modeEdition}
                />}
                label="Lecture des commentaires public"
                labelPlacement="start"
              />
            </Grid>
            <Grid item xs={3}>
              <FormControlLabel
                control={<Switch
                  checked={actif}
                  onChange={this.handleSwitch}
                  color="primary"
                  name="actif"
                  inputProps={{ 'aria-label': 'Acteur actif checkbox' }}
                  disabled={!modeEdition}
                />}
                label="Entité active"
                labelPlacement="start"
              />
            </Grid>
            {acteur && acteur._id && acteur._id.length > 0 && modeEdition && !this.props.editeEquipe &&
              <Grid container item xs={12} sx={{ justifyContent: "flex-end", marginBottom: 1 }} >
                <ButtonText
                  action={this.cancelEdit}
                  color="info"
                  texte="Annuler"
                  variant="text"
                  disable={!modeEdition}
                />
                <ButtonText
                  action={this.handleClick}
                  color="primary"
                  texte="Enregistrer"
                  disable={!modeEdition}
                />
              </Grid>
            }
          </Grid>
        </Grid>

        {!this.props.creation && <><AppBar position="static" className={classes.barTabs}>
          <Tabs value={tabsValue} onChange={this.handleChangeTabs} aria-label="Organisation tabs">
            <Tab label="Equipes et Utilisateurs" {...a11yProps(0)} />
            {acteur && acteur._id && isOrga &&
              <Tab label="Catégories" {...a11yProps(1)} />
            }
            <Tab label="Gestion Emails" {...a11yProps(2)} />
            <Tab label="Paramétrage" {...a11yProps(3)} />
          </Tabs>
        </AppBar>
          <TabPanel value={tabsValue} index={0}>
            {acteur && acteur._id &&
              <Grid item xs={12}>
                <UtilisateursSansEquipe acteurId={acteur._id} />
              </Grid>
            }
            {acteur && acteur._id && isOrga &&
              <Grid item xs={12}>
                <OrgaTableau acteur_id={acteur.acteur_id ? acteur.acteur_id : acteur._id} />
              </Grid>
            }
            {!isOrga &&
              <Grid item xs={12}>
                <Autocomplete
                  multiple
                  id="tags-standard"
                  options={utilisateurs}
                  getOptionLabel={(option) =>
                    option.name
                  }
                  defaultValue={defaultUsers}
                  fullWidth
                  filterSelectedOptions
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="standard"
                      label="Sélection des utilisateurs"
                      placeholder="Utilisateurs"
                    />
                  )}
                  onChange={this.handleChangeUtilisateurs}
                />
              </Grid>
            }
          </TabPanel>

          {acteur && acteur._id && isOrga &&
            <TabPanel value={tabsValue} index={1}>
              <CategorieListe organisation={acteur} />
            </TabPanel>}

          <TabPanel value={tabsValue} index={2}>
            <Grid container item xs={12} alignItems="center" margin={2}>
              <Grid item container xs={2} justifyContent="flex-start" >
                <TH variant="h6">Liste des emails </TH>
              </Grid>
              <Grid item container xs={7} alignItems="center" >
                <Grid item xs={4}>
                  <FormControl component="fieldset">
                    <FormControlLabel
                      control={<Switch
                        checked={this.state.envoiMail}
                        onChange={this.handleSwitch}
                        color="primary"
                        name="envoiMail"
                        inputProps={{ 'aria-label': 'Envoi de mails actif' }}
                      />}
                      label="Envoi de mails actif"
                      labelPlacement="start"
                    />
                  </FormControl>
                </Grid>
                <Grid item container alignItems="center" spacing={1} xs={8}>
                  {this.state.envoiMail &&
                    <Grid marginRight={1}>
                      <Input
                        type="number"
                        label={`Jour(s)`}
                        value={this.state.frequence || ""}
                        handleChange={this.handleNumberChange}
                        name="frequence"
                        min={1}
                        max={14}
                      />
                    </Grid>
                  }
                  {this.state.envoiMail && this.props.acteur.frequenceRappel !== "" && this.state.frequence &&
                    <Grid marginRight={1}>
                      <ButtonText
                        action={this.handleValidNumberChange}
                        color="primary"
                        texte="Valider"
                        disable={!(this.state.frequence !== this.props.acteur.frequenceRappel) && this.props.acteur.frequenceRappel !== null}
                      />
                    </Grid>
                  }
                  {this.state.envoiMail && this.props.acteur.frequenceRappel !== 0 && this.props.acteur.frequenceRappel !== "" && this.props.acteur.frequenceRappel !== null &&
                    <Typography variant="caption" sx={{ color: grey[500] }}> les emails de rappels vont être envoyés tous les {this.props.acteur.frequenceRappel !== 1 ? this.props.acteur.frequenceRappel : ""} jour{this.props.acteur.frequenceRappel === 1 ? "" : "s"} </Typography>
                  }
                </Grid>
              </Grid>
              <Grid item container xs={3} justifyContent='flex-end'>
                <ButtonText
                  action={this.handleEmail}
                  color="primary"
                  icon={<Add />}
                  texte="Ajouter une adresse"
                />
              </Grid>
            </Grid>
            {emailsList}
          </TabPanel>
          <TabPanel value={tabsValue} index={3}>
            <Parametrages
              acteur={acteur}
            />
          </TabPanel>
        </>}
      </Grid>
    )
  }
}

function mapStateToProps(state) {
  return {
    frequencesMail: state.frequencesMail,
    utilisateurs: state.utilisateurs
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteEmail: (data) => dispatch(deleteEmail(data)),
    updateEmail: (data) => dispatch(updateEmail(data)),
    addEmail: (idActeur, data) => dispatch(addEmail(idActeur, data)),
    addNotification: (obj) => dispatch(addNotification(obj)),
    changeFrequenceMail: (id, frequence) => dispatch(changeFrequenceMail(id, frequence)),
    getUtilisateursByActeur: (id) => dispatch(getUtilisateursByActeur(id)),
    searchByKeycloakId: (id) => dispatch(searchByKeycloakId(id)),
    setActeur: (data) => dispatch(setActeur(data)),
  }
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Acteur));