import React, { Component } from 'react';
import { connect } from "react-redux";
import withStyles from '@mui/styles/withStyles';
import { getActeurs, setActeur } from "../../actions/acteur";
import { getFrequencesMail } from "../../actions/frequencesMail";
import ActeurCard from "../Acteur/ActeurCard";
import { Button, Grid } from "@mui/material";
import WrapperActeur from "./WrapperActeur";
import { SearchInput } from "../composants_internes";


const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  rootPaper: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
  },
  margin: {
    margin: theme.spacing(2),
  },
  padding: {
    padding: `0 ${theme.spacing(2)}`,
  },
  chip: {
    margin: theme.spacing(1),
  },
  card: {
    minHeight: 75,
    marginTop: theme.spacing(20),
  },
  title: {
  },
  button: {
    backgroundColor: '#3867cf',
    color: '#ffffff',
    '&:hover': {
      backgroundColor: '#033878'
    }
  },
});


class ActeursListe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      acteur: false,
      acteurForm: false,
      filterOrganisation: ""
    };
  }

  componentDidMount = async () => {
    await this.props.getActeurs();
    this.props.getFrequencesMail();
  };

  handleClick = (acteur) => {
    this.setState({ acteur, acteurForm: false, openActeur: true });
  }

  handleSaveActeur = async (acteur) => {
    const res = await this.props.setActeur(acteur);
    if (res && res.success) {
      this.setState({ acteurForm: false, acteur: false });
      this.props.getActeurs();
    }
  }

  handleShowForm = () => {
    this.setState({ acteurForm: !this.state.acteurForm, acteur: false });
  }

  handleChangeFilterOrganisation = (value) => {
    this.setState({ filterOrganisation: value });
  }

  handleClear = () => {
    this.setState({ filterOrganisation: "" })
  }

  render() {
    const { classes, acteurs } = this.props;
    const { acteurForm, filterOrganisation } = this.state;

    const organisationsFiltered = filterOrganisation.length > 0 && acteurs ? acteurs.filter(acteur => acteur.entite.toUpperCase().indexOf(filterOrganisation.toUpperCase()) !== -1) : acteurs;

    const acteursRender = organisationsFiltered ? Object.keys(organisationsFiltered).map(key => {
      return <Grid item xs={3} key={"acteur-" + key}><ActeurCard acteur={organisationsFiltered[key]}
        handleClick={this.handleClick}
        handleSave={this.handleSaveActeur}
      /></Grid>
    }) : [];

    return (
      <Grid container key={"ActeursListe"}>
        <Grid container item xs={12} justifyContent="center" alignItems="center" spacing={2}>
          <Grid item xs={4}>
            <SearchInput
              label="Recherche une organisation"
              value={filterOrganisation?.toUpperCase()}
              setValue={this.handleChangeFilterOrganisation}
              handleClear={this.handleClear}
              fullWidth
            />
          </Grid>
          <Grid item>
            <Button onClick={this.handleShowForm} className={classes.button}>Ajouter une organisation</Button>
          </Grid>
        </Grid>
        {acteursRender}
        <Grid item xs={12}>
          {acteurForm &&
            <WrapperActeur
              handleSave={this.handleSaveActeur}
              handleClose={this.handleShowForm}
              enabled={true}
              open={acteurForm}
            />
          }
        </Grid>
      </Grid>
    )
  }

}

function mapStateToProps(state) {
  return {
    acteurs: state.acteurs,
    frequencesMail: state.frequencesMail,
    auth: state.auth,
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    getActeurs: () => dispatch(getActeurs()),
    setActeur: (data) => dispatch(setActeur(data)),
    getFrequencesMail: () => dispatch(getFrequencesMail()),
  }
};


export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(ActeursListe));