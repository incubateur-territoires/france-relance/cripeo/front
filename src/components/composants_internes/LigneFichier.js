import * as React from 'react';
import { useState } from 'react';
import { ListItem, ListItemAvatar, Avatar, ListItemText, IconButton, Grid } from '@mui/material';
import { Article, Delete, Image, TextFields, PictureAsPdf, Download } from '@mui/icons-material';
import CircularProgress from '@mui/material/CircularProgress';
import { tabCorresExt, enumTypeFile } from './actions/fichiers';
import { SuppressionPrompt } from '../composants_internes';


export function LigneFichier({ fichier, downloadFunction, deleteFunction }) {
    const [loadingFile, setLoadingFile] = useState(false);
    const [loadingDeleteFile, setLoadingDeleteFile] = useState(false);
    const [statusOpen, setStatusOpen] = useState(false);
    const typeFile = tabCorresExt.get(fichier.extension)?.type;

    const downloader = async () => {
        setLoadingFile(true);
        if (downloadFunction) await downloadFunction(fichier);
        setLoadingFile(false);
    }

    const handleDelete = () => {
        setStatusOpen(true);
    }

    const handleClose = () => {
        setStatusOpen(false);
        setLoadingDeleteFile(false);
    }

    const fileToDelete = async () => {
        setStatusOpen(false);
        setLoadingDeleteFile(true);
        if (deleteFunction) await deleteFunction(fichier);
        setLoadingDeleteFile(false);
    }

    return (
        <ListItem
            secondaryAction={
                <Grid container spacing={1}>
                    {downloadFunction &&
                        <Grid item>
                            {loadingFile &&
                                <CircularProgress
                                    size={36}
                                    sx={{
                                        position: 'relative',
                                        top: 0,
                                        left: 9,
                                        zIndex: 1,
                                    }}
                                />
                            }
                            {!loadingFile &&
                                <IconButton
                                    edge="end"
                                    aria-label="download"
                                    color="primary"
                                    onClick={downloader}
                                    disabled={loadingDeleteFile}
                                >
                                    <Download />
                                </IconButton>
                            }
                        </Grid>

                    }
                    {fichier.canDelete && deleteFunction &&
                        <Grid item>
                            {loadingDeleteFile &&
                                <CircularProgress
                                    size={36}
                                    sx={{
                                        position: 'relative',
                                        top: 3,
                                        left: 9,
                                        zIndex: 1,
                                    }}
                                    color="error"
                                />
                            }
                            {!loadingDeleteFile &&
                                <IconButton
                                    edge="end"
                                    aria-label={`Delete fichier ${fichier.titre}`}
                                    color="error"
                                    onClick={handleDelete}
                                    disabled={loadingFile}
                                >
                                    <Delete />
                                </IconButton>
                            }
                        </Grid>
                    }
                </Grid>
            }
        >
            <ListItemAvatar>
                <Avatar color="secondary">
                    {typeFile === enumTypeFile.IMAGE && <Image />}
                    {typeFile === enumTypeFile.DOC && <Article />}
                    {typeFile === enumTypeFile.PDF && <PictureAsPdf />}
                    {typeFile === enumTypeFile.TEXTE && <TextFields />}
                </Avatar>
            </ListItemAvatar>
            <ListItemText primary={fichier.titre} secondary={typeFile} />
            {fileToDelete && fichier.canDelete && deleteFunction &&
                <SuppressionPrompt
                    open={statusOpen}
                    deleteFunction={fileToDelete}
                    cancelFunction={handleClose}
                    ariaLabel={fichier.titre}
                />
            }
        </ListItem>
    )
}