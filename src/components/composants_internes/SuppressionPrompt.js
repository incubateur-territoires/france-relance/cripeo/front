import * as React from 'react';
import { DialogActions, DialogContentText } from '@mui/material';
import { ButtonText, Prompt } from '../composants_internes';


export function SuppressionPrompt({ titre, texte, open, cancelFunction, deleteFunction, ariaLabel, noEscape }) {


    return (
        <Prompt
            open={open}
            onClose={cancelFunction}
            titre={titre || "Suppression de document"}
            noEscape={noEscape}
            actions={(
                <DialogActions>
                    <ButtonText
                        texte="Annuler"
                        action={cancelFunction}
                        variant='text'
                        ariaLabel={`Fermer la modal de suppression ${ariaLabel}`}
                    />
                    <ButtonText
                        texte="Annuler"
                        action={() => {
                            if (deleteFunction) deleteFunction();
                        }}
                        variant='text'
                        ariaLabel={`Supprimer ${ariaLabel}`}
                    />
                </DialogActions>
            )}
            maxWidth={"xs"}
        >
            <DialogContentText id={`alert-dialog-suppression-document-${titre?.replace(/\s/g, '')}`}>
                {texte || "Voulez-vous supprimer ce document ?"}
            </DialogContentText>
        </Prompt>
    )
}