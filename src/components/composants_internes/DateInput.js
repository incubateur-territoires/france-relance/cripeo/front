import * as React from 'react';
import { useState } from 'react';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import {LocalizationProvider} from '@mui/x-date-pickers';
import { DesktopDatePicker } from '@mui/x-date-pickers/DesktopDatePicker';
import frLocale from 'date-fns/locale/fr';
import { TextField } from '@mui/material';
import { isValid } from 'date-fns';


export function DateInput({ label, value, setValue, disable }) {
    // const dateRegEx = /(([12][0-9]{3})-(0[1-9]|1[012])-(3[01]|[012][0-9]))|((3[01]|[012][0-9])\/(0[1-9]|1[012])\/([12][0-9]{3}))/i;
    const charRegEx = /[a-zA-Z]+/i;

    const [error, setError] = useState(isValid(new Date(value)) || value === "" ? false : true)


    const minDate = new Date("1800-01-01").getTime()
    const maxDate = new Date("3000-01-01").getTime()

    const handleChange = newValue => {

        if (isValid(newValue) && minDate <= new Date(newValue).getTime() && new Date(newValue).getTime() <= maxDate) {
            setError(false)
            setValue(newValue);
        }
        else if (!newValue) {
            setError(false)
            setValue("")
        }
        else {
            setError(true)
        }
    };
    const handleError = (reason, value) => {
        console.log(reason, value);
    }


    return (
        <LocalizationProvider dateAdapter={AdapterDateFns} adapterLocale={frLocale}>
            <DesktopDatePicker
                label={label}
                value={isValid(new Date(value)) ? value : null}
                inputFormat="dd/MM/yyyy"
                onChange={handleChange}
                // acceptRegex={dateRegEx}
                onError={handleError}
                minDate={new Date("1800-01-01").getTime()}
                maxDate={new Date("3000-01-01").getTime()}
                renderInput={(params) => {
                    if (typeof value === "string" && charRegEx.test(value.toString())) {
                        setError(true)
                    } else if (typeof value === "string" && !charRegEx.test(value) && isValid(new Date(value))) {
                        setError(false)
                    }

                    return (
                        <TextField
                            aria-label={`Champs date ${label}`}
                            {...params}
                            error={params?.error || error}
                            helperText={params?.error || error ? 'Format de date invalide' : null}
                            inputProps={{ ...params.inputProps, placeholder: "jj/mm/aaaa" }}
                        />
                    );
                }}
                disabled={disable}
            />
        </LocalizationProvider>
    );
}
