import * as React from 'react';
import { useState } from 'react';
import { Box, Step, Stepper, StepLabel, Button, Typography } from '@mui/material';
import { ButtonText } from './ButtonText';
import NextIcon from "@mui/icons-material/NavigateNext";
import BeforeIcon from '@mui/icons-material/NavigateBefore';



export function StepperDefault({ steps, handleClose, labelEnd }) {

    const [stepIndex, setStepIndex] = useState(0);
    const [stepCompleted, setStepCompleted] = useState(new Array(steps.length).fill(false))

    const handleNext = async () => {
        let continueSubmit = true;
        if (steps[stepIndex].controlSubmit !== undefined) {
            continueSubmit = !!(steps[stepIndex]?.controlSubmit?.());
        }

        if (continueSubmit && steps[stepIndex].submitFunction !== undefined) {
            await steps[stepIndex]?.submitFunction();
        }

        if (continueSubmit && stepIndex < steps.length - 1) {
            setStepCompleted(prevArr => prevArr.map((comp, index) => {
                if (index !== stepIndex) { return comp }
                else return !comp

            }))
            setStepIndex((prevStep) => prevStep + 1);
        }
    }


    const handleSkip = () => {
        if (stepIndex < steps.length - 1)
            setStepIndex((prevStep) => prevStep + 1);
    }

    const handleBack = () => {
        if (steps[stepIndex].precFunction !== undefined) {
            steps[stepIndex]?.precFunction?.();
        }
        setStepIndex((prevStep) => prevStep > 0 ? prevStep - 1 : 0);
    }

    return (
        <Box sx={{ width: '100%' }}>
            <Stepper nonLinear activeStep={stepIndex}>
                {steps.map((step, index) =>
                    <Step key={step.label} completed={stepCompleted[index]}>
                        <StepLabel
                        >
                            {stepCompleted[index] &&
                                <Typography color="primary">
                                    {step.label}
                                </Typography>
                            }
                            {!stepCompleted[index] &&
                                <Typography>
                                    {step.label}
                                </Typography>
                            }
                        </StepLabel>
                    </Step>
                )}
            </Stepper>
            <Box sx={{ padding: 2 }}>
                {steps[stepIndex]?.component}
            </Box>
            <React.Fragment>
                <Box sx={{ display: 'flex', flexDirection: 'row', pt: 2 }}>
                    {steps[stepIndex].prec &&
                        <ButtonText
                            texte="Retour"
                            action={handleBack}
                            icon={<BeforeIcon />}
                            color='info'
                            iconPosition='left'
                        />
                    }
                    <Box sx={{ flex: '1 1 auto' }} />
                    {steps[stepIndex].skip &&
                        <Button color="inherit" onClick={handleSkip} sx={{ mr: 1 }}>
                            Passer
                        </Button>
                    }
                    <ButtonText
                        action={handleClose}
                        color='primary'
                        variant="text"
                        texte='Annuler'
                    />
                    <ButtonText
                        action={handleNext}
                        icon={(stepIndex !== steps.length - 1) ? <NextIcon /> : undefined}
                        color='primary'
                        texte={labelEnd && stepIndex === steps.length - 1 ? labelEnd : stepIndex === steps.length - 1 ? 'Terminer' : 'Suivant'}
                    />
                </Box>
            </React.Fragment>
        </Box>
    )
}