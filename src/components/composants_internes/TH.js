import React from 'react';
import { Typography } from '@mui/material';


export function TH({ variant, children, color, align, margin }) {
    return (
        <Typography variant={variant || "h6"} color={color || "primary"} sx={{
            textAlign: align || 'left',
            margin: margin ? margin : "",
            fontWeight: "bold",
        }}>
            {children}
        </Typography>
    );
}