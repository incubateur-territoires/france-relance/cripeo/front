import * as React from 'react';
import { ButtonText } from '../composants_internes';
import { AddCircle } from '@mui/icons-material';

export function ButtonAdd({ action, disable }) {
    return (
        <ButtonText
            texte="Ajouter"
            action={action}
            color='primary'
            disable={disable}
            icon={<AddCircle />}
        />
    );
}