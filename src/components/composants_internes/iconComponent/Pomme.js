import React from "react";


function Pomme({fill = "#fefefe"}){

  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="-19.5 332.163 55.268 66.337" width="200" height="230">
      <path d=" M 11.268 393.897 C 9.896 393.042 7.948 393.155 6.268 393.897 C -4.394 401.268 -11.362 395.485 -16.71 376.77 C -17.186 376.545 -22.909 351.047 -5.71 348.77 C -4.831 348.654 -4.137 348.436 -3.627 348.115 Q -10.46 343.27 -11.71 333.77 C 0.254 332.82 7.264 338.164 9.29 349.77 C 11.597 346.146 12.132 340.245 13.268 338.77 C 15.9 338.935 17.276 339.955 17.29 341.77 C 14.707 344.437 13.05 347.447 12.29 350.77 C 26.947 342.583 37.634 354.77 33.29 374.77 C 28.549 394.492 21.513 399.771 11.268 393.897 Z " fill={fill} vectorEffect="non-scaling-stroke" strokeWidth="1" stroke={fill} strokeOpacity="0.99" strokeLinejoin="miter" strokeLinecap="square" strokeMiterlimit="3"/>
    </svg>
  );
}



export default Pomme;