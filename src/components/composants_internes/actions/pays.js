import {pays} from '../sources/pays';


export const getPays = (filter) => {
  let result= [];
  try {
    const paysArray = Object.values(pays());
    if(paysArray.length > 0) {
      result = paysArray.filter((item) => {
        return item.toUpperCase().includes(filter.toUpperCase())
      }).map(com => {
        return {
          label: com,
          value: com
        }
      })
    }
    return result.splice(0, 10);
  } catch(error) {
    console.error("Error: ", error);
    return result;
  }
}