import axios from 'axios';

const API_URL = "https://gateway-api.calvados.fr/adresses";


export const cpBano = async (cp)  => {
  if(cp.length >= 3) {
    const url = API_URL + '/?q=' + encodeURIComponent(cp.trim()) + '&limit=10';
    return callBano(url, "municipality");
  }
  return [];
};

export const communeBano = async (commune)=> {
  if(commune.length >= 3) {
    const url = API_URL + '/?q=' + encodeURIComponent(commune.trim()) + '&limit=5';
    return callBano(url, "municipality");
  }
  
  return [];
};

export const searchFullAdresse = async (adresse) => {
  if(adresse.length >= 3) {
    const url = API_URL + '/?q=' + encodeURIComponent(adresse.trim()) + '&limit=5';
    return fullBano(url);
  }
  
  return [];
}

export const adresseBano = async (voie, cp) => {
  if(voie.length >= 3) {
    const url = API_URL + '/?q=' + encodeURIComponent(voie.trim()) + '&postcode=' + encodeURIComponent(cp.trim()) + '&limit=5';
    return callBano(url, "all");
  }
  return [];
};


const callBano = async (url, filter) => {
  const instance = axios.create();
  let result = [];
  try {
    const res = await instance.get(url);
    const data= res.data;
    if(data.features.length > 0) {
      result = data.features.filter((item) => {
        return filter === 'all' || item.properties.type === filter;
      });
    }
    return result;
  } catch(error) {
    console.error("Error: ", error);
    return result;
  }
};

const fullBano = async(url) => {
  const instance = axios.create();
  let result= [];
  try {
    const res = await instance.get(url);
    const data = res.data;
    if(data.features.length > 0) {
      result = data.features;
    }
    return result;
  } catch(error) {
    console.error("Error: ", error);
    return result;
  }
}; 