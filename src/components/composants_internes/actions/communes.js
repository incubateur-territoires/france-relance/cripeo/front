import {communes} from '../sources/fr';


export const getCommunes = (filter) => {  
  let result= [];
  const communesArray = communes();
  try {
    if(communesArray.length > 0) {
      result = communesArray.filter((item) => {
        return item.city.toUpperCase().includes(filter.toUpperCase());
      })
    }
    return result.splice(0, 10);
  } catch(error) {
    console.error("Error: ", error);
    return result;
  }
};

export const getOptionsCommunes = (filter) => {  
  let result = [];
  const communesArray = communes();
  try {
    if(communesArray.length > 0) {
      result = communesArray.filter((item) => {
        return item.city.toUpperCase().includes(filter.toUpperCase());
      }).map(com => {
        return {
          label: com.city,
          value: com.city
        }
      })
    }
    return result.splice(0, 10);
  } catch(error) {
    console.error("Error: ", error);
    return result;
  }
};