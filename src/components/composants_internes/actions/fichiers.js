const enumTypeFile = {
    IMAGE: "Image",
    DOC: "Document Word",
    PDF: "Document PDF",
    TEXTE: "Document Texte",
  }
  
  const tabCorresMIME = new Map();
  tabCorresMIME.set("image/bmp", {ext: ".bmp", type: enumTypeFile.IMAGE});
  tabCorresMIME.set("image/gif", {ext: ".gif", type: enumTypeFile.IMAGE});
  tabCorresMIME.set("image/jpeg", {ext: ".jpg/.jpeg", type: enumTypeFile.IMAGE});
  tabCorresMIME.set("image/png", {ext: ".png", type: enumTypeFile.IMAGE});
  tabCorresMIME.set("application/pdf", {ext: ".pdf", type: enumTypeFile.PDF});
  tabCorresMIME.set("application/msword", {ext: ".doc", type: enumTypeFile.DOC});
  tabCorresMIME.set("application/vnd.openxmlformats-officedocument.wordprocessingml.document", {ext: ".docx", type: enumTypeFile.DOC});
  tabCorresMIME.set("application/vnd.oasis.opendocument.text", {ext: ".odt", type: enumTypeFile.DOC});
  tabCorresMIME.set("text/plain", {ext: ".txt", type: enumTypeFile.TEXTE});
  
  const tabCorresExt = new Map();
  tabCorresExt.set('.jpg', {mime: 'image/jpeg', type: enumTypeFile.IMAGE});
  tabCorresExt.set('.jpeg', {mime: 'image/jpeg', type: enumTypeFile.IMAGE});
  tabCorresExt.set('.gif', {mime: 'image/gif', type: enumTypeFile.IMAGE});
  tabCorresExt.set('.png', {mime: 'image/png', type: enumTypeFile.IMAGE});
  tabCorresExt.set('.bmp', {mime: 'image/bmp', type: enumTypeFile.IMAGE});
  tabCorresExt.set('.pdf', {mime: 'application/pdf', type: enumTypeFile.PDF});
  tabCorresExt.set('.doc', {mime: 'application/msword', type: enumTypeFile.DOC});
  tabCorresExt.set('.docx', {mime: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', type: enumTypeFile.DOC});
  tabCorresExt.set('.odt', {mime: 'application/vnd.oasis.opendocument.text', type: enumTypeFile.DOC});
  tabCorresExt.set('.txt', {mime: 'text/plain', type: enumTypeFile.TEXTE});
  
  export {tabCorresMIME, tabCorresExt, enumTypeFile};