import * as React from 'react';
import { useState, useEffect, useCallback } from 'react';
import { FormControl, MenuItem, TextField, Box, Grid } from '@mui/material';
import { CheckCircle } from '@mui/icons-material';
import Autocomplete, { createFilterOptions } from '@mui/material/Autocomplete';


export function AutoComplete({ label, value, setValue, addValue, options, disable, defaultValue, initialEmpty, allowAddValue, refreshOptions }) {
    const [isDefaultValueEnabled, setIsDefaultValueEnabled] = useState(true);
    const [optionsFinal, setOptionsFinal] = useState([])
    const [inputVal, setInputVal] = React.useState(value ? value : defaultValue ? defaultValue : "");
    const [selectValue, setSelectValue] = React.useState(null);

    useEffect(() => {
        setSelectValue(options?.find(option => option?.value?.toString() === value?.toString()) || value);
        // eslint-disable-next-line
    }, [value, options])

    useEffect(() => {
        if (defaultValue && !value && isDefaultValueEnabled) {
            setIsDefaultValueEnabled(false)
            const checkOptionValid = options.some(option => option.value === defaultValue);
            if (checkOptionValid) {
                setValue(defaultValue);
                setInputVal(defaultValue);
                setSelectValue({ label: defaultValue, value: defaultValue })
            }
        }
        // eslint-disable-next-line
    }, [options, defaultValue, value, isDefaultValueEnabled]);

    useEffect(() => {
        if (initialEmpty) {
            const opts = [{ value: "", label: initialEmpty }, ...options]
            setOptionsFinal(opts);
        } else {
            setOptionsFinal(options);
        }
        // eslint-disable-next-line
    }, [options, initialEmpty])


    const handleChange = useCallback((event, newValue) => {
            event.preventDefault()
            if (newValue && newValue.newValue && newValue.newValue.toString() === "new" && addValue) {
                addValue(newValue.value.toString());
            } else if (newValue && newValue.value) {
                // Create a new value from the user input
                setSelectValue({
                    label: newValue.label,
                    value: newValue.value
                });
                setValue(newValue.value);
            } else {
                setValue("")
                setSelectValue(null);
            }
        // eslint-disable-next-line
        },[setSelectValue, setValue, options],
    );

    const filter = createFilterOptions();

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth disabled={disable}>
                <Autocomplete
                    value={selectValue || null}
                    onChange={handleChange}
                    inputValue={inputVal}
                    selectOnFocus
                    clearText="Supprimé"
                    clearOnBlur
                    isOptionEqualToValue={(option, value) => option.value === value.value}
                    handleHomeEndKeys
                    noOptionsText="Saisie impossible"
                    onInputChange={(event, newInputValue) => {
                        if (event) {
                            setInputVal(newInputValue);
                            if (refreshOptions) refreshOptions(newInputValue);
                        }
                    }}
                    filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        const { inputValue } = params;
                        // Suggest the creation of a new value
                        const isExisting = options.some((option) => inputValue === option.label);
                        if (inputValue !== '' && !isExisting && allowAddValue) {
                            filtered.push({
                                value: inputValue,
                                label: `Ajouter "${inputValue}"`,
                                newValue: "new",
                            });
                        }
                        return filtered;
                    }}
                    renderOption={(props, option) => 
                        <MenuItem key={option.value + option.label} {...props}>
                            {option?.newValue && option?.newValue === "new" ?
                                <Grid container justifyContent="space-between"><Grid item>{option.label}</Grid><Grid item><CheckCircle color="success" /></Grid></Grid>
                                :
                                option.label
                            }
                        </MenuItem>
                    }
                    options={optionsFinal}
                    sx={{ width: 300 }}
                    renderInput={(params) => <TextField {...params} label={label} />}
                    freeSolo={allowAddValue ? true : false}
                    disabled={disable}
                />
            </FormControl>
        </Box>
    );
}
