import * as React from 'react';
import { useCallback, useState, useEffect } from 'react';
import { TextField, Grid, Box, Paper, Autocomplete, Divider, Chip } from '@mui/material';
import { InputDefault, TH, ButtonValidForm } from '../composants_internes';
import { searchFullAdresse, cpBano, communeBano, adresseBano} from './actions/bano';


export function Adresse({ label, stepper, setForm, disable, valuesForm, displayInsee = false }) {
    const [valueCp, setValueCp] = useState(valuesForm?.cp || '');
    const [valueCommune, setValueCommune] = useState(valuesForm?.commune || '');
    const [valueVoie, setValueVoie] = useState(valuesForm?.voie || '');
    const [valueLieudit, setValueLieudit] = useState(valuesForm?.lieudit || '');
    const [valueCodeInsee, setValueCodeInsee] = useState("");
    const [res, setRes] = useState([]);

    const handleSubmit = useCallback((event) => {
        event.preventDefault();
        setForm({
            cp: valueCp,
            commune: valueCommune,
            voie: valueVoie,
            lieudit: valueLieudit,
            codeInsee: valueCodeInsee
        });
        // eslint-disable-next-line
    }, [valueCp, valueCommune, valueVoie, valueLieudit]
    );

    useEffect(() => {
        if (stepper) {
            setForm({
                cp: valueCp,
                commune: valueCommune,
                voie: valueVoie,
                lieudit: valueLieudit,
                codeInsee: valueCodeInsee,
            });
        }
        // eslint-disable-next-line
    }, [valueCp, valueCommune, valueVoie, valueLieudit, setForm, stepper]);

    let choiceBano = async (choice, data) => {
        switch (choice) {
            case 'cp':
                setValueCp(data);
                setRes([...(await cpBano(data))]);
                break;
            case 'commune':
                setValueCommune(data);
                setRes([...(await communeBano(data))]);
                break;
            case 'voie':
                setValueVoie(data);
                setRes([...(await adresseBano(data, valueCp))]);
                break;
            case 'full':
                setRes([...(await searchFullAdresse(data))]);
                break;
            default:
                break;
        }
    };

    const handleEvent = async (choix, event, value) => {
        if (event) {
            event.stopPropagation();
            event.persist();
            await choiceBano(choix, value);
        }
    };

    const handleSelect = (event, newValue) => {
        event.stopPropagation();
        event.persist();
        if (newValue) {
            const feature = newValue;
            if (
                feature.properties.type === 'street' ||
                feature.properties.type === 'housenumber' ||
                feature.properties.type === 'locality'
            ) {
                setValueVoie(feature.properties.name);
                setValueCommune(feature.properties.city);
                setValueCp(feature.properties.postcode);
                setValueCodeInsee(feature.properties.citycode);
            } else if (feature.properties.type === 'municipality') {
                setValueCp(feature.properties.postcode);
                setValueCommune(feature.properties.city);
                setValueCodeInsee(feature.properties.citycode);
                setValueVoie('');
            }
            if (feature.properties.oldcity) {
                setValueLieudit(feature.properties.oldcity);
            } else {
                setValueLieudit('');
            }
            setRes([]);
        }
    };

    return (
        <Box sx={{ margin: stepper ? 0 : 2 }}>
            <TH>{label}</TH>
            <Paper elevation={1} sx={{ marginBottom: stepper ? 0 : 2, padding: 2 }}>
                <Box sx={{ m: 2 }}>
                    <Grid container item xs={12}>
                        <Grid item xs={12}>
                            <Autocomplete
                                disabled={disable}
                                id="adresse"
                                role="adresse"
                                filterOptions={(x) => x}
                                options={res}
                                getOptionLabel={(option) => option.properties.label}
                                sx={{ width: '100%' }}
                                onChange={handleSelect}
                                onInputChange={(e, value) => handleEvent('full', e, value)}
                                renderInput={(params) => (
                                    <TextField
                                        {...params}
                                        label="Recherche"
                                        InputProps={{
                                            ...params.InputProps,
                                            endAdornment: (
                                                <React.Fragment>
                                                    {params.InputProps.endAdornment}
                                                </React.Fragment>
                                            ),
                                        }}
                                    />
                                )}
                            />
                        </Grid>
                    </Grid>
                </Box>
                <Divider>
                    <Chip label="OU" />
                </Divider>
                <Box sx={{ m: 2 }}>
                    <Grid container item xs={12} spacing={2}>
                        <Grid item xs={12}>
                            <InputDefault
                                label="Voie"
                                value={valueVoie}
                                setValue={setValueVoie}
                                fullWidth
                                disable={disable}
                            />
                        </Grid>
                        <Grid item>
                            <InputDefault
                                label="Code postal"
                                value={valueCp}
                                setValue={setValueCp}
                                disable={disable}
                            />
                        </Grid>
                        <Grid item>
                            <InputDefault
                                label="Commune"
                                value={valueCommune}
                                setValue={setValueCommune}
                                disable={disable}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <InputDefault
                                label="Lieu-dit"
                                value={valueLieudit}
                                setValue={setValueLieudit}
                                fullWidth
                                disable={disable}
                            />
                        </Grid>
                        {displayInsee && <Grid item xs={12}>
                            <InputDefault
                                label="Code Insee"
                                value={valueCodeInsee}
                                setValue={setValueCodeInsee}
                                fullWidth
                                disable={true}
                            />
                        </Grid>}
                    </Grid>
                </Box>
            </Paper>
            {!disable && !stepper && (
                <Grid container item xs={12} spacing={2} justifyContent="flex-end">
                    <ButtonValidForm action={handleSubmit} />
                </Grid>
            )}
        </Box>
    );
}
