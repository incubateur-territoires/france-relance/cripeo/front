import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { FormControl, InputLabel, Select, MenuItem, Box, Grid, Typography } from '@mui/material';


export function SelectDefault({ label, value, setValue, options, disable, defaultValue, initialEmpty, }) {
    const [optionsFinal, setOptionsFinal] = useState([]);

    useEffect(() => {
        if (initialEmpty) {
            const opts = [{ value: '', label: initialEmpty }, ...options];
            setOptionsFinal(opts);
        } else {
            setOptionsFinal(options);
        }
        if (defaultValue && !value) {
            const checkOptionValid = options.some(
                (option) => option.value === defaultValue
            );
            if (checkOptionValid) setValue(defaultValue);
        }
        // eslint-disable-next-line
    }, [options, defaultValue]);

    const renderOptions = optionsFinal.map((option, index) => (
        <MenuItem
            key={`${option.value}${option.label}${index}`}
            value={option.value}
            sx={{ whiteSpace: 'normal' }}
        >
            {initialEmpty && index === 0 &&
                <Grid container justifyContent="false" direction="column">
                    <Typography variant="body1" sx={{ whiteSpace: 'normal' }}>
                        {option.label}
                    </Typography>
                </Grid>
            }
            {!initialEmpty || index !== 0 ?
                <Grid container justifyContent="false" direction="column">
                    <Typography variant="body1" sx={{ whiteSpace: 'normal' }}>
                        {option.label}
                    </Typography>
                </Grid>
                : null
            }
        </MenuItem>
    ));

    const checkValueInOptions = () =>
        optionsFinal.some((option) => option.value === value);

    const handleChange = useCallback((event) => {
        event.stopPropagation();
        setValue(event.target.value);
    },
        [setValue]
    );

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth disabled={disable}>
                <InputLabel id={`${label.split(' ').join('')}-label`}>
                    {label}
                </InputLabel>
                <Select
                    labelId={`${label.split(' ').join('')}-label`}
                    value={checkValueInOptions() ? value : ''}
                    onChange={handleChange}
                    label={label}
                >
                    {renderOptions}
                </Select>
            </FormControl>
        </Box>
    );
}
