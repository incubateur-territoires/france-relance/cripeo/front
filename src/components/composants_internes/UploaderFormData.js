import * as React from 'react';
import { Fragment } from 'react';
import { useSnackbar } from 'notistack';
import { useDropzone } from 'react-dropzone';
import { Typography, Grid, List, ListItem, ListItemText, ListItemButton, ListItemIcon, Box } from '@mui/material';
import { Folder, Add, Delete } from '@mui/icons-material';
import { TH, ButtonText, ButtonIcon, SuppressionPrompt } from "../composants_internes";
import styled from '@emotion/styled';
import { tabCorresMIME } from './actions/fichiers';



const getColor = (props) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isFocused) {
        return '#2196f3';
    }
    return '#eeeeee';
}

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${props => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border .24s ease-in-out;
`;

const ALLOWED_FILE_TYPES = ["image/jpeg", "image/png", "application/pdf"];
const MAX_FILE_SIZE = 5000000; // 5MB
const MAX_NB_FILES = 3;


export const b64toBlob = (b64Data, contentType = '', sliceSize = 512) => {
    const byteCharacters = Buffer.from(b64Data, 'base64').toString('utf8');
    const byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        const slice = byteCharacters.slice(offset, offset + sliceSize);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
    }

    return new Blob(byteArrays, { type: contentType });
}

export const renameFile = (originalFile, newName) => {
    return new File([originalFile], newName, {
        type: originalFile.type,
        lastModified: originalFile.lastModified,
    });
}

export function UploaderFormData({ deleteFile, _disable, fichiers, uploadFile, _multiple, allowedFileTypes, maxFileSize, maxNbFiles }) {
    const [statusOpen, setStatusOpen] = React.useState(false);
    const [keyToDelete, setKeyToDelete] = React.useState("");
    const { enqueueSnackbar } = useSnackbar();
    const { getRootProps, getInputProps, isFocused, open, isDragAccept, isDragReject } = useDropzone({
        onDrop: async files => {

            if (fichiers.length + files.length > (maxNbFiles || MAX_NB_FILES)) {
                enqueueSnackbar("Le nombre de fichiers est trop important, le maximum autorisé est de " + (maxNbFiles || MAX_NB_FILES), { variant: 'error' });
                return;
            }

            const invalidFiles = files.filter(file => {
                if (!(allowedFileTypes || ALLOWED_FILE_TYPES).includes(file.type)) {
                    enqueueSnackbar("Le type de fichier est invalide", { variant: 'error' });
                    return true;
                }
                else if (file.size > (maxFileSize || MAX_FILE_SIZE)) {
                    enqueueSnackbar("La taille du fichier est trop importante", { variant: 'error' });
                    return true;
                }
                return false;
            });
            if (invalidFiles.length) return;

            const newFiles = files.map(file => {
                return renameFile(file, file.name.replace(/\s/g, '').normalize("NFD").replace(/[\u0300-\u036f]/g, ""));
            })
            await uploadFile(Array.from(newFiles));
        },
        noClick: true,
        noKeyboard: true,
        multiple: true,
    });

    const handleDelete = (key) => () => {
        setKeyToDelete(key);
        setStatusOpen(true);
    }

    const deleteSubmit = () => {
        deleteFile(keyToDelete)
        setStatusOpen(false);
    }

    const fichiersRender = fichiers
        .map((fichier, key) => {
            const filedata = fichier;
            const name = filedata.name;
            return (
                <ListItem
                    key={key}
                    secondaryAction={
                        <ButtonIcon
                            action={handleDelete(key)}
                            icon={<Delete />}
                            texte={"Supprimer le document"}
                            color={'error'}
                        />
                    }
                >
                    <ListItemButton>
                        <ListItemIcon>
                            <Folder />
                        </ListItemIcon>
                        <ListItemText
                            primary={name || "Nom du fichier"}
                        />
                    </ListItemButton>
                </ListItem>
            )
        })


    const handleClose = async () => {
        setStatusOpen(false);
    };

    return (
        <Fragment>
            <TH>Documents</TH>
            <List>
                {fichiersRender}
            </List>
            <Container {...getRootProps({ isFocused, isDragAccept, isDragReject })}>

                <Grid container item spacing={1}>
                    <List dense>


                        <input
                            {...getInputProps()}
                            aria-label="Upload input"
                        />
                        <p>Vous pouvez "Glisser - Déposer" des fichiers sur le rectangle gris, ou cliquer sur le bouton pour télécharger des documents</p>

                        <Box sx={{ margin: 3 }}>
                            <ButtonText
                                action={open}
                                color="primary"
                                iconPosition="left"
                                icon={<Add />}
                                texte="Ajouter des documents"
                                variant="contained"
                            />
                        </Box>

                        <Typography variant="caption">La taille des fichiers ne doit pas être supérieure à {Math.floor((maxFileSize || MAX_FILE_SIZE) / Math.pow(1024, 2))}Mo</Typography>
                        <br />
                        <Typography variant="caption">Le nombre de fichiers téléchargeables ne peut excéder {(maxNbFiles || MAX_NB_FILES)}</Typography>
                        <br />
                        <Typography variant="caption">Seulement les fichiers : {(allowedFileTypes || ALLOWED_FILE_TYPES).map(text => {
                            return tabCorresMIME.get(text)?.ext;
                        }).join(', ')} seront acceptés</Typography>
                    </List>
                    <SuppressionPrompt
                        deleteFunction={deleteSubmit}
                        cancelFunction={handleClose}
                        open={statusOpen}
                        ariaLabel={`${keyToDelete}`}
                    />
                </Grid>
            </Container>
        </Fragment>
    );
}
