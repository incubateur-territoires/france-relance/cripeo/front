import * as React from 'react';
import { ButtonText } from '../composants_internes';

export function ButtonCancel({action, disable, ariaLabel}) {
    return (
      <ButtonText
        texte="Annuler"
        action={action}
        disable={disable}
        variant='text'
        ariaLabel={ariaLabel}
        />
    );
  }