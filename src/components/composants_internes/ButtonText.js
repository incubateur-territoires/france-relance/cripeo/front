import * as React from 'react';
import {useCallback} from 'react';
import { Button } from '@mui/material';


const debounce = (mainFunction, delay) => {
    let timer;
    return function (...args) {
      clearTimeout(timer);
      timer = setTimeout(() => {
        mainFunction(...args);
      }, delay);
    };
  };
  

export function ButtonText({action, texte, disable, color, icon, iconPosition, variant, ariaLabel, delay}) {
    const onDebounce = debounce(action, 300);
    // eslint-disable-next-line react-hooks/exhaustive-deps
    const cbOnDebounce = useCallback(onDebounce, []);
    const onAction = delay ? cbOnDebounce : action;
    return (
      <Button
        onClick={onAction}
        title={texte}
        disabled={disable}
        endIcon={iconPosition && iconPosition=== 'right' ? icon : !iconPosition ? icon : null}
        startIcon={iconPosition && iconPosition=== 'left' ? icon : null}
        component="span"
        variant={variant || "contained"}
        color={color || "primary"}
        aria-label={ariaLabel}
      >
        {texte}
      </Button>
    )
  }
  