import * as React from 'react';
import { useCallback, useEffect } from 'react';
import { Grid, Paper, Box } from '@mui/material';
import { DateInput, InputDefault, AutoComplete, SelectDefault, TH, ButtonValidForm } from '../composants_internes';
import { getPays } from './actions/pays';
import { getOptionsCommunes } from './actions/communes';


export function FormIndividu({ label, valuesForm, setForm, disable, stepper, nomLegal }) {
    const [civilite, setCivilite] = React.useState(valuesForm?.civilite || null);
    const [nom, setNom] = React.useState(valuesForm?.nom || null);
    const [nomMarital, setNomMarital] = React.useState(valuesForm?.nomMarital || null);
    const [prenom, setPrenom] = React.useState(valuesForm?.prenom || null);
    const [dateNaissance, setDateNaissance] = React.useState(valuesForm?.dateNaissance || null);
    const [lieuNaissance, setLieuNaissance] = React.useState(valuesForm?.lieuNaissance || null);
    const [nationalite, setNationalite] = React.useState(valuesForm?.nationalite || null);

    const [filterLieu, setFilterLieu] = React.useState(valuesForm?.lieuNaissance || null);
    const [filterPays, setFilterPays] = React.useState(null);


    const handleSubmit = useCallback(
        () => {
            setForm({
                civilite,
                nom,
                prenom,
                dateNaissance,
                lieuNaissance,
                nationalite,
                nomMarital,
            });
        }, [civilite, nom, prenom, dateNaissance, lieuNaissance, nationalite, nomMarital, setForm]);

    const handleAddValue = useCallback((value) => {
        setLieuNaissance(value);
        // eslint-disable-next-line
    }, [setLieuNaissance, lieuNaissance]);

    useEffect(() => {
        if (stepper) {
            setForm({
                civilite,
                nom,
                prenom,
                dateNaissance,
                lieuNaissance,
                nationalite,
                nomMarital,
            });
        }
        // eslint-disable-next-line
    }, [civilite, nom, prenom, dateNaissance, lieuNaissance, nationalite, nomMarital])

    let labelNom = 'Nom';
    if (nomLegal) {
        labelNom = 'Nom légal';
    }

    const optionsCommunes = getOptionsCommunes(filterLieu || "");
    const optionsPays = getPays(filterPays || "");

    return (
        <Box sx={{ margin: stepper ? 0 : 2 }}>
            <TH>{label}</TH>
            <Paper elevation={1} sx={{ marginBottom: stepper ? 0 : 2, padding: 2 }}>
                <Grid container spacing={2}>

                    <Grid container item xs={12} spacing={2}>
                        <Grid item>
                            <SelectDefault
                                label="Civilité"
                                value={civilite || ''}
                                setValue={setCivilite}
                                options={[
                                    { value: 'M', label: 'Monsieur' },
                                    { value: 'Mme', label: 'Madame' }
                                ]}
                                disable={disable}
                            />
                        </Grid>
                        <Grid item>
                            <InputDefault
                                label={labelNom}
                                value={nom || ''}
                                setValue={setNom}
                                aria-describedby={`Champs de ${label}`}
                                disable={disable}
                            />
                        </Grid>
                        {nomLegal &&
                            <Grid item>
                                <InputDefault
                                    label="Nom marital"
                                    value={nomMarital || ''}
                                    setValue={setNomMarital}
                                    aria-describedby={`Champs de ${label}`}
                                    disable={disable}
                                />
                            </Grid>
                        }
                        <Grid item>
                            <InputDefault
                                label="Prénom"
                                value={prenom || ''}
                                setValue={setPrenom}
                                aria-describedby={`Champs de ${label}`}
                                disable={disable}
                            />
                        </Grid>
                    </Grid>
                    <Grid container item xs={12} spacing={2}>
                        <Grid item>
                            <DateInput
                                label="Date de naissance"
                                value={dateNaissance || ''}
                                setValue={setDateNaissance}
                                aria-describedby={`Champs de ${label}`}
                                disable={disable}
                            />
                        </Grid>
                        <Grid item>
                            <AutoComplete
                                label="Lieu de naissance"
                                value={lieuNaissance || ''}
                                options={optionsCommunes}
                                allowAddValue={true}
                                addValue={(value) => {
                                    handleAddValue(value);
                                }}
                                setValue={(value) => {
                                    setLieuNaissance(value);
                                }}
                                refreshOptions={(newValue) => { setFilterLieu(newValue) }}
                                defaultValue=""
                                disable={disable}
                            />
                        </Grid>
                        <Grid item>
                            <AutoComplete
                                label="Pays de nationalité"
                                value={nationalite || ''}
                                options={optionsPays}
                                setValue={(value) => {
                                    setNationalite(value);
                                }}
                                refreshOptions={(newValue) => { setFilterPays(newValue) }}
                                defaultValue=""
                                disable={disable}
                            />
                        </Grid>
                    </Grid>
                </Grid>
            </Paper>
            {(!disable && !stepper) &&
                <Grid container item xs={12} spacing={2} justifyContent="flex-end">
                    <ButtonValidForm action={handleSubmit} />
                </Grid>
            }
        </Box>

    );



}