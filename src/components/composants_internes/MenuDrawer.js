import * as React from 'react';
import { styled } from '@mui/material/styles';
import MuiDrawer from '@mui/material/Drawer';
import ListItemIcon from '@mui/material/ListItemIcon';
import { Grid, Link, Tooltip, ListItemText, ListItemButton, ListItem, IconButton, Divider, List } from "@mui/material";
import { ChevronRight, ChevronLeft } from '@mui/icons-material';


const openedMixin = (theme) => ({
    width: drawerWidth,
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(8)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));

const drawerWidth = 300;
const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);


export function MenuDrawer({ links }) {
    const [open, setOpen] = React.useState(false);
    const handleDrawerOpen = () => {
        setOpen(!open);
    };

    const renderList = links.map((lien, index) => {
        return (
            <ListItem key={index} disablePadding sx={{
                display: 'block', '&:hover': {
                    overflow: "visible",
                    whiteSpace: "normal"
                },
            }}>
                {lien.link &&
                    <ListItemButton
                        sx={{
                            minHeight: 48,
                            justifyContent: open ? 'initial' : 'center',
                            px: 2.5,
                        }}
                    >
                        {lien.linkTitle && lien.RouterLink &&
                            <Link
                                sx={{ textDecoration: 'none', color: lien.color ? lien.color : "primary" }}
                                to={lien.link || '#'}
                                component={lien.RouterLink}
                            >
                                <Tooltip title={lien.linkTitle}>
                                    <ListItemIcon
                                        sx={{
                                            minWidth: 0,
                                            mr: open ? 3 : 'auto',
                                            justifyContent: 'center',
                                            color: lien.color ? lien.color : "primary",
                                        }}
                                    >
                                        {lien.icon}
                                    </ListItemIcon>
                                </Tooltip>
                                {open ? <ListItemText primary={lien.linkTitle} sx={{ textOverflow: "ellipsis" }} /> : null}
                            </Link>
                        }
                        {lien.linkTitle && !lien.RouterLink &&
                            <Link
                                sx={{
                                    textDecoration: 'none',
                                    color: lien.color ? lien.color : "primary",
                                    display: "flex"
                                }}
                                href={lien.link || '#'}
                            >
                                <Tooltip title={lien.linkTitle}>
                                    <ListItemIcon
                                        sx={{
                                            minWidth: 0,
                                            mr: open ? 3 : "auto",
                                            ml: 1,
                                            justifyContent: 'center',
                                            color: lien.color ? lien.color : "primary",
                                        }}>

                                        {lien.icon}
                                    </ListItemIcon>
                                </Tooltip>
                                {open ? <ListItemText primary={lien.linkTitle} sx={{ width: "100%", overflow: "hidden" }} /> : null}
                            </Link>
                        }
                    </ListItemButton>
                }
            </ListItem>
        )
    })

    return (
        <Grid item xs={12} >
            <Drawer variant="permanent" open={open} >
                <DrawerHeader>
                    <IconButton onClick={handleDrawerOpen}>
                        {open ? <ChevronLeft /> : <ChevronRight />}
                    </IconButton>
                </DrawerHeader>
                <Divider />
                <List>
                    {renderList}
                </List>

            </Drawer>
        </Grid>
    );
}