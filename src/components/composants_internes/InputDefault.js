import * as React from 'react';
import { useCallback, useState, useEffect } from 'react';
import { TextField } from '@mui/material';


export function InputDefault({ label, value, setValue, disable, type, fullWidth, name, required, multiline, rows, maxRows, minRows, placeholder, validation }) {
    const [error, setError] = useState({ error: false })

    const handleChange = useCallback((event) => {
        event.stopPropagation();
        if (validation !== null) {
            const retourError = validation?.(event.target.value) || { error: false };
            setError(retourError);
        }
        setValue(event.target.value);
        // eslint-disable-next-line
    }, [setValue]);

    useEffect(() => {
        if (validation !== null) {
            const retourError = validation?.(value) || { error: false };
            setError(retourError);
        }
        // eslint-disable-next-line
    }, []);



    return (
        <TextField
            id={name}
            value={value}
            label={label}
            type={type}
            InputLabelProps={type === "number" ? { shrink: true, } : {}}
            onChange={handleChange}
            aria-describedby={`Champs de ${label}`}
            disabled={disable}
            fullWidth={fullWidth}
            name={name}
            required={required}
            multiline={multiline}
            rows={rows}
            maxRows={maxRows}
            minRows={minRows}
            placeholder={placeholder}
            error={error.error}
            helperText={error?.message}
        />
    );
}
