import React from 'react';
import { Paper, Button } from '@mui/material';
import styled from '@emotion/styled';
import { TH } from "../composants_internes";
import { Home } from '@mui/icons-material';
import SvgPomme from './iconComponent/Pomme';



const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`;

export const P = styled.p`
  font-size: 40px;
  line-height: 1.5;
  margin: 0.625rem 0 1.5rem 0;
`;



export function Error404({ color = "#0e3c7a", iconReplacement }) {
    return (
        <Paper
            sx={{
                backgroundColor: (t) => t.palette.background.default,
                margin: 0,
                height: `calc(100vh - 64px)`,
            }}
        >
            <div
                style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: `100%`,
                }}
            >
                <Wrapper>
                    <div style={{
                        marginTop: "-8vh",
                        fontWeight: "bold",
                        fontSize: "300px",
                        display: "flex",
                        alignItems: "center",
                        color: color
                    }}>
                        4
                        {iconReplacement ?
                            iconReplacement
                            :
                            <SvgPomme fill={color} />
                        }
                        4
                    </div>
                    <TH>Page introuvable</TH>
                </Wrapper>
                <Button
                    color="primary"
                    aria-label="accueil"
                    variant="contained"
                    href="/"
                    style={{ marginTop: 20 }}
                    startIcon={<Home />}
                >
                    Page d'accueil
                </Button>
            </div>
        </Paper>
    )
}