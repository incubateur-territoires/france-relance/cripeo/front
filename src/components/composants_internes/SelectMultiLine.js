import * as React from 'react';
import { useCallback } from 'react';
import { FormControl, InputLabel, Select, MenuItem, Box, Grid, Typography } from '@mui/material';
import { grey, blue } from '@mui/material/colors';


export function SelectMultiLine({ label, value, setValue, options, disable }) {
    const renderOptions = options.map((option, index) => (
        <MenuItem key={`${option.value}${index}`} value={option.value}>
            <Grid container justifyContent="false" direction="column">
                <Typography variant="body1" sx={{ color: blue['700'] }}>{option.label}</Typography>
                <Typography variant="caption" sx={{ color: grey['700'] }}>{option.secondLabel}</Typography>
            </Grid>
        </MenuItem>
    ));

    const handleChange = useCallback((event) => {
        event.stopPropagation();
        setValue(event.target.value);
    },
        [setValue],
    );

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth disabled={disable}>
                <InputLabel id={`${label.split(' ').join('')}-label`}>
                    {label}
                </InputLabel>
                <Select
                    labelId={`${label.split(' ').join('')}-label`}
                    value={value || ''}
                    onChange={handleChange}
                    label={label}
                >
                    {renderOptions}
                </Select>
            </FormControl>
        </Box>
    );
}
