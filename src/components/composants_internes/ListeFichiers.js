import * as React from 'react';
import { Fragment } from 'react';
import { List } from '@mui/material';
import { LigneFichier } from '../composants_internes';

export function ListeFichiers({ fichiers, maxWidth, deleteFunction, downloadFunction }) {

    const fichiersRender = fichiers?.map(fichier => 
        <LigneFichier
            key={fichier._id}
            fichier={fichier}
            deleteFunction={deleteFunction}
            downloadFunction={downloadFunction}
        />
    )

    return (
        <Fragment>
            <List sx={{ width: '100%', maxWidth: maxWidth || 360, bgcolor: 'background.paper' }}>
                {fichiersRender}
            </List>
        </Fragment>
    )
}