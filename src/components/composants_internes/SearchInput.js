import * as React from 'react';
import { useCallback, useState, useEffect } from 'react';
import { TextField, InputAdornment, IconButton } from '@mui/material';
import {Search, Clear} from '@mui/icons-material';


export function SearchInput({ label, value, setValue, disable, type, fullWidth, search, required, multiline, rows, maxRows, minRows, placeholder, validation, handleClear }) {
    const [error, setError] = useState({ error: false })

    const handleChange = useCallback((event) => {
        event.stopPropagation();
        if (validation !== null) {
            const retourError = validation?.(event.target.value) || { error: false };
            setError(retourError);
        }
        setValue(event.target.value);
        // eslint-disable-next-line
    }, [setValue]);

    useEffect(() => {
        if (validation !== null) {
            const retourError = validation?.(value) || { error: false };
            setError(retourError);
        }
        // eslint-disable-next-line
    }, [])

    return (
        <TextField
            id={search}
            value={value}
            label={label}
            type={type}
            InputLabelProps={type === "number" ? { shrink: true, } : {}}
            onChange={handleChange}
            aria-describedby={`Champs de ${label}`}
            disabled={disable}
            fullWidth={fullWidth}
            name={search}
            required={required}
            multiline={multiline}
            rows={rows}
            maxRows={maxRows}
            minRows={minRows}
            placeholder={placeholder}
            error={error.error}
            helperText={error?.message}
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <IconButton
                            onClick={() => handleClear()}>
                            <Clear />
                        </IconButton>
                        <Search />
                    </InputAdornment>
                )
            }}
        />
    );
}