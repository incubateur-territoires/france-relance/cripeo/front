import * as React from 'react';
import { ButtonText } from '../composants_internes';
import { Delete } from '@mui/icons-material';

export function ButtonDelete({action, disable, ariaLabel}) {
    return (
      <ButtonText
        texte="Supprimer"
        action={action}
        color='error'
        disable={disable}
        icon={<Delete />}
        variant='contained'
        ariaLabel={ariaLabel}
        />
    );
  }