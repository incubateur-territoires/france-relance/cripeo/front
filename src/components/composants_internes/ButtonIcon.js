import * as React from 'react';
import { IconButton } from '@mui/material';


export function ButtonIcon({ action, texte, disable, color, icon }) {

    return (
        <IconButton
            // @ts-ignore
            onClick={action}
            title={texte}
            disabled={disable}
            color={color || "primary"}
            component="span"
        >
            {icon}
        </IconButton>
    );
}
