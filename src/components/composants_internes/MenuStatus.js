import * as React from 'react';
import { useCallback, useEffect, useState } from 'react';
import { IconButton, Menu, Tooltip, MenuItem, ListItemIcon, ListItemText } from "@mui/material";
import SignalWifiStatusbar4BarIcon from "@mui/icons-material/SignalWifiStatusbar4Bar";
import CircleIcon from "@mui/icons-material/Circle";
import CrossIcon from "@mui/icons-material/Close";

const DEFAULT_INTERVAL = 5 * 60000;

export function ItemStatus({ label, interval, url, callApi, updateStatusGlobal }) {
    const [status, setStatus] = useState(0);

    useEffect(() => {

        const checkApi = async () => {
            if (url) {
                fetch(url)
                    .then(result => {
                        updateStatusGlobal(result.status === 200)
                        setStatus(result.status);
                    })
                    .catch(e => {
                        setStatus(400);
                        updateStatusGlobal(false)
                        console.log(e);
                    });
            }
            else if (callApi) {
                const response = await callApi()
                setStatus(response)
                updateStatusGlobal(response === 200)
            }
            else {
                setStatus(400)
                updateStatusGlobal(false)
            }
        }
        checkApi();
        let id = setInterval(checkApi, interval || DEFAULT_INTERVAL);
        return () => clearInterval(id);
        // eslint-disable-next-line
    }, [url, interval]);

    return (
        <MenuItem >
            <ListItemIcon>
                {status === 200 ? <CircleIcon color={"success"} /> : <CrossIcon color={"error"} />}
            </ListItemIcon>
            <ListItemText>{label}</ListItemText>
        </MenuItem>
    )
}



export function MenuStatus({ services }) {
    const [statusOpen, setStatusOpen] = React.useState(null);
    const [statusGlobal, setStatusGlobal] = React.useState(services.map(serv => false));

    const updateStatusGlobal = useCallback((index) => (valeur) => {
        setStatusGlobal(state => {
            let newStatus = [...state];
            newStatus[index] = valeur;
            return newStatus
        });
    }, [setStatusGlobal]);

    const handleClose = async () => {
        setStatusOpen(null);
    };

    const menuDisplay = services?.map((service, index) => (
        <ItemStatus
            key={index}
            label={service.label}
            interval={service.interval}
            url={service.url}
            updateStatusGlobal={updateStatusGlobal(index)}
            callApi={service.callApi}
        />
    ));

    const handleMenuStatus = (event) => {
        setStatusOpen(event.currentTarget);
    };

    const statusCheck = statusGlobal.reduce((previousValue, currentValue) => previousValue && currentValue,
        true);

    return (
        <div>
            <Tooltip title={"Statut des services"}>
                <IconButton
                    size="large"
                    aria-label="service status"
                    aria-controls="service-status"
                    aria-haspopup="true"
                    onClick={handleMenuStatus}
                    color={statusCheck ? 'inherit' : 'error'}
                >
                    <SignalWifiStatusbar4BarIcon />
                </IconButton>
            </Tooltip>
            <Menu
                id="service-status"
                anchorEl={statusOpen}
                anchorOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                keepMounted
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={Boolean(statusOpen)}
                onClose={handleClose}
            >
                {menuDisplay}
            </Menu>
        </div>
    )
}