import * as React from 'react';
import { DialogTitle, Dialog, DialogContent, IconButton } from '@mui/material';
import {Close} from '@mui/icons-material';


function PromptTitle({ children, onClose, ...other }) {
    return (
        <DialogTitle sx={{ m: 0, p: 2 }} {...other}>
            {children}
            {onClose ? (
                <IconButton
                    aria-label="close"
                    onClick={(e) => onClose(e)}
                    role="validate"
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <Close />
                </IconButton>
            ) : null}
        </DialogTitle>
    );
}


export function Prompt({ titre, children, open, actions, onClose, maxWidth, noEscape }) {
    const handleClose = (e, reason) => {
        e.preventDefault()
        if (noEscape && reason !== undefined) {
            if (reason !== "escapeKeyDown" || reason !== "backdropClick") {
            }
        } else {
            onClose()
        }
    }
    return (
        <Dialog
            open={!!open}
            keepMounted
            onClose={(e, r) => handleClose(e, r)}
            aria-labelledby="alert-dialog-suppression-title"
            aria-describedby="alert-dialog-suppression-description"
            maxWidth={maxWidth || 'sm'}
            fullWidth
        >
            <PromptTitle id="alert-dialog-slide-title" onClose={(e, r) => handleClose(e, r)}>
                {titre || 'Prompt'}
            </PromptTitle>
            <DialogContent>{children}</DialogContent>
            {actions}
        </Dialog>
    );
}
