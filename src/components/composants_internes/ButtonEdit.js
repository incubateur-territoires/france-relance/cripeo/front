
import * as React from 'react';
import { ButtonText } from '../composants_internes';
import { Edit } from '@mui/icons-material';


export function ButtonEdit({ action, disable }) {
    return (
        <ButtonText
            texte="Modifier"
            action={action}
            color='info'
            disable={disable}
            icon={<Edit />}
            variant='outlined'
        />
    );
}