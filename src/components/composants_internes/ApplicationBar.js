import * as React from 'react';
import { AppBar, Box, Toolbar, IconButton, Typography, Link, Button, Tooltip, createTheme, ThemeProvider } from '@mui/material';
import { Home, Logout } from '@mui/icons-material';
import Logo from './sources/logo_dep_blanc.svg';
import { useCallback } from 'react';
import { MenuDrawer } from "../composants_internes";



export function ApplicationBar({ texte, color, modules, link, linkTitle, RouterLink, fixed, keycloak, menuDrawer }) {

    let palette = {};

    if (color?.includes('#') && color?.length > 2) {
        palette = {
            palette: {
                primary: {
                    main: color,
                },
            },
        };
    }

    const theme = createTheme(palette);

    const moduleDisplay = modules.map((module, index) => (
        <Box sx={{ display: { xs: 'none', md: 'flex' } }} key={index}>
            {module.component}
        </Box>
    ));

    const logout = useCallback(() => {
        keycloak?.logout();
    }, [keycloak]);

    return (
        <Box sx={{ flexGrow: 1, p: 0 }}>
            <ThemeProvider theme={theme}>
                <AppBar
                    position={fixed ? 'fixed' : 'static'}
                    color="primary"
                    sx={
                        fixed
                            ? {
                                zIndex: (theme) => theme.zIndex.drawer + 1,
                            }
                            : {}
                    }
                >
                    <Toolbar disableGutters={false}>
                        {menuDrawer && <MenuDrawer links={menuDrawer} />}
                        {link && link !== '#' &&
                        <Button
                            color="inherit"
                            sx={{ marginRight: 3 }}
                            startIcon={<Home></Home>}
                        >
                            {linkTitle && RouterLink &&
                                <Link
                                    sx={{ textDecoration: 'none', color: '#fff' }}
                                    to={link || '#'}
                                    component={RouterLink}
                                >
                                    Accueil
                                </Link>
                            }
                            {linkTitle && !RouterLink &&
                                <Link
                                    sx={{ textDecoration: 'none', color: '#fff' }}
                                    href={link || '#'}
                                >
                                    Accueil
                                </Link>
                            }
                            {!linkTitle && 'retour sans lien'}
                        </Button>
                        }
                        {Logo &&
                            <img
                                alt="Calvados"
                                height="30px"
                                src={Logo}
                                style={{ marginRight: '20px' }}
                            />
                        }

                        {texte &&
                            <Typography
                                variant="h6"
                                component="div"
                                sx={{
                                    flexGrow: 1,
                                    textAlign: 'left',
                                    textTransform: 'uppercase',
                                }}
                            >
                                {texte}
                            </Typography>
                        }

                        {moduleDisplay}

                        {keycloak &&
                            <Tooltip title="Déconnexion">
                                <IconButton color="inherit" onClick={logout}>
                                    {' '}
                                    <Logout />
                                </IconButton>
                            </Tooltip>
                        }
                    </Toolbar>
                </AppBar>
            </ThemeProvider>
        </Box>
    );
}
