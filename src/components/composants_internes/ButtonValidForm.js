import * as React from 'react';
import { ButtonText } from '../composants_internes';

export function ButtonValidForm({ action, disable }) {
    return (
        <ButtonText
            texte="Valider"
            action={action}
            color='primary'
            disable={disable}
            variant='text'
        />
    );
}