import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@mui/styles/withStyles';
import FichierAutorisation from '@mui/icons-material/AttachFile';
import {Button, Grid} from "@mui/material/Button";
import connect from "react-redux/es/connect/connect";

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  textAdornment: {
    boxSizing: "border-box"
  }
});

class LoadFile extends PureComponent {

  state = {
    value: this.props.value
  };

  render() {
    const { echange } = this.props;
    return (
      <Grid item sm={6} xs={12}>
        {echange &&
          <FichierAutorisation />}
        <Button
          variant="contained"
          component="label"
        >
          Ajouter un document
          <input
            type="file"
            style={{ display: "none" }}
            name="filedata"
            onChange={this.props.onSelectFile}
          />
        </Button>
      </Grid>
    );
  }

}

LoadFile.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    echange: state.echange,
  };
}

export default withStyles(styles)(connect(mapStateToProps, null)(LoadFile));
