import { ACTIONS } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case ACTIONS:
      return action.payload || false;
    default:
      return state;
  }
}
