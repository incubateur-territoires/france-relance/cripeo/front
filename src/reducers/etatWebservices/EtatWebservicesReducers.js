import { ETAT_WEBSERVICES } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case ETAT_WEBSERVICES:
      return action.payload || false;
    default:
      return state;
  }
}
