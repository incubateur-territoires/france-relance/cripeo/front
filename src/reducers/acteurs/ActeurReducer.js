import {
  ACTEUR,
  EMAIL_DEL,
  EMAIL_UPDATE
} from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case ACTEUR:
      return action.payload || false;
    case EMAIL_DEL:
      return action.payload || false;
    case EMAIL_UPDATE:
      return action.payload || false;
    default:
      return state;
  }
}
