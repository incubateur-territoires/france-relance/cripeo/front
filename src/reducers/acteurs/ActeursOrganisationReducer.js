import { ACTEURS_ORGANISATION } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case ACTEURS_ORGANISATION:
      return action.payload || false;
    default:
      return state;
  }
}
