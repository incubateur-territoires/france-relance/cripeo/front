import { ACTEURS, ACTEUR_CLEAN } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case ACTEURS:
      return action.payload || false;
    case ACTEUR_CLEAN:
      return [];
    default:
      return state;
  }
}
