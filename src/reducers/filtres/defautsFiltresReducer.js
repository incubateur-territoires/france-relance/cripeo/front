import {
  FILTRE_DEFAUT
} from '../../actions/types';

export default function defaultFiltresReducer(state = {}, action) {
  switch (action.type) {
    case FILTRE_DEFAUT:
      return action.payload || false;
    default:
      return state;
  }
}
