import {
  FILTRE_INIT,
  FILTRE_CLEAN,
  FILTRE_RECHERCHE,
  FILTRE_CATEGORIE,
  FILTRE_REFERENCE_EXISTE,
  FILTRE_SECTEUR_ADM,
  FILTRE_SECTEUR_GEO,
  FILTRE_URGENCE,
  FILTRE_URGENCE_ROOT,
  FILTRE_REFRESH,
} from '../../actions/types';

import {
  multiRechercheChamps,
  majRetourMultiRecherche,
} from '../../components/Recherche/recherche';

const INIT_STATE = {
  secteurGeoSelectValue: "all",
  secteurAdmSelectValue: "all",
  filterReferenceExist: false,
  filterUrgence: false,
  filterUrgenceRoot: "tous",
  recherche: "",
  categoryFilter: "",
  dossiers: {
    creations: [],
    aTraiter: [],
    enCours: [],
    clos: [],
  },
  organisation: "",
  loaded: false,
  defauts: null,
  init: false,
  manuel: false,
};

const PARAMS = {
  data: [],
  champs: [{
    list: ['individuPrincipalNom', 'individuSecondaire.0.nom', 'individuSecondaire.1.nom', 'individuSecondaire.2.nom', 'numeroCassiope', 'numeroEN', 'numeroSolis', 'numeroTPE', 'individuPrincipal.individuSolis'],
    more: ['individuPrincipalNom', 'individuSecondaire.0.nom', 'individuSecondaire.1.nom', 'individuSecondaire.2.nom', 'numeroCassiope', 'numeroEN', 'numeroSolis', 'numeroTPE', 'individuPrincipal.individuSolis'],
    importance: [5, 3, 3, 3, 6, 6, 8, 6, 8],
    noSpace: [false, false, false, false, false, false, false, false, false]
  }],
  champRetour: [{ _id: '_id' }],
  renderList: [],
  iconFilter: [],
  categoryFilter: ''
};

export default function filtreReducer(state = INIT_STATE, action) {
  switch (action.type) {
    case FILTRE_RECHERCHE:
      state.manuel = true;
      return filtreChange('recherche', state, action.payload.value, action.payload.dossiers);
    case FILTRE_CATEGORIE:
      state.manuel = true;
      return filtreChange('categoryFilter', state, action.payload.value, action.payload.dossiers);
    case FILTRE_URGENCE:
      state.manuel = true;
      return filtreChange('filterUrgence', state, action.payload.value, action.payload.dossiers);
    case FILTRE_REFERENCE_EXISTE:
      state.manuel = true;
      return filtreChange('filterReferenceExist', state, action.payload.value, action.payload.dossiers);
    case FILTRE_SECTEUR_GEO:
      state.manuel = true;
      return filtreChange('secteurGeoSelectValue', state, action.payload.value, action.payload.dossiers);
    case FILTRE_SECTEUR_ADM:
      state.manuel = true;
      return filtreChange('secteurAdmSelectValue', state, action.payload.value, action.payload.dossiers);
    case FILTRE_URGENCE_ROOT:
      state.manuel = true;
      return filtreChange('filterUrgenceRoot', state, action.payload.value, action.payload.dossiers);
    case FILTRE_CLEAN:
      return clean(
        state,
        action.payload.dossiers,
        action.payload.organisation,
        action.payload.value
      );
    case FILTRE_INIT:
      return init(
        state,
        action.payload.dossiers,
        action.payload.organisation,
        action.payload.defauts,
      );
    case FILTRE_REFRESH:
      return refresh(
        state,
        action.payload.dossiers,
      );
    default:
      return state;
  }
}

const refresh = (state, dossiers) => {
  return { ...state, dossiers: activeFilter(state, dossiers) };
}

const clean = (state, dossiers, organisation, value) => {
  if (value === 'profil') {
    const prepareState = { ...INIT_STATE, dossiers, ...state.defauts, defauts: state.defauts, organisation };
    return { ...prepareState, dossiers: activeFilter(prepareState, dossiers) };
  }
  else {
    return { ...INIT_STATE, dossiers, defauts: state.defauts, organisation };
  }
};

const init = (state, dossiers, organisation, defauts) => {
  if (state.init && JSON.stringify(state.defauts) !== '{}' && state.manuel) {
    return { ...state, dossiers: activeFilter(state, dossiers), defauts: defauts, organisation };
  } else if (state.init) {
    return { ...state, dossiers: activeFilter(state, dossiers), ...defauts, defauts: defauts, organisation };
  }
  return { ...INIT_STATE, dossiers, ...defauts, defauts: defauts, init: true, organisation };
}

const filtreChange = (typeKey, state, value, dossiers) => {
  let newState = { ...state };
  newState[typeKey] = value;
  newState.dossiers = activeFilter(newState, dossiers);
  return newState;
};

const activeFilter = (state, dossiers) => {
  if (!dossiers) return null;

  let dossiersFilter = {
    creations: byRecherche(state, dossiers.creations),
    aTraiter: byRecherche(state, dossiers.aTraiter),
    enCours: byRecherche(state, dossiers.enCours),
    clos: byRecherche(state, dossiers.clos),
  };

  if (state.categoryFilter && state.categoryFilter !== "")
    dossiersFilter = {
      creations: byCategories(state, dossiersFilter.creations),
      aTraiter: byCategories(state, dossiersFilter.aTraiter),
      enCours: byCategories(state, dossiersFilter.enCours),
      clos: byCategories(state, dossiersFilter.clos),
    }

  if (state.secteurGeoSelectValue && state.secteurGeoSelectValue !== "all")
    dossiersFilter = {
      creations: bySecteurGeographique(state, dossiersFilter.creations),
      aTraiter: bySecteurGeographique(state, dossiersFilter.aTraiter),
      enCours: bySecteurGeographique(state, dossiersFilter.enCours),
      clos: bySecteurGeographique(state, dossiersFilter.clos),
    }

  if (state.secteurAdmSelectValue && state.secteurAdmSelectValue !== "all")
    dossiersFilter = {
      creations: bySecteursAdministratif(state, dossiersFilter.creations),
      aTraiter: bySecteursAdministratif(state, dossiersFilter.aTraiter),
      enCours: bySecteursAdministratif(state, dossiersFilter.enCours),
      clos: bySecteursAdministratif(state, dossiersFilter.clos),
    }

  if (state.filterReferenceExist)
    dossiersFilter = {
      creations: byNumeroDossierReference(state, dossiersFilter.creations),
      aTraiter: byNumeroDossierReference(state, dossiersFilter.aTraiter),
      enCours: byNumeroDossierReference(state, dossiersFilter.enCours),
      clos: byNumeroDossierReference(state, dossiersFilter.clos),
    }

  if (state.filterUrgence)
    dossiersFilter = {
      creations: dossiersFilter.creations,
      aTraiter: byUrgence(state, dossiersFilter.aTraiter),
      enCours: byUrgence(state, dossiersFilter.enCours),
      clos: byUrgence(state, dossiersFilter.clos),
    }
  if (state.filterUrgenceRoot !== "Tous")
    dossiersFilter = {
      creations: dossiersFilter.creations,
      aTraiter: byUrgenceRoot(state, dossiersFilter.aTraiter),
      enCours: byUrgenceRoot(state, dossiersFilter.enCours),
      clos: byUrgenceRoot(state, dossiersFilter.clos),
    }

  return { ...dossiersFilter };
};

const byRecherche = (state, dossiers) => {
  if (state.recherche && state.recherche !== "") {

    const recherches = state.recherche.trim().split(" ");
    const inFine = PARAMS.champs.map((tabChamp, key) => {

      const filtre = multiRechercheChamps(dossiers, recherches, tabChamp.more, tabChamp.noSpace, tabChamp.exceptions, tabChamp.importance);
      return majRetourMultiRecherche(dossiers, filtre, PARAMS.champRetour[key]);
    });
    if (inFine && Array.isArray(inFine)) {
      return (inFine[0].data);
    }
  } else {
    return (dossiers);
  }
};

const byCategories = (state, dossiers) => {
  return dossiers.filter(dos => dos.categories?.findIndex(cat => {
    if (cat) return cat.libelle === state.categoryFilter
    return false;
  }) !== -1);
}

const bySecteurGeographique = (state, dossiers) => {

  return dossiers.filter(dos => {
    if (dos.secteurGeo && dos.secteurGeo.length > 0) {
      return dos.secteurGeo.findIndex(sect => {
        if (sect) return sect.code === state.secteurGeoSelectValue && sect.acteur_id === state.organisation.acteur_id
        return false;
      }) !== -1;
    }
    return false;
  }
  )
}

const byNumeroDossierReference = (state, dossiers) => {
  return dossiers.filter(dos => {
    if (dos.numeroApplication && dos.numeroApplication.length > 0) {
      return dos.numeroApplication.findIndex(num => {
        if (num && num.numero) return String(num.acteur_id) === String(state.organisation.acteur_id)
        return false;
      }) !== -1;
    }
    return false;
  });
}

const bySecteursAdministratif = (state, dossiers) => {
  return dossiers.filter(dos => {
    if (state.secteurAdmSelectValue !== 'aucun') {
      if (dos.secteurOrganisation && dos.secteurOrganisation.length > 0) {
        return dos.secteurOrganisation.findIndex(sect => {
          if (sect) return String(sect.secteur_id) === String(state.secteurAdmSelectValue) && String(sect.acteur_id) === String(state.organisation.acteur_id)
          return false;
        }) !== -1;
      }
      return false;
    } else {
      return dos.secteurOrganisation.length === 0;
    }
  });
}

const byUrgence = (state, dossiers) => {
  return dossiers.filter(dos => {
    return dos.urgent
  });
}

const byUrgenceRoot = (state, dossiers) => {
  if (state.filterUrgenceRoot === "urgent") {
    return dossiers.filter(dos => {
      return dos?.urgenceRoot?.some(e => e === true) || false
    })
  }
  else if (state.filterUrgenceRoot === "non_urgent") {
    return dossiers.filter(dos => {
      return dos?.urgenceRoot?.some(e => e === false) || false
    })
  }
  return dossiers;
}
