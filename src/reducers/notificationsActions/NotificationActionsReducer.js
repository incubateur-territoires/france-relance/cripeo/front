import {
  NOTIFICATIONACTION_FETCH,
  NOTIFICATIONACTION_DEL,
  NOTIFICATIONACTION_ADD,
} from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case NOTIFICATIONACTION_FETCH:
      return action.payload.sort((a, b) => new Date(b.created) - new Date(a.created)) || false;
    case NOTIFICATIONACTION_ADD:
      return state.push(action.payload);
    case NOTIFICATIONACTION_DEL:
      const index = state.findIndex(elem => elem._id === action.payload._id);
      if (index !== -1)
        return state.splice(index, 1);
      return state;
    default:
      return state;
  }
}
