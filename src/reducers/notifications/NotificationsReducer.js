import { NOTIFICATION_ADD, NOTIFICATION_DEL } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
    switch (action.type) {
        case NOTIFICATION_DEL: {
            const etat = state;
            etat.shift();
            return etat || false;
        }
        case NOTIFICATION_ADD: {
            return Object.assign([], state.concat([action.payload]));
        }
        default:
            return state;
    }
}