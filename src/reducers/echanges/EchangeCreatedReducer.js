import {
  ECHANGE_CREATED, ENDED_CREATION_ECHANGE
} from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case ECHANGE_CREATED:
      return action.payload || false;
    case ENDED_CREATION_ECHANGE:
      return null || false;
    default:
      return state;
  }
}
