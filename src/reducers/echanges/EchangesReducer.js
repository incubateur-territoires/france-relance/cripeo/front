import { ECHANGES, ECHANGES_PUSH } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case ECHANGES:
      return action.payload || false;
    case ECHANGES_PUSH:
      let render = [...state];
      action.payload.forEach(echange => {
        const index = render.findIndex(ech => echange._id === ech._id);
        if (index === -1)
          render.push(echange);
      });
      return render;
    default:
      return state;
  }
}
