import { combineReducers } from 'redux';
import authReducer from './authReducer';
import keycloak from "./keycloak";
import loginReducer from "./loginReducer";
import DossiersTraiterReducer from "./dossiers/DossiersTraiterReducer";
import DossiersCoursReducer from "./dossiers/DossiersCoursReducer";
import DossiersClosReducer from "./dossiers/DossiersClosReducer";
import DossiersCreationReducer from "./dossiers/DossiersCreationReducer";
import DossiersReducer from "./dossiers/DossiersReducer";
import DossierReducer from "./dossiers/DossierReducer";
import HistoriquesReducer from "./historiques/HistoriquesReducer";
import HistoriqueReducer from "./historiques/HistoriqueReducer";
import CivilitesReducer from "./civilites/CivilitesReducer";
import ActeurReducer from "./acteurs/ActeurReducer";
import ActeursReducer from "./acteurs/ActeursReducer";
import ActeursOrganisationReducer from "./acteurs/ActeursOrganisationReducer";
import EchangeReducer from "./echanges/EchangeReducer";
import EchangeCreatedReducer from './echanges/EchangeCreatedReducer';
import EchangesReducer from "./echanges/EchangesReducer";
import EtatWebservicesReducers from "./etatWebservices/EtatWebservicesReducers";
import NotificationActionsReducer from './notificationsActions/NotificationActionsReducer';
import UtilisateurReducers from "./utilisateurs/UtilisateurReducer";
import UtilisateursReducers from "./utilisateurs/UtilisateursReducer";
import UtilisateursequipeReducers from "./utilisateurs/UtilisateursequipeReducer";
import GroupesReducers from "./groupes/GroupesReducer";
import RolesReducer from "./roles/RolesReducer";
import MasksReducer from "./masks/MasksReducer";
import CategorieReducer from "./categories/CategorieReducer";
import CategoriesReducer from "./categories/CategoriesReducer";
import ActionReducer from "./actions/ActionReducer";
import ActionsReducer from "./actions/ActionsReducer";
import NotificationsReducer from "./notifications/NotificationsReducer";
import CirconscriptionsReducer from "./circonscriptions/CirconscriptionsReducer";
import DocumentsReducer from "./documents/DocumentsReducer";
import DocumentsactionsReducer from "./documents/DocumentsactionsReducer";
import DocumentslisteReducer from "./documents/DocumentslisteReducer";
import frequencesMailReducer from './frequencesMail/frequencesMailReducer';

import filtresReducer from './filtres/FiltresReducer';
import defautsFiltresReducer from './filtres/defautsFiltresReducer';

export default combineReducers({
  keycloak,
  auth: authReducer,
  login: loginReducer,
  dossiers: DossiersReducer,
  dossiersEnCours: DossiersCoursReducer,
  dossiersATraiter: DossiersTraiterReducer,
  dossiersClos: DossiersClosReducer,
  dossiersEnCreation: DossiersCreationReducer,
  dossier: DossierReducer,
  historique: HistoriqueReducer,
  historiques: HistoriquesReducer,
  civilites: CivilitesReducer,
  acteurs: ActeursReducer,
  acteursOrganisation: ActeursOrganisationReducer,
  acteur: ActeurReducer,
  echange: EchangeReducer,
  echangeCreated: EchangeCreatedReducer,
  echanges: EchangesReducer,
  etatWebservices: EtatWebservicesReducers,
  utilisateurs: UtilisateursReducers,
  utilisateur: UtilisateurReducers,
  notificationsAction: NotificationActionsReducer,
  groupes: GroupesReducers,
  roles: RolesReducer,
  masks: MasksReducer,
  categories: CategoriesReducer,
  categorie: CategorieReducer,
  actions: ActionsReducer,
  action: ActionReducer,
  notifications: NotificationsReducer,
  circonscriptions: CirconscriptionsReducer,
  documents: DocumentsReducer,
  documentsactions: DocumentsactionsReducer,
  documentsliste: DocumentslisteReducer,
  frequencesMail: frequencesMailReducer,
  utilisateursequipe: UtilisateursequipeReducers,
  filtres: filtresReducer,
  defautsFiltres: defautsFiltresReducer,
})
