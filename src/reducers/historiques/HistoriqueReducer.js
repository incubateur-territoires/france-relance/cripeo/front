import { HISTORIQUE } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case HISTORIQUE:
      return action.payload || false;
    default:
      return state;
  }
}