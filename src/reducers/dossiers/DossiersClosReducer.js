import { DOSSIERS_CLOS, DOSSIERS_CLOS_PUSH } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case DOSSIERS_CLOS:
      return action.payload || false;
    case DOSSIERS_CLOS_PUSH:
      let render = [...state];
      action.payload.forEach(dossier => {
        const index = render.findIndex(dos => dos._id === dossier._id);
        if (index === -1)
          render.push(dossier);
      });
      return render;
    default:
      return state;
  }
}
