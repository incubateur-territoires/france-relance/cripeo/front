import {
  DOSSIER,
  DOSSIER_ERR,
  REMOVE_INDIVIDU,
  UPDATE_INDIVIDU,
  UPDATE_ADRESSE,
  UPDATE_REFERENCE,
  DOSSIER_CLEAR,
  UPDATE_SECTEUR_ADMINISTRATIF
} from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case DOSSIER:
      return action.payload || false;
    case DOSSIER_ERR:
      console.log({ error: true, status: action.payload });
      return { error: true, status: action.payload };
    case REMOVE_INDIVIDU:
      return action.payload || false;
    case UPDATE_INDIVIDU:
      return action.payload || false;
    case UPDATE_ADRESSE:
      return action.payload || false;
    case UPDATE_REFERENCE:
      return action.payload || false;
    case UPDATE_SECTEUR_ADMINISTRATIF:
      return action.payload || false;
    case DOSSIER_CLEAR:
      return null;
    default:
      return state;
  }
}
