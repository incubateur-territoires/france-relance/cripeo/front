import { DOSSIERS_TRAITER, DOSSIERS_TRAITER_PUSH } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case DOSSIERS_TRAITER:
      return action.payload || false;
    case DOSSIERS_TRAITER_PUSH:
      let render = [...state];
      action.payload.forEach(dossier => {
        const index = render.findIndex(dos => dos._id === dossier._id);
        if (index === -1)
          render.push(dossier);
      });
      return render;
    default:
      return state;
  }
}
