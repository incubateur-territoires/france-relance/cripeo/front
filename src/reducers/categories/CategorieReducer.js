import {
  CATEGORIE,
  REMOVE_ACTION,
  UPDATE_ACTION,
} from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = null, action) {
  switch (action.type) {
    case CATEGORIE:
      return action.payload || false;
    case REMOVE_ACTION:
      return action.payload || false;
    case UPDATE_ACTION:
      return action.payload || false;
    default:
      return state;
  }
}
