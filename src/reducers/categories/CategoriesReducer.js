import { CATEGORIES, REMOVE_CATEGORIE } from "../../actions/types";

// eslint-disable-next-line import/no-anonymous-default-export
export default function (state = [], action) {
  switch (action.type) {
    case CATEGORIES:
      return action.payload || false;
    case REMOVE_CATEGORIE:
      return action.payload || false;
    default:
      return state;
  }
}
