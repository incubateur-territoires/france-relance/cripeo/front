FROM node:21.2.0 as build-deps

ARG NODE_ENV=staging
ENV NODE_ENV=$NODE_ENV

WORKDIR /usr/src/app
COPY . .

RUN yarn install
RUN yarn run build




FROM nginx:1.22-alpine
RUN rm -rf /etc/nginx/conf.d
COPY conf /etc/nginx
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
